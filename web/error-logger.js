function ErrorLogger(title, varname) {
  this.title = title;
  this.varname = varname;
  this.showErrorLog = false;
  this.debugWindow = null;
  this.autoshow = false
  this.log = null
  this.logEntries = []
}

function LogEntry(type, klass, text) {
  this.type = type
  this.date = new Date()
  this.klass = klass
  this.text = text
}

ErrorLogger.prototype.enableAutoShow = function () {
  this.autoshow = true;
}

// Private
ErrorLogger.prototype.isActive = function() {
  return (this.debugWindow) && (!this.debugWindow.closed);
}

ErrorLogger.prototype.show = function() {
    if (!this.isActive()) {
      this.debugWindow = window.open();
      
      /* Workaround for epiphany bug #314012 */
      this.debugWindow.document.write("<html><body></body></html>");
      this.debugWindow.document.close();
      
      var html = this.debugWindow.document.documentElement
	  this.debugWindow.document.title = this.title
	  var body = this.debugWindow.document.body
	  var errorlog = this
	  var div = this.debugWindow.document.createElement("div")
	  this.log = div
	  div.setAttribute("id", "ErrorLoggerDebugLog")
	  body.appendChild(div)
      var i;
      for (i = 0; i < this.logEntries.length; i++) {
        this.appendLogEntry(this.logEntries[i])
      } 
      this.logEntries = []
    }
}

ErrorLogger.prototype.hide = function() {
  try {
    if (this.debugWindow != null) {
      this.debugWindow.close();
      this.debugWindow = null
    }
  } catch (e) {
    //alert(e);
  }
}

// Private
ErrorLogger.prototype.append = function(klass, header, text) {
  var entry = new LogEntry(header, klass, text)
  if (this.isActive()) {
    this.appendLogEntry(entry, null)    
  } else {
     this.logEntries.push(entry)
  }
}

ErrorLogger.prototype.appendLogEntry = function (entry) {
  var logEntry = this.debugWindow.document.createElement("p")
  var span = this.debugWindow.document.createElement("span")  
  logEntry.appendChild(span)
  if (entry.klass == "ErrorLoggerError") {
    span.style.color = "red";
    span.style.fontWeight = "bolder";
  }
  span.style.fontSize = "larger";  
  span.appendChild(this.debugWindow.document.createTextNode(entry.type))
  span = this.debugWindow.document.createElement("span")
  span.style.marginLeft = "1em";
  span.appendChild(this.debugWindow.document.createTextNode(entry.text))
  logEntry.appendChild(span)  
  logEntry.appendChild(this.debugWindow.document.createElement("br"))
  span = this.debugWindow.document.createElement("span")
  span.style.fontSize = "smaller";
  span.style.color = "gray";
  span.appendChild(this.debugWindow.document.createTextNode(new Date()))
  logEntry.appendChild(span)
  this.log.appendChild(logEntry)
  this.log.appendChild(this.debugWindow.document.createElement("hr"))  
}

ErrorLogger.prototype.clear = function() {
  this.debugHTML = "";
  if (this.isActive()) {
    this.log.innerHTML = "";
  }
}   

ErrorLogger.prototype.reportEvent = function(details) {
  try {
    this.append("ErrorLoggerEvent", "Event", details)
  } catch (e) {
    alert(details);
  }
}

ErrorLogger.prototype.reportError = function(text) {
  try {
    this.haveError = true;  
    this.append("ErrorLoggerError", "Error", text)
    if (this.autoshow) {
      this.autoshow = false
      this.show();
    }
  } catch (e) {
    alert(text);
  }
}

// Define the default logger
var errorLog = new ErrorLogger("Yarrr Debug Log", "errorLog");
