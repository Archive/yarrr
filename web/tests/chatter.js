words = ["teh", "is", "rocks", "test", "gnome", "yarrr", "has", "a", "foo", "this", "hello", "yes", "no", "blah"];

function randomChatLine() {
  var line = "";
  var numWords = 3 + Math.floor(Math.random()*15);
  for (var i = 0; i < numWords; i++) {
    var word = Math.floor( Math.random()*words.length )
    line = line + words[word];
    if (i != numWords -1)
      line = line + " ";
  }
  return line;
}

function chatDelay() {
  return 2000 + Math.random()*5000;
}

function doChat(chat) {
  chat.chatText.value = randomChatLine();
  chat.send();

  setTimeout(function () { doChat(chat); }, chatDelay());
}

function doTest() {
  var chat;
  for (i in theTopic.discussions)
    chat = theTopic.discussions[i].chat;
   
   setTimeout(function () { doChat(chat); }, chatDelay());
}


window.yarrrTest = doTest;
