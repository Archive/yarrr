<%@ include file="prelude.jspf" %>
<html>
<head>
    <title>Login error</title>
</head>
<body>
Error while logging in. User name or password are incorrect.
<a href="<%=request.getParameter("redirect")%>">Back</a>
</body>
</html>
