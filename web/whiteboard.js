/*
 	bg = "Buccanneer Graphics" system.  Definitely not an acronym referring to JavaScript's cardiovascular capabilities concerning hollow-horned, bearded ruminant mammals of the genus Capra.
	
	Coordinate systems
	==================
	D - Document Space 
	W - Window Space
	L - Local space of canvas
	
	These are used for:
	(i) suffixes for variable names
	(ii) in method names e.g. W2L is window->local converter
*/

function bgPoint(x, y)
{
	this.x = x;
	this.y = y;
}

bgPoint.prototype.toString = function()
{
	return "(" + this.x + ", " + this.y + ")";
}

bgPoint.prototype.toXmlRpc = function()
{
	return "<struct>" + "<member><name>x</name><value><int>" + this.x + "</int></value></member>" + "<member><name>y</name><value><int>" + this.y + "</int></value></member></struct>";
}

function bgStroke(whiteboardEditorView, layerNode, color, previewColor, width, bgStrokeId, startbgPointL)
{
	this.whiteboardEditorView = whiteboardEditorView;

	this.bgStrokeId = bgStrokeId;
	this.color = color;
	this.previewColor = previewColor;
	this.width = width;
		
	this.pointArray = new Array();
	this.storebgPoint(startbgPointL);
	
	// Create div node to hold the bgStroke, with the given id:
	this.bgStrokeNode = whiteboardEditorView.document.createElement("div");	
	layerNode.appendChild(this.bgStrokeNode);
	this.bgStrokeNode.setAttribute("id", bgStrokeId);	
	
	this.lastbgPointL = startbgPointL;
	this.jg = new jsGraphics(bgStrokeId, whiteboardEditorView.toplevelWindow);
	this.jg.setColor(previewColor);
	this.jg.setStroke(width);
}

bgStroke.prototype.getWhiteboard = function ()
{
	 return this.whiteboardEditorView.getWhiteboard();
}

bgStroke.prototype.storebgPoint = function(pointL)
{
	this.pointArray[this.pointArray.length] = pointL;
}

bgStroke.prototype.addbgPoint = function(nextbgPointL)
{
	this.storebgPoint(nextbgPointL);
	var lastD = this.whiteboardEditorView.L2D(this.lastbgPointL);
	var nextD = this.whiteboardEditorView.L2D(nextbgPointL);
	var offs = this.width/2;
	this.jg.drawLine(lastD.x - offs, lastD.y - offs,
					 nextD.x - offs, nextD.y - offs);
	this.jg.paint();
	this.lastbgPointL = nextbgPointL;
}

bgStroke.prototype.toXmlRpc = function()
{
	var result = "<struct><member><name>color</name><value><string>" + this.color + "</string></value></member>";
	result += "<member><name>width</name><value><int>" + this.width + "</int></value></member>";
	result += "<member><name>points</name><value><array><data>";
	for (var i=0;i<this.pointArray.length;i++) {
		result += "<value>" + this.pointArray[i].toXmlRpc() + "</value>";
	}	
	result += "</data></array></value></member></struct>";
	return result;
}

// define empty callback hook so that calls can be async:    
function dummyXmlRpcCallback(returnval, error) {
   	if (error!=null) {	
    	reportException(error);
    }
}

bgStroke.prototype.finish = function()
{
	errorLog.reportEvent("finishing bgStroke: \"" + this.pointArray.toString() + "\"");

	var whiteboard = this.getWhiteboard();

	// send to server...
	yarrr.addWhiteboardStroke(whiteboard.id, this, this.bgStrokeId, dummyXmlRpcCallback);
}

var g_numStrokes = 0;

function handleMouseDown(event)
{
	var canvasNode = event.currentTarget; 
	var whiteboard = canvasNode.whiteboard;
	var whiteboardEditorView = canvasNode.whiteboardEditorView;

	if (whiteboardEditorView.disableTools)
		return;

	if (whiteboardEditorView.currentTool == "text") {
		return;
	}
	
	// Handle case where we lost the mouse-up on an earlier stroke (if it occurred outside our window) by deleting the old stroke:
	if (whiteboardEditorView.currentbgStroke!=null) {
		whiteboardEditorView.layerNode.removeChild(whiteboardEditorView.currentbgStroke.bgStrokeNode);
		whiteboardEditorView.currentbgStroke=null;
	}
	
	var bgStrokeId = canvasNode.layerId + "bgStroke" + g_numStrokes++;
	
	whiteboardEditorView.currentbgStroke = new bgStroke(whiteboardEditorView, 
		whiteboardEditorView.layerNode,
		whiteboardEditorView.currentColor,
		whiteboardEditorView.currentPreviewColor,
		whiteboardEditorView.currentWidth,
		bgStrokeId,
		whiteboardEditorView.getEventbgPointL(event));

	event.preventDefault()
}

function handleMouseUp(event)
{
	var canvasNode = event.currentTarget; 
	var whiteboard = canvasNode.whiteboard;
	var whiteboardEditorView = canvasNode.whiteboardEditorView;
	
	if (whiteboardEditorView.disableTools)
		return;
		
	if (whiteboardEditorView.currentTool == "text") {
		var point = whiteboardEditorView.getEventbgPointL(event);
		
  		var input = document.createElement("input");
		input.setAttribute("type", "text");
		input.setAttribute("value", "");
		
	        var div = document.createElement("form");
	        div.appendChild(input);
	        div.style.display = "inline";
 		div.style.position = "absolute";
		
		whiteboardEditorView.layerNode.appendChild(div);	
		
		div.style.left = event.clientX + "px";
		div.style.top = (event.clientY - div.offsetHeight) + "px";
		
		whiteboardEditorView.disableTools = true;
		
		input.focus();
		div.onsubmit = function () { 
		   if (input.value != "") {
  	             yarrr.addWhiteboardText(whiteboard.id, 
	  			  input.value, 
	  			  point.x, point.y, 
				  whiteboardEditorView.currentColor,
	  			  dummyXmlRpcCallback);	
	           }
		   whiteboardEditorView.layerNode.removeChild(div);	
		   whiteboardEditorView.disableTools = false;
		   return false;
		 };
		return;
	}
	
	var currentbgStroke = whiteboardEditorView.currentbgStroke;
	whiteboardEditorView.currentbgStroke = null;
	currentbgStroke.finish();
	event.preventDefault()
}

function handleMouseMove(event)
{
	var canvasNode = event.currentTarget; 
	var whiteboard = canvasNode.whiteboard;
	var whiteboardEditorView = canvasNode.whiteboardEditorView;
	
	if (whiteboardEditorView.disableTools)
		return;
		
	if (whiteboardEditorView.currentTool == "text") {
	  return;
	}
	
	
	if (whiteboardEditorView.currentbgStroke) {
		whiteboardEditorView.currentbgStroke.addbgPoint(whiteboardEditorView.getEventbgPointL(event));
	}
	event.preventDefault()
}

Whiteboard.prototype = new ReferencableObject();
WhiteboardImage.prototype = new Object();
WhiteboardView.prototype = new WhiteboardImage();
WhiteboardThumbnailView.prototype = new WhiteboardView();
WhiteboardEditorView.prototype = new WhiteboardView();

function Whiteboard(id, discussion)
{	
	// Superclass construction:
	this.id = id;
	this.discussion = discussion;
	this.discussionId = discussion.id;
	this.width = null;
	this.height = null;
	
	// Create views:
	this.thumbnailView = new WhiteboardThumbnailView(this);	
	this.editorView = null;

 	var precached = stealPrecachedReferencableObject(this.id)
	if (precached != null)
		this.sync(precached)
	else
		this.update();
}

Whiteboard.prototype.updateCallback = function(result, err) {
	// get new background image from server (the subsequent onload will trigger further activity)
	errorLog.reportEvent("whiteboard.updateCallback:" + result);
	this.latestKnownServerVersion = result;
	
	this.notifyViews();
}
	
Whiteboard.prototype.notifyViews = function()
{
	this.thumbnailView.handleUpdate();
	if (this.editorView!=null) {
		this.editorView.handleUpdate();
	}
}

Whiteboard.prototype.addToChat = function()
{
	this.discussion.addWhiteboardToChat(this, this.latestKnownServerVersion);
}

Whiteboard.prototype.sync = function(contents) 
{
	if ("version" in contents) {
		if (this.latestKnownServerVersion != contents.version) {
			this.latestKnownServerVersion = contents.version;
			this.notifyViews();
		}
	}
}

Whiteboard.prototype.update = function()
{
	errorLog.reportEvent("whiteboard update");
	var whiteboard = this;
	yarrr.getWhiteboardVersion(whiteboard.id, function (result, err) { whiteboard.updateCallback (result, err); } );	
}

Whiteboard.prototype.openWhiteboardEditorView = function()
{
	// deal with closed windows:
	if (this.editorView!=null) {
		if (this.editorView.toplevelWindow.closed) {
			this.editorView = null;
		}
	}
	
	if (this.editorView!=null) {
		// Bring window to top (and give kbd focus:
		this.editorView.toplevelWindow.focus();
	} else {
		// Create new window:
		this.editorView = new WhiteboardEditorView(this);
	}
}

/* WhiteboardImage */

function WhiteboardImage(whiteboardId)
{
	this.imageElement = null;
	this.requestedImageVersion = null;
	this.whiteboardId = whiteboardId;
}

WhiteboardImage.prototype.getWhiteboardId = function()
{
	return this.whiteboardId;
}

WhiteboardImage.prototype.setImageElement = function(imageElement)
{
	this.imageElement = imageElement;
       
        var styleString = "background-image: url(\"" + this.getImageUrl(null, "background") +"\");";
        styleString +="background-color: transparent;";
        styleString +="vertical-align: middle;";
        this.imageElement.setAttribute("style", styleString);
	this.requestImageUrl();
}

WhiteboardImage.prototype.setVersion = function(version)
{
	this.requestImageUrl(version);
}

WhiteboardImage.prototype.requestImageUrl = function(version)
{
	this.requestedImageVersion = version
	if (this.imageElement) {
		var imageUrl = this.getImageUrl(version, "foreground");
		errorLog.reportEvent("setting image URL:" + imageUrl);	
		this.imageElement.setAttribute("src", imageUrl);
		errorLog.reportEvent("...done");
	}
}

WhiteboardImage.prototype.getImageUrl = function(version, layerString)
{
	yarrr_assert ((layerString=="foreground" || layerString=="background"), "WhiteboardImage.prototype.getImageUrl");

	var result = "/yarrr/Whiteboard?id=" + this.getWhiteboardId();
	if (version!=null) {
		result += "&version=" + version;
	}
	result += "&imageType=" + this.getImageUrlSizeType() + "-" + layerString;
	return result;
}

/* WhiteboardView */

function constructWhiteboardView(whiteboardView, whiteboard)
{
	whiteboardView.whiteboardId = whiteboard.id;
	whiteboardView.whiteboard = whiteboard;
	whiteboardView.latestKnownServerVersion = null;
}

function WhiteboardView(whiteboard)
{
}

WhiteboardView.prototype.getWhiteboard = function()
{
	 return this.whiteboard;
}

/* called by the whiteboard on each view after an update */
WhiteboardView.prototype.handleUpdate = function()
{
	this.setVersion(this.whiteboard.latestKnownServerVersion);
}

/* WhiteboardThumbnailView */

function WhiteboardThumbnailView(whiteboard, version)
{
	constructWhiteboardView(this, whiteboard)
	
	imageListEl = document.createElement("span");
        imageListEl.setAttribute("class", "whiteboardThumbnailImageHolder");
	this.imageEl = document.createElement("img");
	imageListEl.appendChild(this.imageEl);
	this.imageEl.whiteboardView = this;
	this.imageEl.setAttribute("class", "whiteboardThumbnailImage");
	this.imageEl.onclick = function() { whiteboard.openWhiteboardEditorView(); };
	this.imageEl.onload = function() {globalOnImageReload(this);};	
	whiteboard.discussion.whiteboards_list.appendChild(imageListEl);
	
	this.setImageElement(this.imageEl);
}

WhiteboardThumbnailView.prototype.getImageUrlSizeType = function()
{
	return "thumbnail";
}

WhiteboardThumbnailView.prototype.onImageReload = function()
{
	errorLog.reportEvent("WhiteboardThumbnailView onImageReload()");

	var whiteboard = this.getWhiteboard();
	
	if (whiteboard.width==null) {
		whiteboard.width = this.imageEl.naturalWidth*5;
	}
	if (whiteboard.height==null) {
		whiteboard.height = this.imageEl.naturalHeight*5;
	}
}

/* WhiteboardEditorView */

function WhiteboardEditorView(whiteboard)
{	
	constructWhiteboardView(this, whiteboard)

	// We set an initial width/height so that we have some kind of sane-sized drawing area; this will get changed when the image load finishes:
	var effectiveWidth;
	var effectiveHeight;	
	
	if (this.width!=null) {
		effectiveWidth = this.width;
	} else {
		effectiveWidth = 640;
	}
	if (this.height!=null) {
		effectiveHeight = this.height;
	} else {
		effectiveHeight = 480;
	}

	this.toplevelWindow = window.open(null, whiteboard.getId(), "width="+(effectiveWidth+20) + ", height="+(effectiveHeight+200), null);
	
	this.document = this.toplevelWindow.document;
	var html = this.generateInitialHtml();
	this.document.write(html);

	var linkElement = this.document.createElement("link");	
	linkElement.setAttribute("rel", "stylesheet");
	linkElement.setAttribute("href", "../stylesheets/whiteboard-editor.css");
	//this.document.head.appendChild(linkElement);

	var colorChooserDiv = appendDiv(this.document.body, "colorChooserDiv");
	var whiteboardDiv = appendDiv(this.document.body, "whiteboardDiv");	

        var toolbar = new Toolbar();
        toolbar.setOrientation('horiz');
        var b;

        b = new ToolRadio();
        b.setImage("draw.png");
        b.setGroup("toolgroup");
        b.connectMethod("toggled", this, this.toolToggled, "draw");
        toolbar.add(b);

	b.setActive(true); // Select initial tool;
        
        b = new ToolRadio();
        b.setImage("text.png");
        b.setGroup("toolgroup");
        b.connectMethod("toggled", this, this.toolToggled, "text");
        toolbar.add(b);
        
        b = new ToolDropdown();
        b.setFollowChildButton(true);
        b.connectMethod("itemActivated", this, this.colorSelected);
        toolbar.add(b);
        
        var sub = b.getSubToolbar();
        var first = this.addColorButton(sub, "#000000", "#808080", 5);
	this.addColorButton(sub, "#ff0000", "#800000", 5);
	this.addColorButton(sub, "#00ff00", "#008000", 5);
	this.addColorButton(sub, "#0000ff", "#000080", 5);	
	this.addColorButton(sub, "#ffffff", "#f0f0f0", 25);
	
	first.click(); // Select initial color

        b = new ToolButton();
        b.setText("Snapshot to Chat");
        b.connect("clicked", function () { whiteboard.addToChat(); });
        toolbar.add(b);
        
        toolbar.addToDocument(whiteboardDiv);

	this.canvasNodeId = "whiteboardEditor-" + whiteboard.getId();
	this.canvasNode = this.document.createElement("div");	
	this.canvasNode.setAttribute("id", this.canvasNodeId);
	this.canvasNode.setAttribute("class", "whiteboardEditor");
	whiteboardDiv.appendChild(this.canvasNode);

	this.latestFetchedOpVersion = 0;

	this.canvasNode = this.document.getElementById(this.canvasNodeId);
	var imageId = this.canvasNodeId + "BackgroundImage";
	this.canvasNode.layerId = this.canvasNodeId + "Layer" + Math.round((Math.random()*10000)); // FIXME;

	// Clientside Whiteboard strokes only appear if I create both img and inner div tag in one single assignment of innerHTML
	// Trying to split up creation (and manipulation of the DOM tree) doesn't work: the stroke DIVs get intermingled with IMG elements
	var innerHTML = "";	
	innerHTML = "<img id=\"" + imageId + "\" class=\"whiteboardEditorImage\"";
	innerHTML += "width=\"" + effectiveWidth + "\" height=\"" + effectiveHeight + "\"";
	innerHTML += ">";
	innerHTML += "<div id=\"" + this.canvasNode.layerId + "\" />"
	innerHTML += "</img>";
	this.canvasNode.innerHTML = innerHTML;
	this.imageEl = this.document.getElementById(imageId);
	
	// non-working fragment of code that would add image via DOM manipulation:
	/*
	this.imageEl = appendElement(this.canvasNode, "img");
	this.imageEl.setAttribute("id", imageId);
	this.imageEl.setAttribute("class", "whiteboardEditorImage");
	this.imageEl.setAttribute("width", effectiveWidth);
	this.imageEl.setAttribute("height", effectiveHeight);
	*/

	// non-working fragment of code that adds div via innerHTML manipulation of already-created img:
	/*	
	this.imageEl.innerHTML = "<div id=\"" + this.canvasNode.layerId + "\" />"
	*/
	this.layerNode = this.document.getElementById(this.canvasNode.layerId)
	
	// non-working fragment of code that adds div via DOM manipulation:
	/*
	this.layerNode = appendElement(this.imageEl, "div");
	this.layerNode.setAttribute("id", this.canvasNode.layerId);
	*/
	
	this.setImageElement(this.imageEl);
	
	this.imageEl.whiteboardView = this;
	this.imageEl.onload = function() {globalOnImageReload(this);};	
	this.canvasNode.whiteboard = whiteboard;
	this.canvasNode.whiteboardEditorView = this;
	this.currentbgStroke = null;
	this.disableTools = false;
	
	// Bind events:
	this.canvasNode.addEventListener("mousedown", handleMouseDown, false);
	this.canvasNode.addEventListener("mouseup", handleMouseUp, true);
	this.canvasNode.addEventListener("mousemove", handleMouseMove, true);
}

WhiteboardEditorView.prototype.toolToggled = function(item, tool)
{
        if (item.getActive()) {
  		this.currentTool = tool;
  	}
}

WhiteboardEditorView.prototype.addColorButton = function(toolbar, color, previewColor, thickness)
{
        b = new ToolButton();
        b.data.color = color;
        b.data.previewColor = previewColor;
        b.data.thickness = thickness;
        b.setIconColor(color);
        toolbar.add(b);
        return b;
}

WhiteboardEditorView.prototype.colorSelected = function(toolbarItem, selectedItem)
{
	this.currentColor = selectedItem.data.color;
	this.currentPreviewColor = selectedItem.data.previewColor;
	this.currentWidth = selectedItem.data.thickness;	
}

WhiteboardEditorView.prototype.generateInitialHtml = function()
{
	var html = "<html><head><title>Whiteboard</title>";
  	//html += "<link rel=\"stylesheet\" href=\"../stylesheets/whiteboard-editor.css\"/>"
	html += "</head><body style=\"background-color: #8faccf;\">";
	html += "</body></html>";

	return html;
}

WhiteboardEditorView.prototype.getImageUrlSizeType = function()
{
	return "fullsize";
}

WhiteboardEditorView.prototype.getScreenToWindowOffsetX = function()
{
	// Netscape 6:
	return this.toplevelWindow.pageXOffset;
	
	// FIXME: is this document.body.scrollTop for IE ?
}


WhiteboardEditorView.prototype.getScreenToWindowOffsetY = function()
{
	// Netscape 6:
	return this.toplevelWindow.pageYOffset;
	
	// FIXME: is this document.body.scrollLeft for IE ?
}

WhiteboardEditorView.prototype.W2L = function(pointW)
{
	var pointD = this.W2D(pointW);
	var pointL = this.D2L(pointD);
	return pointL;
}

WhiteboardEditorView.prototype.W2D = function(pointW)
{
	// Offset window coordinates by current scroll:
	return new bgPoint(pointW.x + this.getScreenToWindowOffsetX(),
		 	 		 pointW.y + this.getScreenToWindowOffsetY());
}

WhiteboardEditorView.prototype.calcL2DOffset = function()
{
	// Walk up the DOM tree, gathering a cumulative offset:
	var iterator = this.canvasNode;
	var cumulativeOffset = new bgPoint(0,0);
	
	// FIXME: need a better way to stop at the document node (so that the <body> is the last one processed)
	while (iterator!=null) {
	//while (iterator.nodeName!="#document") {
	
		if (iterator.offsetLeft==undefined) {
			break;
		}
		cumulativeOffset.x += iterator.offsetLeft;
		cumulativeOffset.y += iterator.offsetTop;
		
		iterator = iterator.offsetParent;
	}
	
	return cumulativeOffset;
}

WhiteboardEditorView.prototype.D2L = function(pointD)
{
	var L2DOffset = this.calcL2DOffset();	
	return new bgPoint(pointD.x - L2DOffset.x,
					 pointD.y - L2DOffset.y);
}

WhiteboardEditorView.prototype.L2D = function(pointL)
{
	var L2DOffset = this.calcL2DOffset();	
	return new bgPoint(pointL.x + L2DOffset.x,
					 pointL.y + L2DOffset.y);
}

WhiteboardEditorView.prototype.getEventbgPointL = function(event)
{
//	errorLog.reportEvent("event at clientX=" + event.clientX + " clientY="+ event.clientY);
//	errorLog.reportEvent("event at layerX=" + event.layerX + " layerY="+ event.layerY);
	var pointW = new bgPoint(event.clientX, event.clientY);

//	errorLog.reportEvent("pointW=" + pointW);

	var pointL = this.W2L(pointW);

//	errorLog.reportEvent("pointL=" + pointL);

	return pointL;
}

WhiteboardEditorView.prototype.getOpIdsBetweenCallback = function(result, err)
{
	var newbgOps = result;
	this.latestFetchedOpVersion = newbgOps.latestFetchedVersion;
   	
   	var bgOpsIds = newbgOps.opIds;
	errorLog.reportEvent("getOpIdsBetween: returned " + bgOpsIds.length + " bgOps ");	
   	
	// remove any local bgOps now on server:
	for (var i=0;i<bgOpsIds.length; i++) {
		this.removebgOp(bgOpsIds[i]);
	}
}

WhiteboardEditorView.prototype.onImageReload = function()
{
	errorLog.reportEvent("WhiteboardEditorView onImageReload()");

	var whiteboard = this.getWhiteboard();

	if (whiteboard.width==null) {
		whiteboard.width = this.imageEl.naturalWidth;
	}
	if (whiteboard.height==null) {
		whiteboard.height = this.imageEl.naturalHeight;
	}
	
	this.imageEl.setAttribute("width", this.imageEl.naturalWidth);
	this.imageEl.setAttribute("height", this.imageEl.naturalHeight);

	if (this.requestedImageVersion!=null) {
		if (this.latestFetchedOpVersion!=this.requestedImageVersion) {
			var whiteboardEditorView = this;
			yarrr.getOpIdsBetween(whiteboard.id, this.latestFetchedOpVersion, this.requestedImageVersion, function(result, err) {whiteboardEditorView.getOpIdsBetweenCallback(result,err);} );
		}
	}
}

function globalOnImageReload(imageElement)
{
	errorLog.reportEvent("globalOnImageReload()");
	imageElement.whiteboardView.onImageReload();
}

WhiteboardEditorView.prototype.removebgOp = function(bgOpId)
{
	var bgOpNode = this.document.getElementById(bgOpId)
	if (bgOpNode!=null) {
		this.layerNode.removeChild(bgOpNode);
	}
}

function suppressImageDrag(imageElement)
{

	imageElement.addEventListener("mousedown", function(event) {event.preventDefault();}, false);
	imageElement.addEventListener("mouseup", function(event) {event.preventDefault();}, true);
	imageElement.addEventListener("mousemove", function(event) {event.preventDefault();}, true);		
}

