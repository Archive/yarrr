yarrrWikiHeaderText = ${headertext}
yarrrWikiTitle = ${JStopictitle}

function yarrrFullInit(rootobj) {
  errorLog.reportEvent("starting yarrrFullInit")
  // Hack
  if (wikiYarrrXmlRpcURL.search(/localhost/) >= 0) {
    errorLog.enableAutoShow();
    var form = document.createElement("form")
    var button = document.createElement("input")
    button.setAttribute("type", "button")
    button.setAttribute("value", "Show \u2620 Log");
    button.addEventListener("click", function (e) { errorLog.show(); }, false);
    form.appendChild(button)
    document.body.appendChild(form)
  }
 
  theTopic = new EmbeddedTopic(rootobj)
  registerObject(theTopic);

  errorLog.reportEvent("calling yarrrWikiInit")
  yarrrWikiInit(rootobj)
  errorLog.reportEvent("adding init hook") 
  theTopic.addInitHook(function () { yarrrWikiFullInit(theTopic); })  
}

function yarrrInit() {
  try {
  setWidgetImageDir(wikiYarrrRoot + "images/");
  
  var rootobj = "${topicid}"
  errorLog.reportEvent("starting yarrrInitFull")  
  yarrrInitFull("${anonymousPerson}", "${person}", rootobj, "${clientInstanceId}", 
                ${referencableObjectPreCache}, wikiYarrrRoot, wikiYarrrXmlRpcURL, yarrrFullInit)
  } catch (e) {
    errorLog.reportError(e);
  }
}
 
if (window.addEventListener) window.addEventListener("load",yarrrInit,false); 
else if (window.atatachEvent) window.attachEvent("onload",yarrrInit);