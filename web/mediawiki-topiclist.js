function EmbeddedYarrrSummary(id) {
  if (arguments.length > 0) {
    this.init(id)
  }
}

EmbeddedYarrrSummary.prototype = new ReferencableObject()
EmbeddedYarrrSummary.prototype.constructor = EmbeddedYarrrSummary;
EmbeddedYarrrSummary.superclass = ReferencableObject.prototype;

EmbeddedYarrrSummary.prototype.init = function (id) {
  EmbeddedYarrrSummary.superclass.init.call(this, id)
  this.activetopics = {}
  this.inactivetopics = {}
  EmbeddedYarrrSummary.superclass.postInit.call(this)    
}

function handleObjectDeltaFromDict(curobjects, newobjects) {
  var deleted = []
  for (var obj in curobjects) {
    if (!(obj in newobjects)) {
      deleted.push(obj)
    }
  }
  for (var i = 0; i < deleted.length; i++) {
    var id = deleted[i]
    var obj = objectRegistry.lookup(id)
    if (obj != null) {
      obj.remove();
      objectRegistry.deregister(obj)
    }
  }
}

EmbeddedYarrrSummary.prototype.sync = function (content) {
  for (var i = 0; i < content.activetopics.length; i++) {
    var id = content.activetopics[i];
    if (!(id in this.activetopics)) {
      this.activetopics[id] = new EmbeddedActiveTopicSummary(id);
      registerObject(this.activetopics[id])
    }
  }
  handleObjectDeltaFromDict(this.activetopics, getArrayAsObject(content.activetopics))
  EmbeddedYarrrSummary.superclass.sync.call (this, content)  
}

function EmbeddedActiveTopicSummary(id) {
  if (arguments.length > 0) {
    this.init(id)
  }
}

EmbeddedActiveTopicSummary.prototype = new ReferencableObject()
EmbeddedActiveTopicSummary.prototype.constructor = EmbeddedActiveTopicSummary;
EmbeddedActiveTopicSummary.superclass = ReferencableObject.prototype;

EmbeddedActiveTopicSummary.prototype.init = function (id) {
  EmbeddedActiveTopicSummary.superclass.init.call(this, id)
  
  this.discussions = {};
  this.whiteboards = {};
  this.presentPersons = {};
  this.presentPersonNames = [];
  
  var topicsummaries = document.getElementById("yarrrActiveTopicList")
  this.li = document.createElement("li")
  topicsummaries.appendChild(this.li)
  this.summary_div = document.createElement("div")
  this.summary_div.setAttribute("class", "yarrrActiveTopicSummary")
  this.li.appendChild(this.summary_div)
  
  // Title
  this.pageLink = document.createElement("a")
  this.pageLink.setAttribute("class", "yarrrActiveTopicSummaryLink")
  this.summary_div.appendChild(this.pageLink)  
  
  this.summary_div.appendChild(document.createTextNode(" ("))
  
  // Present persons
  this.presentpersons_span = document.createElement("span")
  this.presentpersons_span.setAttribute("class", "yarrrActiveTopicSummaryPresentPersons")
  this.summary_div.appendChild(this.presentpersons_span)
  this.summary_div.appendChild(document.createTextNode(")"))
  
  EmbeddedActiveTopicSummary.superclass.postInit.call(this, id)
}

EmbeddedActiveTopicSummary.prototype.sync = function (content) {
  this.pageLink.innerHTML = "";
  this.pageLink.appendChild(document.createTextNode(content.name))
  this.pageLink.setAttribute("href", createYarrrWikiURI(content.name));

  var have_presentpersons = content.presentpersons.length > 0
  this.presentpersons_span.innerHTML = ""; 
  for (var i = 0; i < content.presentpersons.length; i++) {
    var person = content.presentpersons[i]
    var personelt = document.createElement("span")
    personelt.setAttribute("class", "yarrrPresentPerson")
    personelt.appendChild(document.createTextNode(person))
    this.presentpersons_span.appendChild(personelt)
    if (i < content.presentpersons.length - 1) {
      this.presentpersons_span.appendChild(document.createTextNode(", "))
    }
  }

  if (have_presentpersons) 
    this.li.style.display = "";
  else
    this.li.style.display = "none";
  EmbeddedActiveTopicSummary.superclass.sync.call (this, content)      
}

EmbeddedActiveTopicSummary.prototype.remove = function () {
  this.li.parentNode.removeChild(this.li)
}

// Global hook called from wikiembed-topiclist.jsp
function yarrrWikiSummaryInit (rootobj) {
 try {
   errorLog.reportEvent("yarrrWikiSummaryInit") 
   // Just a stub, but here in case we need it later
   errorLog.reportEvent("yarrrWikiSummaryInit finished")  
  } catch (e) {
    errorLog.reportError("yarrrWikiSummaryInit failed: " + e);
  }   
}

// Global hook called from wikiembed-topiclist.jsp
function yarrrWikiSummaryFullInit (topiclist) {
  try {
    errorLog.reportEvent("yarrrWikiSummaryFullInit") 
    // Just a stub, but here in case we need it later    
    errorLog.reportEvent("yarrrWikiSummaryFullInit finished")
  } catch (e) {
    errorLog.reportError("yarrrWikiSummaryFullInit failed: " + e);
  }
}
