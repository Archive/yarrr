function yarrr_assert(boolCondition, stringDescription)
{
	if (!boolCondition) {
		throw new Error ("assertion failed: "+stringDescription);
	}
}

function getSelection(textarea) {
  // FIXME: This is moz only:
  var selection = new Object();
  selection.start = textarea.value.length
  selection.end = selection.start;
  try {
    selection.start = textarea.selectionStart;
    selection.end = textarea.selectionEnd;
  } catch (e) {
    // Mozilla errors here if hidden
  }
  return selection
}

function setSelection(textarea, selection) {
  // FIXME: This is moz only:
  try {
    textarea.setSelectionRange(selection.start, selection.end);
  } catch (e) {
    // Mozilla errors here if hidden
  }
}

function longestCommonSubstring(a, b) {
  var i;
  for (i = 0; i < a.length && i < b.length; i++) {
    if (a.charAt(i) != b.charAt(i))
      break;
  }
  
  return a.substring(0, i);
}

function leadingZero(nr)
{
	if (nr < 10) nr = "0" + nr;
	return nr;
}

function getPrettyDate(date) {
  var weekday = new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
  var month = new Array("January", "February", "March", "April", "May", "June", "July", "August",
			"September", "October", "November", "December");
  var now = new Date();

  if (now.getFullYear() == date.getFullYear()) {
    if (date.getMonth() == date.getMonth() && now.getDay() == date.getDay()) {
      return "Today " + date.getHours() + ":" + leadingZero(date.getMinutes());
    } else {
      return month[date.getMonth()] + " " + date.getDay();
    }
  } else {
    return month[date.getMonth()] + " " + date.getDay() + " " + date.getFullYear();
  }
}

function getObjectKeys(obj) {
  var keys = []
  for (key in obj) {
    keys.push(key)
  }
  return keys;
}

function getArrayAsObject(arr) {
  var ret = {}
  for (var i = 0; i < arr.length; i++) {
    ret[arr[i]] = 1;
  }
  return ret;
}
    
function dumpObjectKeysToString(obj) {
  return getObjectKeys(obj).join(", ")
}

function yarrrMarkupToText(ht) {
  var text = "";
  var xmlext = importModule("xml");
	
  var xmlDoc = xmlext.parseXML(ht)
  var rootNode = xmlDoc.documentElement;
  if (rootNode == null)
    throw new Error("Malformed yarrrML");
  for(var i=0; i < rootNode.childNodes.length; i++){
    var node = rootNode.childNodes.item(i);
    if (node.nodeType == 1) {
      switch (node.tagName) {
        case "text":
        case "bold":
        case "underlined":
        case "topic-link":
        case "link":
          if (node.firstChild) {
            text += node.firstChild.nodeValue;
          }
          break;
        case "whiteboard":
          text += "[whiteboard]";
	      break;
        default:
          throw new Error("Malformed yarrrML element: " + node.tagName);
      }
    }
  }
  
  return text;
}

function yarrrMarkupToHTML(ht, doc, outnode) {
  var xmlext = importModule("xml");

  var xmlDoc = xmlext.parseXML(ht)
  var rootNode = xmlDoc.documentElement;
  if (rootNode == null)
    throw new Error("Malformed yarrrML");
  for(var i=0; i < rootNode.childNodes.length; i++){
    var node = rootNode.childNodes.item(i);
    if (node.nodeType == 1) {
      switch (node.tagName) {
        case "text":
          if (node.firstChild)
            outnode.appendChild(doc.createTextNode(node.firstChild.nodeValue));
          break;
        case "bold":
          var span = doc.createElement("span");
          span.setAttribute("class", "boldText");
          span.appendChild(doc.createTextNode(node.firstChild.nodeValue));
          outnode.appendChild(span);
          break;
        case "underlined":
          var span = doc.createElement("span");
          span.setAttribute("class", "underlinedText");
          span.appendChild(doc.createTextNode(node.firstChild.nodeValue));
          outnode.appendChild(span);
          break;
        case "topic-link":
          var topicname = node.firstChild.nodeValue
          var link = doc.createElement("a")
          link.setAttribute("href", topicname)
          link.setAttribute("class", "topicLink")
          link.appendChild(doc.createTextNode(topicname))
          outnode.appendChild(doc.createTextNode("["))
          outnode.appendChild(link);
          outnode.appendChild(doc.createTextNode("]"))
          break;
        case "link":
          var title = node.firstChild.nodeValue;
          var link = doc.createElement("a")
          link.setAttribute("target", "_blank");
          link.setAttribute("href", node.getAttribute("href"));
          link.setAttribute("class", "link");
          link.appendChild(doc.createTextNode(title));
          outnode.appendChild(link);
          break;
        default:
          throw new Error("Malformed yarrrML element: " + node.tagName);
      }
    }
  }
}

function appendElement(parentElement, name) {
	var newElement = parentElement.ownerDocument.createElement(name);	
	parentElement.appendChild(newElement);
	return newElement;	
}

function appendDiv(parentElement, id) {
	var divElement = appendElement(parentElement, "div");	
	divElement.setAttribute("id", id);
	return divElement;	
}
