<%@ include file="prelude.jspf" %>

<html>
  <head>
    <title><c:out value="${topictitle}" escapeXml="true"/></title>
    <c:url var="stylesheet" value="/stylesheets/topic.css"/>
    <link rel="stylesheet" href="${stylesheet}"/>
  </head> 
  <body>

<%@ include file="clientcore.jspf" %>

<script type="text/javascript">
var theTopic;
var focused = false;
var unread_prio = 0;

function updateTitle() {
  var curtitle = ${JStopictitle}
  if (this.unread_prio == 1)
    curtitle = "\u2620 " + ${JStopictitle}
  else if (this.unread_prio == 2)
    curtitle = "\u2620\u2620 " + ${JStopictitle}
  window.document.title = curtitle;
}

function setFocus(isfocused) {
  focused = isfocused
  if (focused && unread_prio != 0) {
    unread_prio = 0;
    updateTitle();
  }
}

function handleOnUnread(priority) { 
  // priority:
  // 0: no unseen things
  // 1: unseen things
  // 2: unseen things for you
  if (!focused && priority > unread_prio) {
    unread_prio = priority;
    updateTitle();
  }
}

function appendElement(parentElement, name)
{
	var newElement = parentElement.ownerDocument.createElement(name);	
	parentElement.appendChild(newElement);
	return newElement;	
}

function openDiscussion() {
  theTopic.openDiscussion();
}

function yarrrFullInit(rootobj) {
  theTopic = new Topic(rootobj, function (priority) { handleOnUnread(priority); } )
  registerObject(theTopic) 
  errorLog.reportEvent("init");
 
  window.onblur = function () { setFocus(false); };
  window.onfocus = function () { setFocus(true); };
  
  var form = document.getElementById("statementForm");
  var input = document.getElementById("statementInput");
  if (getYarrrPerson().isAnonymous()) {
    input.setAttribute("disabled", "true"); 
  } else {
    input.removeAttribute("disabled");
  }
  form.onsubmit = function() {
    theTopic.addStatement(input.value);
    input.value = "";
    return false;
  }
}

function yarrrInit() {
  try {
     var rootobj = "${topicid}"
     yarrrInitFull("${anonymousPerson}", "${person}", rootobj, "${clientInstanceId}", 
                   ${referencableObjectPreCache}, "/yarrr/", "/yarrr/execute", yarrrFullInit)
     if (window.yarrrTest != undefined)
       window.yarrrTest();
  } catch (e) {
    alert("Error initializing page: " + e.toString());
  }
}

if (window.addEventListener) window.addEventListener("load",yarrrInit,false);
else if (window.attachEvent) window.attachEvent("onload",yarrrInit);

</script>

<div id="sidebar">
  <h1><c:out value="${topictitle}" escapeXml="true"/></h1>
  <h2><span id="topicCreationInfo"></span></h2>
  <div id="statements">
    <div id="statementsPlaceholder"></div>
    <form id="statementForm">
      New:&nbsp;<input id="statementInput" type="text"/><br/>
    </form>
  </div>  
  <div id="people">
    <h1 class="unimportant">People:</h1>
    <div id="peoplePlaceholder"></div><br/>
    <div id="personControlPlaceholder"></div>
  </div>
  <br/>
  <div id="messageArea">
  </div>
</div>

<div id="topiccontents">
  <div id="discussions"></div>
  <div id="closedcomments"></div>
</div>

</body>
</html>
