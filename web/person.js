var yarrrAnonymousId;
var yarrrPerson;

function Person(id) {
  this.id = id;
  this.control = null;
  this.registered = false;
  this.name = "";
  
  var precached = stealPrecachedReferencableObject(this.id);
  if (precached != null)
    this.sync(precached);
  else 
    this.update();
}

Person.prototype = new ReferencableObject();

Person.prototype.setControl = function(control) {
  this.control = control;
  this.updateControl();
}

Person.prototype.getName = function(name) {
  return this.name;
}

Person.prototype.setName = function(name) {
  yarrr.setPersonName(name, function (result, err) { window.location.reload(); });
}

Person.prototype.isAnonymous = function() {
   return this.getId() == yarrrAnonymousId;
}

Person.prototype.updateControl = function() {
  this.control.setName(this.name);
  this.control.setRegistered(this.registered, !this.isAnonymous());
  this.control.setAnonymous(this.isAnonymous());
}

Person.prototype.sync = function(contents) {
	this.name = contents.name;
	this.registered = contents.registered;
	if (this.control != null) {
	  this.updateControl();
	}
}

function PersonControl(container, anonymous) {
  var control = this;
  this.container = container;

  var authorNameSpan = document.createElement("span");
  authorNameSpan.setAttribute("id", "authorName");
  container.appendChild(authorNameSpan);
  
  var yarrrNameInput = document.createElement("form")
  yarrrNameInput.setAttribute("action", getYarrrURL("login.jsp"));
  yarrrNameInput.setAttribute("method", "post");
  authorNameSpan.appendChild(yarrrNameInput);
  
  yarrrNameInput.appendChild (document.createTextNode ("Nick: "));
  
  this.nameInput = document.createElement("input")
  this.nameInput.setAttribute("type", "text")
  this.nameInput.setAttribute("id", "authorName")
  this.nameInput.setAttribute("name", "nick")
  yarrrNameInput.appendChild(this.nameInput)

  var input = document.createElement("input")
  input.setAttribute("name", "redirect")
  input.setAttribute("type", "hidden")
  input.setAttribute("value", window.location)
  yarrrNameInput.appendChild(input)
  
  input = document.createElement("input")
  input.setAttribute("type", "submit")
  input.setAttribute("value", "Change")
  yarrrNameInput.appendChild(input)
  
  yarrrNameInput.onsubmit = function () {
    if (!control.anonymous) {
      control.setName(control.nameInput.value);
      return false;
    }
  };
  
  this.registerDiv = document.createElement("a");
  this.registerDiv.setAttribute("id", "registeredStatus");
  container.appendChild(this.registerDiv);
  
  getYarrrPerson().setControl(this);  
}

PersonControl.prototype.setAnonymous = function(anonymous) {
  this.anonymous = anonymous
}

PersonControl.prototype.setName = function(name) {
  this.nameInput.value = name;
}

PersonControl.prototype.setRegistered = function(registered, can_register) {
  this.registerDiv.innerHTML = "";
  
  if (!can_register) {
  } else if (registered) {
    this.registerDiv.appendChild(document.createTextNode("Registered user"));
  } else {
    var a = document.createElement("a")
	a.setAttribute("href", getYarrrURL("register-form.jsp?redirect=" + window.location));
	a.appendChild(document.createTextNode("Register"));
	this.registerDiv.appendChild(a);
  }
}

function getYarrrPerson() {
  return yarrrPerson;
}

function initYarrrPerson(anonymousid, personid) {
  yarrrAnonymousId = anonymousid;
  
  if (personid == null || personid == anonymousid || ((typeof wikiIsLoggedIn) != undefined && !wikiIsLoggedIn)) {
    yarrrPerson = new Person(yarrrAnonymousId);
  } else {
    yarrrPerson = new Person(personid);
  }
}