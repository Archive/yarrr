var yarrrRootURL = null;

function getYarrrURL(fragment) {
  return yarrrRootURL + fragment;
}

var theYarrr;

function yarrrInitXmlRpc(rpcurl) {
    yarrr = new xmlrpc.ServerProxy(rpcurl, yarrrMethods);
}

function yarrrInitFull(anonymousPerson, person, rootobj, clientInstanceId, precache, urlprefix, xmlrpcprefix, fullinit) {
    referencableObjectPreCache = precache
    if (yarrr == null)
	    yarrrInitXmlRpc(xmlrpcprefix);
    if (yarrrRootURL == null)
	    yarrrRootURL = urlprefix
    initYarrrPerson(anonymousPerson, person);
 
    theYarrr = new Yarrr(rootobj, clientInstanceId)
  
    // Now we have registered all objects and we can start polling for changes to them
    theYarrr.poll();
  
    fullinit(rootobj)
}