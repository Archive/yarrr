function ClosedCommentLog(comment, id) {
  //To avoid multiple tabs accessing yarrr using the same window, we add a random number to the id
  this.logWindow = window.open("", "log" + Math.random(), "width=450, height=350");
  this.comment = comment;

  var ccLog = this;	
  yarrr.getClosedCommentMessages(this.comment.id, function (result, err) { ccLog.getClosedCommentMessagesResult(result, err); });
}

ClosedCommentLog.prototype.getClosedCommentMessagesResult = function (result, err) {
  if (err) {
    errorLog.reportError('getClosedCommentMessages failed: ' + err);
    return;
  }
  try {
  
  var chatLog;
  chatLog = this.setupInitialText()
  for (var i = 0; i < result.length; i++) {
    var msg = result[i]
    Message.prototype.formatDate (msg, chatLog)
    Message.prototype.formatAuthor(msg, chatLog)
    Message.prototype.formatMessage(msg, chatLog)
    chatLog.appendChild(document.createElement("br"))
  }
  } catch (e) {
    errorLog.reportError(e)
  }
}

ClosedCommentLog.prototype.startCommentLookup  = function () {
  alert ("in startCommentLookup");
}

ClosedCommentLog.prototype.setupInitialText = function ()
{
  var document = this.logWindow.document;

  // Create the title
  document.title = ("Log")
  nodes = document.getElementsByTagName ("head");
  if (nodes.length == 1) {
    // FIXME: This pretty much sucks.  I can't get it to get mozilla
    // to load the .css file when you link to it.  I'm setting the style
    // here, based on what's in topic.css.  If you make changes there that
    // you want to propagate to the logs, you need to update this.
	var styleElement = document.createElement("style");
    styleElement.type = 'text/css';
	styleElement.appendChild (document.createTextNode (" \
span.chatauthorFromMe { font-weight: bold; color: #aa00aa;} \
span.chatauthor { font-weight: bold; margin-left: 2em; } \
span.chatauthorToMe { font-weight: bold; color: #ffaa00; } \
.logWindow { overflow: auto; height: 300px; border: 1px solid #f0f0f0; } \
span.chatcontent { margin-left: 2em; } \
span.chatdate { color: gray; } \
"));
    nodes[0].appendChild (styleElement);
  }

  // Create the Body
  var header = document.createElement ("h2");
  header.appendChild(document.createTextNode("Logs:"));
  document.body.appendChild (header);
  var chatLog = document.createElement ("div");
  chatLog.setAttribute ("class", "logWindow");
  document.body.appendChild (chatLog);

  return chatLog;
}

function ClosedComment(id, topicid) {
  if ( arguments.length > 0 )
    this.init (id, topicid)
}

ClosedComment.prototype = new ReferencableObject();

ClosedComment.prototype.init = function (id, topicid) {
  this.id = id;
  this.topicid = topicid;
 
  this.got_messages = false;
  var topicelt = window.document.getElementById("closedcomments");
  
  // Make a table to put the authors in
  var table = document.createElement ("table");
  table.setAttribute ("width", "100%");
  var tr = document.createElement ("tr");
  topicelt.insertBefore(table, topicelt.firstChild);
  table.appendChild (tr);

  // Set up the subscribers to the statement
  var td = document.createElement ("td");
  td.setAttribute ("align", "right");
  td.setAttribute ("valign", "top");
  td.setAttribute ("width", "50px");
  tr.appendChild (td);  
  this.authorCell = td;

  // Some padding between the authors and the content
  var td = document.createElement ("td");
  td.setAttribute ("width", "8px");
  tr.appendChild (td);  
  
  td = document.createElement ("td");
  td.setAttribute ("align", "left");
  td.setAttribute ("valign", "top");
  tr.appendChild (td);
  
  this.contentCell = td;
  
  this.contentelt = document.createElement("div");
  this.contentelt.setAttribute("class", "closedcomment");
  this.contentelt.setAttribute("id", id);

  this.commentdiv = document.createElement("div")
  this.commentdiv.appendChild(document.createTextNode("Loading..."))
  
  this.contentCell.appendChild(this.commentdiv); 

  var precached = stealPrecachedReferencableObject(this.id)
  if (precached != null)
    this.sync(precached)
  else
    this.update();
}

ClosedComment.prototype.addAction = function (id, text) {
  if (!this.actionsForm) {
    this.actionsForm = document.createElement("form");
    this.contentCell.appendChild(this.actionsForm);
  }
  
  var comment = this;
  
  var input = document.createElement("input");
  this.actionsForm.appendChild(input);
  input.setAttribute("type", "button");
  input.setAttribute("value", text);
  input.onclick = function () { comment.executeAction(id); return false; };
}

ClosedComment.prototype.executeAction = function (id, text) {
}

ClosedComment.prototype.sync = function (contents) {
  var comment = this;

  try {
  var canProponent = false;

  for (var i = 0; i < contents.authors.length; i++) {
    if (getYarrrPerson().getName() == contents.authors[i]) {
      canProponent = true;
    }
  }

  this.authorCell.innerHTML = "";

  for (var i = 0; i < contents.proponents.length; i++) {
    var span = document.createElement("span");
    span.setAttribute("class", "closedcommentproponent");
    span.appendChild(document.createTextNode(contents.proponents[i]));
    this.authorCell.appendChild(span);
    if (i < contents.proponents.length - 1) {
      this.authorCell.appendChild(document.createElement("br"));
    }
    if (getYarrrPerson().getName() == contents.proponents[i]) {
      canProponent = false;
    }
  }
  if (canProponent) {
    if (contents.proponents.length != 0) {
      this.authorCell.appendChild(document.createElement("br"));
    }
    var a = document.createElement("a");
    a.setAttribute("class", "chataction");
    a.setAttribute("href", "#");
    a.onclick = function () { comment.addProponent(getYarrrPerson().getId()); return false; };
    this.authorCell.appendChild(a);
  
    var img = document.createElement("img");
    img.setAttribute("src", getYarrrURL("images/subscribe_message.png"));
    a.appendChild(img);
  }

  var closedcomment_elt = this.commentdiv;
  closedcomment_elt.innerHTML = "";
  
  closedcomment_elt.appendChild(document.createTextNode("Closed by "));
  var span = document.createElement("span");
  span.setAttribute("class", "closedcommentcloser");
  closedcomment_elt.appendChild(span);
  span.appendChild(document.createTextNode(contents.closer));
  closedcomment_elt.appendChild(document.createTextNode(" - "));
  span = document.createElement("span");
  span.setAttribute("class", "closedcommentdate");
  var prettyDate = getPrettyDate(contents.date);
  closedcomment_elt.appendChild(document.createTextNode(prettyDate));
  
  var a = document.createElement("a")
  a.setAttribute("class", "ClosedCommentChatLogLink")
  a.setAttribute("href", "#")
  a.appendChild(document.createTextNode("Discussion (closed)"))
  a.onclick = function () { comment.showLogs(); return false; };
  closedcomment_elt.appendChild(a)
  
  closedcomment_elt.appendChild(document.createElement("br"));
  
  var div = document.createElement("pre");
  div.setAttribute("class", "closedcommentcontent");
  closedcomment_elt.appendChild(div);
  yarrrMarkupToHTML(contents.text, document, div);
  } catch (e) {
    errorLog.reportError('ClosedComment sync failed: ' + e)
  }
}

ClosedComment.prototype.showLogs = function () {
  // deal with closed windows:
  if (this.commentLog != null) {
	if (this.commentLog.logWindow.closed) {
      this.commentLog = null;
	}
  }
	
  if (this.commentLog != null) {
     // Bring window to top (and give kbd focus:
		this.commentLog.logWindow.focus();
	} else {
	  // Create new window:
	  this.commentLog = new ClosedCommentLog(this);
	}
}

ClosedComment.prototype.addProponentResult = function (result, err) {
  if (err != null) {
    errorLog.reportError('addProponent failed: ' + err)
  }
}

ClosedComment.prototype.addProponent = function (name) {
  var comment = this;
  yarrr.addProponent(this.id, name, function (result, err) { comment.addProponentResult(result, err); });
}

function EmbeddedClosedComment(id, topicid) {
  this.init(id, topicid);
  this.addAction("copytowiki", "Copy to wiki");
}

EmbeddedClosedComment.prototype = new ClosedComment();
EmbeddedClosedComment.constructor = EmbeddedClosedComment.constructor;
EmbeddedClosedComment.superclass = ClosedComment.prototype;

EmbeddedClosedComment.prototype.sync = function (contents) {
  EmbeddedClosedComment.superclass.sync.call(this, contents);

  this.text = yarrrMarkupToText(contents.text);
}

EmbeddedClosedComment.prototype.executeAction = function (id, text) {
  EmbeddedClosedComment.superclass.executeAction(this, id, text);
  
  if (id == "copytowiki") {
    this.copyToWiki();
  }
}

EmbeddedClosedComment.prototype.copyToWiki = function () {
  var urllib = importModule("urllib");
  
  var text = "\n\n== ==\n" + this.text;
  var data = "action=edit&append=1&text=" + encodeURI(text);
  urllib.postURL(wikiArticleEditURL, null, null, data,
  				 [["Content-Type", "application/x-www-form-urlencoded"]]);

  window.location.reload();
}
