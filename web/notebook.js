

Notebook.prototype._createTabs = function () {
  this.span = this.document.createElement("span");
  this.span.setAttribute("class", "notebookTabs")
  var thenotebook = this;
  var i;

  for (var i = 0; i < this.labels.length; i++) {
    var span = document.createElement ("span");
	span.setAttribute ("class", "notebookInactive");
    var tab = document.createElement("a");
    tab.page = i;
    tab.appendChild (document.createTextNode (this.labels[i]));
    tab.setAttribute("href", "#");
    tab.addEventListener("click", function (e) { e.stopPropagation(); thenotebook.setCurrentPage (this.page); return false; }, false);
   	this.span.appendChild (span);
   	span.appendChild (tab);

   	this.label_nodes[i] = span;
  }
}

Notebook.prototype.show = function () {
  this.span.style.display = 'inline';
}

Notebook.prototype.hide = function () {
  this.span.style.display = 'none';
}

function clearTabLabel (span) {
  for (var i = 0; i < span.childNodes.length; i++) {
    var childNode = span.childNodes[i];
    if (childNode.nodeName == "B") {
      span.replaceChild (childNode.firstChild, childNode);
	  span.setAttribute ("class", "notebookInactive");
    }
  }
}

function setTabLabelActive (document, span) {
  var bold = document.createElement ("b");
  var child = span.firstChild;
  span.replaceChild (bold, child);
  bold.appendChild(child);
  
  span.setAttribute ("class", "notebookActive");
}

Notebook.prototype._updateActive = function () {

  // Remove bold from all the labels
  for (var i = 0; i < this.label_nodes.length; i++) {
    clearTabLabel (this.label_nodes[i]);
  }

  // Set the active one to be bold 
  setTabLabelActive (this.document, this.label_nodes [this.current_page]);
  
  // Hide/show relevant pages
  for (var i = 0; i < this.contents.length; i++) {
    var page = this.contents[i];
    if (i == this.current_page)
      page.style.display  = 'block';
	else
      page.style.display = 'none';
  }
}

function Notebook (document, labels, contents) {

  if (labels.length != contents.length)
    throw new Error ("mismatch in label and contents lengths");

  this.document = document;
  this.labels = labels;
  this.contents = contents;

  this.label_nodes = [];
  this.current_page = 0;

  this._createTabs ();
  this._updateActive ();
}

Notebook.prototype.setCurrentPage = function (page) {
  if (this.current_page != page) {
    this.current_page = page;
    this._updateActive ();
  }
}

Notebook.prototype.getNode = function () {
  return this.span;
}
