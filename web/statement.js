function Statement(topicid, id, container) {
  this.id = id
  this.topicid = topicid
  this.container = container

  this.contentelt = document.createElement("div")
  this.contentelt.setAttribute("class", "statement")
  this.contentelt.setAttribute("id", id)
  container.appendChild(this.contentelt) 

  var precached = stealPrecachedReferencableObject(this.id)
  if (precached != null)
    this.sync(precached)
  else 
    this.update()
}

Statement.prototype = new ReferencableObject();

Statement.prototype.sync = function(contents) {
  var statement = this;
  var subscribed = false;
  var name = getYarrrPerson().getName();

  this.contentelt.innerHTML = "";

  // Make a table to put the subscribers in
  var table = document.createElement ("table");
  table.setAttribute ("width", "100%");
  var tr = document.createElement ("tr");
  this.contentelt.appendChild (table);
  table.appendChild (tr);

  // Set up the subscribers to the statement
  var td = document.createElement ("td");
  td.setAttribute ("align", "right");
  td.setAttribute ("valign", "top");
  td.setAttribute ("width", "50px");
  tr.appendChild (td);  
  for (var i = 0; i < contents.subscribers.length; i++) {
    if (i > 0)
      td.appendChild (document.createElement ("br"));

    var span = document.createElement ("span");
    if (contents.subscribers[i] == contents.author)
      span.setAttribute ("class", "statementAuthor");
    else
      span.setAttribute ("class", "statementProponent");
    td.appendChild (span);
    span.appendChild (document.createTextNode (contents.subscribers[i]));
    
    if (contents.subscribers[i] == name) {
      subscribed = true;
    }
  }

  // Add the statement itself
  td = document.createElement ("td");
  td.setAttribute ("align", "left");
  td.setAttribute ("valign", "top");
  tr.appendChild (td);
  var span = document.createElement ("span");
  span.setAttribute ("class", "statement");
  td.appendChild (span);
  span.appendChild (document.createTextNode(contents.content));


  // "Me too" or "Not me" button
  tr = document.createElement ("tr");
  td = document.createElement ("td");
  td.setAttribute ("align", "right");
  td.setAttribute ("colspan", "2");
  table.appendChild (tr);
  tr.appendChild (td);
  var form = document.createElement("form");
  var input = document.createElement("input");
  input.setAttribute("type", "button");
  if (getYarrrPerson().isAnonymous()) {
    input.setAttribute("disabled", "true")
  }

  if (!subscribed) {
    input.setAttribute("value", "Me too");
    input.onclick = function () { statement.subscribe(); return false; };
  } else {
    input.setAttribute("value", "Not me");
    input.onclick = function () { statement.unsubscribe(); return false; };
  }

  form.appendChild(input);
  td.appendChild(form);

  this.contentelt.appendChild(document.createElement ("br"));
}

Statement.prototype.remove = function() {
  this.container.removeChild(this.contentelt);
  this.contentelt = null;
  deregisterObject(this);
}

Statement.prototype.subscribe = function() {
  yarrr.subscribeStatement(this.topicid, this.id, function (res, err) {});
}

Statement.prototype.unsubscribe = function() {
  yarrr.unsubscribeStatement(this.topicid, this.id, function (res, err) {});
}
