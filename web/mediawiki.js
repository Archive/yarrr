var theYarrrDiscussion = null;

// Utility functions
function createYarrrWikiURI(page) {
  return wikiArticlePath + "?title=" + page;
}

function createYarrrWikiLink(page, text) {
  var a = document.createElement("a")
  a.setAttribute("href", createYarrrWikiURI(page))
  a.appendChild(document.createTextNode("login"))
  return a;
}

function updateTimestamp () {
  var urllib = importModule("urllib");
  urllib.getURL(createYarrrWikiURI(yarrrWikiTitle) + "&action=timestamp&live=true",
  				null, null, null,
  				function (request) {
  				  wikiArticleTimestamp = request.responseText;
  				});
}

function updateWikiContent() {
/*
  var urllib = importModule("urllib");
  urllib.getURL(createYarrrWikiURI(yarrrWikiTitle) + "&live=true", null, null, null,
  				function (request) {
  				  var articleContent = document.getElementById("articleContent");
  				  articleContent.innerHTML = request.responseText;
  				   
  				  // FIXME this is broken, because it will initialize more
  				  // than it should. No point in fixing it now though, since
  				  // we are going to move the chat outside the content.
  				  yarrrInit();
  				});
*/
}

// Global hook called from wikiembed.jsp
function yarrrWikiPreInit () {
  errorLog.reportEvent("yarrrWikPreiInit")
  /* Add the notification area */
  var bodyContent = document.getElementById("content")
  if (bodyContent) {
    var elt = document.createElement("div")
    elt.setAttribute("id", "yarrrNotificationArea")
    bodyContent.insertBefore(elt, bodyContent.firstChild)
  }
  
  var wikiHeader = document.getElementById("yarrrWikiHeader");
  if (wikiHeader) {
    var h1 = document.createElement("h1")
    h1.appendChild(document.createTextNode(yarrrWikiHeaderText))
    wikiHeader.appendChild(h1)
  }
  errorLog.reportEvent("yarrrWikPreiInit finished")  
}

// Global hook called from wikiembed.jsp
function yarrrWikiInit (rootobj) {
 errorLog.reportEvent("yarrrWikiInit")
 // Undo mediawiki damage
 wikiArticleEditURL = wikiArticleEditURL.replace(/&amp;/g, "&") 
 if (getYarrrPerson().isAnonymous()) {
    var wikiHeader = document.getElementById("yarrrWikiHeader");
    if (wikiHeader) {
      var div = document.createElement("div")
      div.setAttribute("class", "yarrrLoginNotification")
      div.appendChild(document.createTextNode("You must "))
      div.appendChild(createYarrrWikiLink("Special:Userlogin&returnto=" + yarrrWikiTitle, "login"))
      div.appendChild(document.createTextNode(" to use the discussion."))
      wikiHeader.appendChild(div)
    }
  }
  
  // Override mediawiki's logout link
  var logoutLi = document.getElementById("pt-logout");
  if (logoutLi) {
    logoutLi.addEventListener("click", function (e) { 
                                         e.preventDefault();
                                         e.stopPropagation();
                                         try {
                                           yarrr.logout(function (result, err) { 
                                               if (err) { errorLog.reportError(err); return; }; 
                                               document.location = createYarrrWikiURI("Special:Userlogout&returnto=" + yarrrWikiTitle);
                                             });
                                         } catch (e) {
                                           errorLog.reportError(e);
                                           alert('failed to invoke logout: '+e);
                                         }
                                         return true;
                                        }, false);
  }
 errorLog.reportEvent("yarrrWikiInit finished")  
}

// This is a separate function to correctly close over
// sectnum.  Yes, JavaScript sucks.
function yarrrWikiSetupSection(discussion, div, sectnum)
{
  var links = div.getElementsByTagName("a")
  var j;
  for (j = 0; j < links.length; j++) {
    var link = links[j]
    if (link.getAttribute("title") == yarrrWikiTitle) {
      var clickhandler;
      if (getYarrrPerson().isAnonymous()) {
        // FIXME use mediawiki's edit page - difficult to get from action=edit to article or vice versa             
        clickhandler = function () { alert("You must be logged in to edit the page"); };
      } else {
        div.sectionNumber = sectnum
        clickhandler = function () { discussion.handleSectionEdit (sectnum); };
      }
      link.addEventListener("click", function (e) { e.preventDefault(); e.stopPropagation(); clickhandler(); }, false);        
    } 
  }
}

// Global hook called from wikiembed.jsp
function yarrrWikiFullInit (topic) {
  try {
  errorLog.reportEvent("yarrrWikiFullInit") 
  var discussions = getObjectKeys(topic.discussions)
  if (discussions.length != 1) {
    errorLog.reportError("invalid number of discussions: " + discussions.length);
    return;
  }
  var discussion = topic.discussions[discussions[0]]
  var i;
  var divs = document.getElementsByTagName("div")
  var sectnum = 0
  for (i = 0; i < divs.length; i++) {
    var div = divs[i]
    if (div.getAttribute("class") == "editsection") {
      sectnum++
      yarrrWikiSetupSection(discussion, div, sectnum)
    }
  }
  errorLog.reportEvent("yarrrWikiFullInit finished")
  } catch (e) {
    errorLog.reportError("yarrrWikiFullInit failed: " + e);
  }
}

function EmbeddedComment(id, container) {
  if (arguments.length <= 0) {
    return;
  }
  this.init(id, container);  
}

EmbeddedComment.prototype = new Comment();
EmbeddedComment.prototype.constructor = EmbeddedComment
EmbeddedComment.superclass = Comment.prototype;

EmbeddedComment.prototype.init = function (id, container) {
  this.state = 'unknown'
  this.prevState = 'unknown'
  EmbeddedComment.superclass.init.call(this, id, container);
  this.closebutton.value = "Save";
  this.openedBy.data = "Editing started by ";
}

EmbeddedComment.prototype.sync = function (contents) {
  EmbeddedComment.superclass.sync.call(this, contents)
  
  var comment = this;
  if (("state" in contents) && this.state != contents.state) {
    this.prevState = this.state
    this.state = contents.state
    errorLog.reportEvent("comment " + this + " entered state \"" + this.state + "\" from \"" + this.prevState + "\"")
    if (contents.state == 'editing') {
      if (this.prevState == 'saving') {
        this.savingNotification.style.display = "none";
        this.editor.setEnabled(true)
      } else if (this.prevState == 'unknown' || this.prevState == 'save-complete') {
        this.rootDiv.style.display = "block";
        var data = theYarrrDiscussion.replaceSection(this.name, this.container);
        comment.wikiSectionNextSibling = data[0]
        comment.wikiSectionNodes = data[1]
        comment.wikiSectionParent = data[2]
      }
    } else if (contents.state == 'saving') {
      if (this.prevState == 'unknown') {
        this.rootDiv.style.display = "block";
        var data = theYarrrDiscussion.replaceSection(this.name, this.container);
        comment.wikiSectionNextSibling = data[0]
        comment.wikiSectionNodes = data[1]
        comment.wikiSectionParent = data[2]
      }
      this.savingNotification.style.display = "block";
      this.savingNotificationPerson.innerHTML = "";
      this.saver = contents.saver
      var span = document.createElement("span")
      span.className = "chatauthor";
      span.appendChild(document.createTextNode(this.saver))
      this.savingNotificationPerson.appendChild(span);
      this.editor.setEnabled(false)
    } else if (contents.state == 'save-complete') {
      this.rootDiv.style.display = "none";
      // If we had this comment open, we need to reload the section
      if (!this.uninitialized && this.prevState != 'unknown') {
         errorLog.reportEvent("reloading section");
         comment.needReload();
      }
    }  
  }  
}

function EmbeddedDiscussion(id, topic) {
  if (arguments.length <= 0) {
    return;
  }
  theYarrrDiscussion = this;
  this.init(id, topic)
}

EmbeddedDiscussion.prototype = new Discussion()
EmbeddedDiscussion.prototype.constructor = EmbeddedDiscussion
EmbeddedDiscussion.superclass = Discussion.prototype;

EmbeddedDiscussion.prototype.init = function (id, topic) {
  this.presentPersons = []
  EmbeddedDiscussion.superclass.init.call(this, id, topic)
}

EmbeddedDiscussion.prototype.createComment = function (commentId) {
  var dummy_div = document.createElement("div")
  dummy_div.setAttribute("class", "yarrrLiveSectionContainer")
 
  var comment = new EmbeddedComment(commentId, dummy_div);
  var discussion = this;
  
  comment.onremove = function () { var comment = this; discussion.handleLiveCommentRemoval(comment); }
  comment.onNeedReload = function() { discussion.handleLiveCommentReload(comment); }  

  return comment;
}

EmbeddedDiscussion.prototype.replaceSection = function (sectnum, node)
{
    var content = document.getElementById("articleContent")
    
    var isSection = function (div) { return div.className == "editsection" || div.className == "yarrrLiveSectionContainer"; }
    
    var divs = document.getElementsByTagName("div")
    var commentnum = 0
    var editsection = null
    for (var i = 0; i < divs.length; i++) {
      if (!isSection(divs[i]))
        continue;
       commentnum++
       if (commentnum == sectnum) {
         editsection = divs[i]
       } 
    }
    if (editsection == null) {
      errorLog.reportError("Couldn't find editsection number " + sectnum)
      return;
    }
        
    errorLog.reportEvent("replacing section " + sectnum)
    
    var removedNodes = []
    var parent = editsection.parentNode
    var anchor = editsection.nextSibling
    /* Take out the edit section */
    parent.removeChild(editsection)
    removedNodes.push(editsection)
    
    /* Stick the live comment where the first part of the section was */
    var elt = anchor.nextSibling
    parent.removeChild(anchor)
    removedNodes.push(anchor)
    var next = elt.nextSibling
    parent.replaceChild(node, elt)
    removedNodes.push(elt)
    /* Remove all other section components */
    elt = next
    while (elt && !isSection(elt) && elt.className != "printfooter") {
      next = elt.nextSibling
      parent.removeChild(elt)
      removedNodes.push(elt)
      elt = next
    }
    return [elt, removedNodes, parent]
}

EmbeddedDiscussion.prototype.handleSectionEdit = function (sectnum) {
  try {
  errorLog.reportEvent("requesting source for section " + sectnum);
  var discussion = this
  var url = createYarrrWikiURI(yarrrWikiTitle) + "&sectsource=true&live=true&section=" + sectnum         
  var urllib = importModule("urllib");
  urllib.getURL(url, null, null, null,
  				function (request) {
  				  try {
  				    errorLog.reportEvent("opening comment for section " + sectnum + " (" + request.responseText.length + " characters)");
  				    var text = request.responseText
  				    // Mediawiki gives us some leading whitespace, kill it
  				    while (text.match(/^\s+/)) {
  				      text = text.replace(/^\s*/, '')
  				    }
  				    discussion.openNamedComment("" + sectnum, text);
  				  } catch (e) {
  				    errorLog.reportError("Failed to load section source for live: " + e);
  				  }
  				});
  } catch (e) {
    errorLog.reportError("Failed to handle section edit " + e);
  }  				
}

EmbeddedDiscussion.prototype.defaultExpand = function () {
  // Do not expand default discussion
}

EmbeddedDiscussion.prototype.openNamedComment = function (name, contents) {
  var discussion = this;
  /* TODO - why do we have this timeout?  */
  setTimeout(function () {
    yarrr.openLiveComment(discussion.id, name, contents, function (result, err) {
      if (err) { errorLog.reportError('openLiveComment failed: ' + err); return; }
      discussion.openedCommentId = result;
    });
  }, 10);
}

EmbeddedDiscussion.prototype.notifyPresentPersons = function (people) {
  this.presentPersons = people;
  this.renderSummary();
}

EmbeddedDiscussion.prototype.renderSummary = function () {
  this.summary_span.innerHTML = ""; 
  this.summary_span.appendChild(document.createTextNode("Present People: "))
  name_list = "";
  for (var i = 0; i < this.presentPersons.length; i++) {
    if (i > 0)
      name_list += ", ";
    name_list += this.presentPersons [i];
  }
  this.summary_span.appendChild (document.createTextNode (name_list));  
}

EmbeddedDiscussion.prototype.handleLiveCommentCancel = function (commentid) {
  yarrr.deleteLiveComment(this.id, commentid, function (result, err) { if (err) { errorLog.reportError('deleteLiveComment failed: ' + err); }; })
}

EmbeddedDiscussion.prototype.handleLiveCommentReload = function (comment) {
  var discussion = this;
  var name = comment.name
  this.restoreCommentSection(comment)
  var urllib = importModule("urllib");
  var url = createYarrrWikiURI(yarrrWikiTitle) + "&live=true&section=" + name
  errorLog.reportEvent("reloading section content from " + url);   
  urllib.getURL(url, null, null, null,
  				function (request) {
  				  try {
  				    errorLog.reportEvent("setting content for section " + name + " (" + request.responseText.length + " characters)");
  				    var dummyDiv = document.createElement("div")
  				    dummyDiv.innerHTML = request.responseText;
  				    // Kill preceeding blank text
  				    while (dummyDiv.firstChild.nodeType == 3 && dummyDiv.firstChild.nodeValue.match(/^\s+$/))
  				       dummyDiv.removeChild(dummyDiv.firstChild)
  				    var data = discussion.replaceSection(name, dummyDiv)
  				  } catch (e) {
  				    errorLog.reportError("Failed to update section content: " + e);
  				  }
  				});
  updateTimestamp();
}

EmbeddedDiscussion.prototype.restoreCommentSection = function (comment) {
  errorLog.reportEvent("restoring section DOM for " +comment.name);
  var discussion = this
  for (var i = 0; i < comment.wikiSectionNodes.length; i++) {
    comment.wikiSectionParent.insertBefore(comment.wikiSectionNodes[i], comment.wikiSectionNextSibling)
  }
  comment.wikiSectionNodes = null
  comment.wikiSectionNextSibling = null
}

EmbeddedDiscussion.prototype.handleLiveCommentRemoval = function (comment) {
  if (comment.state != 'editing') {
    errorLog.reportEvent("ignoring removal for non-editing comment " + comment.name);
    return;
  }
  this.restoreCommentSection(comment)
}

EmbeddedDiscussion.prototype.onCommentClose = function (comment) {
  try {
  var editlocation = wikiArticleEditURL + "&section=" + comment.name
  var form = document.createElement("form")
  form.setAttribute("method", "post")
  form.setAttribute("action", editlocation)
  var summary = document.createElement("input")
  form.appendChild(summary)
  summary.setAttribute("name", "wpSummary")
  summary.setAttribute("type", "text")
  var tb1 = document.createElement("input")
  tb1.setAttribute("type", "textarea")
  tb1.setAttribute("name", "wpTextbox1")
  form.appendChild(tb1)
  var tb2 = document.createElement("input")
  tb2.setAttribute("type", "textarea")
  tb2.setAttribute("name", "wpTextbox2")
  form.appendChild(tb2)
  var tb3 = document.createElement("input")
  tb3.setAttribute("type", "textarea")
  tb3.setAttribute("name", "wpEdittime")
  form.appendChild(tb3)
  form.style.display = "none";
  this.topdiv.appendChild(form)
  
  tb1.value = comment.editor.getText()
  tb3.value = wikiArticleTimestamp;
  yarrr.liveCommentSaving(comment.id)  
  errorLog.reportEvent("submitting comment")
  form.submit()
  } catch (e) { 
    errorLog.reportError("Failed to close comment: " + e)
    return
  }
}

EmbeddedDiscussion.prototype.initOpenComment = function (discussiondiv) {
 /* Override */
}
