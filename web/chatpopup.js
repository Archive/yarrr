// Crazy popup stuff, written by Seth Nickell
// Mangled into objects by Colin Walters

function Popup (container, id, personName, text) {
  if (arguments.length > 0) {
    this.init(container, id, personName,text)
  }
}
  
Popup.prototype = new Object();
Popup.prototype.constructor = Object
Popup.superclass = Object.prototype;

Popup.prototype.init = function (container, id, personName, text) {
  this.container = container
  this.id = id
  this.div = this.createPopupDiv(personName, text);
}

Popup.prototype.deleteDiv = function () {
	this.div.parentNode.removeChild(this.div);
}

Popup.prototype.getY = function () {
	return this.Y;
}

Popup.prototype.setY = function (newY) {
	this.Y = newY;
	this.div.style.top = newY + 'px';
}

Popup.prototype.setOpacity = function (newOpacity) {
	this.div.style.opacity = newOpacity;
}

Popup.prototype.createPopupDiv = function (name, text) {
	var div = document.createElement("div");
	div.className = "yarrrTextPopup";	
	
	var nameNode = document.createTextNode(name + "    ");
	var nameSpan = document.createElement("span");
	nameSpan.className = "yarrrPopupName";
	
	nameSpan.appendChild(nameNode);
	div.appendChild(nameSpan);
	
	var textNode = document.createTextNode(text);
	var textSpan = document.createElement("span");
	textSpan.className = "yarrrPopupLine";
	
	textSpan.appendChild(textNode);
	div.appendChild(textSpan);
		
	// Add the text popup to the container
	errorLog.reportEvent ('div id |' + this.container.id + '|');
	this.container.appendChild(div);
	return div;
}

Popup.prototype.toString = function () {
  return "[Popup " + this.id + "]";
}

function PopupManager(container, basey, height, visible, expiration, speed) {
  if (arguments.length > 0) {
    this.init(container, basey, height, visible, expiration, speed) 
  }
}
  
PopupManager.prototype = new Object();
PopupManager.prototype.constructor = Object
PopupManager.superclass = Object.prototype;

PopupManager.prototype.init = function (container, basey, height, visible, expirationTargetY, speed) {
  this.container = container
  this.idSeq = 0
  // Can't init, we inherit from Object
  // PopupManager.superclass.init.call(this)
  if (!basey)
    basey = 40;
  this.baseY = basey
  this.minBaseY = basey
  if (!height)
    height = 40
  this.height = height
  if (!visible)
    visible = 2500
  this.visibleTime = visible
  if (!expirationTargetY)
    expirationTargetY = 10
  this.expirationTargetY = expirationTargetY
  if (!speed)
    speed = 30 // pixels/sec
  this.speed = speed
  
  this.popups = new Array(); 
  this.expiringPopups = new Array();
  this.animationIntervalID = null;
  this.animationInterval = 75;
}

PopupManager.prototype.createPopup = function (personName, text) {
	var popup = new Popup(this.container, this.idSeq, personName, text);
	this.idSeq++;
	popup.setY(this.baseY);
	this.baseY += this.height;
	popup.expirationTime = (new Date()).getTime() + this.visibleTime
	this.popups.push(popup);
}

PopupManager.prototype.startAnimation = function () {
	var manager = this;
	if (this.animationIntervalID == null) {
		this.lastTime = (new Date()).getTime();
		this.animationIntervalID = window.setInterval(function () { manager.animationTick() }, this.animationInterval);	
	}
}

PopupManager.prototype.stopAndClearPopups = function () {
  if (this.animationIntervalID != null) {
    window.clearInterval(this.animationIntervalID);
    this.animationIntervalID = null;
  }
  for (var i = 0; i < this.popups.length; i++) {
    this.popups[i].deleteDiv();
  }
  this.popups = []
}

// Expire any popups that are past their expiration time
PopupManager.prototype.expirePopups = function (time) {
		for (i=0; i < this.popups.length; i++) {
			if (time > this.popups[i].expirationTime) {
				// Remove this popup, and push it onto the expiring popups
				this.expiringPopups.push(this.popups[i]);	
				this.popups.splice(i, 1);		
			}
		}
}

PopupManager.prototype.animationTick = function () {
	var time = (new Date()).getTime();
	
	this.expirePopups(time);
	
	var elapsedSeconds = (time - this.lastTime) / 1000;
	this.lastTime = time;		
	var deltaY = this.speed * elapsedSeconds;
	// Move expiring popups
	for (var i = this.expiringPopups.length - 1; i >= 0; i--) {
		var newY = this.expiringPopups[i].getY() - deltaY;
		if (newY < this.expirationTargetY) {
			this.expiringPopups[i].deleteDiv();
			this.expiringPopups.splice(i, 1);
		} else {
			this.expiringPopups[i].setY(newY);
			this.expiringPopups[i].setOpacity(1.0 - (newY - this.minBaseY) / (this.expirationTargetY - this.minBaseY));
			this.baseY = newY + this.height;
		}
	}

	if (this.popups.length > 0) {
		this.baseY = this.popups[0].getY();
		for (var i = 0; i < this.popups.length; i++) {
			var targetY = this.minBaseY + (i * this.height);
			var newY = this.popups[i].getY() - deltaY;
			if (newY < targetY) {
				newY = targetY;
			}
			if (newY != this.popups[i].getY()) {
  			  this.popups[i].setY(newY);
  			}
			this.baseY = newY + this.height;
		}
	}
	
	if (this.baseY < this.minBaseY) {
		this.baseY = this.minBaseY;
	}
		
	if (this.popups.length <= 0 && this.expiringPopups.length <= 0) {
		window.clearInterval(this.animationIntervalID);
		this.animationIntervalID = null;
		return;
	}
}
