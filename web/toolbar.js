//------------------------------ Toolbar ------------------------------

function Toolbar() {
  this.initialize();
}

Toolbar.prototype = new Container();
Toolbar.prototype.constructor = Toolbar;
Toolbar.prototype.parentClass = Container;
Toolbar.prototype.signals = ["itemClicked"];

Toolbar.prototype.items = [];
Toolbar.prototype.orientation = 'horiz';

Toolbar.prototype.init = function () {
  this.items = new Array();
}

Toolbar.prototype._updateItem = function (item) {
  if (this.orientation == 'horiz') {
    item.node.style.display = "inline";
  } else {
    item.node.style.display = "block";
  }
}

Toolbar.prototype.setOrientation = function (orientation) {
  if (this.orientation == orientation)
    return;

  this.orientation = orientation;

  if (this.isRealized()) {
    if (this.orientation == 'horiz') {
      this.node.style.display = "inline";
    } else {
      this.node.style.display = "block";
    }
    for (var i = 0; i < this.items.length; i++) {
      this._updateItem(this.items[i]);
    }
  }
}

Toolbar.prototype.add = function (child) {
  if (!child.instanceOf(ToolItem))
    alert("Toolbars need ToolItem children");
    
  child.setParent(this);
  
  this.children.push(child);
  var item = new Object();
  item.widget = child;
  this.items.push(item);
  
  if (this.isRealized()) {
    child.realize();
    item.node = document.createElement("div");
    item.node.appendChild(child.node);
    this.node.appendChild(item.node);
    this._updateItem(item);
  }
}

Toolbar.prototype.realize = function () {
  this.node = document.createElement("div");
  if (this.orientation == 'horiz') {
    this.node.style.display = "inline";
  } else {
    this.node.style.display = "block";
  }
  this.node.style.backgroundColor = "menu";

  this.node.style.paddingTop = "2px";
  this.node.style.paddingBottom = "2px";
  //this.node.style.borderWidth = "1px";
  //this.node.style.borderStyle = "solid";
  //this.node.style.borderColor = "red";
    
  for (var i = 0; i < this.items.length; i++) {
    var item = this.items[i];
    if (!item.widget.isRealized()) {
      item.widget.realize();
      item.node = document.createElement("div");
      this._updateItem(item);
      item.node.appendChild(item.widget.node);
      this.node.appendChild(item.node);
    }
  }
  
}

//------------------------------ ToolItem ------------------------------

function ToolItem() {
  this.initialize();
}

ToolItem.prototype = new Widget();
ToolItem.prototype.constructor = ToolItem;
ToolItem.prototype.parentClass = Widget;
ToolItem.prototype.signals = [];

ToolItem.prototype.realize = function () {
  this.callParent(ToolItem, "realize");
  this.node.style.paddingLeft = "2px";
  this.node.style.paddingRight = "2px";
}

//------------------------------ ToolButton ------------------------------

function ToolButton() {
  this.initialize();
};

ToolButton.prototype = new ToolItem();
ToolButton.prototype.constructor = ToolButton;
ToolButton.prototype.parentClass = ToolItem;
ToolButton.prototype.signals = ["clicked"];

ToolButton.prototype.text = null;
ToolButton.prototype.imageSrc = null;

ToolButton.prototype.init = function () {
  this.buttondown = false;
}

ToolButton.prototype.updateImage = function () {
  if (this.imageSrc != null) {
    if (this.imgNode == null) {
      this.imgNode = document.createElement("img");
      this.imgNode.style.border = "0px";
      this.labelNode.insertBefore(this.imgNode, this.labelNode.firstChild);
    }
    this.imgNode.src = getWidgetImageFile(this.imageSrc);
  } else {
    if (this.imgNode != null) {
      this.labelNode.removeChild(this.imgNode);
      this.imgNode = null;
    }
  }
}

ToolButton.prototype.updateIconColor = function () {
  if (this.iconColor != null) {
    if (this.colorNode == null) {
      this.colorNode = document.createElement("img");
      this.colorNode.style.border = "0px";
      this.colorNode.width = 16;
      this.colorNode.height = 16;
      this.colorNode.src = getWidgetImageFile("empty.gif");

      var after = this.imageNode;
      insertAfter(this.labelNode, this.colorNode, after);
    }
    this.colorNode.style.backgroundColor = this.iconColor;
  } else {
    if (this.colorNode != null) {
      this.labelNode.removeChild(this.colorNode);
      this.colorNode = null;
    }
  }
}

ToolButton.prototype.updateText = function () {
  if (this.text != null) {
    if (this.textNode == null) {
      this.textNode = document.createTextNode(this.text);

      var after = this.imgNode;
      if (this.colorNode != null)
        after = this.colorNode;
      insertAfter(this.labelNode, this.textNode, after);
    } else {
      this.textNode.replaceData(0,this.textNode.length, this.text);
    }
  } else {
    if (this.textNode != null) {
      this.labelNode.removeChild(this.textNode);
      this.textNode = null;
    }
  }
}

ToolButton.prototype.getImage = function () {
  return this.imageSrc;
}

ToolButton.prototype.setImage = function (src) {
  this.imageSrc = src;
  if (this.isRealized()) {
    this.updateImage();
  }
}

ToolButton.prototype.getIconColor = function () {
  return this.iconColor;
}

ToolButton.prototype.setIconColor = function (color) {
  this.iconColor = color;
  if (this.isRealized()) {
    this.updateIconColor();
  }
}

ToolButton.prototype.getText = function () {
  return this.text;
}

ToolButton.prototype.setText = function (text) {
  this.text = text;
  if (this.isRealized()) {
    this.updateText();
  }
}

ToolButton.prototype.handleMouseOver = function () {
  if (this.buttondown)
    this.node.style.borderStyle = "inset";
  else
    this.node.style.borderStyle = "outset";
    
  this.node.style.backgroundColor = "#f0f0f0";
}

ToolButton.prototype.handleMouseOut = function () {
  this.node.style.borderStyle = "none";
  this.node.style.backgroundColor = null;
}

ToolButton.prototype.handleMouseDown = function () {
  this.buttondown = true;
  this.node.style.borderStyle = "inset";
}

ToolButton.prototype.handleMouseUp = function () {
  if (this.buttondown) {
    this.node.style.borderStyle = "outset";
    this.buttondown = false;
    this.click();
  }
}

ToolButton.prototype.click = function () {
    this.emit("clicked");
    if (this.parent != null)
      this.parent.emit("itemClicked", this);
}


ToolButton.prototype.realize = function () {
  this.callParent(ToolButton, "realize");
  this.node.style.display = "inline";
  this.node.style.borderWidth = "1px";
  this.node.style.borderStyle = "none";
  this.node.style.borderColor = "ButtonShadow";

  this.labelNode = document.createElement("a");
  var a= this.labelNode;
  a.setAttribute("href", "#");
  this.node.appendChild(a);
  a.onclick = function() { return false;};
  a.style.textDecoration = "none";
  a.style.color = "ButtonText";

  this.updateImage();
  this.updateIconColor();
  this.updateText();

  var button = this;
  a.onmouseover = function () { button.handleMouseOver() };
  a.onmouseout = function () { button.handleMouseOut() };
  a.onmousedown = function () { button.handleMouseDown() };
  a.onmouseup = function () { button.handleMouseUp() };
}

//------------------------------ ToolToggle ------------------------------

function ToolToggle() {
  this.initialize();
};

ToolToggle.prototype = new ToolButton();
ToolToggle.prototype.constructor = ToolToggle;
ToolToggle.prototype.parentClass = ToolButton;
ToolToggle.prototype.signals = ["toggled"];

ToolToggle.prototype.init = function () {
  this.active = false;
}


ToolToggle.prototype.setActive = function (active) {
  if (this.active == active)
    return;
    
  this.active = active;
  if (this.isRealized()) {
    if (this.active) 
      this.node.style.borderStyle = "inset";
    else
      this.node.style.borderStyle = "outset";

    if (this.active)
      this.node.style.backgroundColor = "#d0d0d0";
    else
      this.node.style.backgroundColor = "#e0e0e0";
  }
  
  this.emit("toggled");
}

ToolToggle.prototype.getActive = function () {
  return this.active;
}

ToolToggle.prototype.realize = function () {
  this.callParent(ToolToggle, "realize");
  if (this.active) 
    this.node.style.borderStyle = "inset";
  else
    this.node.style.borderStyle = "outset";

  if (this.active)
    this.node.style.backgroundColor = "#d0d0d0";
  else
    this.node.style.backgroundColor = "#e0e0e0";
}

ToolToggle.prototype.onclicked = function () {
  this.setActive(!this.active);
}

ToolToggle.prototype.handleMouseOver = function () {
  if (this.buttondown) {
    if (this.active)
      this.node.style.backgroundColor = "#e0e0e0";
    else
      this.node.style.backgroundColor = "#d0d0d0";
  } else {
    this.node.style.backgroundColor = "#f0f0f0";
  }
}

ToolToggle.prototype.handleMouseOut = function () {
  if (this.active)
    this.node.style.backgroundColor = "#d0d0d0";
  else
    this.node.style.backgroundColor = "#e0e0e0";
}

ToolToggle.prototype.handleMouseDown = function () {
  this.buttondown = true;

  if (this.active)
    this.node.style.backgroundColor = "#e0e0e0";
  else
    this.node.style.backgroundColor = "#d0d0d0";
}

ToolToggle.prototype.handleMouseUp = function () {
  if (this.buttondown) {
    this.buttondown = false;
    this.click();
  }
}

//------------------------------ ToolRadio ------------------------------

function ToolRadio() {
  this.initialize();
};

ToolRadio.prototype = new ToolButton();
ToolRadio.prototype.constructor = ToolRadio;
ToolRadio.prototype.parentClass = ToolButton;
ToolRadio.prototype.signals = ["toggled"];

ToolRadio.prototype.init = function () {
  this.group = null;
  this.active = false;
}

ToolRadio.prototype.setGroup = function (group) {
  this.group = group;
}

ToolRadio.prototype.findActive = function () {
  if (this.parent == null)
    return null;

   
  for (var i = 0; i < this.parent.children.length; i++) {
    var child = this.parent.children[i];

    if (child.instanceOf(ToolRadio) &&
        child.group == this.group &&
	child.active) {
      return child;
    }
  }
  
  return null;
}

ToolRadio.prototype.setParent = function (parent) {
  this.callParent(ToolRadio, "setParent", parent);
  var active = this.findActive();
  if (active == null)
    this.setActive(true);
}

ToolRadio.prototype.handleMouseOut = function () {
  if (this.active)
    this.node.style.borderStyle = "outset";
  else 
    this.node.style.borderStyle = "none";

  this.node.style.backgroundColor = null;
}

ToolRadio.prototype.onclicked = function () {
  this.setActive(true);
}

ToolRadio.prototype.setActive = function (active) {
  if (this.active == active)
    return;
    
  this.active = active;

  if (active) {
    for (var i = 0; i < this.parent.children.length; i++) {
      var child = this.parent.children[i];

      if (child != this &&
          child.instanceOf(ToolRadio) &&
          child.group == this.group &&
	  child.active) {
        child.setActive(false);
      }
    }
  }

  if (this.isRealized()) {
    if (this.active) 
      this.node.style.borderStyle = "outset";
    else
      this.node.style.borderStyle = "none";
  }
  
  this.emit("toggled");
}

ToolRadio.prototype.getActive = function () {
  return this.active;
}

ToolRadio.prototype.realize = function () {
  this.callParent(ToolRadio, "realize");
  if (this.active) 
    this.node.style.borderStyle = "outset";
  else
    this.node.style.borderStyle = "none";
}



//------------------------------ ToolDropdown ------------------------------

function ToolDropdown() {
  this.initialize();
};

ToolDropdown.prototype = new ToolButton();
ToolDropdown.prototype.constructor = ToolDropdown;
ToolDropdown.prototype.parentClass = ToolButton;
ToolDropdown.prototype.signals = ["showMenu", "itemActivated"];

ToolDropdown.prototype.init = function () {
  this.subtoolbar = new Toolbar();
  this.subtoolbar.setOrientation("vert");
  this.subtoolbar.connectMethod("itemClicked", this, this.itemClicked);
  this.droppedDown = false;
  this.followChildButton = false;
}


ToolDropdown.prototype.setFollowChildButton = function (follow) {
  this.followChildButton = follow;
}

ToolDropdown.prototype.getSubToolbar = function () {
  return this.subtoolbar;
}

ToolDropdown.prototype.realize = function () {
  this.callParent(ToolDropdown, "realize");
  var dropImg = document.createElement("img");
  dropImg.src = getWidgetImageFile("dropdown.png");
  dropImg.style.border = "0px";
  
  this.labelNode.appendChild(dropImg);

  this.subtoolbar.realize();
  this.dropdown = document.createElement("div");
  this.dropdown.style.position = "absolute";
  this.dropdown.style.display = "none";
  
  this.dropdown.appendChild(this.subtoolbar.node);
  
  this.node.appendChild(this.dropdown);
  
}

ToolDropdown.prototype.itemClicked = function (dropdown, item) {
  this.setDropDown(false);
  this.emit("itemActivated", item);
}

ToolDropdown.prototype.onitemActivated = function (item) {
  if (this.followChildButton && item.instanceOf(ToolButton)) {
    this.setImage(item.getImage());
    this.setIconColor(item.getIconColor());
    this.setText(item.getText());
  }
}

ToolDropdown.prototype.setDropDown = function (down) {
  if (this.droppedDown == down)
    return;

  this.droppedDown = down;
    
  if (this.droppedDown) {
    var pos = getAbsolutePos(this.node);
    if (this.parent.orientation == 'horiz') {
      this.subtoolbar.setOrientation("vert");
      pos.y += this.node.offsetHeight;
    } else {
      this.subtoolbar.setOrientation("horiz");
      pos.x += this.node.offsetWidth;
    }
  
    this.dropdown.style.left = pos.x + "px";
    this.dropdown.style.top = pos.y + "px";   
    this.dropdown.style.display = "block";
  } else {
    this.dropdown.style.display = "none";
  }
  this.emit("showMenu");
}

ToolDropdown.prototype.onclicked = function () {
  this.setDropDown(!this.droppedDown);
}
