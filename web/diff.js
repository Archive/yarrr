/*
 * Based on diff.c found at http://www.ioplex.com/~miallen/libmba/dl/src/diff.c
 * with the following copyright notice:
 *
 *
 * diff - compute a shortest edit script (SES) given two sequences
 * Copyright (c) 2004 Michael B. Allen <mba2000 ioplex.com>
 *
 * The MIT License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

/* This algorithm is basically Myers' solution to SES/LCS with
 * the Hirschberg linear space refinement as described in the
 * following publication:
 *
 *   E. Myers, ``An O(ND) Difference Algorithm and Its Variations,''
 *   Algorithmica 1, 2 (1986), 251-266.
 *   http://www.cs.arizona.edu/people/gene/PAPERS/diff.ps
 *
 * This is the same algorithm used by GNU diff(1).
 */

var DIFF_MATCH = 1;
var DIFF_DELETE = 2;
var DIFF_INSERT = 3;

function _setv(ctx, k, r, val)
{
    var j;
    
    /* Pack -N to N into 0 to N * 2 */
    j = (k <= 0) ? -k * 4 + r : k * 4 + (r - 2);
    
    ctx.buf[j] = val;
}

function _v(ctx, k, r)
{
    var j;
    
    j = (k <= 0) ? -k * 4 + r : k * 4 + (r - 2);
    
    return ctx.buf[j];
}

function FV(ctx, k)
{
    return  _v(ctx, k, 0);
}

function RV(ctx, k)
{
    return _v(ctx, k, 1);
}


function _find_middle_snake(a, aTags, aoff, n,
			    b, bTags, boff, m,
			    ctx, ms)
{
    var delta, odd, mid, d;
    
    delta = n - m;
    odd = delta & 1;
    mid = (n + m) / 2;
    mid += odd;
    
    _setv(ctx, 1, 0, 0);
    _setv(ctx, delta - 1, 1, n);
    
    for (d = 0; d <= mid; d++) {
	var k, x, y;
	
	for (k = d; k >= -d; k -= 2) {
	    if (k == -d || (k != d && FV(ctx, k - 1) < FV(ctx, k + 1))) {
		x = FV(ctx, k + 1);
	    } else {
		x = FV(ctx, k - 1) + 1;
	    }
	    y = x - k;
	    
	    ms.x = x;
	    ms.y = y;
	    
	    while (x < n && y < m && a[aoff+x] == b[boff+y] && aTags[aoff+x] == bTags[boff+y]) {
		x++; y++;
	    }
	    _setv(ctx, k, 0, x);
	    
	    if (odd && k >= (delta - (d - 1)) && k <= (delta + (d - 1))) {
		if (x >= RV(ctx, k)) {
		    ms.u = x;
		    ms.v = y;
		    return 2 * d - 1;
		}
	    }
	}
	for (k = d; k >= -d; k -= 2) {
	    var kr = (n - m) + k;
	    
	    if (k == d || (k != -d && RV(ctx, kr - 1) < RV(ctx, kr + 1))) {
		x = RV(ctx, kr - 1);
	    } else {
		x = RV(ctx, kr + 1) - 1;
	    }
	    y = x - kr;
	    
	    ms.u = x;
	    ms.v = y;
	    while (x > 0 && y > 0 && a[aoff + x - 1] == b[boff + y - 1] && aTags[aoff + x - 1] == bTags[boff + y - 1]) {
		x--; y--;
	    }
	    _setv(ctx, kr, 1, x);
	    
	    if (!odd && kr >= -d && kr <= d) {
		if (x <= FV(ctx, kr)) {
		    ms.x = x;
		    ms.y = y;
		    return 2 * d;
		}
	    }
	}
    }
    
    return -1;
}

function script_add_op(ctx, op, off, len)
{
    var e;
    
    if (len == 0) {
	return;
    }
    
    /* Add an edit to the SES (or
     * coalesce if the op is the same)
     */
    e = ctx.ses[ctx.si];
    if (e == undefined || e.op != op) {
	if (e != undefined) {
	    ctx.si++;
	}
	e = new Object();
	ctx.ses[ctx.si] = e;
	e.op = op;
	e.off = off;
	e.len = len;
    } else {
	e.len += len;
    }
}

function _ses(a, aTags, aoff, n,
	      b, bTags, boff, m,
	      ctx)
{
    var d;
    
    if (n == 0) {
	script_add_op(ctx, DIFF_INSERT, boff, m);
	d = m;
    } else if (m == 0) {
	script_add_op(ctx, DIFF_DELETE, aoff, n);
	d = n;
    } else {
	/* Find the middle "snake" around which we
	 * recursively solve the sub-problems.
	 */
	var ms = new Object();
	d = _find_middle_snake(a, aTags, aoff, n, b, bTags, boff, m, ctx, ms);
	if (d == -1) {
	    return -1;
	} else if (d > 1) {
	    if (_ses(a, aTags, aoff, ms.x, b, bTags, boff, ms.y, ctx) == -1) {
		return -1;
	    }
	    
	    script_add_op(ctx, DIFF_MATCH, aoff + ms.x, ms.u - ms.x);
	    
	    aoff += ms.u;
	    boff += ms.v;
	    n -= ms.u;
	    m -= ms.v;
	    if (_ses(a, aTags, aoff, n, b, bTags, boff, m, ctx) == -1) {
		return -1;
	    }
	} else {
	    var x = ms.x;
	    var u = ms.u;
	    
	    /* There are only 4 base cases when the
	     * edit distance is 1.
	     *
	     * n > m   m > n
	     *
	     *   -       |
	     *    \       \    x != u
	     *     \       \
	     *
	     *   \       \
	     *    \       \    x == u
	     *     -       |
	     */
	    
	    if (m > n) {
		if (x == u) {
		    script_add_op(ctx, DIFF_MATCH, aoff, n);
		    script_add_op(ctx, DIFF_INSERT, boff + (m - 1), 1);
		} else {
		    script_add_op(ctx, DIFF_INSERT, boff, 1);
		    script_add_op(ctx, DIFF_MATCH, aoff, n);
		}
	    } else {
		if (x == u) {
		    script_add_op(ctx, DIFF_MATCH, aoff, m);
		    script_add_op(ctx, DIFF_DELETE, aoff + (n - 1), 1);
		} else {
		    script_add_op(ctx, DIFF_DELETE, aoff, 1);
		    script_add_op(ctx, DIFF_MATCH, aoff + 1, m);
		}
	    }
	}
    }
    
    return d;
}

function generate_script(a, aTags, b, bTags) {
    ctx = new Object();
    
    var d, x, y;
    
    ctx.buf = new Array();
    // ses == shortest edit script
    ctx.ses = new Array();
    // the current index in ses
    ctx.si = 0;
	
    var n = a.length;
    var m = b.length;
    
    /* The _ses function assumes the SES will begin or end with a delete
     * or insert. The following will insure this is true by eating any
     * beginning matches. This is also a quick to process sequences
     * that match entirely.
     */
    x = y = 0;
    while (x < n && y < m && a[x] == b[x] && aTags[x] == bTags[x]) {
	x++; y++;
    }
    script_add_op(ctx, DIFF_MATCH, 0, x);
    
    if ((d = _ses(a, aTags, x, n - x, b, bTags, y, m - y, ctx)) == -1) {
	return null;
    }
    
    return ctx.ses;
}

function create_hunk(a, aTags, b, bTags, script, start, end, context_len)
{
    var hunk = new Object();

    hunk.parts = [];

    /* if needed and possible, extend range to get some MATCH context */
    if (start > 0 && script[start].op != DIFF_MATCH)
	start--;
    if (end < script.length && script[end-1].op != DIFF_MATCH)
	end++;

    for (var i = start; i < end; i++) {
	var edit, off, len, source, sourceTags, part;
	edit = script[i];
	
	part = new Object();
	part.op = edit.op;

	off = edit.off;
	len = edit.len;

	if (edit.op == DIFF_MATCH ||
	    edit.op == DIFF_DELETE) {
	    source = a;
	    sourceTags = aTags;
	} else {
	    source = b;
	    sourceTags = bTags;
	}

	if (i == start) {
	    if (edit.op == DIFF_MATCH && edit.len > context_len) {
		off += (len - context_len);
		len = context_len;
	    }
	    hunk.offset = off;
	}
	if (i == end - 1 && edit.op == DIFF_MATCH && edit.len > context_len) {
	    len = context_len;
	}

	part.data = source.substring(off, off+len);
	if (sourceTags != null)
	    part.tags = sourceTags.slice(off, off+len);

	hunk.parts[hunk.parts.length] = part;
    }

    return hunk;
}

function diff_to_string(hunks) 
{
    var s = "";
    for (var i = 0; i < hunks.length; i++) {
        var hunk = hunks[i];
    	s += "[" + hunk.offset + ",";
    	for (var j = 0; j < hunk.parts.length; j++) {
    	    var part = hunk.parts[j];
    	    if (part.op == DIFF_DELETE)
    	       s += "-";
    	    if (part.op == DIFF_INSERT)
    	       s += "+";
    	    s += part.data;
    	    if (part.tags != null)
    	       s += "|" + part.tags;
    	    if (part.op == DIFF_DELETE)
    	       s += "-";
    	    if (part.op == DIFF_INSERT)
    	       s += "+";
    	}
    	s += "]";
    	if (i != hunks.length - 1)
    	  s += ",";
    }
    return s;
}

function create_context_diff(a, aTags, b, bTags, script, context_len)
{
    var start, end;

    var hunks = [];

    if (script.length == 0)
	return hunks;
    
    start = 0;

    while (start < script.length) {
	/* Find first non-match or short match */
	while (script[start].op == DIFF_MATCH &&
	       script[start].len > context_len) {
	    start++;
	    if (start >= script.length)
		break;
	}

	end = start + 1;

	while (end < script.length &&
	       (script[end].op != DIFF_MATCH ||
		script[end].len <= context_len)) {
	    end++;
	}


	if (start < script.length &&
	    !(end == start + 1 && script[start].op == DIFF_MATCH))
	    hunks[hunks.length] = create_hunk(a, aTags, b, bTags, script, start, end, context_len);
	
	start = end + 1;
    }

    return hunks;
}

function diff_strings(a, b)
{
    var script = generate_script(a, [], b, []);
    return create_context_diff(a, null, b, null, script, 5);
}

function diff_tagged_strings(a, aTag, b, bTag)
{
    var script = generate_script(a, aTag, b, bTag);
    return create_context_diff(a, aTag, b, bTag, script, 5);
}

function tags_match(aTags, bTags) 
{
    for (var i = 0; i < aTags; i++) {
        if (aTags[i] != bTags[i])
            return false;
    }
    return true;
}

function hunk_matches(str, tags, start, hunk, pos)
{
    var parts = hunk.parts;
    
    // If first part wasn't a MATCH, then we need to match at the start
    if (parts[0].op != DIFF_MATCH && pos != start) {
    	return false;
    }
    
    for (var i = 0; i < parts.length; i++) {
	var part = parts[i];
	if (part.op == DIFF_MATCH || part.op == DIFF_DELETE) {
	    var matchstr = part.data;
	    if (matchstr != str.substring(pos, pos + matchstr.length))
		return false;
            if (tags != null && part.tags != null) {
 	    	var matchtags = part.tags;
		if (!tags_match(matchtags, tags.slice(pos, pos + matchtags.length)))
		  return false;
	    }
	    pos += matchstr.length;
	}
    }
    // If last part wasn't a MATCH, then we need to match at the end
    if (parts[parts.length-1].op != DIFF_MATCH && pos != str.length) {
    	return false;
    }
    return true;
}

/* Try to find a place where hunk applies.
 */
function find_matching_position(ctx, hunk)
{
    // Check if hunk offset is right
    var guess = hunk.offset + ctx.apply_offset;
    
    // Never look before start
    if (guess < ctx.start)
	guess = ctx.start;
    
    if (hunk_matches(ctx.input, ctx.tags, ctx.start, hunk, guess))
	return guess;

    var match_string;
    
    if (hunk.parts[0].op != DIFF_MATCH) {
	/* no context before, must be at start of string */
	if (hunk_matches(ctx.input, ctx.tags, ctx.start, hunk, ctx.start))
	    return 0;
	return -1;
    }
    match_string = hunk.parts[0].data;
    
    var before, after, middle, update_before, update_after;

    middle = guess;
    before = after = middle;
    update_before = update_after = true;
    do {
	if (update_after) {
	    /* Search next forward */
	    if (after == -1 || after >= ctx.input.length - 1)
		after = -1;
	    else
		after = ctx.input.indexOf(match_string, after + 1);
	}

	if (update_before) {
	    /* Search next backward */
	    if (before == -1 || before <= ctx.start)
		before = -1;
	    else {
		before = ctx.input.lastIndexOf(match_string, before - 1);
		if (before < ctx.start)
		    before = -1;
	    }
	}

	if (after != -1 && before != -1) {
	    if (middle - before < after - middle) {
		/* Try before */
		if (hunk_matches(ctx.input, ctx.tags, ctx.start, hunk, before))
		    return before;
		update_before = true;
		update_after = false;
	    } else {
		if (hunk_matches(ctx.input, ctx.tags, ctx.start, hunk, after))
		    return after;
		update_before = false;
		update_after = true;
	    }
	} else if (after != -1) {
	    if (hunk_matches(ctx.input, ctx.tags, ctx.start, hunk, after))
		return after;
	    update_before = false;
	    update_after = true;
	} else if (before != -1) {
	    if (hunk_matches(ctx.input, ctx.tags, ctx.start, hunk, before))
		return before;
	    update_before = true;
	    update_after = false;
	}
    } while (before != -1 ||  after != -1);
    
    return -1;
}

function apply_hunk(ctx, hunk, selection)
{
    var parts = hunk.parts;
    for (var i = 0; i < parts.length; i++) {
	var part = parts[i];
	var matchstr = part.data;
	if (part.op == DIFF_INSERT) {
	    if (selection != null) {
	       if (selection.start >= ctx.start)
	         selection.start += matchstr.length;
	       if (selection.end >= ctx.start)
	         selection.end += matchstr.length;
	    }
	    ctx.output += matchstr;
	    if (ctx.tags != null) {
	        if (part.tags != null)
  	    	  ctx.outputTags = ctx.outputTags.concat(part.tags);
  	    	else {
  	    	  // No tags in hunk, assume zero
  	    	  var tags = new Array(matchstr.length);
  	    	  for (var j = 0; j < matchstr.length; j++)
  	    	    tags[j] = 0;
  	    	  ctx.outputTags = ctx.outputTags.concat(tags);
  	        }
 	    }
	} else if (part.op == DIFF_MATCH) {
	    ctx.output += matchstr;
	    if (ctx.tags != null) 
 	    	ctx.outputTags = ctx.outputTags.concat(ctx.tags.slice(ctx.start, ctx.start + matchstr.length));
	    ctx.start += matchstr.length;
	} else {
	    /* DELETE */
	    if (selection != null) {
	       if (selection.start > ctx.start) {
	         var len = matchstr.length;
	         if (len > selection.start - ctx.start)
	           len = selection.start - ctx.start;
	         selection.start -= len;
	       }
	       if (selection.end > ctx.start) {
	         var len = matchstr.length;
	         if (len > selection.end - ctx.start)
	           len = selection.end - ctx.start;
	         selection.end -= len;
	       }
	    }
	    ctx.start += matchstr.length;
	}
    }
    return true;
}

function patch_string(str, diff, selection)
{
  var res = patch_tagged_string(str, null, diff, selection);
  if (res != null) 
    return res.text;
  return null;
}

function patch_tagged_string(str, tags, diff, selection)
{
    var ctx = new Object();
    ctx.input = str;
    ctx.tags = tags;
    ctx.apply_offset = 0;
    ctx.start = 0;
    ctx.output = "";
    if (tags != null) 
      ctx.outputTags = new Array();
    
    var orig_selection_start, orig_selection_end;
    if (selection != null) {
    	orig_selection_start = selection.start;
    	orig_selection_end = selection.end;
    } 

    for (var i = 0; i < diff.length; i++) {
	var hunk = diff[i];

	var pos = find_matching_position(ctx, hunk);
	if (pos == -1) {
	    // failed to patch, reset selection
	    if (selection != null) {
	    	selection.start = orig_selection_start;
	    	selection.end = orig_selection_end;
	    }
	    return null;
	}
	ctx.apply_offset = pos - hunk.offset;

	ctx.output += str.substring(ctx.start, pos);
	if (tags != null) 
 	    ctx.outputTags = ctx.outputTags.concat(tags.slice(ctx.start, pos));
 	    
	ctx.start = pos;
	apply_hunk(ctx, hunk, selection);
    }

    ctx.output += str.substring(ctx.start);
    if (tags != null) 
 	ctx.outputTags = ctx.outputTags.concat(tags.slice(ctx.start));
    
    var res = new Object();
    res.text = ctx.output;
    if (tags != null) {
      res.tags = ctx.outputTags;
    }
    return res;
}

function expand_tag_string(str) {
  expanded = new Array();
  ranges = str.split(",")
  var i, j, n;

  n = 0;
  for (i=0; i < ranges.length; i++) {
    range = ranges[i].split(":")
    tag = parseInt(range[0]);
    num = parseInt(range[1]);
    for (j=0; j < num; j++) {
      expanded[n++] = tag;
    }
  }
  return expanded;    
}

function expand_diff_tags(diff) {
  for (var i = 0; i < diff.length; i++) {
    var hunk = diff[i];
    for (var j = 0; j < hunk.parts.length; j++) {
      var part = hunk.parts[j];
    	    
      if (part.tags != null) {
        part.tags = expand_tag_string(part.tags);
      }
    }
  }
}