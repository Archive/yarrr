function Discussion(id, topic) {
  if (arguments.length > 0) {
    this.init(id, topic) 
  }
}
  
Discussion.prototype = new ReferencableObject();
Discussion.prototype.constructor = Discussion
Discussion.superclass = ReferencableObject.prototype;

Discussion.prototype.init = function (id, topic) {
  Discussion.superclass.init.call(this, id)
  this.livecomments = {};
  this.whiteboards = {};
  this.chat = null;
  var discussion = this;
  this.initialized = false;
  this.topic = topic;
  if (topic)
    this.onUnread = topic.onUnread;

  // Create toplevel div to hold expander, comments, and chat
  var discussions = window.document.getElementById("discussions");
  var elt = document.createElement("div")
  this.discussions = discussions
  this.topdiv = elt
  elt.setAttribute("id", id)
  elt.setAttribute("class", "discussion")
  discussions.appendChild(elt)

  // Popup manager
  var chatpopups = window.document.getElementById("chatpopups");
  this.popupManager = new PopupManager(chatpopups);

  // Create Discussion header
  var discussiondiv = document.createElement ("div");
  discussiondiv.setAttribute("class", "discussionHeader")

  // Create expander
  this.expander = document.createElement("img");
  discussiondiv.appendChild(this.expander);
  // Now we just set up a handler for the whole div, don't want users to have
  // to target the tiny expander
  // this.expander.onclick = function () { discussion.expand() };
  discussiondiv.addEventListener("click", function (e) { e.stopPropagation(); discussion.expand() }, false);
  var accessKeyExpander = document.createElement("form")
  var input = document.createElement("input")
  input.setAttribute("id", "yarrrAccessKeyExpander")
  input.setAttribute("type", "button")
  accessKeyExpander.appendChild(input)
  accessKeyExpander.onsubmit = function () { discussion.expand(); };
  accessKeyExpander.style.display = "none";
  var label = document.createElement("label")
  label.setAttribute("accesskey", "d")
  label.setAttribute("for", "yarrrAccessKeyExpander")
  discussiondiv.appendChild(accessKeyExpander)
  discussiondiv.appendChild(label)  
  
  // Create "Discussion" text
  var span = document.createElement("span");
  span.setAttribute("class", "discussionTitleMain");
  span.appendChild(document.createTextNode("Discussion"));
  discussiondiv.appendChild(span)

  // Add number of open comments
  span = document.createElement("span");
  span.setAttribute("class", "discussionTitleInfo");
  span.appendChild(document.createTextNode(", "))
  discussiondiv.appendChild(span)
  this.summary_span = document.createElement("span")
  this.summary_span.setAttribute("class", "discussionTitleSummary")
  discussiondiv.appendChild(this.summary_span);

  this.initOpenComment(discussiondiv);
  
  // Create title form
  this.title_form_div = document.createElement("div")
  this.title_form = document.createElement("form")
  this.title_form.setAttribute("class", "discussionTitleForm");
  this.title_input = document.createElement("input")
  this.title_input.setAttribute("type", "text")
  this.title_form.onsubmit = function() {
  	yarrr.setDiscussionTitle(discussion.id, discussion.title_input.value);
  	discussion.title_input.value = "";
  	return false;
  };
  this.title_form.appendChild(this.title_input)
  this.title_form_div.appendChild(this.title_form)
  this.title_form_div.style.display = "none";
  discussiondiv.appendChild(this.title_form_div)

  // Add the Notebook
  var whiteboardsPane = document.createElement("div")
  this.chat_div = document.createElement("div")
  this.notebook = new Notebook (document, ['Chats', 'Whiteboards'], [this.chat_div, whiteboardsPane]);
  discussiondiv.appendChild (this.notebook.getNode ());

  // Add the discussiondiv
  this.topdiv.appendChild(discussiondiv);
  
  // Create "contents" div to hold comments, chat, and open button
  this.contents_div = document.createElement("div")
  this.contents_div.setAttribute("class", "discussionContents")
  this.topdiv.appendChild(this.contents_div)
  
  whiteboardsPane.setAttribute("class", "whiteboardsPane");  
  this.contents_div.appendChild(whiteboardsPane);
  
  var formElement = appendElement(whiteboardsPane, "form");
  var input = document.createElement("input")
  input.setAttribute("type", "button")
  input.setAttribute("value", "Create Whiteboard")
  input.onclick = function() { discussion.createWhiteboard(); };
  if (getYarrrPerson().isAnonymous()) {
    input.setAttribute("disabled", "true")
  }
  formElement.appendChild(input);
  whiteboardsPane.appendChild(formElement);
  
  this.whiteboards_list = appendElement(whiteboardsPane, "div")
  this.whiteboards_list.setAttribute("class", "discussionWhiteboards");  
  
  formElement = appendElement(whiteboardsPane, "form");
  formElement.setAttribute("method", "post");
  formElement.setAttribute("enctype", "multipart/form-data");
  formElement.setAttribute("action", "upload-whiteboard-image");  
 
  var inputElement1 = appendElement(formElement, "input");
  inputElement1.setAttribute("type", "file");
  inputElement1.setAttribute("accept", "image/png,image/jpeg");
  inputElement1.setAttribute("name", "image-content");
  appendElement(formElement, "br");
  var inputElement2 = appendElement(formElement, "input");
  inputElement2.setAttribute("type", "submit");
  inputElement2.setAttribute("value", "Upload Image");
  if (getYarrrPerson().isAnonymous()) {
    inputElement2.setAttribute("disabled", "true")
  }
  var inputElement3 = appendElement(formElement, "input");
  inputElement3.setAttribute("type", "hidden");
  inputElement3.setAttribute("name", "discussionId");
  inputElement3.setAttribute("value", this.getId());
  
  // Create chat div, append to contents
  this.chat_div.setAttribute("class", "discussionChat");
  this.contents_div.appendChild(this.chat_div)
  
  // Create comments div, append to elt
  this.comments_div = document.createElement("div")
  this.comments_div.setAttribute("class", "discussionComments")
  this.topdiv.appendChild(this.comments_div)
  
  this.expanded = true; // Will be inverted by expand
  this.expand();
  
  Discussion.superclass.postInit.call(this)
}

Discussion.prototype.initOpenComment = function (discussiondiv) {
  // Create "Open Comment" button
  this.closedCommentButton = document.createElement("form");
  this.closedCommentButton.setAttribute("class", "closedcommentbutton")
  var input = document.createElement("input");
  input.setAttribute("type", "button");
  input.setAttribute("value", "New Comment");
  if (getYarrrPerson().isAnonymous()) {
    input.setAttribute("disabled", "true")
  }
  this.closedCommentButton.appendChild(input);
  discussiondiv.appendChild(this.closedCommentButton)
  input.addEventListener("click", function (e) { try { discussion.openComment(""); e.preventDefault(); e.stopPropagation(); return false; } catch (e) { errorLog.reportError(e); } }, false);  
}

Discussion.prototype.getTopic = function () {
  return this.topic;
}

Discussion.prototype.getComment = function () {
  return this.firstComment;
}

Discussion.prototype.expand = function () {
  var expander = this.expander;
  var expandee = this.contents_div;
  var discussions = this.discussions;
  
  this.expanded = !this.expanded;
  
  if (this.expanded) {
    this.expander.src = getYarrrURL("images/expanded.png");
    expandee.style.display = "block";
    if (this.closedCommentButton) {
      this.closedCommentButton.style.display = "inline";
    }
    this.notebook.show ();
    if (this.chat) {
      this.chat.focus();
    }
  } else {
    this.expander.src = getYarrrURL("images/collapsed.png");
    if (this.closedCommentButton) {
      this.closedCommentButton.style.display = "none";
    }
    expandee.style.display = "none";
    this.notebook.hide ();
    this.popupManager.stopAndClearPopups();
  }
}

Discussion.prototype.openComment = function (contents) {
  var discussion = this;
  setTimeout(function () {
    yarrr.openLiveComment(discussion.id, "", contents, function (result, err) {
      if (err) { errorLog.reportError('openLiveComment failed: ' + err); return; }
      discussion.openedCommentId = result;
    });
  }, 10);
}

Discussion.prototype.getPresentPersonNames = function () {
  if (this.topic)
    return this.topic.getPresentPersonNames();
  else
    return []
}

Discussion.prototype.onCommentClose = function(comment) {
}

Discussion.prototype.createComment = function (commentId)
{
  return new Comment(commentId, this.comments_div);
}

Discussion.prototype.handleMessageAdded = function (message) {
  if (this.expanded)
    return;
  this.popupManager.createPopup(message.authorNick, message.text)
  this.popupManager.startAnimation();
}

Discussion.prototype.sync = function(contents) {
  var discussion = this;
  if (this.chat == null && ('chat' in contents)) {
    this.chat = new Chat(contents.chat, this, this.chat_div);
    this.chat.onMessageAdded = function (message) { discussion.handleMessageAdded(message); }
    registerObject(this.chat)
  }
  // { var x = ''; for (var e in contents) { x+= (e + ': ' + contents[e]); }; errorLog.reportError (x); }
  var comments = contents.comments
  var commentsdict = {};
  for (var i = 0; i < comments.length; i++) {
    var commentid = comments[i];
    commentsdict[commentid] = true;
    if (! (commentid in this.livecomments)) {
      this.livecomments[commentid] = this.createComment(commentid)
      var comment = this.livecomments[commentid]
      if (this.openedCommentId == commentid) {
        this.livecomments[commentid].focus()
        this.openedCommentId = null;
      }

      comment.oncancel = function () { discussion.handleLiveCommentCancel(commentid); }
      comment.onclose = function () { discussion.onCommentClose(comment); };
      registerObject(comment)
      if (this.onUnread) {
        this.onUnread(1)
      }
    }
  }
  var deletedComments = []
  this.firstComment = null;
  for (var commentid in this.livecomments) {
    if (! (commentid in commentsdict)) {
      deletedComments.push(commentid)
    } else if (!this.defaultComment) {
      this.firstComment = this.livecomments[commentid];
    }
  }
  for (var i = 0; i < deletedComments.length; i++) {
    var commentid = deletedComments[i]
    var comment = objectRegistry.lookup(commentid)
    if (comment == null) {
      // We were the user that closed this; skip deregistration
    } else {
      comment.remove();
    }
    delete this.livecomments[commentid]
  }
  for (var i = 0; i < contents.whiteboards.length; i++) {
    var whiteboard = contents.whiteboards[i]
    if (!(whiteboard in this.whiteboards)) {
      this.whiteboards[whiteboard] = new Whiteboard(whiteboard, discussion);
      registerObject(this.whiteboards[whiteboard])
      if (whiteboard == this.openedWhiteboardId) {
        this.whiteboards[whiteboard].openWhiteboardEditorView()
        this.openedWhiteboardId = null;
      } 
    }
  }  
  if (!contents.isDefault) {
    this.title_input.value = contents.title;
    if (!this.initialized) {
      this.title_form_div.style.display = "inline";
    }
  }
  
  if (!contents.isDefault) {
    this.creator_span.innerHTML = "";
    this.creator_span.appendChild(document.createTextNode(contents.creator))
  }
  
  if (!this.initialized && !this.expanded && contents.isDefault) {
    this.defaultExpand()
  }
  this.initialized = true;
  Discussion.superclass.sync.call(this, content)  
}

Discussion.prototype.defaultExpand = function () {
  this.expand();
}


Discussion.prototype.notifyPresentPersons = function (people) {
}

Discussion.prototype.handleLiveCommentCancel = function (commentid) {
  yarrr.deAuthorLiveComment(this.id, commentid, function (result, err) { if (err) { errorLog.reportError('deAuthorLiveComment failed: ' + err); }; })
}

Discussion.prototype.addWhiteboardToChat = function(whiteboard, version) {
  this.chat.sendWhiteboard (whiteboard, version);
}

Discussion.prototype.createWhiteboard = function() {
  errorLog.reportEvent("discussion::createWhiteboard (button pressed)");
  var discussion = this;
  yarrr.createWhiteboard(this.id, function (res, err) { discussion.openedWhiteboardId = res; });
}
