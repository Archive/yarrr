<%@ include file="prelude.jspf" %>
<html>
<head>
    <title>&#9760</title>
    <c:url var="stylesheet" value="/stylesheets/topic.css"/>
    <link rel="stylesheet" href="${stylesheet}"/>
</head>
<body>
<%@ include file="clientcore.jspf" %>
<script type="text/javascript">

function yarrrFullInit(rootobj) {
  var theYarrrSummary = new YarrrSummary(rootobj)
  registerObject(theYarrrSummary)
}

function yarrrInit() {
  try {
     var rootobj = "${yarrrId}"
     yarrrInitFull("${anonymousPerson}", "${person}", rootobj, "${clientInstanceId}", 
                    ${referencableObjectPreCache}, "/yarrr/", "/yarrr/execute", yarrrFullInit)
  } catch (e) {
    alert("Error initializing page: " + e.toString());
  }
}

if (window.addEventListener) window.addEventListener("load",yarrrInit,false);
else if (window.attachEvent) window.attachEvent("onload",yarrrInit);

</script>
<h2>Active Topics</h2>
<div id="topicList"><ul id="topicListUL"></ul></div>
<h3>Inactive Topics</h3>
<div id="inactiveTopicList"><ul id="inactiveTopicListUL"></ul></div>
</body>
</html>
