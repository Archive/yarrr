function yarrrFullInit(rootobj) {
  try {
    errorLog.reportEvent("starting yarrrFullInit")
    // Hack
    if (wikiYarrrXmlRpcURL.search(/localhost/) >= 0) {
      errorLog.enableAutoShow();
      var form = document.createElement("form")
      var button = document.createElement("input")
      button.setAttribute("type", "button")
      button.setAttribute("value", "Show \u2620 Log");
      button.addEventListener("click", function (e) { errorLog.show(); }, false);
      form.appendChild(button)
      document.body.appendChild(form)
    }
 
    theTopicSummary = new EmbeddedYarrrSummary(rootobj)
    registerObject(theTopicSummary);

    errorLog.reportEvent("calling yarrrWikiSummaryInit")
    yarrrWikiSummaryInit(rootobj)
    errorLog.reportEvent("adding init hook") 
    theTopicSummary.addInitHook(function () { yarrrWikiSummaryFullInit(theTopicSummary); })  
  } catch (e) {
    alert("Error in yarrrFullInit: " + e)
    errorLog.reportError(e);
  }  
}

function yarrrInit() {
  try {
    setWidgetImageDir(wikiYarrrRoot + "images/");
  
    var rootobj = "${yarrrId}"
    errorLog.reportEvent("starting yarrrInitFull")  
    yarrrInitFull("${anonymousPerson}", "${person}", rootobj, "${clientInstanceId}", 
                  ${referencableObjectPreCache}, wikiYarrrRoot, wikiYarrrXmlRpcURL, yarrrFullInit)
  } catch (e) {
    alert("Error in yarrrInit: " + e)  
    errorLog.reportError(e);
  }
}
 
if (window.addEventListener) window.addEventListener("load",yarrrInit,false); 
else if (window.atatachEvent) window.attachEvent("onload",yarrrInit);