function Editor() {
}

Editor.prototype.changedCallback = function () {};

//replaces old text
//resets selection
Editor.prototype.setText = function (text, colors) {
  alert("unimplemented editor method");
}

//appends at end of text
//remembers old selection
Editor.prototype.appendText = function (text, color, newLine) {
  alert("unimplemented editor method");
}

Editor.prototype.focus = function () {
  alert("unimplemented editor method");
}

Editor.prototype.setEnabled = function (enabled) {
  alert("unimplemented editor method");
}

Editor.prototype.getText = function () {
  alert("unimplemented editor method");
}

Editor.prototype.getSelection = function () {
  alert("unimplemented editor method");
}

Editor.prototype.setSelection = function (selection) {
  alert("unimplemented editor method");
}

Editor.prototype.getScroll = function () {
  alert("unimplemented editor method");
}

Editor.prototype.setScroll = function (scroll) {
  alert("unimplemented editor method");
}

Editor.prototype.appendTo = function (node) {
  alert("unimplemented editor method");
}

// called after a while when the text has been changed
Editor.prototype.setChangedCallback = function (callback) {
  this.changedCallback = callback;
}

Editor.prototype.getLastText = function () {
  return this.lastText;
}

Editor.prototype.getLastColors = function () {
  return this.lastColors;
}

Editor.prototype.setLastState = function (text, colors) {
  this.lastText = text;
  this.lastColors = colors;
}

Editor.prototype.setColorMap = function(map) {
  this.colorMap = map;
}

Editor.prototype.setIgnoreColor = function(color) {
  this.ignoreColor = color;
}

Editor.prototype.getColor = function(color) {
  if (this.ignoreColor == color)
    return "#ffffff";
    
  if (color < this.colorMap.length)
    return this.colorMap[color];
    
  return this.colorMap[this.colorMap.length-1];
}


Editor.prototype.saveLocalState = function (localChangesAgainst) {
  var state = new Object();
  
  var liveText = this.getText();
  state.selection = this.getSelection();
  state.oldScroll = this.getScroll();

  if (liveText != this.lastText) {
    // fix up selection back to the lastText state
    var reverseDiff = diff_strings(liveText, this.lastText);
    patch_string(liveText, reverseDiff, state.selection);
  }

  if (localChangesAgainst == null)
    localChangesAgainst = this.lastText;

  state.localChanges = null;
  if (liveText != localChangesAgainst) {
    // We have some uncommited changes, save them
    state.localChanges = diff_strings(localChangesAgainst, liveText);
  }  
  
  return state;
}

Editor.prototype.restoreLocalState = function (state) {
  // Re-apply old local changes
  var newLiveText = this.lastText;
  var newLiveColors = this.lastColors;
  
  if (state.localChanges != null) {
    res = patch_tagged_string(this.lastText, this.lastColors, state.localChanges, state.selection);
    if (res == null) {
      alert("There was an editing conflict, some of your local changes didn't apply");
    } else {
      newLiveText = res.text;
      newLiveColors = res.colors;
    }
  }

  this.setText(newLiveText, newLiveColors);
  this.setSelection(state.selection);
  this.setScroll(state.oldScroll);
}

// localChangesAgainst (optional) is the text we sent, and is now 
// considered the base for calculating the outstanding changes
Editor.prototype.applyPatch = function (diff, localChangesAgainst) {
  var state = this.saveLocalState(localChangesAgainst);

  // Apply the diff	
  res = patch_tagged_string(this.lastText, this.lastColors, diff, state.selection);
  this.lastText = res.text;
  this.lastColors = res.tags;

  this.restoreLocalState(state);
}

Editor.prototype.hasChanges = function () {
  return this.lastText != this.getText();
}

Editor.prototype.getChanges = function () {
  return diff_strings(this.lastText, this.getText());
}

Editor.prototype.editedTimeout = function() {
  if (this.hasChanges())
    this.changedCallback();
  this.editedTimeoutObj = null;
}

Editor.prototype.handleEdited = function() {
  if (this.editedTimeoutObj == null) {
    var editor = this;
    this.editedTimeoutObj = setTimeout(function () { editor.editedTimeout() }, 1000);
  }  
}

/*******************************************************
 ******************* Plain Editor **********************
 *******************************************************/

function PlainEditor(enabled) {
  this.lastText = "";
  this.lastColors = [];

  this.textarea = document.createElement("textarea")
  this.textarea.setAttribute("class", "commentarea")
  this.textarea.setAttribute("wrap", "virtual")
  this.textarea.setAttribute("cols", "72")
  this.textarea.setAttribute("rows", "8")

  var editor = this;
  this.textarea.onkeydown = function() { editor.handleEdited(); };
  this.textarea.onkeyup = function() { editor.handleEdited(); };
  this.textarea.onmousedown = function() { editor.handleEdited(); };
  this.textarea.onmouseup = function() { editor.handleEdited(); };
  
  this.setEnabled(enabled)
}

PlainEditor.prototype = new Editor();
PlainEditor.prototype.constructor = PlainEditor;

PlainEditor.prototype.appendTo = function (node) {
   node.appendChild(this.textarea);
}

PlainEditor.prototype.setText = function (text, colors) {
  this.textarea.value = text;
}

PlainEditor.prototype.setEnabled = function (enabled) {
  if (!enabled)
    this.textarea.setAttribute("disabled", "true");
  else
    this.textarea.removeAttribute("disabled")
}

PlainEditor.prototype.appendText = function (text, color, newLine) {
  if (newLine && this.textarea.value != "") {
    this.textarea.value += "\n";
  }
  this.textarea.value += text;
  this.handleEdited();
}

PlainEditor.prototype.getText = function () {
  return this.textarea.value;
}

PlainEditor.prototype.getSelection = function () {
  return getSelection(this.textarea);
}

PlainEditor.prototype.setSelection = function (selection) {
  setSelection(this.textarea, selection);
}

PlainEditor.prototype.getScroll = function () {
  return this.textarea.scrollTop;
}

PlainEditor.prototype.setScroll = function (scroll) {
  this.textarea.scrollTop = scroll;
}

PlainEditor.prototype.focus = function () {
  this.textarea.focus();
}

/*******************************************************
 ******************* Rich Editor ***********************
 *******************************************************/

function RichEditor(enabled) {
  this.lastText = "";
  this.lastColors = [];
  this.enabled = enabled;

  this.iframe = document.createElement("iframe")
  this.iframe.setAttribute("width", "500 px")
  this.iframe.setAttribute("height", "300 px")
  this.iframe.setAttribute("src", "about:blank")
}

RichEditor.prototype = new Editor();
RichEditor.prototype.constructor = RichEditor;

RichEditor.prototype.appendTo = function (node) {
  node.appendChild(this.iframe);
  if (this.enabled)
    this.iframe.contentWindow.document.designMode = "on";
  
  var frameHtml = "<html id=\"inside_html\">\n";
  frameHtml += "<head>\n";
  frameHtml += "<style>\n";
  frameHtml += "body {\n";
  frameHtml += "	background: #FFFFFF;\n";
  frameHtml += "	margin: 0px;\n";
  frameHtml += "	padding: 0px;\n";
  frameHtml += "	font-family: monospace;\n";
  /* Browser specific (not valid) styles to make preformatted text wrap */
  frameHtml += "	white-space: pre-wrap;\n";       /* css-3 */
  frameHtml += "	white-space: -moz-pre-wrap;\n";  /* Mozilla, since 1999 */
  frameHtml += "	white-space: -pre-wrap;\n";      /* Opera 4-6 */
  frameHtml += "	white-space: -o-pre-wrap;\n";    /* Opera 7 */
  frameHtml += "	word-wrap: break-word;\n";       /* Internet Explorer 5.5+ */
  frameHtml += "}\n";
  frameHtml += "</style>";
  frameHtml += "</head>";
  frameHtml += "<body>";
  frameHtml += "</body>\n";
  frameHtml += "</html>";

  var frameDoc = this.iframe.contentWindow.document;
  frameDoc.open();
  frameDoc.write(frameHtml);
  frameDoc.close();
  
  var editor = this;
  frameDoc.addEventListener("keyup", function() { editor.handleEditedRich(); }, false);
  frameDoc.addEventListener("keydown", function() { editor.handleEditedRich(); }, false);
  frameDoc.addEventListener("mouseup", function() { editor.handleEditedRich(); }, false);
  frameDoc.addEventListener("mousedown", function() { editor.handleEditedRich(); }, false);
}

RichEditor.prototype.setEnabled = function (enabled) {
  this.enabled = enabled;
  /* FIXME do something else? */
}

RichEditor.prototype.setText = function (text, colors) {
  var doc = this.iframe.contentWindow.document;
  var body = doc.body;
  while (body.hasChildNodes())
    body.removeChild(body.lastChild);
    
  if (text.length == 0)
    return;    
    
  var start = 0;  
  var end = 1;
  var color = colors[0];
  var lastWasNewline = false;
  
  do {
    while (end < text.length && colors[end] == color)
      end++;

    var span = doc.createElement("span");
    span.style.backgroundColor = this.getColor(color);
    
    // Handle newlines
    var textStart = start;
    var textEnd;
    while (textStart < end) {
      var node;
      if (text[textStart] == "\n") {
	node = doc.createElement("br");
	textEnd = textStart + 1;
	lastWasNewline = true;
      } else {
        textEnd = textStart + 1
        while (textEnd < end && text[textEnd] != "\n") {
          textEnd++;
        }
        node = doc.createTextNode(text.substring(textStart, textEnd));
	lastWasNewline = false;
      }
      span.appendChild(node)
      textStart = textEnd;
    }
    body.appendChild(span);
    
    start = end;
    color = colors[end];
  } while (end < text.length)

  // Mozilla always adds this, so we do too, since we 
  // compensate for it in getText()
  if (lastWasNewline) {
    node = doc.createElement("br");
    body.appendChild(node);
  }
}

RichEditor.prototype.appendText = function (text, color, newLine) {
  var doc = this.iframe.contentWindow.document;
  var body = doc.body;
  var node;
  if (newLine && this.getText() != "") {
    node = doc.createElement("br");
    body.appendChild(node);
  }
  node = doc.createTextNode(text);
  body.appendChild(node);
  this.handleEdited();
}

RichEditor.prototype.extractText = function (node) {
  if (node.nodeType == Node.TEXT_NODE) {
    return node.data;
  }
  var str = "";
  
  for (var i = 0; i < node.childNodes.length; i++) {
    str = str + this.extractText(node.childNodes[i]);
  }
  
  if (node.nodeType == Node.ELEMENT_NODE) {
    if (node.tagName == "BR" || node.tagName == "br") {
      str = str + "\n";
    }
  }
  
  return str;
}

RichEditor.prototype.getText = function () {
  var doc = this.iframe.contentWindow.document;

  text = this.extractText(doc.body);
  
  // Mozilla design mode keeps adding an extra <br> at the last line
  // so we strip it out here. This is also compensated in setText().
  if (text[text.length-1] == "\n")
    text = text.substring(0,text.length-1);
    
  //alert ("'" + text + "'");
  return text;
}

RichEditor.prototype.pointToOffset = function (pointContainer, pointOffset, node, data) {
  if (node == null) {
    var doc = this.iframe.contentWindow.document;
    node = doc.body;
  }
  if (data == null) {
    data = new Object();
    data.pos = 0;
  }
  
  if (node.nodeType == Node.TEXT_NODE) {
    if (node == pointContainer) {
      return data.pos + pointOffset;
    }
    data.pos += node.data.length;
    return -1;
  }
  
  for (var i = 0; i < node.childNodes.length; i++) {
    if (node == pointContainer && pointOffset == i)
      return data.pos;
    var res = this.pointToOffset(pointContainer, pointOffset, node.childNodes[i], data);
    if (res != -1)
      return res;
  }
  
  if (node.nodeType == Node.ELEMENT_NODE) {
    if (node.tagName == "BR" || node.tagName == "br") {
      data.pos += 1;
    }
  }
  
  return -1;
}

RichEditor.prototype.offsetToPoint = function (offset, node, data) {
  if (node == null) {
    var doc = this.iframe.contentWindow.document;
    node = doc.body;
  }
  if (data == null) {
    data = new Object();
    data.pos = 0;
  }
  
  if (node.nodeType == Node.TEXT_NODE) {
    if (data.pos + node.data.length > offset) {
      var res = new Object();
      res.container = node;
      res.offset = offset - data.pos;
      return res;
    }
    data.pos += node.data.length;
    return null;
  }
  
  for (var i = 0; i < node.childNodes.length; i++) {
    var child = node.childNodes[i]; 

    if (child.nodeType == Node.ELEMENT_NODE &&
       (child.tagName == "BR" || child.tagName == "br") &&
       offset == data.pos) {
      var res = new Object();
      res.container = node;
      res.offset = i;
      return res;
    }
    var res = this.offsetToPoint(offset, child, data);
    if (res != null)
      return res;
  }
  
  if (node.nodeType == Node.ELEMENT_NODE) {
    if (node.tagName == "BR" || node.tagName == "br") {
      data.pos += 1;
    }
  }
  
  return null;
}

RichEditor.prototype.getSelection = function () {
  var frameWindow = this.iframe.contentWindow;
  var frameSelection = frameWindow.getSelection();
  var selection = new Object();
  selection.start = 0;
  selection.end = 0;
  if (frameSelection.rangeCount > 0) {
    var range = frameSelection.getRangeAt(frameSelection.rangeCount - 1).cloneRange();
    selection.start = this.pointToOffset(range.startContainer, range.startOffset);
    if (selection.start < 0)
      selection.start = 0;
    selection.end = this.pointToOffset(range.endContainer, range.endOffset);
    if (selection.end < 0)
      selection.end = 0;
  }
  
  return selection;
}

RichEditor.prototype.setSelection = function (selection) {
  var frameWindow = this.iframe.contentWindow;
  var frameSelection = frameWindow.getSelection();
  
  var range = frameWindow.document.createRange();
  var start = this.offsetToPoint(selection.start);
  var end = this.offsetToPoint(selection.end);
  
  if (start != null && end != null) {
    range.setStart(start.container, start.offset);
    range.setEnd(end.container, end.offset);
    
    frameSelection.removeAllRanges();
    frameSelection.addRange(range);
  }
}

RichEditor.prototype.getScroll = function () {
  var frameWindow = this.iframe.contentWindow;
  var scroll = new Object();
  scroll.x = frameWindow.pageXOffset;
  scroll.y = frameWindow.pageYOffset;
  
  return scroll;
}

RichEditor.prototype.setScroll = function (scroll) {
  var frameWindow = this.iframe.contentWindow;
  frameWindow.scrollTo(scroll.x, scroll.y);
}

RichEditor.prototype.focus = function () {
  //TODO this.iframe.focus();
}

RichEditor.prototype.handleEditedRich = function() {
  var frameWindow = this.iframe.contentWindow;
  if (frameWindow.getSelection().isCollapsed) {
    var frameDoc = frameWindow.document;
    frameDoc.execCommand("hilitecolor", false, "#ffffff");
  }
  this.handleEdited();
}
 