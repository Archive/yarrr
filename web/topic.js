function yarrrNotify(text) {
    notify = document.getElementById("yarrrNotificationArea");
    if (notify) {
      var div = document.createElement("div")
      div.setAttribute("class", "yarrrNotification")
      notify.innerHTML = "";
      div.appendChild(document.createTextNode(text));
      notify.appendChild(div)
    } 
}

function BaseTopic (id, onUnread) {
  if (arguments.length == 0)
    return;
  this.init(id, onUnread)
}

BaseTopic.prototype = new ReferencableObject();
BaseTopic.prototype.constructor = BaseTopic;
BaseTopic.superclass = ReferencableObject.prototype;

BaseTopic.prototype.init = function (id, onUnread) {
  BaseTopic.superclass.init.call(this, id)
  this.onUnread = onUnread
  this.closedcomments = {};
  this.updateVersion = null;
  
  this.peopleElt = window.document.getElementById("peoplePlaceholder")
  BaseTopic.superclass.postInit.call(this)
}

BaseTopic.prototype.setPresentPersons = function (persons) {
  this.presentPersonNames = persons;
  this.renderPeople();
}

BaseTopic.prototype.renderPeople = function () {
  var elt = this.peopleElt;
  elt.innerHTML = "";
  var span = document.createElement ("span");
  span.setAttribute ("class", "peopleList");
  elt.appendChild (span);

  name_list = "";
  for (var i = 0; i < this.presentPersonNames.length; i++) {
    if (i > 0)
      name_list += ", ";
    name_list += this.presentPersonNames [i];
  }
  span.appendChild (document.createTextNode (name_list));
}

BaseTopic.prototype.getPresentPersonNames = function () {
  return this.presentPersonNames;
}

BaseTopic.prototype.sync = function (content) {
  var topic = this;

  this.setPresentPersons(content.presentpersons);
  BaseTopic.superclass.sync.call(this, content)   
}

function EmbeddedTopic(id) {
  this.discussions = new Object();
  this.init(id, null);
}

EmbeddedTopic.prototype = new BaseTopic();
EmbeddedTopic.prototype.constructor = EmbeddedTopic;
EmbeddedTopic.superclass = BaseTopic.prototype;

EmbeddedTopic.prototype.sync = function (content) {
  EmbeddedTopic.superclass.sync.call(this, content);

  var topic = this;

  if (this.updateVersion != null && (this.updateVersion < content.updateVersion)) {
    updateWikiContent();
  }
  this.updateVersion = content.updateVersion
  
  for (var i = 0; i < content.discussions.length; i++) {
    var id = content.discussions[i];
    if (!(id in this.discussions)) {
      this.discussions[id] = new EmbeddedDiscussion(id, this);
      var discussion = this.discussions[id]
      registerObject(discussion)
    }
  }
  
  /* Ignore closed comments */
  
  for (discussionid in this.discussions) {
    var discussion = this.discussions[discussionid]
    discussion.notifyPresentPersons(this.presentPersonNames)
  }
}

EmbeddedTopic.prototype.renderPeople = function () {
// Don't do anything here, this is handled by the discussion
}

Topic.prototype = new BaseTopic();
Topic.prototype.constructor = Topic;
Topic.superclass = BaseTopic.prototype;

function Topic(id, onUnread) {
  this.discussions = new Object();
  this.statements = {};
  this.presentPersons = {};
  this.presentPersonNames = [];
  this.edited_timeout = null;
  this.creator = ""
  this.creatorelt = document.getElementById("topicCreationInfo");
  this.personControl = null;
  
  this.statementsElt = window.document.getElementById("statementsPlaceholder") 
  this.personElt = document.getElementById("personControlPlaceholder");
  
  this.init(id, onUnread);
}

Topic.prototype.getId = function() {
  return this.id;
}

Topic.prototype.setCreator = function (creator, creationDate) {
  this.creator = creator;
  this.creationDate = creationDate;
  
  this.creatorelt.innerHTML = "";
  this.creatorelt.appendChild(document.createTextNode("Created by "))
  var span = document.createElement("span")
  span.setAttribute("class", "topicCreator");
  span.appendChild(document.createTextNode(this.creator));
  this.creatorelt.appendChild(span)
  this.creatorelt.appendChild(document.createTextNode(" - "))
  var span = document.createElement("span")
  span.setAttribute("class", "topicCreationDate");
  span.appendChild(document.createTextNode(getPrettyDate(this.creationDate)));
  this.creatorelt.appendChild(span)
}

Topic.prototype.sync = function (content) {
  Topic.superclass.sync.call(this, content);

  var topic = this;
  this.setCreator(content.creator, content.creationDate);

  if (!this.personControl) {
    this.personControl = new PersonControl(this.personElt);
  }

  for (var i = 0; i < content.discussions.length; i++) {
    var id = content.discussions[i];
    if (!(id in this.discussions)) {
      this.discussions[id] = new Discussion(id, this);
      var discussion = this.discussions[id]
      registerObject(discussion)
      discussion.onCommentClose = function (commentid) {
      	yarrr.closeLiveComment(topic.getId(), discussion.getId(), commentid, function (result, err) {});
      } 
    }
  }

  for (var i = 0; i < content.closed.length; i++) {
    var id = content.closed[i];
    if (!(id in this.closedcomments)) {
      this.closedcomments[id] = new ClosedComment(id, this.id);
      registerObject(this.closedcomments[id])
      if (this.onUnread) {
        this.onUnread(1);
      }
    }
  }
  
  statementsdict = {}
  for (var i = 0; i < content.statements.length; i++) {
    var statement = content.statements[i];
    statementsdict[statement] = true;
    if (!(statement in this.statements)) {
      this.statements[statement] = new Statement(this.id, statement, this.statementsElt);
      registerObject(this.statements[statement])
    }
  }
  
  for (var statementid in this.statements) {
    if (!(statementid in statementsdict)) {
      this.statements[statementid].remove();
      delete this.statements[statementid];
    }
  }
}

Topic.prototype.openDiscussion = function() {
  yarrr.openDiscussion(this.id, function (res, err) {});
}

Topic.prototype.addStatement = function(text) {
  yarrr.addStatement(this.id, text, function (res, err) {});
}

function createTopicCallback(result, err) {
  try {
    if (err) {
      errorLog.reportError(err);
      return;
    }
    window.location.reload();
  } catch (e) {
    errorLog.reportError(e);
  }
}

function createTopic(name, baseurl) {
  yarrr.createTopic(name, getYarrrPerson().getId(), function (result, err) { createTopicCallback(result, err); });
}
