var MESSAGE_TYPE_CHAT = 0;
var MESSAGE_TYPE_ACTION = 1;
var MESSAGE_TYPE_WHITEBOARD = 2;

function Message(chat, container, contents) {
  this.chat = chat;

  var row = document.createElement("tr");
  container.appendChild(row);
  this.contentelt = row;
  
  this.create(contents);
  chat.onMessageInitialized (this);
}

Message.prototype = new ReferencableObject();

Message.prototype.subscribe = function() {
  yarrr.subscribeChatMessage(this.chat.id, this.index, function (res, err) { } );
}

Message.prototype.unsubscribe = function() {
  yarrr.unsubscribeChatMessage(this.chat.id, this.index, function (res, err) { } );
}

Message.prototype.updateSubscribersCallback = function(res, err) {
  this.createSubscribers(res);
}

Message.prototype.updateSubscribers = function() {
  var message = this;
  yarrr.getChatMessageSubscribers(this.chat.id, this.index, function (res, err) { message.updateSubscribersCallback(res, err); });
}

Message.prototype.isToMe = function() {
  return this.toMe;
}

_chatFormatDate = function(date) {
   var minutes = date.getMinutes();
   if (minutes < 10) {
     minutes = "0" + minutes;
   }
   var hours = date.getHours();
   return hours + ":" + minutes;
}

Message.prototype.copyToComment = function() {
  var comment = this.chat.discussion.getComment();
  if (comment) {
    comment.appendText(this.text, true);
  } else {
    this.chat.discussion.openComment(this.text);
  }
}

Message.prototype.formatSubscribeAction = function(container) {
   while (container.hasChildNodes() != 0)
     container.removeChild(container.firstChild);
     
  var message = this;

  var a = document.createElement("a");
  a.setAttribute("class", "chataction");
  a.setAttribute("href", "#");
  if (this.subscribed) {
    a.onclick = function () { message.unsubscribe(); return false; };
  } else {
    a.onclick = function () { message.subscribe(); return false; };
  }
  container.appendChild(a);
  
  var img = document.createElement("img");
  if (this.subscribed) {
    img.setAttribute("src", getYarrrURL("images/unsubscribe_message.png"));
  } else {
    img.setAttribute("src", getYarrrURL("images/subscribe_message.png"));
  }
  a.appendChild(img);
}

Message.prototype.formatCopyAction = function(container) {  
  var message = this;

  var a = document.createElement("a");
  a.setAttribute("class", "chataction");
  a.setAttribute("href", "#");
  a.onclick = function () { message.copyToComment(); return false; };
  container.appendChild(a);
  
  var img = document.createElement("img");
  img.setAttribute("src", getYarrrURL("images/chat_to_comment.png"));
  a.appendChild(img);
}

Message.prototype.formatDate = function(contents, container) {
  var elt = document.createElement("span");
  elt.setAttribute("class", "chatdate");
  container.appendChild(elt);
  var text = document.createTextNode(_chatFormatDate(contents.date));
  elt.appendChild(text);
}

Message.prototype.formatAuthor = function(contents, container) {
  var elt = document.createElement("span");
  if (contents.type == MESSAGE_TYPE_ACTION) {
    elt.setAttribute("class", "chatauthorByMe");
    container.setAttribute("class", "chatauthorByMe");
  } else if (this.fromMe) {
    elt.setAttribute("class", "chatauthorFromMe");
  } else if (this.toMe) {
    elt.setAttribute("class", "chatauthorToMe");
  } else {
    elt.setAttribute("class", "chatauthor");
  }
  container.appendChild(elt);
  if (contents.type != MESSAGE_TYPE_ACTION) {
    text = document.createTextNode(contents.nick);
  } else {
    text = document.createTextNode("*");
  }
  elt.appendChild(text);
}

Message.prototype.formatMessage = function(contents, container) {
  var elt;

  if (contents.type == MESSAGE_TYPE_ACTION) {
    elt = document.createElement("span");
    elt.setAttribute("class", "chatauthorByMe");
    text = document.createTextNode(contents.nick);
    elt.appendChild(text);
    container.appendChild(elt);
    container.appendChild(document.createTextNode(" "));
  }
  
  elt = document.createElement("span");
  elt.setAttribute("class", "chattext");
  if (contents.type == MESSAGE_TYPE_WHITEBOARD) {
    this.formatWhiteboard(contents, elt);
  } else {
    yarrrMarkupToHTML(contents.contents, document, elt);
  }
  container.appendChild(elt);
}

Message.prototype.setupImage = function(img, fg, bg) {
  var url = "/yarrr/ArchivedImage?id=" + fg;
  img.setAttribute("src", url);
  if (bg != null) {
    url = "/yarrr/ArchivedImage?id=" + bg;
    var styleString = "background-image: url(\"" + url +"\");";
    styleString +="background-color: transparent;";
    styleString +="vertical-align: middle;";
    img.setAttribute("style", styleString);
  }
}

Message.prototype.formatWhiteboard = function(contents, container) {
  var link = document.createElement("a")
  link.setAttribute("href", "#");
  container.appendChild(link);
  
  var img = document.createElement("img")
  this.setupImage(img, contents.whiteboardFgThumb, contents.whiteboardBgThumb);
  link.appendChild(img);

  var message = this;
  
  link.onclick = function () { 
    var toplevel = window.open();
    var html = "<html><head><title>Whiteboard</title>";
    html += "</head><body style=\"background-color: #8faccf;\">";
    html += "</body></html>";
    toplevel.document.write(html);
	
    var img = toplevel.document.createElement("img");
    message.setupImage(img, contents.whiteboardFg, contents.whiteboardBg);
    toplevel.document.body.appendChild(img);
    return false;
  };
}

Message.prototype.formatSubscribers = function(subscribers, container) {
   while (container.hasChildNodes() != 0)
     container.removeChild(container.firstChild);

  if (subscribers.length) {
    var elt = document.createElement("span");
    elt.setAttribute("class", "chatsubscribers");
    var subscribersText = "";
    for (var i = 0; i < subscribers.length; i++) {
      subscribersText += subscribers[i];
      if (i != subscribers.length - 1) {
        subscribersText += ", ";
      }
    }

    var text = document.createTextNode('(' + subscribersText + ')');
    elt.appendChild(text);
    container.appendChild(elt);
  }
}

Message.prototype.create = function(contents) {
  this.contentelt.innerHTML = "";
  this.index = contents.index;
  var name = getYarrrPerson().getName();
  this.authorNick = contents.nick
  this.fromMe = contents.author == getYarrrPerson();
  if ('contents' in contents) {
    this.toMe = contents.contents.indexOf(name) != -1;
    this.text = yarrrMarkupToText(contents.contents);
  } else {
    this.toMe = false;
    this.text = "";
  }

  var col1 = document.createElement("td");
  this.formatDate(contents, col1);
  this.contentelt.appendChild(col1);

  var col2 = document.createElement("td");
  this.formatAuthor(contents, col2);
  this.contentelt.appendChild(col2);
    
  var col3 = document.createElement("td");
  col3.setAttribute("class", "chatcontent");
  this.formatMessage(contents, col3);
  this.contentelt.appendChild(col3);
  
  var col4 = document.createElement("td");
  col4.setAttribute("class", "chatcontent");
  this.subscribersElement = col4;
  this.contentelt.appendChild(col4);
  
  var col5 = document.createElement("td");
  col5.setAttribute("class", "chatactions");
  this.subscriberActionElement = col5;
  this.contentelt.appendChild (col5);

  var col6 = document.createElement("td");
  col6.setAttribute("class", "chatactions");
  this.formatCopyAction(col6);
  this.contentelt.appendChild (col6);
  
  this.createSubscribers(contents.subscribers);
}

Message.prototype.createSubscribers = function(subscribers) {
  var name = getYarrrPerson().getName();

  this.subscribed = false;
  for (var i = 0; i < subscribers.length; i++) {
    if (subscribers[i] == name) {
      this.subscribed = true;
    }
  }

  this.formatSubscribers(subscribers, this.subscribersElement);
  this.formatSubscribeAction(this.subscriberActionElement);
        
}


function Chat(id, discussion, container) {
  this.id = id;
  this.messages = {};
  this.discussion = discussion
  this.oldMessageIndex = 0;
  this.oldChangeIndex = 0;
  this.onUnread = discussion.onUnread
  
  this.chatWindow = document.createElement("div")
  this.chatWindow.setAttribute("class", "chatwindow")
  container.appendChild(this.chatWindow)
  this.chatTable = document.createElement("table");
  this.chatWindow.appendChild(this.chatTable);
  var windowEntry = document.createElement("div")
  container.appendChild(windowEntry)
  windowEntry.setAttribute("class", "chatWindowEntry")
  var form = document.createElement("form")
  windowEntry.appendChild(form)
  form.appendChild(document.createTextNode("Chat: "))
  var input = document.createElement("input")
  input.setAttribute("type", "text")
  input.setAttribute("class", "chatWindowInput")
  form.appendChild(input)
  
  var chat = this;
  form.onsubmit = function () { chat.send(); return false; };
  if (getYarrrPerson().isAnonymous()) {
    input.setAttribute("disabled", "true")
  }
  input.onkeypress = function (e) { return chat.onkeydown(e); };
  this.chatText = input
  
  var precached = stealPrecachedReferencableObject(this.id)
  if (precached != null)
    this.sync(precached)
  else
    this.update();
}

Chat.prototype = new ReferencableObject();

Chat.prototype.onkeydown = function (e) {
  if (e.keyCode == 9) { // got TAB
    e.preventDefault();
    this.complete();
    e.stopPropagation();
    return false;
  }
  return true;
}

Chat.prototype.complete = function () {
  var selection = getSelection(this.chatText);
  if (selection == undefined)
    return;
    
  var atStart = false;
  var fullMatch = false;
  var start = this.chatText.value.lastIndexOf(" ", selection.start);
  if (start == -1) {
    start = 0;
    atStart = true;
  } else {
    start = start + 1;
  }
  
  var currentText = this.chatText.value.substring(start, selection.start);
  var expansion = null;
  var alternatives = this.discussion.getPresentPersonNames();
  
  for (var i = 0; i < alternatives.length; i++) {
    var alternative = alternatives[i];
    
    if (alternative.substring(0, currentText.length) == currentText) {
      if (expansion == null) {
        fullMatch = true;
        expansion = alternative.substring(currentText.length);
      } else {
        fullMatch = false;
        expansion = longestCommonSubstring(expansion, alternative.substring(currentText.length));
      }
    }
  }

  if (expansion != null && expansion != "") {
    if (fullMatch)
      expansion = expansion + (atStart?": ":" ");
    this.chatText.value = this.chatText.value.substring(0, selection.start) +
                    expansion + 
                    this.chatText.value.substring(selection.end);
    selection.start += expansion.length;
    selection.end = selection.start;
    setSelection(this.chatText, selection);
  }
}

Chat.prototype.sendCallback = function (result, err) {
  this.chatText.value = "";
}

Chat.prototype.handleCommands = function (command, args) {
  var chat = this;
  
  switch (command) {
    case "nick":
      getYarrrPerson().setName(args);
      this.chatText.value = "";
      break;
    case "me":
      yarrr.addChatAction(this.discussion.getTopic().id, this.id,
                               args, function (res, err) { chat.sendCallback (res,err); } );
      break;
    default:
      this.chatText.value = "";
  }
}

Chat.prototype.send = function () {
  var text = this.chatText.value;
  if (text.length == 0)
    return;
  errorLog.reportEvent("sending chat message: " + text);
  
  if (text[0] == '/') {
    var command = text.substring (1, text.indexOf(' '));
    var args = text.substring (text.indexOf(' ') + 1);
    this.handleCommands(command, args);
  } else {
    var chat = this;
    yarrr.addChatMessage(this.discussion.getTopic().id, this.id, text,
    					 function (result, err) { chat.sendCallback (result,err); } );
  }
}

Chat.prototype.sendWhiteboard = function (whiteboard, version) {
  yarrr.addChatWhiteboard(this.discussion.getTopic().id, this.id,
                          whiteboard.id, version, function (result, err) {} );
}

Chat.prototype.onMessageInitialized = function (message) {
  this.scrollToBottom();
  
  if (this.onUnread) {
    if (message.isToMe())
      this.onUnread(2);
    else
      this.onUnread(1);
  }
}

// Signal
Chat.prototype.onMessageAdded = function (message) {}

Chat.prototype.addMessage = function (message) {
  var chat = this;
  var index = message.index;
  this.messages[index] = message;
  this.onMessageAdded(message)
}

Chat.prototype.scrollToBottom = function () { 
  this.chatWindow.scrollTop = this.chatWindow.scrollHeight;
}

Chat.prototype.sync = function(contents) {
  this.oldMessageIndex = contents.lastMessage;
  this.oldChangeIndex = contents.lastChange;

  var messages = contents.messages;
  for (var i = 0; i < messages.length; i++) {
    var message = new Message(this, this.chatTable, messages[i]);
    this.addMessage(message);
  }
}

Chat.prototype.updateCallback = function (result, err) { 
  if (err) {
    errorLog.reportError('Chat update failed: ' + err);
    return;
  }
  try {
    this.oldMessageIndex = result.lastMessage;
    this.oldChangeIndex = result.lastChange;
    var messages = result.messages;
    var changes = result.changes;
  
    for (var i = 0; i < messages.length; i++) {
      var data = messages[i];
      var message = new Message(this, this.chatTable, data);
      this.addMessage(message);
    }
    for (var i = 0; i < changes.length; i++) {
      var change = changes[i];
      message = this.messages[change.messageIndex];
      if (change.type == 0) {
        message.updateSubscribers();
      }
    }
  } catch (e) {
    errorLog.reportError('Chat updated failed: ' + e)
  }
}

// Overrides superclass update() to be more efficient
Chat.prototype.update = function () {
   var chat = this;
   yarrr.getChatUpdates(this.id, this.oldMessageIndex, this.oldChangeIndex, function (result, err) { chat.updateCallback (result, err); } );
}

Chat.prototype.focus = function () { 
  this.chatText.focus();
}
