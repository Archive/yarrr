function hunk_to_html(hunk)
{
    var str = "";
    
    var parts = hunk.parts;
    for (var i = 0; i < parts.length; i++) {
	var p = parts[i];
	var data = p.data;
	if (p.tags != null)
	    data = data + "|" + p.tags;
	if (p.op == DIFF_MATCH) {
	    str += data;
	} else if (p.op == DIFF_DELETE) {
	    str += "<font color='#ff0000'>" + data + "</font>";
	} else if (p.op == DIFF_INSERT) {
	    str += "<font color='#00ff00'>" + data + "</font>";
	} else {
	    alert("error in context diff");
	}
    }
    return str;
}

function write_diff(diff)
{
    for (var i = 0; i < diff.length; i++) {
	var hunk = diff[i];
	document.write("hunk " + i + ", offset: " + hunk.offset + ", num parts: " + hunk.parts.length + ": '" + hunk_to_html(hunk));
	document.write("'<br>\n");
    }
}

function test(test, a, b) {
    document.write("Testing " + test + " - <br>'" + a + "'<br>'"+b+"'<br>\n");
    
    document.write("normal:<br>");
    var diff = diff_strings(a, b);
    write_diff(diff);

    var res = patch_string(a, diff);
    if (res != b)
       document.write("<b>patch failed!</b> (" + res + ")");
       
    document.write("reversed:<br>");
    diff = diff_strings(b, a);
    write_diff(diff);

    var res = patch_string(b, diff);
    if (res != a)
       document.write("<b>patch failed!</b>");
        
    document.write("<p>");
}

test("Identical empty",
     "",
     "");

test("Identical",
     "dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
     "dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.");

test("insert at start",
     "dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
     "aaadude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.");

test("insert near start",
     "dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
     "duaaade, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.");

test("insert at end",
     "dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
     "dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.bbb");

test("insert near end",
     "dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
     "dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeeaaand.");

test("insert in middle",
     "dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
     "dude, what the fuckbbbb is going on here. This string is pretty long. And it never seems to eeeeeend.");

test("multiple inserts in middle",
     "dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
     "dude, what the fuckbbbb is going on here. This string is pretty lobbbng. And it never seems to eeeeeend.");

test("multiple inserts near",
     "dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
     "dude, what the fuck is going on haaaeraaae. This string is pretty long. And it never seems to eeeeeend.");

test("random change",
     "dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
     "dude, what the *beep* is going on here. This string is pretty long. And it never seems to end.");

var orig = "dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.";
var a = "dude, what the *beep* is going on here. This string is pretty long. And it never seems to end.";
var b = "aaadude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.";
var diff1 = diff_strings(orig, a);
var diff2 = diff_strings(orig, b);
var c = patch_string(orig, diff1);
var d = patch_string(c, diff2);
document.write(d);

a = "";
b = "a";
diff1 = diff_strings(a, b);
c = patch_string(a, diff1);
if (c != b)
  alert("new char bug 1: " + c);
     
a = "aaaaaaaaaaa";
b = "aaaaaaaaaaaX";
diff1 = diff_strings(a, b);
c = patch_string("c" + a, diff1);
if (c != "c" + b)
  alert("fuzzy bug 1: " + c);
  
a = "aaaaaaaaaaa";
b = "Xaaaaaaaaaaa";
diff1 = diff_strings(a, b);
c = patch_string(a + "c", diff1);
if (c != b + "c")
  alert("fuzzy bug 2");
  
var str = "hallå på dig! detta verkar funka. alexander sdfsdfjkhdfhjksdfa\n1hej och haaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa1hej och ha\nhejsan gullle gubben, ";
var hunk = new Object();
hunk.offset=157;
hunk.parts = [{op:DIFF_MATCH, data:"ben, "}, {op:DIFF_INSERT, data:"skall vi ta en pro"}];
var res = patch_string(str, [hunk]);
if (res == null)
  alert("scan backwards bug");


document.write("<p>Tagged strings:</p>");

a = "aaa";
aTag = [0,0,0];
b = "aaabb";
bTag = [0,1,0,1,1];
d = diff_tagged_strings(a, aTag, b, bTag);
write_diff(d);
var res = patch_tagged_string(a, aTag, d, null);
document.write("<p>res: " + res.text  + " : " + res.tags);



