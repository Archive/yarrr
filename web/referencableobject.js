window.addEventListener("unload", function (e) { errorLog.hide(); return true; }, false);

var xmlrpc = importModule("xmlrpc");
var yarrr = null;
var yarrrMethods = ["dumpObjects",
	"getPerson",
	"createTopic",
	"openDiscussion",
	"setDiscussionTitle",
	"openLiveComment",
	"closeLiveComment",
	"deleteLiveComment",
	"liveCommentSaving",	
	"deAuthorLiveComment",
	"getClosedCommentMessages",
	"addProponent", // ClosedComment
	"updateLiveComment", 
	"getLiveCommentDiff", 
	"getPollUrl",
	"getPollChanges", 
	"getChatUpdates", 
	"addChatMessage", 
	"createWhiteboard",
	"addWhiteboardStroke",
	"addWhiteboardText",
	"getOpIdsBetween",
	"getWhiteboardVersion",
	"addStatement",
	"subscribeStatement",
	"unsubscribeStatement",
	"addChatWhiteboard",
	"addChatAction",
	"subscribeChatMessage",
	"unsubscribeChatMessage",
	"getChatMessageSubscribers",
	"setPersonName",
	"login",
	"logout",
	"register"
	];

var objectRegistry;

function deregisterObject(obj) {
  objectRegistry.deregister(obj)
}

function registerObject(obj) {
  objectRegistry.register(obj)
}	

var referencableObjectPreCache = {};

function stealPrecachedReferencableObject(ref) {
  var ret = null;
  if (referencableObjectPreCache[ref] != undefined) {
    ret = referencableObjectPreCache[ref]
    delete referencableObjectPreCache[ref]
  }
  return ret;
}
	
function ReferencableObjectRegistry() {
  this.references = {}
}

ReferencableObjectRegistry.prototype.register = function (obj) {
  this.references[obj.getId()] = obj;
}

ReferencableObjectRegistry.prototype.deregister = function (obj) {
  delete this.references[obj.getId()]
}

ReferencableObjectRegistry.prototype.lookup = function (ref) {
  return this.references[ref];
}

function ReferencableObject() {
}

ReferencableObject.prototype.init = function (id) {
  if (arguments.length == 0)
    return;
  this.id = id
  this.inithooks = []
  this.didInit = false    
}

ReferencableObject.prototype.postInit = function () {
  var precached = stealPrecachedReferencableObject(this.id)
  if (precached != null)
    this.doSync(precached)
  else
    this.update();
}

ReferencableObject.prototype.getId = function () {
  return this.id;
}

ReferencableObject.prototype.addInitHook = function (func) {
  if (this.didInit) {
    errorLog.reportEvent("object " + this + " already initialized, invoking init callback immediately")    
    func.call()
  } else {
    this.inithooks.push(func)
  }
}

ReferencableObject.prototype.doSync = function (contents) {
  this.sync(contents);
  errorLog.reportEvent("synchronizing object: " + this)
}

ReferencableObject.prototype.sync = function (contents) {
  if (!this.didInit) {
    errorLog.reportEvent("calling init hooks for object: " + this)  
    for (i = 0; i < this.inithooks.length; i++) {
      this.inithooks[i].call()
    }
    this.inithooks = null
    this.didInit = true    
  }
}

ReferencableObject.prototype.dumpObjectsResult = function (result, err) {
  if (err) {
    errorLog.reportError('dumpObjects failed: ' + err);
    return;
  }
  try {
    var contents = result[0]
    // Ignore objects that went away for now
    if (contents == "UnknownReferenceException")
      return;
    errorLog.reportEvent("dumpObjects returned" + result + ", " + err);
    this.doSync(result[0]);

  } catch (e) {
    errorLog.reportError('Synchronization of object ' + this.getId() + ' failed: ' + e);
  }
}

ReferencableObject.prototype.update = function() {
  var obj = this;
  yarrr.dumpObjects([this.getId()], function (result, err) { obj.dumpObjectsResult(result, err); });
}

ReferencableObject.prototype.toString = function () {
  return this.getId();
}

objectRegistry = new ReferencableObjectRegistry()

function Yarrr(context, clientInstanceId) {
  this.context = context;
  this.clientInstanceId = clientInstanceId;
  this.pollScript = null;
  this.pollState = 0;
  this.gotUpdatePoll = false;

  this.pollCounter = 0;
  var yarrrInst = this;
  window.pollDone = function (cnt) { 
    try {
      if (cnt == yarrrInst.pollCounter) {
        yarrrInst.gotPollReturn();
      }
    } catch (e) {
      errorLog.reportError("pollDone: " + e);
    }
  };
}

Yarrr.prototype.gotChanges = function (result, err) {
  if (this.verifyState(5, 2))
    return;
    
  if (err) {
    errorLog.reportError("getPollChanges failed: " + err);
    return;
  }
    
  try {
    var changes = result.changes;

    for (var i = 0; i < changes.length; i++) {
      var changedobj = objectRegistry.lookup(changes[i]);
      // We can actually get signals for objects we haven't seen yet
      // It is safe to ignore them
      if (changedobj != null)
        changedobj.update();
    }
    
    this.gotPollUrl (result.pollUrl, null);
  } catch (e) {
    errorLog.reportError("poll gotChanges: " + e);
  }
}

Yarrr.prototype.getChanges = function () {
  if (this.verifyState(4, 5))
    return;
    
  var yarrrInst = this;
  yarrr.getPollChanges(this.context, this.clientInstanceId, function (result, err) { yarrrInst.gotChanges (result, err); });
}

Yarrr.prototype.gotPollReturn = function () {
  if (this.verifyState(3, 4))
    return;

  try {    
    this.pollCounter = this.pollCounter + 1;
    document.body.removeChild(this.pollScript);
    this.pollScript = null;
    this.getChanges();
  } catch (e) {
    errorLog.reportError("gotPollReturn: " + e);
  }
}

Yarrr.prototype.updatePoll = function() {
  try {
    if (this.pollScript != null) {
      this.gotPollReturn();
    } else if (this.pollState == 2) {
      // We're getting the poll url already, but the interested list might have changed
      // since then (which is why updatePoll was called) so we immediately get changes
      this.gotUpdatePoll = true;
    }
  } catch (e) {
    errorLog.reportError ("updatePoll: " + e);
  }
}

Yarrr.prototype.gotPollUrl = function (result, err) {
  if (this.verifyState(2, 3))
    return;
    
  if (err) {
    errorLog.reportError("getPollURL failed: " + err);
    return;
  }
    
  try {  
    var yarrrInst = this;
    var url = result;

    if (url == "" || this.gotUpdatePoll) {
      // No polling, get changes immediately
      this.gotUpdatePoll = false;
      this.pollState = 4;
      this.getChanges();
    } else {
      var script = document.createElement("script");
      script.setAttribute("type", "text/javascript");
      script.setAttribute("src", url + "?" + this.pollCounter);
      document.body.appendChild(script);
      this.pollScript = script;
    }
  } catch (e) {
    errorLog.reportError("gotPollUrl: " + e);
  }
}

Yarrr.prototype.getPollUrl = function() {
  if (this.verifyState(1, 2))
    return;
    
  try {
    var yarrrInst = this;
    yarrr.getPollUrl(this.context, this.clientInstanceId, function (result, err) { yarrrInst.gotPollUrl (result, err); });
  } catch (e) {
    errorLog.reportError('Poll failed: ' + e);
  }
}

// Start the poll loop
Yarrr.prototype.poll = function() {
  if (this.verifyState(0, 1))
    return;
  
  this.getPollUrl();
}

Yarrr.prototype.verifyState = function(expectedState, newState) {
  if (this.pollState != expectedState) {
    errorLog.reportError("Wrong pollState " + this.pollState + ", expecting " + expectedState);
    return true;
  }
  this.pollState = newState;
  return false;  
}
