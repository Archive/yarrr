<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:url var="topref" value="/"/>

<html>
<head>
<title>Unknown topic: <c:out value="${topictitle}" escapeXml="true"/></title>
</head>
<body onload="init()">

<script type="text/javascript" src="${topref}/client.js"></script>
<script type="text/javascript">
function init() {
  yarrrInitXmlRpc("/yarrr/execute");
  initYarrrPerson("${anonymousPerson}", "${person}");
  var buttondiv = document.getElementById("createtopicbutton")
  var input = document.createElement("input")
  input.setAttribute("type", "button")
  input.setAttribute("value", "Create this topic")
  input.onclick = function() { createTopic(${JStopictitle}, "${topref}"); };
  buttondiv.appendChild(input);
}
</script>

<h3>Unknown topic <samp><c:out value="${topictitle}" escapeXml="true"/></samp></h3>

<a href="${topref}">All Topics</a><br/>
<div id="createtopicbutton" class="createtopicbutton"></div>

</body>
</html>