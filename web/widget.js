//--------------------------- Helper functions-------------------------

function getAbsolutePos(e) {
  var pos = { x: 0, y: 0 };
  while (e != null) {
    pos.x += e.offsetLeft;
    pos.y += e.offsetTop;
    e = e.offsetParent;
  }
  return pos;
};

function insertAfter(parent, node, after) {
  var before;
  if (after == null)
    before = parent.firstChild;
  else
    before = after.nextSibling;

  if (before != null)
    parent.insertBefore(node, before);
  else
    parent.appendChild(node);
}


function objectClass(obj) {
  return obj.constructor;
}

function superClass(klass) {
  return klass.prototype.parentClass;
}

var widgetImageDir = "";

function setWidgetImageDir(dir) {
  widgetImageDir = dir;
}

function getWidgetImageFile(image) {
  if (image == null)
    return null;
  if (image.substr(0, 3) == "../" ||
      image.substr(0, 2) == "./" ||
      image.substr(0, 1) == "/" ||
      image.substr(0, 5) == "http:") {
    return image;
  }
  return widgetImageDir + image;
}


//------------------------------ Widget ------------------------------


function Widget() {
  this.initialize();
}

Widget.widgetCount = 0;
Widget.prototype.parentClass = null;
Widget.prototype.signals = [];

Widget.prototype.parent = null;
Widget.prototype.node = null;

Widget.prototype.init = function() {
  this.data = new Object();
}

Widget.prototype.instanceOf = function (otherClass) {
   var klass = objectClass(this);
   while (klass != null) {
      if (klass == otherClass)
         return true;
      klass = superClass(klass);
   }
   return false;
}

Widget.prototype.initialize = function(klass) {
  var inits = [];
  klass = this.constructor;
  while (klass != null) {
    if ("init" in klass.prototype) {
      var method = klass.prototype["init"];
      if (typeof(method) == "function") {
        inits.push(method);
      }
    }
    klass = superClass(klass);
  }
  for (var i = 0; i < inits.length; i++)
    inits[i].call(this);
}


Widget.prototype.callParent = function(klass, methodname) {
  klass = superClass(klass);
  while (klass != null) {
    if (methodname in klass.prototype) {
      var method = klass.prototype[methodname];
      if (typeof(method) == "function") {
        var args = new Array();
	for (var i = 2; i < arguments.length; i++)
	  args.push(arguments[i]);
        return method.apply(this, args);
      }
    }
    klass = superClass(klass);
  }
  alert("No parent method " + methodname);
}


Widget.prototype.realize = function () {
  this.node = document.createElement("div");
}

Widget.prototype.isRealized = function () {
  return this.node != null;
}

Widget.prototype.setParent = function (parent) {
  this.parent = parent;
}

Widget.prototype.getId = function () {
  if (!('id' in this)) {
    Widget.widgetCount++;
    this.id = "_W" + Widget.widgetCount;
  }
  return this.id;
}

Widget.prototype.emit = function (signal /*, args... */) {
  var funcname = "on"+signal;
  
  if (funcname in this && typeof(this[funcname]) == "function") {
    var args = new Array();
    for (var j = 1; j < arguments.length; j++)
      args.push(arguments[j]);
    this[funcname].apply(this, args);
  }

  if (this.handlers != null && signal in this.handlers) {
    var calls = this.handlers[signal];
    for (var i=0; i < calls.length; i++) {
      var call = calls[i];

      var thisobj = this;
      if (call.methodObj != null)
        thisobj = call.methodObj;
	
      var args = new Array();
      args.push(this);
      for (var j = 1; j < arguments.length; j++)
        args.push(arguments[j]);
      args.push(call.data);

      call.func.apply(thisobj, args);
    }
  }
}

Widget.prototype.hasSignal = function (signal) {
  var klass = objectClass(this);
  while (klass != null) {
    if ("signals" in klass.prototype) {
      for (var i = 0; i < klass.prototype.signals.length; i++) {
        if (klass.prototype.signals[i] == signal)
          return true;
      }	  
    }
    klass = superClass(klass);
  };
  
  return false;
}

Widget.prototype.connectMethod = function (signal, object, method, data) {
  this.connect(signal, method, data, object);
}


Widget.prototype.connect = function (signal, func, data, methodObj) {
  if (!this.hasSignal(signal))
    alert("object has no signal named " + signal);
    
  if (this.handlers == null)
    this.handlers = new Object();

  if (!(signal in this.handlers))
    this.handlers[signal] = new Array();

  var call = new Object();
  call.func = func;
  call.data = data;
  call.methodObj = methodObj;

  this.handlers[signal].push(call);
}

Widget.prototype.addToDocument = function (docNode) {
  if (typeof(docNode) == "string")
    docNode = window.document.getElementById(docNode);

  if (!this.isRealized()) {
    this.realize();
  }

  docNode.appendChild(this.node);
}


//------------------------------ Container ------------------------------

function Container() {
  this.initialize();
}

Container.prototype = new Widget();
Container.prototype.constructor = Container;
Container.prototype.parentClass = Widget;
Container.prototype.signals = [];

Container.prototype.children = [];

Container.prototype.init = function (child) {
  children = new Array();
}

Container.prototype.add = function (child) {
  child.setParent(this);
  this.children.push(child);
  if (this.isRealized()) {
    child.realize();
    this.node.appendChild(child.node);
  }
}

Container.prototype.realize = function () {
  this.callParent(Container, "realize");
  
  for (var i = 0; i < this.children.length; i++) {
    var child = this.children[i];
    if (!child.isRealized()) {
      child.realize();
      this.node.appendChild(child.node);
    }
  }
}

