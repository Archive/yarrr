function Comment(id, container) {
  if (arguments.length > 0) {
    this.init(id, container) 
  }
}

Comment.prototype = new ReferencableObject();
Comment.prototype.constructor = Comment;
Comment.superclass = ReferencableObject.prototype;

Comment.prototype.init = function (id, container) {
  Comment.superclass.init.call(this, id)
  this.editedTimeoutObj = null;

  this.callActive = false;
  this.sendScheduled = false;
  this.getScheduled = false
  
  this.sendingVersion = undefined;
  this.sendingText = "";
  
  this.name = "";

  // We know the initial version is always 0, and its empty
  this.oldVersion = 0;

  this.container = container;
  var div = document.createElement("div");
  this.rootDiv = div;
  div.setAttribute("class", "comment");
  this.container.appendChild(div);
  
  /* Main area */
  var editorEnabled = !getYarrrPerson().isAnonymous();
  this.editor = new PlainEditor(editorEnabled);
  // Temporarily disable RichEditor -- it only works on Deer Park right now
  //this.editor = new RichEditor(editorEnabled);
  this.editor.setColorMap(this.hiliteColors);
  this.editor.appendTo(div);
  var comment = this;
  this.editor.setChangedCallback(function() { comment.editorChanged(); });
  
  this.savingNotification = document.createElement("div")
  this.savingNotification.appendChild(document.createTextNode("Saving started by "))
  this.savingNotificationPerson = document.createElement("span")
  this.savingNotification.appendChild(this.savingNotificationPerson)
  this.savingNotification.style.display = "none";
  div.appendChild(this.savingNotification)

  /* Footer */
  div.appendChild(document.createElement("br"))
  var footer = document.createElement("span")
  footer.setAttribute("class", "commenttitle")
  this.openedBy = document.createTextNode("Opened by ")
  footer.appendChild(this.openedBy)
  div.appendChild(footer);
  
  this.openerElt = document.createElement("span")
  this.openerElt.setAttribute("class", "commentopener")
  div.appendChild(this.openerElt)
  div.appendChild(document.createTextNode(", authors: "))
  this.authorsElt = document.createElement("span")
  this.authorsElt.setAttribute("class", "commentauthors")
  div.appendChild(this.authorsElt)
  
  div.appendChild(document.createElement("br"))
  
  this.closebutton = document.createElement("input")
  this.closebutton.setAttribute("type", "button")
  this.closebutton.setAttribute("value", "Close")
  div.appendChild(this.closebutton)
  this.closebutton.style.display = "none";
  if (getYarrrPerson().isAnonymous())
    this.closebutton.setAttribute("disabled", true);
  
  this.cancelbutton = document.createElement("input")
  this.cancelbutton.setAttribute("type", "button")
  this.cancelbutton.setAttribute("value", "Cancel")
  div.appendChild(this.cancelbutton)
  this.cancelbutton.style.display = "none";
  if (getYarrrPerson().isAnonymous())
    this.cancelbutton.setAttribute("disabled", true);
  
  // We haven't got the initial version yet
  this.uninitialized = true;
  var comment = this;

  this.closebutton.onclick = function() { comment.close(); return false; };
  this.cancelbutton.onclick = function() { comment.cancel(); return false; };
  
  Comment.superclass.postInit.call(this)  
}

// Signal
Comment.prototype.oncancel = function () { }

Comment.prototype.appendText = function (text, newLine) {
  this.editor.appendText(text, 0, newLine);
}

Comment.prototype.focus = function() {
  this.editor.focus();
}

Comment.prototype.gotNewVersion = function(newVersion, diff, localChangesAgainst) {
  errorLog.reportEvent("gotNewVersion (" + newVersion + "," + diff_to_string(diff) + ", " + localChangesAgainst + ") - oldVersion=" + this.oldVersion);
  if (!this.uninitialized && newVersion <= this.oldVersion) {
    // We already know about this version
    return;
  }

  // We now have some data now
  this.uninitialized = false;
  this.oldVersion = newVersion;

  this.editor.applyPatch(diff, localChangesAgainst);
}

Comment.prototype.sendReply = function(result, err) {
  errorLog.reportEvent("updateLiveComment returned version: " + result.toString() + ", err: " + err);
  if (result.length == 0) {
    // There was a failure applying the patch we sent.
    // We schedule an update to get the new state. 
    // The local changes that didn't apply will be applied
    // or thrown away in the update handler.
    this.update();
    this.callFinished();
    return;
  }
    
  var newVersion = result[0];
    
  if (newVersion <= this.oldVersion) {
    errorLog.reportEvent("updateLiveComment reply - already know about version");
    // We already know about this version
    this.callFinished();
    return;
  }

  var realDiff = result[1];
  expand_diff_tags(realDiff);

  errorLog.reportEvent("updateLiveComment reply - got: " + newVersion +", " + ( (realDiff == null) ? "null" : diff_to_string(realDiff) ) );
    
  if (newVersion == this.sendingVersion + 1) {
    // We now know the current comment contents
    var res = patch_tagged_string(this.sendingAgainstText, this.sendingAgainstColors, realDiff, null);
    this.editor.setLastState(res.text, res.tags);
    this.oldVersion = newVersion;
  } else {
    errorLog.reportEvent("updateLiveComment reply - was not expected version");
      
    // realDiff is now the diff against sendingText/sendingVersion
    if (realDiff == null) {
      //FIXME: handle the case where old version wasn't saved in the server
      errorLog.reportError('base version missing')
    }

    if (this.oldVersion != this.sendingVersion) {
       var res = patch_tagged_string(this.sendingAgainstText, this.sendingAgainstColors, realDiff, null);
       realDiff = diff_tagged_strings(this.editor.getLastText(), this.editor.getLastColors(), res.text, res.tags);
       errorLog.reportEvent("recalculated diff against version " + this.oldVersion + ": " + diff_to_string(realDiff));
    }
      
    errorLog.reportEvent("updateLiveComment reply - calling gotNewVersion()");
    this.gotNewVersion(newVersion, realDiff, this.sendingText);
  }
    
  this.callFinished();
}

Comment.prototype.send = function() {
  if (this.editor.hasChanges()) {
    diff = this.editor.getChanges();
    this.sendingText = this.editor.getText();
    this.sendingAgainstText = this.editor.getLastText();
    this.sendingAgainstColors = this.editor.getLastColors();
    this.sendingVersion = this.oldVersion;
    var comment = this;
    errorLog.reportEvent("Comment.send: calling updateLiveComment with diff \"" + diff + "\"");    
    yarrr.updateLiveComment(this.id, diff, this.oldVersion, 
             function(result, err) { comment.sendReply(result,err); } );
   } else {
      errorLog.reportEvent("Comment.send: no changes");       
     this.callFinished();
   }
}

Comment.prototype.cancel = function() {
  // Emit the signal
  this.oncancel();
}

Comment.prototype.hiliteColors = ["#ddffd7", "#ffebd7", "#fcffd7", "#e4d6ff"];
Comment.prototype.textColors =   ["#66e848", "#ff9a03", "#f5ee23", "#7306fd"];

Comment.prototype.getHiliteColor = function(index) {
  if (index < this.hiliteColors.length)
    return this.hiliteColors[index];
    
  return this.hiliteColors[this.hiliteColors.length-1];
}

Comment.prototype.getTextColor = function(index) {
  if (index < this.textColors.length)
    return this.textColors[index];
    
  return this.textColors[this.textColors.length-1];
}


Comment.prototype.setOpener = function(opener) {
  this.openerElt.innerHTML = ""
  this.opener = opener
  var text = document.createTextNode(this.opener)
  var subspan = document.createElement("span")
  
  if (opener in this.struck) {
    subspan.style["textDecoration"] = "line-through";
  } 
  
  subspan.style.color = this.getTextColor(0);
  
  subspan.appendChild(text)
  this.openerElt.appendChild(subspan)
}

Comment.prototype.addAuthors = function(authors) {
  this.authorsElt.innerHTML = ""

  for (var i = 0; i < authors.length; i++) {
    var author = authors[i]
    var text = document.createTextNode(author);
    var subspan = document.createElement("span")
    subspan.setAttribute("class", "commentauthor")
    
    if (author in this.struck) {
      subspan.style["textDecoration"] = "line-through";
    } 
    subspan.style.color = this.getTextColor(i+1);
    
    subspan.appendChild(text)
    this.authorsElt.appendChild(subspan)
    if (i < authors.length - 1) {
      this.authorsElt.appendChild(document.createTextNode(", "))
    }
  }
}

Comment.prototype.getReply = function(results, err) {
  if (err) {
    errorLog.reportError('Comment getReply failed: ' + err)
    return;
  }
  try {
    errorLog.reportEvent("getLiveComment returned: " + results + ", err: " + err);
    result = results[0];
    
    this.sync(result) ;

    this.callFinished();
  } catch (e) {
    errorLog.reportError('getLiveComment failed: ' + e)  
  }
}

Comment.prototype.sync = function(content) {
  content.tags = expand_tag_string(content.tags);

  // Figure out my index so we can avoid that color
  var me = getYarrrPerson().getName();
  var mycolor = -1;
  if (content.opener == me)
    mycolor = 0;
  for (var i = 0; i < content.authors.length; i++) {
    if (content.authors[i] == me) {
      mycolor = i+1;
      break;
    }
  }
  if (mycolor != -1)
    this.editor.setIgnoreColor(mycolor);
  
  if ("name" in content)
    this.name = content.name;
    
  var diff = diff_tagged_strings(this.editor.getLastText(), this.editor.getLastColors(), content.text, content.tags)
  this.gotNewVersion(content.version, diff, null);
  this.struck = getArrayAsObject(content.struckAuthors)
  this.setOpener(content.opener);
  this.addAuthors(content.authors);
  var authors = getArrayAsObject(content.authors)

  
  if (((me in authors) && !(me in this.struck)) || (me == this.opener)) {
    this.closebutton.style.display = "inline";
    this.cancelbutton.style.display = "inline";
  }

  Comment.superclass.sync.call(this, content)  
}

Comment.prototype.get = function() {
  errorLog.reportEvent("get()");
  var comment = this;

  if (true || this.uninitialized) {
    this.updating = true;
    errorLog.reportEvent("calling getLiveComment");
    yarrr.dumpObjects([this.id], 
                        function(result, err) { comment.getReply(result, err); } );
  } else {
    //FIXME: handle incremental updates.
  }                        
}

Comment.prototype.scheduleSend = function() {
  if (this.callActive) {
    this.sendScheduled = true;
  } else {
    this.callActive = true;
    this.send();
  }
}

Comment.prototype.update = function() {
  if (this.callActive) {
    this.getScheduled = true;
  } else {
    this.callActive = true;
    this.get();
  }
}

Comment.prototype.callFinished = function() {
  this.callActive = false;
  if (this.sendScheduled && !this.uninitialized) {
    this.sendScheduled = false;
    this.callActive = true;
    this.send();
  } else if (this.getScheduled) {
    this.getScheduled = false;
    this.callActive = true;
    this.get();
  }
}

Comment.prototype.onclose = function() {}

Comment.prototype.close = function() {
  this.onclose();
  this.remove();
}

Comment.prototype.onremove = function() {}

Comment.prototype.remove = function() {
  if (this.rootDiv != null) {
    this.container.removeChild(this.rootDiv);
    this.rootDiv = null;
    deregisterObject(this);
  }
  this.onremove();
}

Comment.prototype.onNeedReload = function() { }

Comment.prototype.needReload = function () {
  this.onNeedReload();
}

Comment.prototype.editorChanged = function() {
  this.scheduleSend();
}

