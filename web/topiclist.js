function YarrrSummary(id) {
  this.id = id
  this.activetopics = {}
  this.inactivetopics = {}
  
  var precached = stealPrecachedReferencableObject(this.id)
  if (precached != null) {
    this.sync(precached)
  } else {
    this.update()
  }
}

YarrrSummary.prototype = new ReferencableObject()

function handleObjectDeltaFromDict(curobjects, newobjects) {
  var deleted = []
  for (var obj in curobjects) {
    if (!(obj in newobjects)) {
      deleted.push(obj)
    }
  }
  for (var i = 0; i < deleted.length; i++) {
    var id = deleted[i]
    var obj = objectRegistry.lookup(id)
    if (obj != null) {
      obj.remove();
      objectRegistry.deregister(obj)
    }
  }
}

YarrrSummary.prototype.sync = function (content) {
  // alert(dumpObjectKeysToString(content))
  for (var i = 0; i < content.activetopics.length; i++) {
    var id = content.activetopics[i];
    if (!(id in this.activetopics)) {
      this.activetopics[id] = new ActiveTopicSummary(id);
      registerObject(this.activetopics[id])
    }
  }
  handleObjectDeltaFromDict(this.activetopics, getArrayAsObject(content.activetopics))
 
  for (var i = 0; i < content.inactivetopics.length; i++) {
    var id = content.inactivetopics[i];
    if (!(id in this.inactivetopics)) {
      this.inactivetopics[id] = new InactiveTopicSummary(id);
      registerObject(this.inactivetopics[id])
    }
  }
  handleObjectDeltaFromDict(this.inactivetopics, getArrayAsObject(content.inactivetopics))
}

function ActiveTopicSummary(id) {
  this.id = id;
  this.closedcomments = {};
  this.statements = {};
  this.discussions = {};
  this.whiteboards = {};
  this.presentPersons = {};
  this.presentPersonNames = [];
  
  var topicsummaries = document.getElementById("topicList")
  var ul = document.getElementById("topicListUL")
  this.li = document.createElement("li")
  ul.appendChild(this.li)
  this.summary_div = document.createElement("div")
  this.summary_div.setAttribute("class", "ActiveTopicSummary")
  this.li.appendChild(this.summary_div)
  
  // Title
  this.title_span = document.createElement("span")
  this.title_span.setAttribute("class", "ActiveTopicSummaryTitle")
  this.summary_div.appendChild(this.title_span)  
  
  // Comment count
  this.summary_div.appendChild(document.createTextNode("  "))
  this.commentcount = document.createElement("span")
  this.commentcount.setAttribute("class", "ActiveTopicSummaryCommentCount")
  this.summary_div.appendChild(this.commentcount)
  this.summary_div.appendChild(document.createTextNode(" comments"))
  
  // Discussion count
  this.summary_div.appendChild(document.createTextNode(", "))
  this.discussioncount = document.createElement("span")
  this.discussioncount.setAttribute("class", "ActiveTopicSummaryDiscussionCount")
  this.summary_div.appendChild(this.discussioncount)
  this.summary_div.appendChild(document.createTextNode(" discussions"))
  
  // Present persons
  this.summary_div.appendChild(document.createTextNode("; Present: "))
  this.presentpersons_span = document.createElement("span")
  this.presentpersons_span.setAttribute("class", "ActiveTopicSummaryPresentPersons")
  this.summary_div.appendChild(this.presentpersons_span)
  
  var precached = stealPrecachedReferencableObject(this.id)
  if (precached != null) {
    this.sync(precached)
  } else {
    this.update()
  }
}

ActiveTopicSummary.prototype = new ReferencableObject();

ActiveTopicSummary.prototype.sync = function (content) {
  this.title_span.innerHTML = "";
  var hyperlink = document.createElement("a")
  hyperlink.setAttribute("href", "topic/" + content.name)
  hyperlink.appendChild(document.createTextNode(content.name))
  this.title_span.appendChild(hyperlink);

  this.presentpersons_span.innerHTML = ""; 
  for (var i = 0; i < content.presentpersons.length; i++) {
    var person = content.presentpersons[i]
    var personelt = document.createElement("span")
    personelt.setAttribute("class", "presentperson")
    personelt.appendChild(document.createTextNode(person))
    this.presentpersons_span.appendChild(personelt)
    if (i < content.presentpersons.length - 1) {
      this.presentpersons_span.appendChild(document.createTextNode(", "))
    }
  }
  
  this.commentcount.innerHTML = "";
  this.commentcount.appendChild(document.createTextNode(content.closed.length))

  this.discussioncount.innerHTML = "";
  this.discussioncount.appendChild(document.createTextNode(content.discussions.length))
}

ActiveTopicSummary.prototype.remove = function () {
  var ul = document.getElementById("topicListUL")
  ul.removeChild(this.li)
}

function InactiveTopicSummary(id) {
  this.id = id;
  
  var topicsummaries = document.getElementById("inactiveTopicList")
  var ul = document.getElementById("inactiveTopicListUL")
  this.li = document.createElement("li")
  ul.appendChild(this.li)
  this.summary_div = document.createElement("div")
  this.summary_div.setAttribute("class", "InactiveTopicSummary")
  this.li.appendChild(this.summary_div)
  
  // Title
  this.title_span = document.createElement("span")
  this.title_span.setAttribute("class", "InactiveTopicSummaryTitle")
  this.summary_div.appendChild(this.title_span)  
  
  this.summary_div.appendChild(document.createTextNode("  "))
  this.commentcount = document.createElement("span")
  this.commentcount.setAttribute("class", "InactiveTopicSummaryCommentCount")
  this.summary_div.appendChild(this.commentcount)
  this.summary_div.appendChild(document.createTextNode(" comments"))
  
  var precached = stealPrecachedReferencableObject(this.id)
  if (precached != null) {
    this.sync(precached)
  } else {
    this.update()
  }
}

InactiveTopicSummary.prototype = new ReferencableObject();

InactiveTopicSummary.prototype.sync = function (content) {
  this.title_span.innerHTML = "";
  var hyperlink = document.createElement("a")
  hyperlink.setAttribute("href", "topic/" + content.name)
  hyperlink.appendChild(document.createTextNode(content.name))
  this.title_span.appendChild(hyperlink);
  
  this.commentcount.innerHTML = "";
  this.commentcount.appendChild(document.createTextNode(content.closedCommentsCount))
  
}

InactiveTopicSummary.prototype.remove = function () {
  var ul = document.getElementById("inactiveTopicListUL")
  ul.removeChild(this.li)
}