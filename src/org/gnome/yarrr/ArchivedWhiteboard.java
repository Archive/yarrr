package org.gnome.yarrr;

import java.io.Serializable;


public class ArchivedWhiteboard  {
    private long id;
    private ArchivedImage foreground;
    private ArchivedImage foregroundThumbnail;
    private ArchivedImage background;
    private ArchivedImage backgroundThumbnail;
    
    ArchivedWhiteboard() {
    }
    
    public ArchivedWhiteboard(ArchivedImage foreground, ArchivedImage foregroundThumbnail, 
                              ArchivedImage background, ArchivedImage backgroundThumbnail) {
        
        this.foreground = foreground;
        this.foregroundThumbnail = foregroundThumbnail;
        this.background = background;
        this.backgroundThumbnail = backgroundThumbnail;
        
        HibernateUtil.getSession().save(this);
    }

    public long getId() {
        return id;
    }

    private void setId(long id) {
        this.id = id;
    }

    public ArchivedImage getForeground() {
        return foreground;
    }
    
    public ArchivedImage getForegroundThumbnail() {
        return foregroundThumbnail;
    }

    public ArchivedImage getBackground() {
        return background;
    }
    
    public ArchivedImage getBackgroundThumbnail() {
        return backgroundThumbnail;
    }
    
   /**
     * This is a reference to a db object. Its useful if you need to store a sketch
     * reference in a place where it lives longer than a hibernate session. 
     * 
     * @author alex
     */
    public static class Ref implements Serializable {
        private static final long serialVersionUID = 1L;
        private long id;

        public Ref(ArchivedWhiteboard wb) {
            id = wb.getId();
        }
        
        public Long getId() {
            return new Long(id);   
        }
        
        public ArchivedWhiteboard load() {
            return ArchivedWhiteboard.load(id);
        }
        
        public boolean equals(Object obj) {
            if (obj instanceof Ref) {
                Ref other = (ArchivedWhiteboard.Ref) obj;
                
                return other.id == this.id;
            }
            return false;
        }
        public int hashCode() {
            return (int)id;
        }
        public String toString() {
            return "ArchivedWhiteboard.Ref(" + id + ")";
        }
    }
    
    public Ref getRef() {
        return new Ref(this);
    }
    
    public static ArchivedWhiteboard load(Long id) {
        return (ArchivedWhiteboard)HibernateUtil.getSession().get(ArchivedWhiteboard.class, id);
    }
    
    public static ArchivedWhiteboard load(long id) {
        return load(new Long(id));
    }

}
