/*
 * Created on May 6, 2005
 *
 */
package org.gnome.yarrr;

import java.awt.Graphics2D;

/**
 * @author alex
 */
public abstract class SketchOp {
    abstract void draw(Graphics2D g);
};
