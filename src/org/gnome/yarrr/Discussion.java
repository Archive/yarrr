/*
 * Created on Mar 24, 2005
 */
package org.gnome.yarrr;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.gnome.yarrr.ArchivedImage.ImageTooLargeException;
import org.gnome.yarrr.xmlrpc.XmlRpcSerializable;

/**
 * @author walters
 */
public class Discussion extends ReferencableObject implements XmlRpcSerializable {

	static Log logger = LogFactory.getLog(Discussion.class);
	
	private Person.Ref creator;
	private boolean isDefault;
	private String title;
	private Date creationDate;
	private Map /* String,LiveComment */ comments;
    private List /* Whiteboard*/ whiteboards;
	private Chat chat;
    private ActiveTopic topic;
	
	public Discussion(ActiveTopic topic, Person creator, String title) {
        this.topic = topic;
		this.creator = creator.getRef();
		this.title = title;
		this.isDefault = false;
		this.creationDate = new Date();
		this.comments = new HashMap();
        this.whiteboards = new Vector();
		this.chat = new Chat();
        ReferencableObjectRegistry.register(this);
        topic.addMonitor(this.chat);
	}
	
    /* Comment-related methods: */
	public synchronized LiveComment openComment(Person author, String name, String contents) {
        LiveComment comment;        
        comment = (LiveComment) this.comments.get(name);
        if (comment != null) {
            comment.setStateEditing(author, contents);            
            return comment;
        }
		comment = new LiveComment(this, author, name, contents);
		this.comments.put(name, comment);
        topic.addMonitor(comment);
		signalChanged();
		return comment;
	}
	
	public synchronized void removeComment(LiveComment comment, Person closer) {
		assert(this.comments.values().contains(comment));
		this.comments.remove(comment);
        topic.removeMonitor(comment);
        comment.delete();
		signalChanged();
	}
	
	protected synchronized void deleteComment(LiveComment comment) {
		this.comments.remove(comment.getName());
        topic.removeMonitor(comment);
		comment.delete();
		signalChanged();
	}
	
	public synchronized List /* LiveComment */ getComments() {
		return new Vector(comments.values());
	}
    
    /* Whiteboard-related methods: */
    public synchronized Whiteboard createWhiteboard(Person whiteboardCreator) {
        Whiteboard w = new Whiteboard(whiteboardCreator, 640, 480);
        this.whiteboards.add(w);
        topic.addMonitor(w);
        this.signalChanged();        
        return w;
    }
    
    public synchronized Whiteboard uploadFile(Person whiteboardCreator, BufferedImage image) throws IOException, ImageTooLargeException {
        Whiteboard w = new Whiteboard(whiteboardCreator, image);
        topic.addMonitor(w);
        this.whiteboards.add(w);
        this.signalChanged();        
        return w;        
    }
    
    
    public synchronized List /*Whiteboard */ getWhiteboards() {
        return new Vector(whiteboards);
    }    
	
	public Chat getChat() {
		return this.chat;
	}
	
	public synchronized void xmlRpcSerialize(Map table) {
	    super.xmlRpcSerialize(table);
		table.put("chat", chat);
		table.put("title", this.title);
		table.put("isDefault", new Boolean(this.isDefault));
		table.put("creator", this.creator.load().toString());
		table.put("creationDate", this.creationDate);
		table.put("comments", this.getComments());
        table.put("whiteboards", whiteboards);        
	}

	public synchronized String getTitle() {
		return title;
	}
	
	public synchronized void setTitle(String title) {
		if (this.isDefault()) {
			throw new IllegalArgumentException("Cannot set title of default discussion");
		}
		this.title = title;
		this.signalChanged();
	}
    
	public synchronized boolean isDefault() {
		return isDefault;
	}
    
	public synchronized void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

	public synchronized boolean deAuthorLiveComment(LiveComment comment, Person deAuthor) {
		boolean destroyed = comment.deAuthor(deAuthor);
		if (destroyed) {
			this.deleteComment(comment);
		}
		return destroyed;
	}

    public LiveComment getComment(String commentName) {
        return (LiveComment) this.comments.get(commentName);
    }
}
