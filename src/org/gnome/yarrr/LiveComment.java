/*
 * Created on Mar 16, 2005
 */
package org.gnome.yarrr;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.gnome.yarrr.xmlrpc.XmlRpcHandler;
import org.gnome.yarrr.xmlrpc.XmlRpcMarshaller;
import org.gnome.yarrr.xmlrpc.XmlRpcSerializable;

/**
 * @author alex
 */
public class LiveComment extends ReferencableObject implements XmlRpcSerializable {
	private final int MAX_OLD_VERSIONS = 10;

	static class VersionAndDiff implements XmlRpcMarshaller {
		int version; // The current version, -1 to return error 
		TaggedTextDiff diff; // patch to this version
		
		public VersionAndDiff(int version, TaggedTextDiff diff) {
			this.version = version;
			this.diff = diff;
		}
        
        /**
         * Create "error" object
         */
        public VersionAndDiff() {
            this.version = -1;
            this.diff = null;
        }
        
		public Object xmlRpcMarshal(XmlRpcHandler handler) {
			Vector res = new Vector();
			
            if (version != -1) {
                res.add(new Integer(version));
                if (diff != null)
                    res.add(diff);
            }
			return res;
		}
	}
	
	static class EditingConflictException extends Exception {
		private static final long serialVersionUID = 1L;
	}
	
	static class TooOldVersionException extends Exception {
		private static final long serialVersionUID = 1L;
	}

    private static class OldVersion {
        int version;
        TaggedText contents;
    }
    
    private int version;
	private Date startTime;
	private Person.Ref opener; // tag zero in content
	private ArrayList /* Person.Ref */ authors; // order used as content tags, first element is 1.
	private Set /* Person.Ref */ struckAuthors;
	private TaggedText contents;
    private static final String EDITING_STATE = "editing";
    private static final String SAVING_STATE = "saving";
    private static final String SAVE_COMPLETE_STATE = "save-complete";    
    private String state;
	
	LinkedList /* OldVersion */ oldVersions;
    private String name; /* Separate mapping identifier (other than the reference); used for
                            wiki embedding */
    private Person.Ref saver;

	public LiveComment(Discussion discussion, Person opener, String name, String contents) {
		this.startTime = new Date();
        this.name = name;
		this.authors = new ArrayList();
		this.struckAuthors = new HashSet();
		this.opener = opener.getRef();
		setContents(contents);
        this.state = EDITING_STATE;
        ReferencableObjectRegistry.register(this);
	}	
	
    synchronized int getVersion() {
        return version;
    }
    
	synchronized void saveOldVersion() {
		if (oldVersions.size() >= MAX_OLD_VERSIONS) {
			oldVersions.removeFirst();
		}
        OldVersion old = new OldVersion();
        old.version = version;
        old.contents = contents;
		oldVersions.add(old);
	}

	synchronized private OldVersion getOldVersion(int oldVersion) {
		for (Iterator i = oldVersions.iterator(); i.hasNext(); ) {
			OldVersion o = (OldVersion) i.next();
			if (o.version == oldVersion)
				return o;
		}
		return null;
	}

	synchronized public String getText() {
		return contents.getText();
	}

    /**
     * Get the opener of the comment
     * @return aPerson, from the current hibernate session
     */
    synchronized public Person getOpener() {
        return opener.load();
    }

    /**
     * Get the person currently saving the comment
     * @return aPerson, from the current hibernate session
     */
    synchronized public Person getSaver() {
        return saver.load();
    }    
    
    /**
     * Get all authors, excluding opener.
     * 
     * @return a set of Persons, from the current hibernate session
     */
	synchronized public List getAuthors() {
        return Person.Ref.load(authors);
	}

    /**
     * Get all authors, including opener.
     * the openener is the first, so the order is the same as 
     * the owner tags in the contents.
     * 
     * @return a set of Persons, from the current hibernate session
     */
    synchronized public List getAllAuthors() {
        ArrayList l = new ArrayList();
        l.add(getOpener());
        l.addAll(getAuthors());
        return l;
    }
    
    /**
     * Get the set of struck authors
     * @return a set of Persons, from the current hibernate session
     */
    synchronized public Set getStruckAuthors() {
        return new HashSet(Person.Ref.load(struckAuthors));
    }
    
    /**
     * Add an author to this comment.
     * @param author
     */
    synchronized public void addAuthor(Person author) {
        if (!isAuthor(author)) {
            authors.add(author.getRef());
        }
        struckAuthors.remove(author.getRef());
    }

    synchronized public void addStruckAuthor(Person author) {
        struckAuthors.add(author.getRef());
    }

    public boolean isAuthor(Person person) {
        return opener.isPerson(person) || authors.contains(person.getRef());
    }
    
    public boolean isStruckAuthor(Person person) {
        return struckAuthors.contains(person.getRef());
    }
    
    private int getAuthorTag(Person person) {
        if (opener.isPerson(person))
            return 0;
        
        int tag = 1;
        for (Iterator i = authors.iterator(); i.hasNext(); tag++) {
            Person.Ref author = (Person.Ref) i.next();
            if (author.isPerson(person))
                return tag;
        }
        return 0;
    }

	/**
	 * Update the text of the comment.
	 * 
	 * @param author the person doing the update
	 * @param diff the change
	 * @param basedOnVersion the version of the comment this change was based on
	 * @return the new version number and a diff to it if not the same as the passed one
	 * @throws EditingConflictException if there is a conflict updating the text
	 * @throws ClosedCommentException if comment has been closed
	 */
	synchronized public VersionAndDiff updateContents(Person author, StringDiff diff, int basedOnVersion) throws EditingConflictException {
        addAuthor(author);

        int tag = getAuthorTag(author);
        
        TaggedTextDiff taggedDiff = new TaggedTextDiff(diff, tag);
        
		TaggedText newContent = taggedDiff.applyTo(contents);
        
		if (newContent == null) {
		    return new VersionAndDiff();
		}

        saveOldVersion();

		TaggedTextDiff realDiff = null;
		OldVersion old = getOldVersion(basedOnVersion);
		if (old != null)
		    realDiff = new TaggedTextDiff(old.contents, newContent);
		else 
		    return new VersionAndDiff(); // (the base version is to old)

		
		contents = newContent;	

        version++;
		signalChanged();
		return new VersionAndDiff(version, realDiff);
	}
	
	synchronized public TaggedTextDiff getDiffFrom(int oldVersion) throws TooOldVersionException {
		if (oldVersion == this.version)
			return new TaggedTextDiff();
		
		OldVersion old = getOldVersion(oldVersion);
		if (old != null)
			return new TaggedTextDiff(old.contents, this.contents);
		
		throw new TooOldVersionException();
	}

	public synchronized void xmlRpcSerialize(Map map) {
		super.xmlRpcSerialize(map);
		map.put("text", contents.getText());
        map.put("name", name);
        map.put("state", state);        
        map.put("tags", contents.getTagString());
		map.put("opener", getOpener().toString());
        if (saver != null)
            map.put("saver", getSaver().toString());        
		map.put("authors", personCollectionToVector(getAuthors()));
		map.put("struckAuthors", personCollectionToVector(getStruckAuthors()));
        map.put("version", new Integer(version));  
	}
    
	public boolean deAuthor(Person deAuthor) {
		if (!isAuthor(deAuthor)) {
			throw new IllegalArgumentException("Person \"" + deAuthor + "\" is not an author");
		}

		if (!isStruckAuthor(deAuthor)) {
		    addStruckAuthor(deAuthor);
		}
		
		boolean shouldDelete = struckAuthors.size() == authors.size() + 1; 
		
		if (!shouldDelete) {
		  signalChanged();
		}
		return shouldDelete;
	}
    
    public synchronized void setContents(String contents) {
        this.contents = new TaggedText(contents, 0);
        this.oldVersions = new LinkedList();
    }

    public Date getStartTime() {
        return this.startTime;
    }

    public String getName() {
        return this.name;
    }
    
    private void stateTransition(String expectedOld, String newState) {
        if (this.state != expectedOld)
            throw new RuntimeException("trying to set state \"" + newState + "\" for comment in state \"" + this.state + "\", expected \"" + expectedOld + "\"");
        this.state = newState;
        this.signalChanged();
    }
    
    public void setStateSaving(Person saver) {
        this.saver = saver.getRef();
        stateTransition(EDITING_STATE, SAVING_STATE);
    }    

    public void setStateSaveComplete() {
        stateTransition(SAVING_STATE, SAVE_COMPLETE_STATE);
    }


    public void setStateEditing(Person opener, String contents) {
        stateTransition(SAVE_COMPLETE_STATE, EDITING_STATE);
        this.opener = opener.getRef();
        setContents(contents);
        this.signalChanged();
    }
}
