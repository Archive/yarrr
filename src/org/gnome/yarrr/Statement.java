/*
 * Created on Apr 6, 2005
 */
package org.gnome.yarrr;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author marco
 */
public class Statement extends ReferencableObject {
    private Topic topic;
	private Person author;
	private String content;
	private Set /* Person */ subscribers;
    private Integer version;
    
    Statement(){}
	
	public Statement(Topic topic, Person author, String content) {
        this.topic = topic;
		this.author = author;
		this.content = content;
		this.subscribers = new HashSet();
        HibernateUtil.getSession().save(this);
	}
    
    public synchronized Set getSubscribers() {
        return this.subscribers;
    }
    
    public synchronized boolean hasSubscribers() {
		return !this.subscribers.isEmpty();
	}
    
	public synchronized boolean hasSubscriber(Person subscriber) {
		return this.subscribers.contains(subscriber);
	}
	
	public synchronized void addSubscriber(Person subscriber) {
		if (this.subscribers.contains(subscriber))
			throw new IllegalArgumentException("Subscriber already added: " + subscriber);
		this.subscribers.add(subscriber);
        signalChanged();
	}
	
	public synchronized void removeSubscriber(Person subscriber) {
		if (!this.subscribers.contains(subscriber))
			throw new IllegalArgumentException("Subscriber is not present: " + subscriber);
		this.subscribers.remove(subscriber);
        signalChanged();
	}
	
	public String getContent() {
		return this.content;
	}

	public synchronized void xmlRpcSerialize(Map table) {
		super.xmlRpcSerialize(table);
		table.put("author", author.toString());
		table.put("content", content);
		table.put("subscribers", personCollectionToVector(subscribers));
	}

    public Person getAuthor() {
        return this.author;
    }
}
