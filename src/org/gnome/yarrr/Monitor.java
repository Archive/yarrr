/*
 * Created on Apr 26, 2005
 *
 */
package org.gnome.yarrr;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author alex
 */
public class Monitor {
    private String id;
    private int version;
    private List containers;
    
    private Monitor(ReferencableObject obj) {
        this.id = ReferencableObjectRegistry.getReference(obj);
        this.containers = new ArrayList();
        version = 0;
    }
    
    public static Monitor getInstance(ReferencableObject obj) {
        return new Monitor(obj);
    }
    
    public synchronized void addContainer(ToplevelReferencableObject container) {
        containers.add(container);
    }

    public synchronized void removeContainer(ToplevelReferencableObject container) {
        containers.remove(container);
    }

    public synchronized int numContainers() {
        return containers.size();
    }
    
    public String getId() {
        return id;
    }
    
    public int getVersion() {
        return version;
    }
    
    /**
     * Once the the transaction is complete, causes the clientside "update"
     * method to be called on any Referencable objects that have changed (it
     * does this by returning a result to the pollForChanges XML-RPC call, which
     * causes the clientside callbacks to be invoked)
     */
     public void queueSignalChanged() {
         if (HibernateUtil.isTransactionActive()) {
             HibernateUtil.addChangedObject(this);
         } else {
             commit();
         }
     }
     
    /**
     * Called from the end of a successful transaction to actually signal the change. 
     */
    public void commit() {
        this.version++;
        // We do this to avoid holding the lock on this when invoking container.emitSignalChanged,
        // which is bad since a container can call into the monitor too on another thread.
        List tempList;
        synchronized (this) {
            tempList = new ArrayList(containers);
        }
        for (Iterator i = tempList.iterator(); i.hasNext(); ) {
            ToplevelReferencableObject container = (ToplevelReferencableObject) i.next();
            container.emitSignalChanged(id);
        }
    }
}
