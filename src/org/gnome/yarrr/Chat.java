/*
 * Created on Mar 17, 2005
 */
package org.gnome.yarrr;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.gnome.yarrr.xmlrpc.XmlRpcHandler;
import org.gnome.yarrr.xmlrpc.XmlRpcMarshaller;
import org.gnome.yarrr.xmlrpc.XmlRpcSerializable;

/**
 * @author walters
 *
 */
public class Chat extends ReferencableObject implements XmlRpcSerializable {
	static Log logger = LogFactory.getLog(Chat.class);
    public static int CHAT_CHANGE_TYPE_SUBSCRIBERS = 0;

    static public class Change implements XmlRpcSerializable {
        int type;
        int messageIndex;

        public Change(ChatMessage message, int type) {
            messageIndex = message.getIndex();
            this.type = type; 
        }
        
        public synchronized void xmlRpcSerialize(Map table) {
            table.put("type", new Integer(type));
            table.put("messageIndex", new Integer(messageIndex));
        }
    }
    
    private int lastMessageIndex;
	private List /* Message */ messages;
    private int lastChangeIndex;
    private List /* Change */ changes;
    
    public static Set xmlRpcRemotableMethods = new HashSet();
	
	static public class Update implements XmlRpcMarshaller {
		private static final long serialVersionUID = 1L;
		List messages;
        List changes;
		int lastMessageIndex;
        int lastChangeIndex;
		
        private List serializeList(List list) {
            ArrayList serializedList = new ArrayList();
            for (Iterator i = list.iterator(); i.hasNext(); ) {
                XmlRpcSerializable object = (XmlRpcSerializable)i.next();
                HashMap serialized = new HashMap();
                object.xmlRpcSerialize(serialized);
                serializedList.add(serialized);
            }
           return serializedList;            
        }
        
		public Update(int messageIndex, List newMessages, int changeIndex, List newChanges) {
            messages = serializeList(newMessages);
            changes = serializeList(newChanges);
            lastMessageIndex = messageIndex;
            lastChangeIndex = changeIndex;
		}
		
		public List getMessages() {
			return messages;
		}
        
		public int getLastMessageIndex() {
			return lastMessageIndex;
		}
		
		public Object xmlRpcMarshal(XmlRpcHandler handler) {
			Hashtable table = new Hashtable();
			table.put("lastMessage", new Integer(lastMessageIndex));
			table.put("messages", messages);
            table.put("lastChange", new Integer(lastChangeIndex));
            table.put("changes", changes);
			return table;
		}
	}
	
	public Chat() {
        lastChangeIndex = 0;
        changes = new ArrayList();
        lastMessageIndex = 0;
		messages = new ArrayList();
        ReferencableObjectRegistry.register(this);
	}
	
    synchronized public int getLastMessageIndex() {
        return lastMessageIndex;
    }
    
    synchronized public int getLastChangeIndex() {
        return lastChangeIndex;
    }

    public ChatMessage getMessage(int index) {
        return (ChatMessage)messages.get(index);
    }
    
    synchronized public int addMessage(ChatMessage msg) {
		this.messages.add(msg);
        int oldVersion = this.lastMessageIndex++;
        msg.setIndex(oldVersion);
        this.signalChanged();
		return oldVersion;
	}
    
    static { xmlRpcRemotableMethods.add("getChatUpdates"); }
	synchronized public Update getChatUpdates(int sinceMessage, int sinceChange) {
        List messages, changes;
		if (sinceMessage >= this.messages.size()) {
            messages = new ArrayList();
		} else {
            messages = this.messages.subList(sinceMessage, this.messages.size());
        }
        if (sinceChange >= this.changes.size()) {
            changes = new ArrayList();
        } else {
            changes = this.changes.subList(sinceChange, this.changes.size());
        }
		
		logger.debug("Returning " + messages.size() + " messages for version delta " + sinceMessage + " to " + this.lastMessageIndex);
		
		return new Update(lastMessageIndex, messages, lastChangeIndex, changes);
	}
	
	public ChatMessage[] getMessagesSinceDate(Date startTime) {
		Iterator i;
		int pos;
		for (i = messages.iterator(), pos = 0; i.hasNext(); pos++) {
			ChatMessage message = (ChatMessage)i.next();
			if (message.getDate().after(startTime)) {
				List fetched = messages.subList(pos, this.messages.size());
				return (ChatMessage[]) fetched.toArray(new ChatMessage[fetched.size()]);
			}
		}
		return new ChatMessage[0];
	}

	public synchronized void xmlRpcSerialize(Map table) {
		super.xmlRpcSerialize(table);
		table.put("messages", new Vector(messages));	
        table.put("lastMessage", new Integer(lastMessageIndex));  
        table.put("lastChange", new Integer(lastChangeIndex));  
        // No need to return changes. Already happened changes
        // are already in messages
	}
    
    public void addMessageSubscriber(int index, Person subscriber) {
        ChatMessage message = (ChatMessage) messages.get(index);
        message.addSubscriber(subscriber);
        
        Change change = new Change(message, CHAT_CHANGE_TYPE_SUBSCRIBERS);

        changes.add(change);
        this.lastChangeIndex++;
        
        this.signalChanged();
    }

    public void removeMessageSubscriber(int index, Person subscriber) {
        ChatMessage message = (ChatMessage) messages.get(index);
        message.removeSubscriber(subscriber);
        
        Change change = new Change(message, CHAT_CHANGE_TYPE_SUBSCRIBERS);

        changes.add(change);
        this.lastChangeIndex++;
        
        this.signalChanged();
    }

    public Vector getChatMessageSubscribers(int index) {
        ChatMessage message = (ChatMessage) messages.get(index);
        return ReferencableObject.personRefSetToVector(message.getSubscriberRefs());
    }
}
