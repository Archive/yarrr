package org.gnome.yarrr;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import javax.imageio.ImageIO;

import org.apache.commons.fileupload.FileItem;

public class ImageDecoderFileItem implements FileItem {
    private static final long serialVersionUID = 1L;
    String uploadContentType;
    String fieldName;
    String filenName;
    BufferedImage image;
    
    public ImageDecoderFileItem(String fieldName, String contentType, String fileName) {
        uploadContentType = contentType;
        this.fieldName = fieldName;
        this.filenName = fileName;
    }
    
    public BufferedImage getImage() {
        return image;
    }
    
    public InputStream getInputStream() throws IOException {
        return null;
    }
    
    public String getContentType() {
        return "image/png";
    }
    
    public String getName() {
        return filenName;
    }
    
    public boolean isInMemory() {
        return true;
    }
    
    public long getSize() {
        return 0;
    }
    
    public byte[] get() {
        return null;
    }
    
    public String getString(String arg0) throws UnsupportedEncodingException {
        return null;
    }
    
    public String getString() {
        return null;
    }
    
    public void write(File file) throws Exception {                        
    }
    
    public void delete() {
    }
    
    public String getFieldName() {
        return fieldName;
    }
    
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
    
    public boolean isFormField() {
        return false;
    }
    
    public void setFormField(boolean isFormField) {
    }
    
    public OutputStream getOutputStream() throws IOException {
        return new ByteArrayOutputStream() {
            public void close() throws IOException {
                super.close();
                byte [] data = this.toByteArray(); 
                image = ImageIO.read(new ByteArrayInputStream(data));
            }            
        };
    }    
}
