/*
 * Created on Mar 24, 2005
 */
package org.gnome.yarrr;

import org.gnome.yarrr.ReferencableObject.Reference;
import org.hibernate.proxy.HibernateProxyHelper;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author walters
 */
public class ReferencableObjectRegistry {
	static final String classPackagePrefix = "org.gnome.yarrr.";
    /**
     * @author walters
     */
    
    private static final class RegistryEntry {
        Monitor monitor;
        ReferencableObject obj; // null if persistant
    }
    
	private static Log logger = LogFactory.getLog(ReferencableObjectRegistry.class);
    
	/**
	 * @author walters
	 */
	public static class UnregisteredObjectException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public UnregisteredObjectException() {
		}

		public UnregisteredObjectException(Throwable e) {
			super(e);
		}

		public UnregisteredObjectException(String string) {
			super(string);
		}
	}
    
    public static abstract class LookupFailureException extends Exception {
        private static final long serialVersionUID = 1L;
        protected String ref;
        
        public LookupFailureException(String ref) {
            this.ref = ref;
        }
    }
    
    /*
     * @author walters
     */
    public static class UnknownReferenceException extends LookupFailureException {
        private static final long serialVersionUID = 1L;
        
        public UnknownReferenceException(String ref) {
            super(ref);
        }

        public String toString() { 
            return "Unknown reference: " + ref;
        }
    }
    
    /*
     * @author walters
     */
    public static class MalformedReferenceException extends LookupFailureException {

        private static final long serialVersionUID = 1L;
        
        public MalformedReferenceException(String ref) {
            super(ref);
        }
        
        public String toString() { 
            return "Malformed reference: " + ref;
        }
    }

    private static Set /* Class */ persistentClasses = new HashSet();
    private static Set /* Class */ remoteClasses = new HashSet();
	private static Map /* Long,RegistryEntry */objectRegistry = new HashMap();

	private static AtomicLong idseq = new AtomicLong();
    
    public static void clear() {
        objectRegistry.clear();
    }
    
    public static void registerPersistant(Class klass) {
        persistentClasses.add(klass);
    }

    private static RegistryEntry lookupEntry(Reference ref, boolean create) {
        RegistryEntry entry = (RegistryEntry) objectRegistry.get(ref);
        if (entry == null && create) {
            entry = new RegistryEntry();
            objectRegistry.put(ref, entry);
        }
        return entry;
    }
    
    public synchronized static Monitor addMonitor(ReferencableObject obj, ToplevelReferencableObject container) {
        RegistryEntry entry = lookupEntry(obj.getReference(), true);
        if (entry.monitor == null) 
            entry.monitor = Monitor.getInstance(obj);
        entry.monitor.addContainer(container);
        return entry.monitor;
    }
    
    public synchronized static void removeMonitor(ReferencableObject obj, ToplevelReferencableObject container) {
        Reference key = obj.getReference();
        RegistryEntry entry = lookupEntry(key, true);
        entry.monitor.removeContainer(container);
        if (entry.monitor.numContainers() == 0 && entry.obj == null) {
            objectRegistry.remove(key);
        }
    }
    
    private static Class getObjectClass(ReferencableObject object) {
        return HibernateProxyHelper.getClassWithoutInitializingProxy(object);
    }
    
	public synchronized static void register(ReferencableObject obj) {
        Class klass = getObjectClass(obj);
        if (persistentClasses.contains(klass)) {
            return;
        }
        remoteClasses.add(klass);
        
		long newid = idseq.addAndGet(1);
        obj.setReferenceId(newid);

        RegistryEntry entry = lookupEntry(obj.getReference(), true);
        entry.obj = obj;
        
        logger.info("Registering object " + getReference(obj));
	}
    
	public static String getReference(ReferencableObject obj) throws UnregisteredObjectException {
		long id = obj.getReferenceId();
		if (id < 0)
			throw new UnregisteredObjectException("Unregistered object \"" + obj + "\" of class " + obj.getClass().toString());
		
		return getReference(getObjectClass(obj), new Long(id));
	}
    
    private static void validateClass(Class klass) {
         if (!(persistentClasses.contains(klass) || remoteClasses.contains(klass)))
            throw new UnregisteredObjectException("Invalid class \"" + klass.getName() + "\"");
    }
    
    public static String getReference(Class klass, Long id) {
        validateClass(klass);
        String className = klass.getName();
        if (className.startsWith(classPackagePrefix))
            className = className.substring(classPackagePrefix.length());
        return className + "/" + id;
    }
    
    public static Class getReferenceClass(String reference) {
        try {
            Reference ref = parseReference(reference);
            return ref.klass;
        } catch (Exception e) {
            return null;
        }
    }
	
	private static Reference parseReference(String reference) throws MalformedReferenceException {
		try {
            Reference ref = new Reference();
			int index = reference.indexOf('/');
            ref.klass = Class.forName(classPackagePrefix + reference.substring(0, index));
            validateClass(ref.klass);
			ref.id = Long.parseLong(reference.substring(index + 1));
            return ref;
		} catch (IndexOutOfBoundsException e) {
			throw new MalformedReferenceException(reference);
		} catch (ClassNotFoundException e) {
           throw new UnregisteredObjectException(e);
        }
	}

    public synchronized static ReferencableObject lookup(String reference) throws UnknownReferenceException, MalformedReferenceException {
        Reference ref = parseReference(reference);
        ReferencableObject ret = null;
        
        if (persistentClasses.contains(ref.klass)) {
            ret = lookupObjectViaClass(ref.klass, ref.id);
        } else {
            RegistryEntry entry = lookupEntry(ref, false);
            if (entry != null)
                ret = entry.obj;
        }
        if (ret == null)
            throw new UnknownReferenceException("Object not in registry: " + reference);
        return ret;
    }

    public synchronized static Monitor lookupMonitor(Reference ref) {
        RegistryEntry entry = lookupEntry(ref, false);
        if (entry != null)
            return entry.monitor;
        return null;
    }

    
    public synchronized static Monitor lookupMonitor(ReferencableObject obj) {
        return lookupMonitor(obj.getReference());
    }
    
    public synchronized static Monitor lookupMonitor(String reference) throws MalformedReferenceException {
        logger.debug("Looking up monitor " + reference);
        Reference ref = parseReference(reference);
        RegistryEntry entry = lookupEntry(ref, false);
        if (entry != null)
            return entry.monitor;
        return null;
    }

	private static ReferencableObject lookupObjectViaClass(Class klass, long id) {
        return (ReferencableObject) HibernateUtil.getSession().get(klass, new Long(id));
    }

    /**
	 * Removes an object from the object registry.  This means it can no longer be
	 * looked up via its reference from getReference.
	 * @param object object to remove from registry
	 */
	public synchronized static void deregister(ReferencableObject object) {
		logger.info("Deregistering object " + getReference(object));
		objectRegistry.remove(new Long(object.getReferenceId()));
	}
}
