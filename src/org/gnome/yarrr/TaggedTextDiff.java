package org.gnome.yarrr;

import java.util.Hashtable;
import java.util.Vector;

import org.gnome.yarrr.xmlrpc.XmlRpcHandler;
import org.gnome.yarrr.xmlrpc.XmlRpcMarshaller;

public class TaggedTextDiff extends Diff implements XmlRpcMarshaller {
    public TaggedTextDiff() {
    }

    public TaggedTextDiff(StringDiff strDiff, int tag) {
        this.hunks = new Hunk[strDiff.hunks.length];
        for (int i = 0; i < hunks.length; i++) {
            Hunk strHunk = strDiff.hunks[i];
            Hunk hunk = new Hunk();
            this.hunks[i] = hunk;
            hunk.offset = strHunk.offset;
            hunk.parts = new HunkPart [strHunk.parts.length];
            for (int j = 0; j < hunk.parts.length; j++) {
                HunkPart strPart = strHunk.parts[j];
                HunkPart part = new HunkPart();
                hunk.parts[j] = part;
                part.op = strPart.op;
                String text = ((StringDiffable)strPart.data).str;
                if (part.op == INSERT)
                    part.data = new TaggedText(text, tag);
                else
                    part.data = new TaggedText(text);
            }
        }
    }

    
    public TaggedTextDiff(TaggedText a, TaggedText b) {
        super(a, b);
    }
    
    public TaggedText applyTo(TaggedText text) {
        Diffable diffable = super.applyTo(text);
        return ((TaggedText)diffable);
    }
    
    public Object xmlRpcMarshal(XmlRpcHandler handler) {
        Vector xmlhunks = new Vector();
        
        for (int i = 0; i < hunks.length; i++) {
            Hunk hunk = hunks[i];   
            Hashtable xmlhunk = new Hashtable();
            
            xmlhunk.put("offset", new Integer(hunk.offset));
            Vector xmlparts = new Vector();
            for (int j = 0; j < hunk.parts.length; j++) {
                HunkPart part = hunk.parts[j];
                Hashtable xmlpart = new Hashtable();
                
                xmlpart.put("op", new Integer(part.op));
                TaggedText data = (TaggedText)part.data;
                xmlpart.put("data", data.getText());
                xmlpart.put("tags", data.getTagString());
                
                xmlparts.add(xmlpart);
            }
            xmlhunk.put("parts", xmlparts);
            
            xmlhunks.add(xmlhunk);
        }
        
        return xmlhunks;
    }

}
