package org.gnome.yarrr;

public interface Diffable {
    boolean equalAt(int offset, Diffable other, int otherOffset);
    Diffable getSubset(int offsetStart);
    Diffable getSubset(int offsetStart, int offsetEnd);
    boolean equals(Diffable other, int start, int length);
    int length();
    int findFirstMatch(Diffable match, int start);
    int findLastMatch(Diffable match, int start);
    DiffableBuilder createBuilder();
}
