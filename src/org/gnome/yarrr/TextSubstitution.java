/*
 * Created on May 11, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.gnome.yarrr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author marco
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class TextSubstitution {
	static private List /* TextSubstitution */ substitutions = new ArrayList();
	
	static final int TOPIC_TYPE = 0;
	static final int LINK_TYPE = 1;
	static final int BOLD_TYPE = 2;
	static final int UNDERLINED_TYPE = 3;
	
	private Pattern pattern;
	private int type;
	private Map properties;
	
	public TextSubstitution(String regex, int type) {
		this.pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		this.type = type;
		this.properties = new HashMap();
	}
	
	public void addProperty (String name, String content) {
		if (this.properties.containsKey(name))
			throw new IllegalArgumentException("Property already added: " + name);
		properties.put(name, content);
	}

	private String replaceGroups(Matcher matcher, String content) {
		String result = content;
		
		for (int i = 0; i <= matcher.groupCount(); i++) {
			result = result.replaceAll("\\$"+ i, matcher.group(i));
		}
		
		return result;
	}
	
	public String getProperty(Matcher matcher, String name) {
		if (!this.properties.containsKey(name))
			throw new IllegalArgumentException("Property does not exist: " + name);
		String content = (String)properties.get(name);
		return replaceGroups (matcher, content);
	}
	
	public String getContent(Matcher matcher) {
		return matcher.group(1);
	}
	
	/**
	 * @return Returns the type.
	 */
	public int getType() {
		return type;
	}
	
	/**
	 * @return Returns a matcher.
	 */
	public Matcher matcher(String content) {
		return pattern.matcher(content);
	}
	
	static public void addTextSubstitution(TextSubstitution substitution) {
		substitutions.add(substitution);
	}
	
	static public List /* TextSubstitution */ getTextSubstitutions() {
		/* FIXME this are just some random samples */
		if (substitutions.isEmpty()) {
			TextSubstitution sub;
			
			sub = new TextSubstitution ("(?<= |^)\\*([^*]+)\\*(?= |$)",
								        BOLD_TYPE);
			TextSubstitution.addTextSubstitution(sub);

			sub = new TextSubstitution ("(?<= |^)\\_([^_]+)\\_(?= |$)",
			        					UNDERLINED_TYPE);
			TextSubstitution.addTextSubstitution(sub);
			
			SmartLink.addSmartLinks();
		}
		
		return substitutions;
	}
}
