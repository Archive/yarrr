package org.gnome.yarrr;

import java.util.Hashtable;
import java.util.Vector;

import org.gnome.yarrr.xmlrpc.XmlRpcDemarshaller;
import org.gnome.yarrr.xmlrpc.XmlRpcHandler;
import org.gnome.yarrr.xmlrpc.XmlRpcMarshaller;

public class StringDiff extends Diff implements XmlRpcMarshaller, XmlRpcDemarshaller {
    
    public StringDiff() {
    }
    
    public StringDiff(String a, String b) {
        super(new StringDiffable(a), new StringDiffable(b));
    }
    
    public String applyTo(String str) {
        Diffable diffable = applyTo(new StringDiffable(str));
        return ((StringDiffable)diffable).str;
    }
    
    public Object xmlRpcMarshal(XmlRpcHandler handler) {
        Vector xmlhunks = new Vector();
        
        for (int i = 0; i < hunks.length; i++) {
            Hunk hunk = hunks[i];   
            Hashtable xmlhunk = new Hashtable();
            
            xmlhunk.put("offset", new Integer(hunk.offset));
            Vector xmlparts = new Vector();
            for (int j = 0; j < hunk.parts.length; j++) {
                HunkPart part = hunk.parts[j];
                Hashtable xmlpart = new Hashtable();
                
                xmlpart.put("op", new Integer(part.op));
                xmlpart.put("data", ((StringDiffable)part.data).toString());
                
                xmlparts.add(xmlpart);
            }
            xmlhunk.put("parts", xmlparts);
            
            xmlhunks.add(xmlhunk);
        }
        
        return xmlhunks;
    }

    static public Object xmlRpcDemarshal(Object xmlRpcObject) throws DemarshallingException {
        Vector xmlhunks = (Vector) xmlRpcObject;
        
        Diff diff = new StringDiff();
        diff.hunks = new Hunk[xmlhunks.size()];
        
        for (int i = 0; i < xmlhunks.size(); i++) {
            Hashtable xmlhunk = (Hashtable) xmlhunks.get(i);
            Hunk hunk = new Hunk();
            diff.hunks[i] = hunk;
        
            hunk.offset = ((Integer)xmlhunk.get("offset")).intValue();
            
            Vector xmlparts = (Vector)xmlhunk.get("parts");
            hunk.parts = new HunkPart[xmlparts.size()];
            for (int j = 0; j < hunk.parts.length; j++) {
                Hashtable xmlpart = (Hashtable)xmlparts.get(j);
                HunkPart part = new HunkPart();
                hunk.parts[j] = part;
                
                part.op = ((Integer)xmlpart.get("op")).intValue();
                String str = (String)xmlpart.get("data");
                part.data = new StringDiffable(str);
            }
        }
        return diff;
    }
    static public Class xmlRpcMarshalledType = Vector.class;
    
    public String toString() {
        StringBuilder builder = new StringBuilder();
        
        for (int i = 0; i < hunks.length; i++) {
            Hunk hunk = hunks[i];
            
            hunk.toString(builder);
            if (i != hunks.length-1)
                builder.append(",");
        }
        
        return builder.toString();
    }
}
