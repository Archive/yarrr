/*
 * Created on 22-Mar-2005
 */
package org.gnome.yarrr;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import org.gnome.yarrr.xmlrpc.XmlRpcDemarshaller;
import org.gnome.yarrr.xmlrpc.XmlRpcHandler;
import org.gnome.yarrr.xmlrpc.XmlRpcMarshaller;


public class SketchStroke extends SketchOp implements XmlRpcDemarshaller, XmlRpcMarshaller {
    private ArrayList /* Point */ points;
    private int width;
    private Color color;
    
    SketchStroke() {
        points = new ArrayList();
    }
     
    public void draw(Graphics2D g) {
        g.setColor(color);
        g.setStroke(new BasicStroke(width));
        for (int j=0;j<points.size()-1;j++) {
            Point pointA = (Point) points.get(j);
            Point pointB = (Point) points.get(j+1);
            
            g.drawLine(pointA.x, pointA.y, pointB.x, pointB.y);
        }
    }
   

    static public Class xmlRpcMarshalledType = Hashtable.class;
    static public Object xmlRpcDemarshal(Object xmlRpcObject) throws Exception { 
        Hashtable table = (Hashtable)xmlRpcObject;        

        SketchStroke stroke = new SketchStroke();

        Vector marshalledPoints = (Vector)table.get("points");
        Iterator it = marshalledPoints.iterator();
        
        while (it.hasNext()) {
            stroke.points.add(XmlRpcHandler.demarshalFromClass(Point.class, it.next()));
        }

        stroke.width = XmlRpcHandler.demarshalInt(table, "width", 1);
        stroke.color = XmlRpcHandler.demarshalColor(table, "color", Color.black);
        
        return stroke; 
    }

    public Object xmlRpcMarshal(XmlRpcHandler handler) {
        Hashtable table = new Hashtable();
        table.put("points", points);
        return table;
    }
}