/*
 * Created on Apr 12, 2005
 *
 *
 * Some of this code is based on the WebServer class from the 
 * apache xmlrpc project with the following copyright text:
 * 
 * The Apache Software License, Version 1.1
 *
 * Copyright (c) 2001 The Apache Software Foundation.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by the
 *        Apache Software Foundation (http://www.apache.org/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "XML-RPC" and "Apache Software Foundation" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written
 *    permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache",
 *    nor may "Apache" appear in their name, without prior written
 *    permission of the Apache Software Foundation.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 */

package org.gnome.yarrr;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xmlrpc.Base64;

/**
 * @author alex
 */
public class PollWebserver implements Runnable {
	static Log logger = LogFactory.getLog(PollWebserver.class);
	private int startPort;
	private int numPorts;
	private ServerSocketChannel channels[];
    protected Stack threadpool;
    protected ThreadGroup runners;
    private int lastPortIndex;
    private Collection connections; 
    
    private boolean useKeepAlive;
	private HashMap pollObjects;
	private HashMap addressToObjects;
	private Thread listener;
    private Selector selector;
    private LinkedList newConnectionsForSelector;
    private LinkedList wokenUpConnections;
	
	private static AtomicLong idseq = new AtomicLong();

    protected static final byte[] ctype =
        toHTTPBytes("Content-Type: text/plain\r\n");
    protected static final byte[] clength =
        toHTTPBytes("Content-Length: ");
    protected static final byte[] newline = toHTTPBytes("\r\n");
    protected static final byte[] doubleNewline = toHTTPBytes("\r\n\r\n");
    protected static final byte[] conkeep =
        toHTTPBytes("Connection: Keep-Alive\r\n");
    protected static final byte[] conclose =
        toHTTPBytes("Connection: close\r\n");
    protected static final byte[] ok = toHTTPBytes(" 200 OK\r\n");
    protected static final byte[] server =
        toHTTPBytes("Server: Yarrr PollWebserver 1.0\r\n");
    protected static final byte[] wwwAuthenticate =
        toHTTPBytes("WWW-Authenticate: Basic realm=Yarrr\r\n");

    private static final String HTTP_11 = "HTTP/1.1";

	public class PollObject {
		long id;
		InetAddress address;
		int port;
		Connection connection;
		int version;
		
		PollObject(InetAddress address) {
			id = idseq.addAndGet(1);
			this.port = getPortForAddress(address);
			this.address = address;
			connection = null;
			version = 0;
	
			addObject(this);
		}
		
        /**
         * Call when the poll object isn't used any more.
         * release all resources used by it
         */
        public void delete() {
            markDone();
            removeObject(this);
        }
        
		/**
		 * Called by the webserver when the client has initiated a request to this object.
		 * 
		 * @param newConnection The connection object handling requests on this object
		 * @return true if the object has already changed since the url was created
		 */
		synchronized boolean gotNewConnection(Connection newConnection, int clientVersion) {
			if (version != clientVersion) {
				return false;
			}
			
			logger.debug("PollObj " + this + " got new connection " + newConnection);
			
			if (connection != null) {
				// If we already have a connection for this id/version, return it
				// (This shouldn't normally happen)
				connection.scheduleReply();
				connection = null;
			}
			
			this.connection = newConnection;
			return true;
		}
		
		synchronized void gotConnectionClose(Connection closedConnection) {
			if (connection == closedConnection) {
				connection = null;
			}
		}
		
		public InetAddress getAddress() {
			return address;
		}
		
		public boolean isActive() {
			return connection != null;
		}
		
		public Long getId() {
			return new Long(id);
		}
		
		public int getPort() {
			return port;
		}
		
		public String getURL(String requestUrl) throws URISyntaxException {
			URI uri = new URI(requestUrl);
			return "http:///" + uri.getHost() + ":" + Integer.toString(port) + "/" + Long.toString(id) + "/" + Integer.toString(version);
		}
		
		/**
		 * Called by the app when the event this object is waiting on has happened.
		 *
		 */
		synchronized public void markDone() {
			version++;
			
			logger.debug("Waking up pollobj " + this + ", connection: " + ((connection != null) ? connection.toString(): "null"));
			
			if (connection != null) {
				connection.scheduleReply();
				connection = null;
			}
		}
	}
	
	/**
	 * Get an unused port for the specified ip address.
	 * We use different ports for each new session from an ip
	 * to avoid problem with a maximum number of simultaneous connections 
	 * to a server (host+port).
	 * 
	 * @param address The ip address of the client
	 * @return a port not in use by this client
	 */
	synchronized private int getPortForAddress(InetAddress address) {
		// We synchronize to protect the addressToObject references
		
		List objects = (List)addressToObjects.get(address);
		
		// We start at lastPortIndex so that we don't immediately reuse
		// ports. That may cause problem in the client if they still
		// have an outgoing request to that port
		for (int i = 0; i < numPorts; i++) {
			int portIndex = (i + lastPortIndex) % numPorts;
			int port = startPort + portIndex; 
		
			boolean portUsed = false;
			if (objects != null) {
				for (Iterator it = objects.iterator(); it.hasNext(); ) {
					PollObject obj = (PollObject) it.next();
					if (obj.getPort() == port) {
						portUsed = true;
						break;
					}
				}
			}
			if (!portUsed) {
				lastPortIndex = portIndex + 1;
				return port;
			}
		}
		logger.warn("Too many ports used for address " + address);
		// all used, but we must use something...
		return startPort; 
	}
	
	/**
	 * Lookup the PollObject with the given id
	 * 
	 * @param id
	 * @return
	 */
	synchronized private PollObject lookupObject(long id) {
		return (PollObject) pollObjects.get(new Long(id));
	}
	
	/**
	 * Add the PollObject to the webserver
	 * @param obj
	 */
	synchronized private void addObject(PollObject obj) {
		pollObjects.put(obj.getId(), obj);
		
		List objects = (List)addressToObjects.get(obj.getAddress());
		if (objects == null) {
			objects = new LinkedList();
			addressToObjects.put(obj.getAddress(), objects);
		}
		objects.add(obj);
	}
	
	/**
	 * Remove the PollObject from the webserver
	 * @param obj
	 */
	synchronized private void removeObject(PollObject obj) {
		pollObjects.remove(obj.getId());
		List objects = (List)addressToObjects.get(obj.getAddress());
		if (objects != null) {
			objects.remove(obj);
			if (objects.size() == 0)
				addressToObjects.remove(obj.getAddress());
		}
	}
	
	/**
	 * Create a new PollObject for use by a specific client
	 * 
	 * @param address the ip address of the client
	 * @return a new PollObject
	 */
	synchronized PollObject newPollObjectFor(InetAddress address) {
		// This is synchronized to make sure the port we get is unused
		// until we addObject().
		return new PollObject(address);
	}
	
	/**
	 * Called from worker thread when its done handling a request and
	 * has replied with keep_alive.
	 * 
	 * @param connection
	 */
	synchronized void addActiveConnection(Connection connection) {
		logger.debug("Adding active connection: " + connection);
		
		try {
			SocketChannel channel = connection.getChannel();
			channel.configureBlocking(false);
			logger.debug("adding connection for registering: " + connection);
			synchronized (newConnectionsForSelector) {
				newConnectionsForSelector.add(connection);
			}
		} catch (IOException e) {
			logger.error(e.getStackTrace());
		}
		
		selector.wakeup();
	}

	
	/**
	 * Called when we're handling an incomming request from the connection.
	 * Removes the 
	 * @param connection
	 */
	synchronized void removeActiveConnection(Connection connection) {
		logger.debug("Removing active connection: " + connection);
		newConnectionsForSelector.remove(connection);
		SelectionKey key = connection.getChannel().keyFor(selector);
		if (key != null)
		    key.cancel();
	}
	
	/**
	 * 
	 * @param connection
	 */
	void addWokenUpConnection(Connection connection) {
		logger.debug("Adding woken up connection: " + connection);
		synchronized (wokenUpConnections) {
			wokenUpConnections.add(connection);
		}
		selector.wakeup();
	}
	
	
	/**
	 * The main body of the listener thread that handles incomming new
	 * requests or new request for active connections (keepAlive connections)
	 */
	public void run() {
		List replyConnections = new ArrayList();
		List newConnections = new ArrayList();
		
        while (listener != null) {
        	try {
        		synchronized (newConnectionsForSelector) {
        			for (Iterator i = newConnectionsForSelector.iterator(); i.hasNext(); ) {
        				Connection con = (Connection) i.next();
        				SocketChannel channel = con.getChannel();
        				channel.register(selector, SelectionKey.OP_READ, con);
        			}
        			newConnectionsForSelector.clear();
        		}
    			
            	logger.debug("PollWebserver SLEEPING Zzzzzzzzzzzzzzzzzzzz");
        		selector.select();
            	logger.debug("PollWebserver WOKE UP!");
            	
        		Iterator it = selector.selectedKeys().iterator();

    			newConnections.clear();
    			replyConnections.clear();

        		synchronized (wokenUpConnections) {
        			for (Iterator i = wokenUpConnections.iterator(); i.hasNext(); ) {
        				Connection conn = (Connection) i.next();
    					removeActiveConnection(conn);
        				replyConnections.add(conn);
        			}
        			
        			wokenUpConnections.clear();
        		}
        		
        		while (it.hasNext()) {
        			SelectionKey selKey = (SelectionKey)it.next();
        			it.remove();

        			if (!selKey.isValid())
        				continue;
        			
        			if (selKey.isAcceptable()) {
        				ServerSocketChannel channel = (ServerSocketChannel)selKey.channel();
        				SocketChannel sChannel = channel.accept();
        				
        				if (sChannel != null) {
        					Connection conn = new Connection(sChannel);
        					logger.debug("Accepted new Connection: " + conn);
        					
        					conn.runRequestHandler();
        				}
        			}
        			
        			if (selKey.isReadable()) {
    					Connection conn = (Connection) selKey.attachment();
    					logger.debug("Connection " + conn + " is readable");
    					assert(conn != null);
    					removeActiveConnection(conn);
    					
    					if (conn.getState() == Connection.HANDLING_REQUEST) {
    						// This is closed
    						conn.shutdown();
    					} else {
    						// Have to move these to after the selector cancel set is applied
    						newConnections.add(conn);
    					}
        			}
        		}
        		
    			// apply key cancel set
    			selector.selectNow();
    			
    			for (Iterator i = newConnections.iterator(); i.hasNext(); ) {
    				Connection conn = (Connection) i.next();
					conn.getChannel().configureBlocking(true);
					conn.runRequestHandler();
    			}
    			for (Iterator i = replyConnections.iterator(); i.hasNext(); ) {
    				Connection conn = (Connection) i.next();
					conn.getChannel().configureBlocking(true);
					conn.runReplyHandler();
    			}
        	} catch (InterruptedIOException checkState) {
        		// Timeout while waiting for a client...
        		// try again if still listening.
        	} catch (IOException e) {
        		logger.error(e);
        	}
        }
        
        try {
			selector.close();
			
			closeServerSockets();
			
			synchronized (connections) {
				for (Iterator i = connections.iterator(); i.hasNext(); ) {
					Connection connection = (Connection) i.next();
					i.remove(); // Remove here to avoid removal at shutdown affecting iterator 
					connection.getChannel().configureBlocking(true);
					connection.shutdown();
				}
            }
		} catch (IOException e) {
    		logger.error("shutdown error: " + e.getStackTrace());
		}
	}
	
    protected ServerSocketChannel createServerSocket(int port, int backlog) throws IOException {
        ServerSocketChannel channel = ServerSocketChannel.open();
        channel.configureBlocking(false);
        ServerSocket socket = channel.socket();
        socket.setReuseAddress(true);
        if (socket.getSoTimeout() <= 0)
        	socket.setSoTimeout(4096);
        socket.bind(new InetSocketAddress(port), backlog);
    	return channel;
    }


	private void setupServerSockets(int backlog) throws IOException {
		channels = new ServerSocketChannel[numPorts];
		for (int port = startPort; port < startPort + numPorts; port++) {
			channels[port-startPort] = createServerSocket(port, backlog);
		}
	}
	
	private void closeServerSockets() throws IOException {
		for (int i = 0; i < channels.length; i++) {
	        channels[i].configureBlocking(true);
			channels[i].close();
		}
	}

	public PollWebserver(int startPort, int numPorts) throws IOException {
		this.startPort = startPort;
		this.numPorts = numPorts;
		lastPortIndex = 0;
		threadpool = new Stack();
        runners = new ThreadGroup("PollWebserver Runner");
        pollObjects = new HashMap();
        addressToObjects = new HashMap();
        newConnectionsForSelector = new LinkedList();
        wokenUpConnections = new LinkedList();
        connections = Collections.synchronizedCollection(new LinkedList());
        
        useKeepAlive = true;
        
		setupServerSockets(50);
		
		// Set up selector
		selector = Selector.open();
		
    	for (int i = 0; i < channels.length; i++) {
    		channels[i].register(selector, SelectionKey.OP_ACCEPT);
    	}
	}
	
	public void addConnection(Connection c) {
		// connections is synchronized
	    connections.add(c);
	}

	public void removeConnection(Connection c) {
		// connections is synchronized
	    connections.remove(c);
	}
	
    /**
     * Spawns a new thread which binds this server to the port it's
     * configured to accept connections on.
     *
     * @see #run()
     */
    public void start() {
        // The listener reference is released upon shutdown().
        if (listener == null) {
            listener = new Thread(this, "PollWebserver Listener");
            // Not marked as daemon thread since run directly via main().
            listener.start();
        }
    }
    
    /**
     * Stop listening on the server port.  Shutting down our {@link
     * #listener} effectively breaks it out of its {@link #run()}
     * loop.
     *
     * @see #run()
     */
    public synchronized void shutdown() {
        // Stop accepting client connections
        if (listener != null) {
            Thread l = listener;
            listener = null;
            l.interrupt();
        }
    }

    /**
     * Returns the US-ASCII encoded byte representation of text for
     * HTTP use (as per section 2.2 of RFC 2068).
     */
    protected static final byte[] toHTTPBytes(String text) {
        try {
            return text.getBytes("US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new Error(e.getMessage() +
                            ": HTTP requires US-ASCII encoding");
        }
    }


	public static void main(String[] args) {
		try {
			PollWebserver testServer = new PollWebserver(10000, 10);
			testServer.start();
			PollObject obj1 = testServer.newPollObjectFor(InetAddress.getLocalHost());
			PollObject obj2 = testServer.newPollObjectFor(InetAddress.getLocalHost());
			System.out.println("obj1: " + obj1 + ", url: " + obj1.getURL("http://127.0.0.1:8080/yarrr"));
			System.out.println("obj2: " + obj2 + ", url: " + obj2.getURL("http://127.0.0.1:8080/yarrr"));
			while (true) {
				try {
					Thread.sleep(1000*10);
				} catch (InterruptedException e1) {
				}
				if (true) {
					System.out.println("marking done");
					obj1.markDone();
					obj2.markDone();
				}
				if (false) {
					System.out.println("obj1 active: " + obj1.isActive());
					System.out.println("obj2 active: " + obj2.isActive());
				}
			}
		} catch (IOException e) {
			System.out.println(e);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}
	
	public boolean getUseKeepAlive() {
		return useKeepAlive;
	}
	
	synchronized protected Runner getRunner() {
		try	{
			return (Runner)threadpool.pop();
		} catch (EmptyStackException empty) {
			if (runners.activeCount () > 255)
				throw new RuntimeException("System overload");
			return new Runner();
		}
	}

	synchronized protected void releaseRunner(Runner runner) {
       threadpool.push(runner);
   }

	/**
	 * Class that handles re-using of threads for Runnable object.
	 * Keeps the thread alive if the threadpool isn't too large. 
	 * @author alex
	 */
   class Runner implements Runnable
   {
       Thread thread;
       Runnable job;
       int count;

       public synchronized void execute(Runnable job) throws IOException {
       	   this.job = job;
           count = 0;
           if (thread == null || !thread.isAlive()) {
               thread = new Thread(runners, this);
               thread.start();
           } else {
               this.notify();
           }
       }

       public void run(){
           while (job != null && Thread.currentThread() == thread) {
               job.run();
               count++;
               job = null;

               if (count > 200 || threadpool.size() > 20)
                   return;
               synchronized(this) {
                   releaseRunner(this);
                   try {
                       this.wait();
                   } catch (InterruptedException ir) {
                       Thread.currentThread().interrupt();
                   }
               }
           }
       }
   }

   class Connection {
       private SocketChannel channel;
       private BufferedInputStream input;
       private BufferedOutputStream output;
       private String user, password;
       private String httpVersion;
       private String uriArgs;
       byte[] buffer;
       
       public static final int WAITING_FOR_REQUEST = 1;
       public static final int HANDLING_REQUEST = 2;
       private int state;
       private boolean keepAlive;
       PollObject currentPollObject;
       
       /**
        *
        * @param socket
        * @throws IOException
        */
       public Connection (SocketChannel channel) throws IOException {
           // set read timeout to 30 seconds
           channel.socket().setSoTimeout (30000);
           
           this.channel = channel;
           input = new BufferedInputStream(channel.socket().getInputStream());
           output = new BufferedOutputStream(channel.socket().getOutputStream());
           state = WAITING_FOR_REQUEST;
           currentPollObject = null;
           
           addConnection(this);
       }
       
       public SocketChannel getChannel() {
           return channel;
       }
       
       private byte [] doneString() {
           return toHTTPBytes("this.pollDone('" + uriArgs + "');");
       }
       
       public int getState() {
           return state;
       }
       
       public String toString() {
           String s;
           
           s = "[PollWebserver::Connection";
           if (channel != null) { 
               s += " " + channel.socket().getRemoteSocketAddress();
           }
           
           s+= "]";
           return s;
       }
       
       /**
        * Close the channel and remove this connection from the webserver
        *
        */
       public void close() {
           logger.debug("Closing channel for connection " + this);
           try {
               channel.close();
           } catch (IOException e) {
               logger.warn("Error closing channel: " + e);
           }
           channel = null;
           
           removeConnection(this);
       }
       
       public void shutdown() {
           logger.debug("Remote side of " + this + " closed.");
           if (currentPollObject != null) {
               currentPollObject.gotConnectionClose(this);
           }
           close();
       }
       
       public boolean isClosed() {
           return channel == null;
       }
       
       void runRequestHandler() throws IOException {
           assert(state == WAITING_FOR_REQUEST);
           
           Runner runner = getRunner();
           Runner handleRequestRunner = new Runner() {
               public void run() {
                   handleRequest();
               }
           };
           
           runner.execute(handleRequestRunner);
       }
       
       void scheduleReply() {
           addWokenUpConnection(this);
       }
       
       void runReplyHandler() throws IOException {
           assert(state == HANDLING_REQUEST);
           Runner runner = getRunner();
           Runner handleReplyRunner = new Runner() {
               public void run() {
                   handleReply();
               }
           };
           
           runner.execute(handleReplyRunner);
       }
       
       public void handleRequest() {
           assert(state == WAITING_FOR_REQUEST);
           
           keepAlive = false;
           try {
               // reset user authentication
               user = null;
               password = null;
               String line = readLine();
               // Netscape sends an extra \n\r after bodypart, swallow it
               if (line != null && line.length() == 0)
                   line = readLine();
               int contentLength = -1;
               
               // tokenize first line of HTTP request
               StringTokenizer tokens = new StringTokenizer(line);
               String method = tokens.nextToken();
               String uri = tokens.nextToken();
               httpVersion = tokens.nextToken();
               keepAlive = getUseKeepAlive() && HTTP_11.equals(httpVersion);
               do {
                   line = readLine();
                   if (line != null) {
                       String lineLower = line.toLowerCase();
                       if (lineLower.startsWith("content-length:")) {
                           contentLength = Integer.parseInt(
                                   line.substring(15).trim());
                       }
                       if (lineLower.startsWith("connection:")) {
                           keepAlive = getUseKeepAlive() &&
                           lineLower.indexOf("keep-alive") > -1;
                       }
                       if (lineLower.startsWith("authorization: basic ")) {
                           parseAuth (line);
                       }
                   }
               } while (line != null && line.length() != 0);
               
               uriArgs = "";
               int argPos = uri.indexOf('?'); 
               if (argPos != -1) {
                   uriArgs = uri.substring(argPos + 1);
                   uri = uri.substring(0, argPos);
               }
               
               logger.debug("HTTP Request: " + method + " " + uri + " args: " + uriArgs + ", keep_alive: " + keepAlive + ",  from: " + channel.socket().getRemoteSocketAddress());
               
               boolean gotRequest = false;
               if ("GET".equalsIgnoreCase(method) && uri.charAt(0) == '/') {
                   try {
                       
                       int versionOffset = uri.indexOf('/', 1);
                       long id = Long.parseLong(uri.substring(1, versionOffset));
                       int version = 0;
                       if (versionOffset != -1) {
                           version = Integer.parseInt(uri.substring(versionOffset+1));
                       }
                       
                       PollObject obj = lookupObject(id);
                       gotRequest = true;
                       if (obj != null && obj.gotNewConnection(this, version)) {
                           state = HANDLING_REQUEST;
                           currentPollObject = obj;
                       } else {
                           logger.debug("Sending immediate response to " + this);
                           // Not found or the poll object is finished (or the uri was wrong)
                           writeResponse(doneString(), httpVersion, keepAlive);
                           output.flush();
                       }
                   } catch (NumberFormatException e) {
                       // Not a correctly formed uri
                   }
               }
               
               if (!gotRequest) {
                   // Always close the connection on errors
                   keepAlive = false;
                   logger.warn("Malformed request for " + uri);
                   writeBadRequest(httpVersion, method);
                   output.flush();
               }
               
               // If keeping alive or handling request, add this connection
               // to the selection
               if (state == HANDLING_REQUEST || keepAlive)
                   addActiveConnection(this);
               
           } catch (Exception exception) {
               keepAlive = false;
               logger.error("Exception during request", exception);
           } finally {
               if (state != HANDLING_REQUEST && !keepAlive) {
                   close();
               }
           }
       }
       
       public void handleReply() {
           assert(state == HANDLING_REQUEST);
           logger.debug("Sending response to " + this);
           try {
               currentPollObject = null;
               writeResponse(doneString(), httpVersion, keepAlive);
               output.flush();
               
               state = WAITING_FOR_REQUEST;
               if (keepAlive)
                   addActiveConnection(this);
           } catch (IOException exception) {
               keepAlive = false;
               logger.error(exception);
           } finally {
               if (channel != null && !keepAlive) {
                   close();
               }
           }
       }
       
       /**
        *
        * @return
        * @throws IOException
        */
       private String readLine() throws IOException  {
           if (buffer == null)
               buffer = new byte[2048];
           
           int next;
           int count = 0;
           while (true) {
               next = input.read();
               if (next < 0 || next == '\n')
                   break;
               if (next != '\r')
                   buffer[count++] = (byte) next;
               if (count >= buffer.length)
                   throw new IOException("HTTP Header too long");
           }
           return new String(buffer, 0, count);
       }
       
       /**
        *
        * @param line
        */
       private void parseAuth(String line) {
           try {
               byte[] c = Base64.decode(toHTTPBytes(line.substring(21)));
               String str = new String(c);
               int col = str.indexOf(':');
               user = str.substring(0, col);
               password = str.substring(col + 1);
           } catch (Throwable ignore) {
           }
       }
       
       private void writeResponse(byte[] payload, String httpVersion,
               boolean keepAlive) throws IOException {
           output.write(toHTTPBytes(httpVersion));
           output.write(ok);
           output.write(server);
           output.write(keepAlive ? conkeep : conclose);
           output.write(ctype);
           output.write(clength);
           output.write(toHTTPBytes(Integer.toString(payload.length)));
           output.write(doubleNewline);
           output.write(payload);
       }
       
       private void writeBadRequest(String httpVersion, String httpMethod)  throws IOException {
           output.write(toHTTPBytes(httpVersion));
           output.write(toHTTPBytes(" 400 Bad Request"));
           output.write(newline);
           output.write(server);
           output.write(newline);
           output.write(toHTTPBytes("Incorrect URL"));
       }
       
       private void writeUnauthorized(String httpVersion, String httpMethod) throws IOException {
           output.write(toHTTPBytes(httpVersion));
           output.write(toHTTPBytes(" 401 Unauthorized"));
           output.write(newline);
           output.write(server);
           output.write(wwwAuthenticate);
           output.write(newline);
           output.write(toHTTPBytes("Method " + httpMethod + " requires a " +
           "valid user name and password"));
       }
   }
}
