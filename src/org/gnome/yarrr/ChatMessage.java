/*
 * Created on May 4, 2005
 */
package org.gnome.yarrr;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.gnome.yarrr.xmlrpc.XmlRpcSerializable;


public class ChatMessage implements XmlRpcSerializable {
    public static int MESSAGE_TYPE_CHAT = 0;
    public static int MESSAGE_TYPE_ACTION = 1;
    public static int MESSAGE_TYPE_WHITEBOARD = 2;
    
    /* When you add here, consider whether ClosedComment.SavedMessage needs to be updated */
    private int index;
    private int type;
    private Date date;
	private Person.Ref author;
    private String nick; // Saved because the author may change nick later
    private Set /* Person.Ref */ subscribers;
    
    // Either contents or whiteboard is null
	private String contents;
    private ArchivedWhiteboard.Ref whiteboard;

	public ChatMessage(Person author, String contents, boolean isAction) {
        this(author, new YarrrMarkup(contents), isAction);
	}
	
	public ChatMessage(Person author, YarrrMarkup contents) {
		this(author, contents, false);
	}
    
	protected ChatMessage(Person author, YarrrMarkup contents, boolean isAction) {
        if (isAction)
            this.type = MESSAGE_TYPE_ACTION;
        else
            this.type = MESSAGE_TYPE_CHAT;
        
        this.author = new Person.Ref(author);
        this.nick = author.getNickName();
        this.contents = contents.toString();
        this.whiteboard = null;
        this.date = new Date();
        this.subscribers = new HashSet();
    }
    
    protected ChatMessage(Person author, ArchivedWhiteboard whiteboard) {
        this.author = new Person.Ref(author);
        this.nick = author.getNickName();
        this.contents = null;
        this.type = MESSAGE_TYPE_WHITEBOARD;
        this.date = new Date();
        this.subscribers = new HashSet();
        
        this.whiteboard = whiteboard.getRef();
    }

    public void setIndex(int index) {
        this.index = index;
    }
    
    public int getIndex() {
        return index;
    }
    
	public synchronized void xmlRpcSerialize(Map table) {
        table.put("index", new Integer(index));
        table.put("type", new Integer(type));
		table.put("date", date);
		table.put("author", author.load());
        table.put("nick", nick);
		table.put("subscribers", ReferencableObject.personRefSetToVector(subscribers));
        if (contents != null)
            table.put("contents", contents);
        if (whiteboard != null) {
            ArchivedWhiteboard wb = whiteboard.load();
            table.put("whiteboardFg", Long.toString(wb.getForeground().getId()));
            table.put("whiteboardFgThumb", Long.toString(wb.getForegroundThumbnail().getId()));
            if (wb.getBackground() != null) {
                table.put("whiteboardBg", Long.toString(wb.getBackground().getId()));
                table.put("whiteboardBgThumb", Long.toString(wb.getBackgroundThumbnail().getId()));
            }
        }
	}
	
    public ArchivedChatMessage archive() {
        ArchivedChatMessage archive = new ArchivedChatMessage(type, date, author, nick, subscribers, contents, whiteboard);
        return archive;
    }
    
    public Person.Ref getAuthor() {
        return author;
    }
    
    public String getContents() {
        return contents;
    }
    
    public Date getDate() {
        return date;
    }
    
    public void addSubscriber(Person subscriber) {
        Person.Ref ref = new Person.Ref(subscriber);
		if (this.subscribers.contains(ref))
			throw new IllegalArgumentException("Subscriber already added: " + subscriber);
    	this.subscribers.add(ref);
    }

    public void removeSubscriber(Person subscriber) {
        Person.Ref ref = new Person.Ref(subscriber);
    	if (!this.subscribers.contains(ref))
			throw new IllegalArgumentException("Subscriber is not present: " + subscriber);
    	this.subscribers.remove(ref);
    }
    
    public boolean hasSubscriber(Person p1) {
        Person.Ref ref = new Person.Ref(p1);
        return this.subscribers.contains(ref);
    }
    
    public int getSubscriberSize() {
        return subscribers.size();
    }
    
    public Set getSubscriberRefs() {
        return subscribers;
    }
}