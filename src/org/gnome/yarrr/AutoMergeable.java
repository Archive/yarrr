/*
 * Created on Apr 26, 2005
 */
package org.gnome.yarrr;

/**
 * @author walters
 */
public interface AutoMergeable {
    public boolean merge(Object other);
}

