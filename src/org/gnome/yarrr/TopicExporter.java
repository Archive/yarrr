package org.gnome.yarrr;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class TopicExporter {
    private DocumentBuilder builder;
    
    TopicExporter() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }        
    }

    private String documentToHTMLString(Document document) {
        OutputFormat format = new OutputFormat();
        format.setIndenting(true);
        format.setMethod("HTML");             
        format.setPreserveSpace(true);
        Writer stringWriter = new StringWriter();
        XMLSerializer serializer = new XMLSerializer (stringWriter, format);
        try {
            serializer.asDOMSerializer();
            serializer.serialize(document);
        } catch (IOException e) {
            // Should never happen
            throw new RuntimeException(e);
        }
        
        return stringWriter.toString();
    }
    
    private Node initHTMLDocument(Document document) {
        Node body, node;
        node = document.appendChild(document.createElement("html"));
        body = document.createElement("body");
        node.appendChild(body);
        return body;
    }
    
    private void appendProponents(Set proponents, Document document, Node container) {
        Iterator proponentsIt = proponents.iterator();
        
        if (proponentsIt.hasNext()) {
            Element proponentsEl = document.createElement("span");
            proponentsEl.setAttribute("style", "font-weight: bold;");
            StringBuffer proponentsList = new StringBuffer ("Proponents: ");
            
            while (proponentsIt.hasNext()) {
                Person person = (Person)proponentsIt.next();
                proponentsList.append(person.getNickName());
                if (proponentsIt.hasNext())
                    proponentsList.append(", ");
            }
            
            proponentsEl.appendChild(document.createTextNode(proponentsList.toString()));
            container.appendChild(proponentsEl);
            container.appendChild(document.createElement("br"));
        }
    }
    
    private void appendChatLog(List messages, Document document, Node container) {
        Node node;
        Iterator logIt = messages.iterator();
        
        if (logIt.hasNext()) {
            node = document.createElement("h3");
            node.appendChild(document.createTextNode("Chat log:"));
            container.appendChild(node);
        }
        
        while (logIt.hasNext()) {
            ArchivedChatMessage message = (ArchivedChatMessage)logIt.next();
            
            YarrrMarkup msgContents = new YarrrMarkup();
            try {
                msgContents.parse(message.getContents());
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
            String line = message.getAuthor() + ": " + msgContents.toText();
            container.appendChild(document.createTextNode(line));
            container.appendChild(document.createElement("br"));
        }
    }
    
    public String exportToHTML(Topic topic) {
        Document document;
        Node body;
        
        document = builder.newDocument();
        body = initHTMLDocument(document);
        
        Iterator it = topic.getClosedComments().iterator();
        while (it.hasNext()) {
            ClosedComment comment = (ClosedComment)it.next();
            YarrrMarkup content = new YarrrMarkup();
            
            try {
                content.parse(comment.getContent());
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            Element div = document.createElement("div");
            div.setAttribute("style", "background: #8faccf; padding: 24px; margin: 24px;");
            body.appendChild(div);

            appendProponents(comment.getProponents(), document, div);            
            div.appendChild(document.createTextNode(content.toText()));
            appendChatLog(comment.getMessages(), document, div);
        }
        
        return documentToHTMLString(document);
    }
    
    public void exportAllToHTML(ZipOutputStream stream) {
        byte content[];
        Iterator it;
        Node node;
        
        Document document = builder.newDocument();
        node = initHTMLDocument(document);
        Node list = document.createElement("ul");
        node.appendChild(list);

        it = Topic.allTopics().iterator();
        while (it.hasNext()) {
            Topic topic = (Topic)it.next(); 
            node = document.createElement("li");
            list.appendChild(node);
            Element a = document.createElement("a");
            node.appendChild(a);
            a.setAttribute("href", topic.getName() + ".html");
            a.appendChild(document.createTextNode(topic.getName()));
        }
        
        try {
            content = documentToHTMLString(document).getBytes();
            stream.putNextEntry(new ZipEntry("index.html"));
            stream.write(content, 0, content.length);
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        
        it = Topic.allTopics().iterator();
        while (it.hasNext()) {
            Topic topic = (Topic)it.next();
            content = exportToHTML(topic).getBytes();
            try {
                stream.putNextEntry(new ZipEntry(topic.getName()+".html"));
                stream.write(content, 0, content.length);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
