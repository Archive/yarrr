/*
 * Created on Apr 15, 2005
 */
package org.gnome.yarrr;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.gnome.yarrr.SerializableVisitor.ObjectVisitor;

/**
 * @author walters
 */
public class JSSerializableVisitor extends SerializableVisitor implements ObjectVisitor {

    public class JSCollectionVisitor implements CollectionVisitor {
        
        public void visit(Serializable item, boolean last) throws UnvisitableObjectException {
            JSSerializableVisitor.this.visit(item);
            postMarshal(last);
        }

        public void beginVisit() {
            output.append("[");
        }

        public Object endVisit(boolean last) {
            output.append("]");
            postMarshal(last);
            return null;
        }

    }
    public class JSMapVisitor implements MapVisitor {

        public void visit(Serializable key, Serializable value, boolean last) throws UnvisitableObjectException {
            JSSerializableVisitor.this.visit(key);
            output.append(": ");
            JSSerializableVisitor.this.visit(value);
            postMarshal(last);
        }

        public void beginVisit() {
            output.append("{");
        }

        public Object endVisit(boolean last) {
            output.append("}");
            postMarshal(last);
            return null;
        }
    }
    
    private int maxDepth;
    private int curDepth;
    private StringBuffer output;
    private Pattern stringPattern;
    
    public JSSerializableVisitor(int depth) {
       this.maxDepth = depth;
       this.curDepth = 0;
       this.output = new StringBuffer();
       this.stringPattern = Pattern.compile("[\\\\\"'<>&\\n]");
    }
    
    public JSSerializableVisitor() {
       this(-1);
    }

    private void writeString(String val) {
        this.output.append("'");
        
        Matcher m = stringPattern.matcher(val);
        while (m.find()) {
            m.appendReplacement(this.output, "\\\\");
            char c = val.charAt(m.start());
            if (c == '\n')
                this.output.append("n");
            else
                this.output.append(c);
        }
        m.appendTail(this.output);
        
        this.output.append("'");
    }
    
    private void postMarshal(boolean last) {
        if (!last) {
            output.append(", ");
        }   
    }   
    
    public void marshalFieldName(String fieldname) {
        writeString(fieldname);
        this.output.append(": ");
    }

    public Object marshalInteger(Integer val, boolean last) {
       this.marshalInteger(val);
       this.postMarshal(last);
       return null;
    }

    public Object marshalDouble(Double val, boolean last) {
        this.marshalDouble(val);
        this.postMarshal(last);
        return null;
    }

    public Object marshalString(String val, boolean last) {
        this.marshalString(val);
        this.postMarshal(last);
        return null;
    }

    public Object transformObject(Object val) {
        if (val instanceof Person) {
            val = ((Person) val).toString();
        }
        return val;
    }

    protected MapVisitor getMapVisitor() {
        return new JSMapVisitor();
    }

    protected CollectionVisitor getListVisitor() {
        return new JSCollectionVisitor();
    }

    protected CollectionVisitor getSetVisitor() {
        return new JSCollectionVisitor();
    }

    public void beginVisit() {
        output.append("{");
    }

    public Object endVisit() {
        output.append("}");
        return null;
    }

    protected void visitMember(String member, Serializable obj, boolean last) throws UnvisitableObjectException {
        if (this.maxDepth > 0 && (this.curDepth > this.maxDepth))
            return;
        this.curDepth++;
        this.visit(obj);
        this.curDepth--;
        this.postMarshal(last);
    }
    
    public String toString() {
        return this.output.toString();
    }

    protected ObjectVisitor getObjectVisitor() {
       return this;
    }

    protected Object marshalInteger(Integer val) {
        output.append(val.toString());
        return null;
    }

    protected Object marshalDouble(Double val) {
        output.append(val.toString());
        return null;
    }

    protected Object marshalString(String val) {
        writeString(val);
        return null;
    }

    public Object endVisit(boolean last) {
        return this.endVisit();
    }

    public void marshalFieldValue(Object val) {
    }
}
