package org.gnome.yarrr;

import java.util.Date;
import java.util.Map;
import java.util.Set;

import org.gnome.yarrr.Person.Ref;

/**
 * This is a by-value type that represents a chat line in a chat log at one
 * specific point in time. If later the line changes (new subscriptors, author
 * changed nick, etc) the archived copy is not affected.
 * 
 * @author alex
 */
public class ArchivedChatMessage {
    private int type;
    
    private Person author;
    private String nick;
    private Date date;
    private Set /* Person */ subscribers;
    private String contents; /* in yarrr markup */
    ArchivedWhiteboard whiteboard;
    
    private ArchivedChatMessage() {
        
    }
    
    public ArchivedChatMessage(int type, Date date, Ref author, String nick, Set subscribers, String contents, ArchivedWhiteboard.Ref whiteboard) {
        this.type = type;
        this.date = date;
        this.author = author.load();
        this.nick = nick;
        //TODO: convert subscribers
        this.contents = contents;
        if (whiteboard != null) 
            this.whiteboard = whiteboard.load();
    }

    public String getContents() {
        return contents;
    }
    
    public Person getAuthor() {
        return author;
    }

    public synchronized void xmlRpcSerialize(Map table) {
        table.put("type", new Integer(type));
        table.put("date", date);
        table.put("author", author);
        table.put("nick", nick);
        //TODO: send subscribers list
        //table.put("subscribers", personRefSetToVector(subscribers));
        if (contents != null)
            table.put("contents", contents);
        if (whiteboard != null) {
            ArchivedWhiteboard wb = whiteboard;
            table.put("whiteboardFg", Long.toString(wb.getForeground().getId()));
            table.put("whiteboardFgThumb", Long.toString(wb.getForegroundThumbnail().getId()));
            if (wb.getBackground() != null) {
                table.put("whiteboardBg", Long.toString(wb.getBackground().getId()));
                table.put("whiteboardBgThumb", Long.toString(wb.getBackgroundThumbnail().getId()));
            }
        }
    }

    
}
