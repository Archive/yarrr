package org.gnome.yarrr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.gnome.yarrr.ArchivedImage.ImageTooLargeException;
import org.gnome.yarrr.xmlrpc.XmlRpcSerializable;

/*
 * Created on Mar 16, 2005
 */
public class ActiveTopic extends ToplevelReferencableObject implements XmlRpcSerializable {

	static Log logger = LogFactory.getLog(ActiveTopic.class);

	private long topicId;

	private Discussion defaultDiscussion;
	private ArrayList /* Discussion */discussions;

    private int updateVersion;

	public ActiveTopic(Yarrr yarrr, long topicId) {
		super(yarrr);
		this.topicId = topicId;

		discussions = new ArrayList();
        
        this.updateVersion = 0;

        ReferencableObjectRegistry.register(this);
        addMonitor(this);

		this.defaultDiscussion = new Discussion(this, yarrr.getTheCapn(), "Default Discussion");
		this.defaultDiscussion.setDefault(true);
        addMonitor(this.defaultDiscussion);
     
        /* We need to add monitors for currently existing (persistant) objects */
        addMonitor(getStatements());
        addMonitor(getClosedComments());
	}
    
    public Topic getTopic() {
        return (Topic) HibernateUtil.getSession().get(Topic.class, new Long(this.topicId));
    }

	public synchronized ClosedComment closeComment(Discussion discussion, LiveComment livecomment, Person closer) {
		Set allAuthors = new HashSet(livecomment.getAllAuthors());
        List /* ChatMessage */ messages = Arrays.asList(discussion.getChat().getMessagesSinceDate(livecomment.getStartTime()));
		ClosedComment comment = new ClosedComment(this.getTopic(), closer, livecomment.getText(), allAuthors, livecomment.getStartTime(), messages);
        addMonitor(comment);
		this.getTopic().addClosedComment(comment);
        discussion.removeComment(livecomment, closer);
		this.signalChanged();
		return comment;
	}

	public synchronized Discussion openDiscussion(Person opener) {
		Discussion d = new Discussion(this, opener, "Untitled");
        addMonitor(d);
		this.discussions.add(d);
		this.signalChanged();
		return d;
	}

    public synchronized List getDiscussions() {
    	Vector ret = new Vector();
    	ret.add(this.defaultDiscussion);
    	ret.addAll(this.discussions);
		return ret;
	}
	
    public synchronized void subscribeStatement(Statement statement, Person subscriber) {
        this.getTopic().subscribeStatement(statement, subscriber);
        this.signalChanged();
    }
    
    public synchronized void unsubscribeStatement(Statement statement, Person subscriber) {
        this.getTopic().unsubscribeStatement(statement, subscriber);
        this.signalChanged();
    }
    
	public synchronized Statement addStatement(Person author, String content) {
        Statement statement = new Statement(this.getTopic(), author, content);
        this.getTopic().addStatement(statement);
        addMonitor(statement);
        this.subscribeStatement(statement, author);
		this.signalChanged();
        return statement;
	}

	public synchronized List getStatements() {
		return this.getTopic().getStatements();
	}

	public List getClosedComments() {
		return this.getTopic().getClosedComments();
	}
	
	public int addChatMessage(Chat chat, Person author, String contents, boolean isAction) {
		ChatMessage msg = new ChatMessage(author, contents, isAction);
		return chat.addMessage(msg);
	}
	
	public int addChatWhiteboard(Chat chat, Person author, Whiteboard board, int version) {
	    ArchivedWhiteboard archived;
        try {
            archived = board.archive(version);
        } catch (ImageTooLargeException e) {
            logger.error("archived whiteboard to large, ignoring");
            return 0;
        }

        // Commit before registering new Chat object
        HibernateUtil.commitTransaction();
        
		ChatMessage msg = new ChatMessage(author, archived);
		return chat.addMessage(msg);
	}

	public synchronized void xmlRpcSerialize(Map table) {
		super.xmlRpcSerialize(table);
		table.put("name", this.getTopic().getName());
		table.put("creator", this.getTopic().getCreator().toString());
		table.put("creationDate", this.getTopic().getCreationDate());
		table.put("closed", new Vector(this.getClosedComments()));
		table.put("discussions", this.getDiscussions());
		table.put("statements", new Vector(this.getStatements()));
        table.put("updateVersion", new Integer(this.updateVersion));
	}

	public synchronized Yarrr getYarrr() {
		return yarrr;
	}

	public Person getTheCapn() {
		return yarrr.getTheCapn();
	}    

    public void incrementUpdateVersion() {
        this.updateVersion++;
        this.signalChanged();
    }
}
