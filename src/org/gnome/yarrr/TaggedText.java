package org.gnome.yarrr;

public class TaggedText implements Diffable {
    private String text;
    private short [] tags; // null means unknown
    
    public static class TaggedTextBuilder implements DiffableBuilder {
        StringBuilder textBuilder;
        short [] tags;
        int tagsLen;
        
        public TaggedTextBuilder() {
            textBuilder = new StringBuilder();
            tags = new short[16];
            tagsLen = 0;
        }

        public void append(Diffable diffable) {
            TaggedText toAppend = (TaggedText)diffable;
            
            textBuilder.append(toAppend.text);
            
            // If it doesn't fit in tags, extend it
            while (tags.length < tagsLen + toAppend.tags.length) {
                short [] newTags = new short [tags.length*2];
                System.arraycopy(tags, 0, newTags, 0, tagsLen);
                tags = newTags;
            }
            for (int i = 0; i < toAppend.tags.length; i++) {
                tags[tagsLen++] = toAppend.tags[i];
            }
        }
        
        public Diffable toDiffable() {
            short [] trimmedTags = new short[tagsLen];
            System.arraycopy(tags, 0, trimmedTags, 0, tagsLen);
            return new TaggedText(textBuilder.toString(), trimmedTags);
        }
    }
    
    public TaggedText(String str, int tag) {
        text = str;
        tags = new short[str.length()];
        for (int i = 0; i < str.length(); i++)
            tags[i] = (short)tag;
    }

    public TaggedText(String str) {
        text = str;
        tags = null;
    }
    
    public TaggedText(String str, short [] tags) {
        text = str;
        this.tags = tags;
        
        if (tags != null && str.length() != tags.length)
            throw new IndexOutOfBoundsException();
    }
   
    String getText() {
        return text;
    }
    String getTagString() {
        StringBuilder builder = new StringBuilder();
        int len, tag, i;
        
        tag = -1;
        len = 0;
        for (i = 0; i < tags.length; i++) {
            if (tags[i] != tag) {
                if (len > 0) {
                    builder.append(tag);
                    builder.append(":");
                    builder.append(len);
                    builder.append(",");
                }
                len = 0;
                tag = tags[i];
            }
            len++;
        }
        if (len > 0) {
            builder.append(tag);
            builder.append(":");
            builder.append(len);
            builder.append(",");
        }

        return builder.toString();
    }
    
    public boolean equalAt(int offset, Diffable other, int otherOffset) {
        TaggedText otherText = (TaggedText)other;
        
        return 
            text.charAt(offset) == otherText.text.charAt(otherOffset) &&
            (tags == null || otherText.tags == null ||
             tags[offset] == otherText.tags[otherOffset]);
    }

    public Diffable getSubset(int offsetStart) {
        return getSubset(offsetStart, text.length());
    }

    public Diffable getSubset(int offsetStart, int offsetEnd) {
        short [] tagsubset;
     
        if (offsetStart < 0 || offsetEnd < 0 ||
            offsetStart > offsetEnd || offsetEnd > text.length())
            throw new IndexOutOfBoundsException();
        
        tagsubset = null;
        if (tags != null) {
            tagsubset = new short[offsetEnd - offsetStart]; 
            System.arraycopy(tags, offsetStart, tagsubset, 0, offsetEnd - offsetStart);
        }
        return new TaggedText(text.substring(offsetStart, offsetEnd), tagsubset);
    }

    private boolean tagsEqual(int start, TaggedText other, int otherStart, int length) {
        if (tags == null || other.tags == null)
            return true;
        for  (int i = 0; i < length; i++) {
            if (tags[start + i] != other.tags[otherStart + i])
                return false;
        }
            
        return true;
    }
    
    public boolean equals(Diffable other, int start, int length) {
        TaggedText otherText = (TaggedText)other;
        
        return
           text.equals(otherText.text.substring(start, start+length)) &&
           tagsEqual(0, otherText, start, length);
    }

    public int length() {
        return text.length();
    }

    public int findFirstMatch(Diffable match, int start) {
        TaggedText matchText = (TaggedText)match;
        int index;
        
        while (true) {
            if (start >= text.length())
                return -1;
            index = text.indexOf(matchText.text, start);
            if (index == -1)
                return -1;
            if (tagsEqual(index, matchText, 0, matchText.text.length()))
                return index;
            start = index + 1;
        }
    }

    public int findLastMatch(Diffable match, int start) {
        TaggedText matchText = (TaggedText)match;
        int index;
        
        while (true) {
            if (start < 0)
                return -1;
            index = text.lastIndexOf(matchText.text, start);
            if (index == -1)
                return -1;
            if (tagsEqual(index, matchText, 0, matchText.text.length()))
                return index;
            start = index - 1;
        }
    }

    public DiffableBuilder createBuilder() {
        return new TaggedTextBuilder();
    }

    public String toString() {
        return "[" + text +"|" + getTagString() + "]";
    }
}
