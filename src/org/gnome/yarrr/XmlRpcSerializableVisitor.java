/*
 * Created on Apr 18, 2005
 */
package org.gnome.yarrr;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

/**
 * @author walters
 */
public class XmlRpcSerializableVisitor extends SerializableVisitor {

    public class XmlRpcMapVisitor implements MapVisitor {
        Hashtable table;

        public void beginVisit() {
            table = new Hashtable();
        }

        public void visit(Serializable key, Serializable value, boolean last) throws UnvisitableObjectException {
            Object serializedKey = XmlRpcSerializableVisitor.this.visit(key);
            Object serializedValue = XmlRpcSerializableVisitor.this.visit(value);
            table.put(serializedKey, serializedValue);
        }

        public Object endVisit(boolean last) {
            return table;
        }
    }

    public class XmlRpcCollectionVisitor implements CollectionVisitor {
        ArrayList array;

        public void beginVisit() {
            this.array = new ArrayList();
        }

        public void visit(Serializable item, boolean last) throws UnvisitableObjectException {
            Object serialized = XmlRpcSerializableVisitor.this.visit(item);
            this.array.add(serialized);
        }

        public Object endVisit(boolean last) {
            return array;
        }
    }

    public class XmlRpcObjectVisitor implements ObjectVisitor {
        private Hashtable table;

        private String currentField;

        public void beginVisit() {
            table = new Hashtable();
        }

        public void marshalFieldName(String name) {
            currentField = name;
        }

        public Object marshalInteger(Integer val, boolean last) {
            return val;
        }

        public Object marshalDouble(Double val, boolean last) {
            return val;
        }

        public Object marshalString(String val, boolean last) {
            return val;
        }

        public Object transformObject(Object val) {
            return val;
        }

        public Object endVisit(boolean last) {
            return table;
        }

        public void marshalFieldValue(Object val) {
            table.put(currentField, val);
        }
    }

    public XmlRpcSerializableVisitor() {
    }

    private Set getFieldStringArrayAsSet(Class klass, String fieldname) {
         try {
            Field f = klass.getField(fieldname);
            f.setAccessible(true);
            int modifiers = f.getModifiers();
            if (!Modifier.isStatic(modifiers) || !Modifier.isFinal(modifiers))
                return null;
            Set ret = new HashSet();
            ret.addAll(Arrays.asList((String[]) f.get(null)));
            return ret;
        } catch (NoSuchFieldException e) {
            return null;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected Set getIncludedMethodsForClass(Class klass) {
        return getFieldStringArrayAsSet(klass, "xmlRpcSerializableMethods");
    }

    protected Set getIgnoredFieldsForClass(Class klass) {
        return getFieldStringArrayAsSet(klass, "xmlRpcIgnoredFields");
    }

    protected void marshalFieldName(String name) {
    }

    protected Object marshalInteger(Integer val, boolean last) {
        return val;
    }

    protected Object marshalDouble(Double val, boolean last) {
        return val;
    }

    protected Object marshalString(String val, boolean last) {
        return val;
    }

    protected MapVisitor getMapVisitor() {
        return new XmlRpcMapVisitor();
    }

    protected CollectionVisitor getListVisitor() {
        return new XmlRpcCollectionVisitor();
    }

    protected CollectionVisitor getSetVisitor() {
        return new XmlRpcCollectionVisitor();
    }

    protected ObjectVisitor getObjectVisitor() {
        return new XmlRpcObjectVisitor();
    }

    protected Object marshalInteger(Integer val) {
        return val;
    }

    protected Object marshalDouble(Double val) {
        return val;
    }

    protected Object marshalString(String val) {
        return val;
    }
}
