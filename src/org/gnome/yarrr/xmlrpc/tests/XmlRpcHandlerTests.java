/*
 * Created on Mar 17, 2005
 */
package org.gnome.yarrr.xmlrpc.tests;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import junit.framework.TestCase;

import org.gnome.yarrr.xmlrpc.XmlRpcDemarshaller;
import org.gnome.yarrr.xmlrpc.XmlRpcHandler;

/**
 * @author alex
 */
public class XmlRpcHandlerTests extends TestCase {
	
	static public class CustomHandler extends XmlRpcHandler {
		public int testInt(int i) {
			return i;
		}
		public boolean testBoolean(boolean bool) {
			return bool;
		}
		public String testString(String s) {
			return s;
		}
		public double testDouble(double d) {
			return d;
		}
		public XmlRpcCustomType testCustom(XmlRpcCustomType c) {
			return c;
		}
		public String testRecursiveCustom(XmlRpcRecursiveCustomType c) {
			return new Integer(c.getOtherdata()).toString();
		}
	}

	private Object doCall1(XmlRpcHandler handler, String name, Object arg) throws Exception {
		Vector args = new Vector();
		args.add(arg);
		return handler.execute(name, args);
	}

	private Object doCall1(String name, Object arg) throws Exception {
		return doCall1(new CustomHandler(), name, arg);
	}

	private void verifyCall(String method, Object data) throws Exception {
		Object res = doCall1(method, data);
		assertEquals(res.getClass(), data.getClass());
		assertEquals(res, data);
	}

	private void verifyCall(XmlRpcHandler handler, String method, Object data) throws Exception {
		Object res = doCall1(handler, method, data);
		assertEquals(res.getClass(), data.getClass());
		assertEquals(res, data);
	}
	
	public void testSimpleCalls() throws Exception {
		verifyCall("testInt", new Integer(42));
		verifyCall("testBoolean", new Boolean(false));
		verifyCall("testBoolean", new Boolean(true));
		verifyCall("testString", "");
		verifyCall("testString", "a string");
		verifyCall("testDouble", new Double(0.0));
		verifyCall("testDouble", new Double(-1.0));
	}

	public void testCustomType() throws Exception {
		XmlRpcHandler handler = new CustomHandler();
		verifyCall(handler, "testCustom", new XmlRpcCustomType(42).xmlRpcMarshal(handler));
		verifyCall(handler, "testCustom", new XmlRpcCustomType(0).xmlRpcMarshal(handler));
	}
	
	public void testRecursiveCustomType() throws Exception {
		XmlRpcHandler handler = new CustomHandler();
		List listdata = new ArrayList();
		
		listdata.add(new XmlRpcCustomType(19));
		listdata.add(new XmlRpcCustomType(33));
		XmlRpcRecursiveCustomType rectype = new XmlRpcRecursiveCustomType(42, listdata);

		doCall1(handler, "testRecursiveCustom", rectype);
		
		List listdata2 = new ArrayList();
		listdata2.add(new XmlRpcCustomType(88));
		listdata.add(new XmlRpcRecursiveCustomType(26, listdata2));
		doCall1(handler, "testRecursiveCustom", rectype);
	}

	public void testCustomTypeWronglyMarshalled() throws Exception {
		boolean exception = false;
		try {
			doCall1("testCustom", "not a long val!");
		} catch (XmlRpcDemarshaller.DemarshallingException e) {
			exception = true;
		}
		assertTrue(exception);
	}

	
	public void testNoMethod() throws Exception {
		boolean exception = false;
		try {
			doCall1("noMethod", "str");
		} catch (NoSuchMethodException e) {
			exception = true;
		}
		assertTrue(exception);
	}
	
	public void testWrongArgType() throws Exception {
		boolean exception = false;
		try {
			doCall1("testInt", "str");
		} catch (NoSuchMethodException e) {
			exception = true;
		}
		assertTrue(exception);
	}
	
}
