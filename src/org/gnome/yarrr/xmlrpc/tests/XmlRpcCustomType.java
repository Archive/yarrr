/*
 * Created on Mar 17, 2005
 */
package org.gnome.yarrr.xmlrpc.tests;

import org.gnome.yarrr.xmlrpc.XmlRpcDemarshaller;
import org.gnome.yarrr.xmlrpc.XmlRpcHandler;
import org.gnome.yarrr.xmlrpc.XmlRpcMarshaller;

/**
 * @author alex
 */
public class XmlRpcCustomType implements XmlRpcDemarshaller, XmlRpcMarshaller {
	long data;
	
	long getValue() {
		return data;
	}
	
	public XmlRpcCustomType(long l) {
		data = l;
	}
	
	static public Object xmlRpcDemarshal(Object xmlRpcObject, XmlRpcHandler handler) throws DemarshallingException {
		try {
			long l = Long.parseLong((String)xmlRpcObject);
			return new XmlRpcCustomType(l);
		} catch (NumberFormatException e) {
			throw new DemarshallingException(e);
		}
	}
	static public Class xmlRpcMarshalledType = String.class;
	
	public Object xmlRpcMarshal(XmlRpcHandler handler) {
		return Long.toString(data);
	}
	
	public boolean equals(Object obj) {
		return data == ((XmlRpcCustomType)obj).data;
	}
}
