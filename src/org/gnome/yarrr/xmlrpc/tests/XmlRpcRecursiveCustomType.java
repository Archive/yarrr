/*
 * Created on Mar 18, 2005
 */
package org.gnome.yarrr.xmlrpc.tests;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gnome.yarrr.xmlrpc.XmlRpcHandler;
import org.gnome.yarrr.xmlrpc.XmlRpcMarshaller;

/**
 * @author walters
 */
public class XmlRpcRecursiveCustomType implements XmlRpcMarshaller {

	private static final long serialVersionUID = 1L;
	List listdata;
	int otherdata;
	
	public XmlRpcRecursiveCustomType(int otherdata, List listdata) {
		this.listdata = listdata;
		this.otherdata = otherdata;
	}
	
	public Object xmlRpcMarshal(XmlRpcHandler handler) {
		Map ret = new HashMap();
		
		ret.put("myvector", this.listdata);
		ret.put("otherdata", new Integer(this.otherdata));
		return ret;
	}
	public int getOtherdata() {
		return otherdata;
	}
}
