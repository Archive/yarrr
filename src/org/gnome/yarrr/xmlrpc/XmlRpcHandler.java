/*
 * Created on Mar 16, 2005
 */
package org.gnome.yarrr.xmlrpc;

import java.awt.Color;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xmlrpc.XmlRpc;
import org.gnome.yarrr.xmlrpc.XmlRpcDispatcher.Invoker;
import org.gnome.yarrr.xmlrpc.XmlRpcMarshaller.MarshallingException;

/**
 * @author alex
 */
public class XmlRpcHandler implements org.apache.xmlrpc.XmlRpcHandler {
	static Log logger = LogFactory.getLog(XmlRpcHandler.class);
    
    private List dispatchers;

	public static Object demarshalFromClass(Class c, Object object) throws Exception {
		try {
			Class [] argTypes = new Class[1];
			argTypes[0] = Object.class;
			Method m = c.getMethod("xmlRpcDemarshal", argTypes);

			Object [] args = new Object[1];
			args[0] = object;
			return m.invoke(null, args);
		} catch (NoSuchMethodException e) {
			throw new XmlRpcDemarshaller.DemarshallingException(e);
		} catch (IllegalArgumentException e) {
			throw new XmlRpcDemarshaller.DemarshallingException(e);
		} catch (IllegalAccessException e) {
			throw new XmlRpcDemarshaller.DemarshallingException(e);
		} catch (InvocationTargetException e) {
			Throwable t = e.getTargetException();
			if (t instanceof Exception)
				throw (Exception)t;
			else
				throw new Exception(t);
		}
	}
	private static Class getMarshalledTypeForClass(Class c) {
		try {
			Field f = c.getField("xmlRpcMarshalledType");
			if (!Modifier.isStatic(f.getModifiers()))
				return null;
			return (Class)f.get(null);
		} catch (SecurityException e1) {
			return null;
		} catch (NoSuchFieldException e1) {
			return null;
		} catch (IllegalArgumentException e) {
			return null;
		} catch (IllegalAccessException e) {
			return null;
		}
	}
    
    public static boolean isDemarshallableParameter(Class argType, Object param) {
        if (XmlRpcDemarshaller.class.isAssignableFrom(argType)) {
            Class marshalledClass = getMarshalledTypeForClass(argType);
            if (marshalledClass != null &&
                    marshalledClass.isAssignableFrom(param.getClass()))
                return true;
        }
        return false;
    }

    public static Object demarshalParameter (Class argType, Object param) throws Exception {
        if (XmlRpcDemarshaller.class.isAssignableFrom(argType)) {
            return demarshalFromClass(argType, param);
        } else {
            return param;
        }
    }

	private Object [] demarshalParams(Invoker invoker) throws Exception {
        Method method = invoker.method;
        Object[] params = invoker.params;
		Object [] out = (Object[]) params.clone();
		Class[] argTypes = method.getParameterTypes();
   
		for (int i = 0; i < out.length; i++) {
			Object param = params[i];
			Class argType = argTypes[i];
            Object demarshalled = invoker.demarshalParameter(argType, param);
            if (demarshalled != param) {
                out[i] = demarshalled;
            } else {
                out[i] = demarshalParameter(argType, param);
            }
		}
		return out;
	}
	
	public Object recursivelyMarshal(Invoker invocation, Object obj) throws MarshallingException {
        Object marshalled = invocation.marshal(obj);
        if (marshalled != obj)
            return marshalled;
		if (obj instanceof XmlRpcMarshaller) {
			obj = ((XmlRpcMarshaller) obj).xmlRpcMarshal(this);
		}
		
		if (obj instanceof List) {
	    	List newlist = new Vector();
	    	Iterator it = ((List) obj).iterator();
	    	
	    	while (it.hasNext()) {
	    		newlist.add(recursivelyMarshal(invocation, it.next()));
	    	}
	    	obj = newlist;
	    } else if (obj instanceof Map) {
	    	Map objmap = (Map) obj;
	    	Map newmap = new Hashtable();
	    	Iterator it = objmap.keySet().iterator();
	    	
	    	while (it.hasNext()) {
	    		Object mapkey = it.next();
	    		newmap.put(mapkey, recursivelyMarshal(invocation, objmap.get(mapkey)));
	    	}
	    	obj = newmap;
	    } 
		
	    return obj;
	}
    
	private Object executeDemarshalled(String methodName, Vector params) throws Exception {
		// The last element of the XML-RPC method name is the Java
        // method name.
		int dot = methodName.lastIndexOf('.');
        if (dot > -1 && dot + 1 < methodName.length())
        	methodName = methodName.substring(dot + 1);
        
        Invoker invoke = null;
        Iterator it = this.dispatchers.iterator();
        while (it.hasNext()) {
            XmlRpcDispatcher dispatcher = (XmlRpcDispatcher) it.next();
            invoke = dispatcher.lookupMethod(methodName, params);
            if (invoke != null)
                break;
        }
        if (invoke == null)
            throw new NoSuchMethodException(methodName);

        invoke.preInvoke();
        Object[] realParams = demarshalParams(invoke);
        Object returnVal;
        try {
            if (logger.isInfoEnabled())
                logger.info("Invoking method: " + invoke.method.getName() + " of " + invoke.method.getDeclaringClass());
            returnVal = invoke.method.invoke(invoke.obj, realParams);
            returnVal = recursivelyMarshal(invoke, returnVal);
            invoke.postInvoke();
        } catch (Exception e) {
            invoke.postInvoke(e);
            returnVal = new Boolean(false);
        } finally {
            invoke.postInvokeFinally();
        }

		return returnVal;
	}

    /**
	 * Here we can special-case how xmlrpc parameters are logged
	 */
	private String parameterAsString(Object o) throws NoSuchAlgorithmException {
		String oString = o.toString();
		
		return oString;
	}
	
	/**
	 * Called in order to invoke any methods of this class. Performs
	 * introspection, and pre-processes a few arguments.
	 * @throws  
	 * 
	 * @see XmlRpcHandler.execute
	 */
	public Object execute(String methodName, Vector params) throws Exception {
		try {
			if (logger.isDebugEnabled()) {
				logger.debug("Invoking XML-RPC method " + methodName + "() with " + params.size()  +" arguments:");
				Iterator i;	int n;
				for (n = 1, i = params.iterator(); i.hasNext(); n++) {
					Object o = i.next();
					logger.debug(n +": " + o.getClass().getName() + " = " + parameterAsString(o));
				}
			}
			
			Object res = executeDemarshalled(methodName, params);
            if (res == null)
                res = new Boolean(true);
			logger.debug("Method " + methodName + " returned: " + res);
			return res;
		} catch (Exception e) {
			logger.warn("Exception", e);
			throw e;
		} catch (Error e) {
			logger.warn("Error", e);
			throw e;
        }
	}
	
	public XmlRpcHandler() {
		try {
			// The included parser doesn't handle whitespace only strings:
			// http://issues.apache.org/bugzilla/show_bug.cgi?id=20482
			XmlRpc.setDriver("org.apache.xerces.parsers.SAXParser");
		} catch (ClassNotFoundException e) {
			logger.error("Can't find xml parser class");
		}
		XmlRpc.setEncoding("UTF-8");
        this.dispatchers = new ArrayList();
	}
    
    public void addDispatcher(XmlRpcDispatcher dispatcher) {
        this.dispatchers.add(dispatcher);
    }
    
  static public int demarshalInt(Hashtable table, String name, int defaultValue) {
      Integer intValue = (Integer)table.get(name);
      if (intValue!=null) {
          return intValue.intValue();
      } else {
          return defaultValue;
      }
  }
  
  static public Color demarshalColor(Hashtable table, String name, Color defaultValue) {
      String stringValue = (String)table.get(name);
      if (stringValue!=null) {
          return Color.decode(stringValue);
      } else {
          return defaultValue;
      }
  }

}
