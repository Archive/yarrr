/*
 * Created on Mar 17, 2005
 */
package org.gnome.yarrr.xmlrpc;


/**
 * @author alex
 */
public interface XmlRpcDemarshaller {
	public static class DemarshallingException extends Exception {
		private static final long serialVersionUID = 1L;
		public DemarshallingException(String s) {
			super(s);
		}
		public DemarshallingException(Exception e) {
			super(e);
		}
	}
	

	/* Interfaces can't really contain static methods, so these are commented out
	 * However, 
	 */
	
	/**
	 * Demarshal an xmlRpcObject
	 * @param xmlRpcObject the object as gotten from xmlrpc
	 * @param handler the xml-rpc handler
	 * @return the demarshalled object
	 */
	//static public Object demarshal(Object xmlRpcObject);
	/**
	 * the type the xmlrpc object must have for this demarshaller to work
	 */
	//static public Class marshalledType;
	
}
