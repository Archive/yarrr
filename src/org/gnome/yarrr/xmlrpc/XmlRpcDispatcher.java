/*
 * Created on Apr 26, 2005
 */
package org.gnome.yarrr.xmlrpc;

import java.lang.reflect.Method;
import java.util.Vector;

/**
 * @author walters
 */
public interface XmlRpcDispatcher {
    public class Invoker {
        public Object obj;
        public Method method;
        public Object[] params;
        
        public void preInvoke() {}
        public Object demarshalParameter(Class argType, Object val) { return val; }
        public Object marshal(Object val) { return val; }
        public void postInvoke() {}
        public void postInvoke(Exception e) throws Exception { throw e; };
        public void postInvokeFinally() {}
    }
    public class ChainInvoker extends Invoker {
        Invoker first;
        Invoker second;
        public ChainInvoker(Invoker first, Invoker second) {
            this.first = first;
            this.second = second;
            this.obj = second.obj;
            this.method = second.method;
            this.params = second.params;
        }
        
        public Object demarshalParameter(Class argType, Object val) {
            return second.demarshalParameter(argType, first.demarshalParameter(argType, val));
        }
        
        public Object marshal(Object val) {
            return second.marshal(first.marshal(val));
        }
        public void postInvoke() {
            first.postInvoke();
            second.postInvoke();
        }
        public void postInvoke(Exception e) throws Exception {
            first.postInvoke(e);
            second.postInvoke(e);
        }
        public void postInvokeFinally() {
            first.postInvokeFinally();
            second.postInvokeFinally();
        }
        public void preInvoke() {
            first.preInvoke();
            second.preInvoke();
        }
    }
    
    public Invoker lookupMethod(String methodName, Vector rawParams);
}
