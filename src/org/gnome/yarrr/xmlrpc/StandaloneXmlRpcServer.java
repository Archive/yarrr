package org.gnome.yarrr.xmlrpc;

import java.util.Hashtable;
import org.apache.xmlrpc.WebServer;

/**
 * Provides an XML-RPC server with which custom handler objects
 * can be registered.
 * 
 * @author seth
 */
public class StandaloneXmlRpcServer {

    private static Hashtable portToServer = new Hashtable();
    
	/**
	 * Starts an XML-RPC server on serverPort if it doesn't 
	 * already exist. This constructor does *not* block until 
	 * the server is started.
	 * 
	 * @param serverPort port to start the XML-RPC server on
	 */
    public static StandaloneXmlRpcServer getXmlRpcServer(int serverPort) {
    	Integer serverPortObject = new Integer(serverPort);
    	StandaloneXmlRpcServer xmlRpcServer = (StandaloneXmlRpcServer)portToServer.get(serverPortObject);
    	if (xmlRpcServer == null) {
    		xmlRpcServer = new StandaloneXmlRpcServer(serverPort);
    	}
    	return xmlRpcServer;
    }
	
    private int serverPort;
	private WebServer server;
	
    private StandaloneXmlRpcServer(int serverPort) {
        this.serverPort = serverPort;
        this.server = new WebServer(this.serverPort);
        this.server.start();        
        // TODO is there a way to block (or add a call to do so) until the server has started?
    }
    
    /**
     * @return the port the XML-RPC server is running on
     */
    public int getServerPort() {
    	return this.serverPort;
    }
    
    /**
     * Halt the XML-RPC server (this does *not* block, server may take extra
     * time to shut down)
     */
    public void stop() {
        // TODO is there a way to block (or add a call to do so) until the server has stopped?
    	this.server.shutdown();
    	portToServer.remove(new Integer(this.getServerPort()));
    }
    
    /**
     * Share a Java object through XML-RPC. Any public methods of the object
     * that have recognized parameter types (see <a href="http://ws.apache.org/xmlrpc/types.html">XML-RPC supported types</a>)
     * will be invocable through XML-RPC at "name.methodName"
     * @param name the namespace to use for methods in this object
     * @param object object whose methods will be exposed over XML-RPC
     */
    public void shareObject(String name, Object object) {
        server.addHandler(name, object);
    }
    
    protected void finalize() throws Throwable {
    	try {
    		stop();
    	} catch (Throwable t) {
    		super.finalize();
    	}
    }
}
