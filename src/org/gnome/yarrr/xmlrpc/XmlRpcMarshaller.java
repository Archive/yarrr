/*
 * Created on Mar 17, 2005
 */
package org.gnome.yarrr.xmlrpc;

/**
 * @author alex
 */
public interface XmlRpcMarshaller {
	public static class MarshallingException extends Exception {
		private static final long serialVersionUID = 1L;
		public MarshallingException(String s) {
			super(s);
		}
		public MarshallingException(Exception e) {
			super(e);
		}
	}
	/**
	 * Marshal a custom object to its xmlrpc form
	 * @param handler the xmlrpc handler
	 * @return an object suitable to passing through xmlrpc
	 */
	public Object xmlRpcMarshal(XmlRpcHandler handler) throws MarshallingException;
}
