/*
 * Created on Apr 1, 2005
 */
package org.gnome.yarrr.xmlrpc;

import java.util.Map;

/**
 * @author walters
 */
public interface XmlRpcSerializable {
	public void xmlRpcSerialize(Map table);
}
