/*
 * Created on Apr 8, 2005
 */
package org.gnome.yarrr;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.gnome.yarrr.xmlrpc.XmlRpcHandler;
import org.gnome.yarrr.xmlrpc.XmlRpcMarshaller;

/**
 * @author walters
 *	
 * This object represents a ReferencableObject which can be polled for changes,
 * maintains presence, and can be recursively serialized to JavaScript.
 */
public class ToplevelReferencableObject extends ReferencableObject {
	/**
	 * @author walters
	 */
    public interface ChangeListener {
        public void changed(ToplevelReferencableObject obj, String ref);
    }

    static public class PollChangesResult implements XmlRpcMarshaller {
        public Vector changes;
        public String pollUrl;
        
        PollChangesResult(Vector changes, String pollUrl) {
            this.changes = changes;
            this.pollUrl = pollUrl;
        }
        
        public Object xmlRpcMarshal(XmlRpcHandler handler) {
            Hashtable table = new Hashtable();
            table.put("changes", changes);
            table.put("pollUrl", pollUrl);
            return table;
        }
    }
    
    private static ChangeListener listener;
    
	protected Yarrr yarrr;

	private Map /* String,ClientPoll */ clientPolls;
    private Map /* Reference,Monitor */ monitors;
    
	private static Log logger = LogFactory.getLog(ToplevelReferencableObject.class);
    
    public static void setChangeListener(ChangeListener listener) {
        ToplevelReferencableObject.listener = listener;
    }
	
	public ToplevelReferencableObject(Yarrr yarrr) {
		// Can't pass "this" as argument to super in Yarrr constructor:
		if (yarrr == null)
			this.yarrr = (Yarrr) this;
		else
			this.yarrr = yarrr;
		
		this.clientPolls = new HashMap();
        this.monitors = new HashMap();
	}

    public void addMonitor(Collection objs) {
        for (Iterator i = objs.iterator(); i.hasNext(); ) {
            ReferencableObject obj = (ReferencableObject) i.next();
            addMonitor(obj);
        }
    }

    public synchronized void addMonitor(ReferencableObject obj) {
        Monitor m = ReferencableObjectRegistry.addMonitor(obj, this);
        monitors.put(obj.getReference(), m);
    }
    
    public synchronized void removeMonitor(ReferencableObject obj) {
        Reference ref = obj.getReference();
        monitors.remove(ref);
        ReferencableObjectRegistry.removeMonitor(obj, this);
    }
    
    
	public synchronized String getPollUrl(Person person, String clientInstanceId) {
		ClientPoll poll = yarrr.getClientPollFor(clientInstanceId);
		if (poll == null) {
			logger.warn("No ClientPoll object for client instance " + clientInstanceId);
			return "";
		}
		
        poll.setToplevel(this);
        clientPolls.put(clientInstanceId, poll);
        
		if (poll.updatePerson(person))
		    signalChanged();
			
		boolean changed = poll.hasChanges(monitors);
		if (changed) 
			return ""; // No need to poll, there is already changes
		
		return poll.getPollUrl();
	}
  
	public synchronized PollChangesResult getPollChanges(Person person, String clientInstanceId) {
        Vector changes;
        String uri;
        
		ClientPoll poll = yarrr.getClientPollFor(clientInstanceId);
		if (poll == null) { 
		    changes = new Vector();
            uri = "";
        } else {
            if (poll.updatePerson(person))
                signalChanged();
            
            changes = poll.getChanges(monitors);
            uri = poll.getPollUrl();
        }
        
        return new PollChangesResult(changes, uri);
	}

	public synchronized void emitSignalChanged(String ref) {
        
	    logger.debug("Object \"" + this + "\" emitting changed: \"" + ref + "\"");
        if (listener != null)
            listener.changed(this, ref);
        
		for (Iterator i = clientPolls.values().iterator(); i.hasNext(); ) {
			ClientPoll poll = (ClientPoll) i.next();
			logger.debug("Marking poll " + poll + " as done");
			poll.markDone();
		}
	}
	
	public synchronized void clientInactive(ClientPoll poll) {
		clientPolls.remove(poll.getClientInstanceId());
		signalChanged();
	}
	
	public synchronized Set getPresentPersons() {
		Set persons = new HashSet();
		
		for (Iterator i = clientPolls.values().iterator(); i.hasNext(); ) {
			ClientPoll poll = (ClientPoll) i.next();
			Person person = poll.getPerson(); 
			if (person != null) 
				persons.add(person);
		}
		return persons;
	}
	
	/**
	 * Recursively inserts into #precache a mapping from
	 * object reference -> serialized form.  This requires
	 * that the object's serialized form contain no circular
	 * references, etc.
	 * 
	 * @param precache a map
	 * @param obj a referenceable object to precache 
	 * @param depth 
	 */
	private void buildReferencePreCache(Map precache, ReferencableObject obj, int depth, ClientPoll clientPoll) {
		
		if (depth == 0)
			return;
		else if (depth < -1)
			depth = -1;
		
		if (clientPoll != null) {
			clientPoll.preloadVersion(obj);
		}
		
		String ref = ReferencableObjectRegistry.getReference(obj);
		Map serialized = new Hashtable();
		obj.xmlRpcSerialize(serialized);
		
		if (precache.containsKey(ref))
            return;
		
		Iterator it = serialized.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Entry) it.next();
			Object member = entry.getValue();
			Collection col;
			
			if (member instanceof Map) {
				col = ((Map) member).values();
			} else if (member instanceof Collection) {
				col = (Collection) member;
			} else if (member instanceof ReferencableObject) {
				col = null;
				buildReferencePreCache(precache, (ReferencableObject) member, depth - 1, clientPoll);
			} else {
				col = null;
			}
			
			if (col != null) {
				Collection colMember = (Collection) member;
				Iterator colMemberIt = colMember.iterator();
				
				while (colMemberIt.hasNext()) {
					Object recValue = colMemberIt.next();
					if (recValue instanceof ReferencableObject) {
						buildReferencePreCache(precache, (ReferencableObject) recValue, depth - 1, clientPoll);
					}
				}
			}
		}
		
		precache.put(ref, serialized);
	}
	
	public synchronized void xmlRpcSerialize(Map table) {
		super.xmlRpcSerialize(table);
		table.put("presentpersons", personCollectionToVector(this.getPresentPersons()));
	}
	
	public synchronized String getReferenceableObjectPreCache(int depth, String sessionId, Person person) {
		Map precache = new HashMap();
        ClientPoll clientPoll = yarrr.getClientPollFor(sessionId);
        if (person != null) {
            // We need to add the current user to the precache so that we can e.g.
            // compare for the users name in cached chat lines 
            this.buildReferencePreCache(precache, person, 1, clientPoll);
        }
		this.buildReferencePreCache(precache, this, depth, clientPoll);
		JSBuilder builder = new JSBuilder();
		builder.appendStringMap(precache);
		return builder.toString();
	}
	
	public String getReferenceableObjectPreCache(String sessionId, Person person) {
		return this.getReferenceableObjectPreCache(-1, sessionId, person);
	}
}
