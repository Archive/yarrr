/*
 * Created on Apr 24, 2005
 */
package org.gnome.yarrr;

import java.util.Random;

/**
 * @author walters
 */
public class DBValidity {
    long id;
    long validity;
    
    public DBValidity() {}
  
    public DBValidity(boolean create) {
        this.validity = new Random().nextLong();
        HibernateUtil.getSession().save(this);
    }
    
    public static DBValidity loadOrCreate() {
        DBValidity validity = load();
        if (validity == null) {
            validity = new DBValidity(true);
        }
        return validity;
    }
    
    public static DBValidity load() {
        return (DBValidity) HibernateUtil.getSession().createCriteria(DBValidity.class).uniqueResult();
    }
}