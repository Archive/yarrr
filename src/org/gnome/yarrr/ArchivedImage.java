package org.gnome.yarrr;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.Blob;
import java.sql.SQLException;

import org.hibernate.Hibernate;

import com.keypoint.PngEncoderB;

/**    }

 * A bitmaped image, saved in the database. 
 * 
 * @author alex
 */
public class ArchivedImage {
    private final static int pngCompressionLevel = 7;
    private final static int maxDataSize = 200000; // Update in .hbm.xml if changed
    
    public static class ImageTooLargeException extends Exception {
        private static final long serialVersionUID = 1L;
    }
    
    private long id;
    private Blob pngData;
    
    ArchivedImage() {
    }

    public ArchivedImage(BufferedImage image, boolean encodeAlpha) throws ImageTooLargeException {
        PngEncoderB encoder = new PngEncoderB(image, encodeAlpha, PngEncoderB.FILTER_NONE, pngCompressionLevel);
        byte [] data = encoder.pngEncode();
        
        if (data.length > maxDataSize)
            throw new ImageTooLargeException();
        
        pngData = Hibernate.createBlob(data);
        
        HibernateUtil.getSession().save(this);
    }
    
    public long getId() {
        return id;
    }

    private void setId(long id) {
        this.id = id;
    }

    public void writeTo(OutputStream out) throws IOException {
        try {
            InputStream in = pngData.getBinaryStream();
            while (true) {
                int data = in.read();
                if (data == -1) {
                    break;
                }
                out.write(data);
            }
            in.close();
        } catch (SQLException e) {
            throw new IOException("Can't load image from blob" + e.getStackTrace());
        }
    }
    
    public Ref getRef() {
        return new Ref(this);
    }

   /**
     * This is a reference to a db object. Its useful if you need to store a image
     * reference in a place where it lives longer than a hibernate session. 
     * 
     * @author alex
     */
    public static class Ref implements Serializable {
        private static final long serialVersionUID = 1L;
        private long id;

        public Ref(ArchivedImage image) {
            id = image.getId();
        }
        
        public Long getId() {
            return new Long(id);   
        }
        
        public ArchivedImage load() {
            return ArchivedImage.load(id);
        }
        
        public boolean equals(Object obj) {
            if (obj instanceof Ref) {
                Ref other = (ArchivedImage.Ref) obj;
                
                return other.id == this.id;
            }
            return false;
        }
        public int hashCode() {
            return (int)id;
        }
        public String toString() {
            return "ArchivedImage.Ref(" + id + ")";
        }
    }
    
    public static ArchivedImage load(Long id) {
        return (ArchivedImage)HibernateUtil.getSession().get(ArchivedImage.class, id);
    }
    
    public static ArchivedImage load(long id) {
        return load(new Long(id));
    }
}
