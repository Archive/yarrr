package org.gnome.yarrr;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

/**
 * @author alex
 */
public class HibernateUtil {
	private static SessionFactory sessionFactory = null;
    
	private static ThreadLocal threadSession = null;
	private static ThreadLocal threadTransaction = null;
    private static ThreadLocal /* <Set<Monitor>> */ threadReferencableObjectChanges = null;
    
    static Log logger = LogFactory.getLog(HibernateUtil.class);
    
	public static boolean initialized() {
		return sessionFactory != null;
	}
	
	public static Configuration buildConfiguration(String url) throws HibernateException {
		return new Configuration()
		.setProperty("hibernate.connection.driver_class", "org.apache.derby.jdbc.EmbeddedDriver")
        .setProperty("hibernate.dialect", "org.hibernate.dialect.DerbyDialect")
        .setProperty("hibernate.connection.url", url)
        .addClass(Person.class)
        .addClass(Statement.class)
        .addClass(ClosedComment.class)
		.addClass(Topic.class)
        .addClass(DBValidity.class)
        .addClass(ArchivedImage.class)
        .addClass(ArchivedWhiteboard.class)
		.setProperty(Environment.HBM2DDL_AUTO, "update");
	}
	
	public static void init(Configuration cfg) throws HibernateException {
		if (initialized())
			return;
		
		// We would like to do all these in a static initializer, but
		// that doesn't let us use a different db for the tests
		threadSession = new ThreadLocal();
		threadTransaction = new ThreadLocal();
        threadReferencableObjectChanges = new ThreadLocal();

        logger.info("initializing Hibernate");
		//cfg.setProperty("hibernate.show_sql", "true");
	
        try {
            sessionFactory = cfg.buildSessionFactory();
        } catch (HibernateException e) {
               // Hibernate will complain here for various reasons if there are problems with the classes
               // we'll be persisting (e.g. errors in the .hbm.xml) 
               // Unfortunately this code gets called ATM from a static initialiser, and it can be difficult
               // to see where the exception gets thrown.  Printing it out here should make it easier to spot problems.
            System.out.println(e.getMessage());
            throw e;
        }
	}
	
	public static void shutdown() throws HibernateException {
        logger.info("shutting down Hibernate");
		sessionFactory.close();
		sessionFactory = null;
		threadSession = null;
		threadTransaction = null;
        threadReferencableObjectChanges = null;
	}

	public static Session getSession() throws HibernateException {
		assert sessionFactory != null;
		Session s = (Session) threadSession.get();
		// Open a new Session, if this thread has none yet
		if (s == null) {
			s = sessionFactory.openSession();
			threadSession.set(s);
		}
		return s;
	}
    
    // Should only be used by test methods
    public static Session openNewSession() {
        return sessionFactory.openSession();
    }
    
    //  Should only be used by test methods
    public static void setSession(Session session) throws HibernateException {
        threadSession.set(session);
        threadTransaction.set(null);
    }
    
    //  Should only be used by test methods
    public static void setTransaction(Session session, Transaction transaction) {
        threadSession.set(session);
        threadTransaction.set(transaction);
    }
    
    //  Should only be used by test methods
    public static Transaction getTransaction() {
        return (Transaction) threadTransaction.get();
    }
    
    public static boolean isTransactionActive() {
        return getTransaction() != null;
    }
	
	/**
	 * Commits the current transaction and begins a new transaction
	 * in a new session. This is useful to really test database changes,
	 * since if you are using the same session you're just testing
	 * in-memory objects. 
	 * 
	 * @throws HibernateException
	 */
	public static void newTestTransaction() throws HibernateException {
		commitTransaction();
		closeSession();
		beginTransaction();
	}
	
	public static void closeSession() throws HibernateException {
		Session s = (Session) threadSession.get();
		threadSession.set(null);
		if (s != null && s.isOpen()) {	
			// If there is an uncommited/rollbacked transaction open, try to roll it back
			Transaction tx = (Transaction) threadTransaction.get();
			threadTransaction.set(null);
			try {
				if (tx != null && !tx.wasCommitted() && !tx.wasRolledBack()) {
					tx.rollback();
				}
			} catch (Exception e) {
				// Whatever, we tried at least
			}
			
			s.close();
		}
	}
	
	public static void beginTransaction() throws HibernateException {
		Transaction tx = (Transaction) threadTransaction.get();
		if (tx == null) {
			Session s = getSession();
			tx = s.beginTransaction();
			threadTransaction.set(tx);
		}
        Set changes = (Set) threadReferencableObjectChanges.get();
        if (changes == null) {
            threadReferencableObjectChanges.set(new HashSet());
        }
	}
    
    public static void addChangedObject(Monitor m) {
        Set s = (Set) threadReferencableObjectChanges.get();
        s.add(m);
    }

	public static void commitTransaction() throws HibernateException {
		Transaction tx = (Transaction) threadTransaction.get();
		try {
			if (tx != null && !tx.wasCommitted() && !tx.wasRolledBack()) {
				tx.commit();
                Iterator it = ((Set)threadReferencableObjectChanges.get()).iterator();
                while (it.hasNext()) {
                    Monitor m = (Monitor) it.next();
                    m.commit();
                }
			}
			threadTransaction.set(null);    
            threadReferencableObjectChanges.set(null);
		} catch (HibernateException ex) {
			rollbackTransaction();
			throw ex;
		}
	}
	
	public static void rollbackTransaction() throws HibernateException {
		Transaction tx = (Transaction) threadTransaction.get();
		try {
			threadTransaction.set(null);
			if (tx != null && !tx.wasCommitted() && !tx.wasRolledBack()) {
				tx.rollback();
			}
            threadReferencableObjectChanges.set(null);
		} finally {
			closeSession();
		}
	}

	
}
