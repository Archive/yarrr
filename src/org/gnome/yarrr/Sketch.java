package org.gnome.yarrr;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Sketch implements Serializable {
    private static final long serialVersionUID = 1L;
    private List /* SketchOp */ ops;
    private int width;
    private int height;

    public Sketch(int width, int height) {
        this.width = width;
        this.height = height;
        ops = new ArrayList();
    }
    
    public void addOperation(SketchOp op) {
        ops.add(op);
    }
    
    public BufferedImage draw(boolean transparent) {
        return draw(ops.size(), transparent);
    }

    public BufferedImage draw(int numOps, boolean transparent) {        
        // FIXME: Port to Cairo once we have Java bindings for it (and packages!)
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = (Graphics2D) image.getGraphics();
        
        Color color;
        if (transparent)
            color = new Color(0xff, 0xff, 0xff, 0x00); // set to fully transparent white
        else
            color = new Color(0xff, 0xff, 0xff, 0xff); // set to white

        g.setBackground(color);

        g.clearRect(0, 0, width, height);            
        
        g.setColor(Color.lightGray);

        Iterator i = ops.iterator();
        for (int n = 0; i.hasNext() && n < numOps; n++) {
            SketchOp op = (SketchOp) i.next();
            
            op.draw(g);
        }
        
        return image;        
    }

    public int getHeight() {
        return height;
    }
    public int getWidth() {
        return width;
    }

    protected Object clone() {
        return clone(ops.size());
    }

    protected Sketch clone(int numOps) {
        Sketch sketch = new Sketch(width, height);
        
        // Don't make clones of the actual ops, as they
        // are not modified after creation
        Iterator i = ops.iterator();
        for (int n = 0; i.hasNext() && n < numOps; n++) {
            SketchOp op = (SketchOp) i.next();
            sketch.ops.add(op);
        }
        
        return sketch;
    }
}
