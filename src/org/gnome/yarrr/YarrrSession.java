package org.gnome.yarrr;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.gnome.yarrr.Person.NickNameExistsException;



public class YarrrSession {
    private static final String PERSON_TAG_COOKIE = "yarrrPersonTag";
    private static final String PERSON_ID_COOKIE = "yarrrPersonId";
    private static final String DB_VALIDITY_COOKIE = "yarrrDBValidity";
    private static Log logger = LogFactory.getLog(YarrrSession.class);
    
    /* 1000 hours */
    private static final int personCookieLife = 3600000;

    private Person.Ref person;
    private HttpServletRequest currentRequest;
    private HttpServletResponse currentResponse;
    private Yarrr yarrr;
    
    YarrrSession(Yarrr yarrr, Person.Ref person) {
    	setPerson(person);
        this.yarrr = yarrr;
    }
    
    void setPerson(Person.Ref person) {
    	this.person = person;
    }
    
    Person.Ref getPerson() {
        return person;
    }
    
    Person loadPerson() {
        return person.load();
    }

    public HttpServletRequest getCurrentRequest() {
        return currentRequest;
    }

    public void setCurrentRequest(HttpServletRequest currentRequest) {
        this.currentRequest = currentRequest;
    }

    public HttpServletResponse getCurrentResponse() {
        return currentResponse;
    }

    public void setCurrentResponse(HttpServletResponse currentResponse) {
        this.currentResponse = currentResponse;
    }
    
    private void deletePersonCookies() {
        Cookie cookies[] = currentRequest.getCookies();
        
        for (int i = 0; cookies != null && i < cookies.length; i++) {
            String name = cookies[i].getName();

            if (name.equals(DB_VALIDITY_COOKIE) ||
                name.equals(PERSON_ID_COOKIE) ||
                name.equals(PERSON_TAG_COOKIE)) {
                logger.debug("Deleting cookie " + name);
                cookies[i].setMaxAge(0);
                getCurrentResponse().addCookie(cookies[i]);
            }
        }
    }
    
    private void setupPersonCookies(Person person) {
        String ref = ReferencableObjectRegistry.getReference(person);
        Cookie cookie;

        cookie = new Cookie(PERSON_ID_COOKIE, ref);
        cookie.setMaxAge(personCookieLife);
        currentResponse.addCookie(cookie);

        cookie = new Cookie(PERSON_TAG_COOKIE, person.getTag());
        cookie.setMaxAge(personCookieLife);
        currentResponse.addCookie(cookie);

        cookie = new Cookie(DB_VALIDITY_COOKIE, new Long(yarrr.getDBValidity()).toString());
        cookie.setMaxAge(personCookieLife);
        currentResponse.addCookie(cookie);
    }

    public Person loginAsGuest(String name) throws NickNameExistsException {
        Person guestPerson = Person.create(name);
        setPerson(guestPerson.getRef());
        setupPersonCookies(guestPerson);
        return guestPerson;
    }

    public boolean login(String nick, String password) {
        Person person = Person.lookup(nick);
        if (person != null) {
            return login(person, password);
        } else {
            return false;
        }
    }

    public boolean login(Person loginPerson, String password) {
        if (loginPerson.getPassword().equals(password)) {
            setPerson(loginPerson.getRef());
            setupPersonCookies(loginPerson);
            return true;
        }
        return false;
    }
    
    public boolean registerPerson(String nick, String email, String password) {
        Person person;
        try {
            person = Person.create(nick);
            registerPerson(person, email, password);
        } catch (NickNameExistsException e) {
            return false;
        }
        return true;
    }

    public boolean registerPerson(Person person, String email, String password) {
        if (person.isRegistered()) {
            return false;
        } else {
            person.setEmail(email);
            person.setPassword(password);
            person.setRegistered(true);
            setupPersonCookies(person);
            
            return true;
        }
    }

    public void logout() {
        deletePersonCookies();
        getCurrentRequest().getSession().invalidate();
    }
}
