/*
 * Created on 22-Mar-2005
 */
package org.gnome.yarrr;

import java.util.Hashtable;

import org.gnome.yarrr.xmlrpc.XmlRpcDemarshaller;
import org.gnome.yarrr.xmlrpc.XmlRpcHandler;
import org.gnome.yarrr.xmlrpc.XmlRpcMarshaller;


public class Point implements XmlRpcDemarshaller, XmlRpcMarshaller {
    int x;
    int y;
    Point (int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    static public Class xmlRpcMarshalledType = Hashtable.class;

    static public Object xmlRpcDemarshal(Object xmlRpcObject) throws DemarshallingException { 
        Hashtable table = (Hashtable)xmlRpcObject;        
        int x = ((Integer)table.get("x")).intValue();  
        int y = ((Integer)table.get("y")).intValue();
        
        return new Point(x,y); 
    }

    /* (non-Javadoc)
     * @see org.gnome.yarrr.xmlrpc.XmlRpcMarshaller#xmlRpcMarshal(org.gnome.yarrr.xmlrpc.XmlRpcHandler)
     */
    public Object xmlRpcMarshal(XmlRpcHandler handler) {
        Hashtable table = new Hashtable();
        table.put("x", new Integer(x));
        table.put("y", new Integer(y));
        return table;
    }

}