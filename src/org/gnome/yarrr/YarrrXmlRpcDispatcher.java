/*
 * Created on Apr 26, 2005
 */
package org.gnome.yarrr;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.gnome.yarrr.ReferencableObjectRegistry.MalformedReferenceException;
import org.gnome.yarrr.ReferencableObjectRegistry.UnknownReferenceException;
import org.gnome.yarrr.xmlrpc.XmlRpcDispatcher;
import org.hibernate.StaleObjectStateException;

/**
 * @author walters
 */
public class YarrrXmlRpcDispatcher implements XmlRpcDispatcher {
    
    private static Log logger = LogFactory.getLog(YarrrXmlRpcDispatcher.class);
    private Object globalMethods;
    public static boolean wrapInvocationsInHibernate = true;
    
    public YarrrXmlRpcDispatcher(Object globalMethods) {
        this.globalMethods = globalMethods;
    }

    private Set getSetField(Class klass, String field) {
        try {
            Field f = klass.getDeclaredField(field);
            return (Set) f.get(null);
        } catch (SecurityException e) {
            throw new RuntimeException(e);
        } catch (NoSuchFieldException e) {
            return new HashSet();
        } catch (Exception e) {
           throw new RuntimeException(e);
        }
    }
    
    private Set getReferenacbleObjectRemoteableMethods(Class klass) {
        return getSetField(klass, "xmlRpcRemotableMethods");
    }
    
    boolean isTransactionMethod(Method m) {
        Class klass = m.getDeclaringClass();
        Set transactions = getSetField(klass, "xmlRpcTransactionMethods");
        if (transactions == null)
            return false;
        return transactions.contains(m.getName());
    }
    
    protected static class YarrrInvoker extends XmlRpcDispatcher.Invoker {
        public Object demarshalParameter(Class argType, Object val) {
            if (ReferencableObject.class.isAssignableFrom(argType) && val.getClass() == String.class)
				try {
					return ReferencableObjectRegistry.lookup((String) val);
                } catch (MalformedReferenceException e) {
                    throw new RuntimeException(e);
				} catch (UnknownReferenceException e) {
                    logger.warn("Invalid reference during demarshalling", e);
					return val;
				}
            return val;
        }
        
        public Object marshal(Object val) {
            if (val instanceof ReferencableObject) {
                return ReferencableObjectRegistry.getReference((ReferencableObject) val);
            }
            return val;
        }
    }
    
    protected static class HibernateTransactionInvoker extends YarrrInvoker {
        
        public void preInvoke() {
            super.preInvoke();
            logger.info("beginning transaction");
            HibernateUtil.beginTransaction();
        }
        
        public void postInvoke() {
            logger.info("committing transaction");
            HibernateUtil.commitTransaction();
            super.postInvoke();
        }
        
        public void postInvoke(Exception e) throws Exception {
            logger.info("rolling back transaction");
            HibernateUtil.rollbackTransaction();
            super.postInvoke(e);
        }
    }
    
    protected static class HibernateSessionInvoker extends YarrrInvoker {
        public void preInvoke() {
            super.preInvoke();
            logger.info("creating session");
            HibernateUtil.getSession();   
        }
        
        public void postInvokeFinally() {
            logger.info("closing session");
            HibernateUtil.closeSession();
            super.postInvokeFinally();
        }
}
    
    protected static class HibernateTransactionMergeInvoker extends HibernateTransactionInvoker {
        public void postInvoke() {
            try {
                super.postInvoke();
            } catch (StaleObjectStateException e) {
                AutoMergeable m = (AutoMergeable) this.obj;
            }

        }
    }
    
    private boolean constructReferencableObjectMethodInvoker(String methodName, Vector params, Invoker invocation) {
        String ref;
        
        if (params.size() == 0 || !(params.get(0) instanceof String))
            return false;
        
        ref = (String) params.get(0);
        
        Class klass = ReferencableObjectRegistry.getReferenceClass(ref);
        if (klass == null)
            return false;
        
        Set remotable = getReferenacbleObjectRemoteableMethods(klass);
        if (!remotable.contains(methodName)) {
            return false;
        }
       
        Method method = findClassMethodByName(klass, methodName);
        if (method.getDeclaringClass() == klass) {
            String objref = (String) params.get(0);
            Object obj;
			try {
				obj = ReferencableObjectRegistry.lookup(objref);
			} catch (MalformedReferenceException e) {
				return false;
			} catch (UnknownReferenceException e) {
                throw new RuntimeException(e);
            }
            List newParams = new ArrayList(params);
            newParams.remove(0);
            invocation.method = method;
            invocation.obj = obj;
            invocation.params = newParams.toArray();
            return true;
        }
        
        return false;
    }
    
    public Invoker lookupMethod(String methodName, Vector rawParams) {
        Invoker invocation;
        invocation = new YarrrInvoker();
            
        if (!constructReferencableObjectMethodInvoker(methodName, rawParams, invocation)) {
            if (!constructLocalMethodInvoker(methodName, rawParams, invocation))
                return null;
        }
        
        if (wrapInvocationsInHibernate) {
            Invoker transactionInvoker;
            if (AutoMergeable.class.isAssignableFrom(invocation.obj.getClass()))
                transactionInvoker = new HibernateTransactionMergeInvoker();
            else
                transactionInvoker = new HibernateTransactionInvoker();
            invocation = new ChainInvoker(transactionInvoker, invocation);
            invocation = new ChainInvoker(new HibernateSessionInvoker(), invocation);
            
        }
        return invocation;
    }
    
    private Method findClassMethodByName(Class klass, String methodName) {
        Method[] methods = klass.getMethods();
        for (int i = 0; i < methods.length; i++) {
            Method method = methods[i];

            if (method.getName().equals(methodName)) {
                int modifiers = method.getModifiers();
                if (Modifier.isPublic(modifiers) && method.getDeclaringClass() != Object.class)
                    return method;
            }
        }
        return null;
    }
    
    private boolean constructLocalMethodInvoker(String methodName, Vector params, Invoker invocation) {
        Method method = findClassMethodByName(globalMethods.getClass(), methodName);
        if (method == null)
            return false;
        if (!Modifier.isStatic(method.getModifiers())) {
            invocation.method = method;
            invocation.obj = globalMethods;
            invocation.params = params.toArray();
            return true;
        }
        return false;
    }
}
