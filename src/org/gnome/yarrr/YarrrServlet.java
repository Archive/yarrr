/*
 * Created on Mar 16, 2005
 */
package org.gnome.yarrr;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipOutputStream;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.DefaultFileItemFactory;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.NDC;
import org.apache.xmlrpc.XmlRpcServer;
import org.gnome.yarrr.Person.NickNameExistsException;
import org.gnome.yarrr.ReferencableObjectRegistry.LookupFailureException;
import org.gnome.yarrr.Yarrr.TopicExistsException;

/**
 * @author alex
 */
public class YarrrServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static ThreadLocal yarrrSession = null;

    static Log logger = LogFactory.getLog(YarrrServlet.class);

    private Yarrr yarrr;

    private XmlRpcServer xmlrpc;

    public YarrrServlet() {
        super();
        this.xmlrpc = new XmlRpcServer();

        yarrrSession = new ThreadLocal();

        yarrr = new Yarrr();
        yarrr.startup(true);
        this.xmlrpc.addHandler("$default", yarrr.getXmlRpcHandler());
    }

    public void destroy() {
        yarrr.shutdown();
        yarrr = null;
    }

    public static YarrrSession getCurrentSession() {
        return (YarrrSession) yarrrSession.get();
    }

    private Person tryAutoLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession httpSession = request.getSession(false);

        if (httpSession == null) {
            httpSession = request.getSession(true);
            return loginByTag(request, response);
        }
        return null;
    }

    private YarrrSession setupHttpSession(HttpServletRequest request, HttpServletResponse response, HttpSession httpSession, boolean poll) {
        YarrrSession session = (YarrrSession) httpSession.getAttribute("yarrrSession");

        if (session == null) {
            session = yarrr.openSession();
            httpSession.setAttribute("yarrrSession", session);
        }

        session.setCurrentRequest(request);
        session.setCurrentResponse(response);

        return session;
    }

    static public void logout(Yarrr yarrr, YarrrSession session) {
    }

    private void getArchivedImage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String stringId = request.getParameter("id");
        long id = Long.parseLong(stringId);

        ArchivedImage image = ArchivedImage.load(id);

        if (image == null) {
            logger.warn("Request for archived image " + id + " which doesn't exist.");
        } else {
            response.setContentType("image/png");
            image.writeTo(response.getOutputStream());
        }
    }

    private void getWhiteboard(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String whiteboardId = request.getParameter("id");
        if (whiteboardId == null)
            return;

        // get ID of whiteboard, activate as necessary
        Whiteboard whiteboard;
        try {
            whiteboard = (Whiteboard) ReferencableObjectRegistry.lookup(whiteboardId);
        } catch (LookupFailureException e) {
            throw new ServletException(e);
        }
        String versionStr = request.getParameter("version");
        String typeStr = request.getParameter("imageType");
        int version;
        if (versionStr != null) {
            version = Integer.valueOf(versionStr).intValue();
        } else {
            version = whiteboard.getVersion();
        }
        response.setContentType("image/png");

        if (typeStr.equals("thumbnail-background")) {
            whiteboard.writeThumbnailBackground(response.getOutputStream());
        } else if (typeStr.equals("fullsize-background")) {
            whiteboard.writeFullsizeBackground(response.getOutputStream());
        } else if (typeStr.equals("thumbnail-foreground")) {
            whiteboard.writeThumbnailEditableLayer(version, response.getOutputStream());
        } else {
            whiteboard.writeFullsizeEditableLayer(version, response.getOutputStream());
        }
    }
    
    private void getExport(HttpServletRequest request, HttpServletResponse response) {
        SimpleDateFormat format = new SimpleDateFormat("yyMMdd-HHmmss");
        String filename = "yarrr-" + format.format(new Date()) + ".zip";
        
        response.setContentType("application/zip");
        response.setHeader("Content-Disposition", "attachment; filename = " + filename); 
        
        try {
            ZipOutputStream stream = new ZipOutputStream(response.getOutputStream());
            TopicExporter exporter = new TopicExporter();
            exporter.exportAllToHTML(stream);
            stream.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void getIndex(HttpServletRequest request, HttpServletResponse response, String clientInstanceId, Person person, String forwardTo) throws ServletException, IOException {
        request.setAttribute("activetopics", new JSBuilder().appendCollection(yarrr.getActiveTopics()).toString());
        request.setAttribute("inactivetopics", new JSBuilder().appendCollection(yarrr.getInactiveTopics()).toString());
        request.setAttribute("yarrrId", ReferencableObjectRegistry.getReference(yarrr));
        // Don't grab all objects, just the yarrr and its topics
        request.setAttribute("referencableObjectPreCache", yarrr.getReferenceableObjectPreCache(2, clientInstanceId, person));
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(forwardTo);
        dispatcher.forward(request, response);
    }

    private void getTopic(HttpServletRequest request, HttpServletResponse response, 
                          String clientInstanceId, String requestedTopic, boolean create,
                          String forwardTo, Person person) throws ServletException, IOException {
        Topic topic;
        ActiveTopic active;
        String topicref;
        
        topic = yarrr.lookupTopic(requestedTopic);
        if (topic == null) {
            if (create) {
                try {
                    topic = yarrr.createTopic(requestedTopic, person);
                } catch (TopicExistsException e) {
                    throw new RuntimeException(e);
                }
            } else {
                logger.info("Request for unknown topic: " + requestedTopic);
                request.setAttribute("topictitle", requestedTopic);
                request.setAttribute("JStopictitle", new JSBuilder().append(requestedTopic).toString());
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/unknowntopic.jsp");
                dispatcher.forward(request, response);
                return;
            }
        }

        active = yarrr.activateTopic(topic);

        topicref = ReferencableObjectRegistry.getReference(active);

        request.setAttribute("topictitle", topic.getName());
        request.setAttribute("JStopictitle", new JSBuilder().append(topic.getName()).toString());
        request.setAttribute("topicid", topicref);
        request.setAttribute("referencableObjectPreCache", active.getReferenceableObjectPreCache(clientInstanceId, person));
        logger.info("Request for topic \"" + topic.getName() + "\"");
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(forwardTo);
        if (dispatcher == null)
            super.doGet(request, response);
        else {
            dispatcher.forward(request, response);
        }
    }

    
    private void doGetInternal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path;
        String requestedTopic;

        Person person = tryAutoLogin(request, response);
        HttpSession httpSession = request.getSession();
        YarrrSession session = setupHttpSession(request, response, httpSession, true);
        
        // Auto-create test user
        if (person == null && request.getParameter("runtest") != null) {
            String testNick = "test" + (int)Math.floor(Math.random()*100000);
            try {
                person = session.loginAsGuest(testNick);
            } catch (NickNameExistsException e) {
                person = null;
            }
        }
        
        if (person != null) {
            session.setPerson(person.getRef());
        }

        path = request.getServletPath();

        logger.info("Request servlet path: " + path);
        
        if (path.equals("/manager/export.jsp")) {
            getExport(request, response);
            return;
        }
              
        if (path.equals("/ArchivedImage")) {
            getArchivedImage(request, response);
            return;
        }

        if (path.equals("/Whiteboard")) {
            getWhiteboard(request, response);
            return;
        }

        // All the following content is dynamic - disable caching
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");

        String clientInstanceId = yarrr.openClientInstance(session, request.getRemoteAddr(), request.getRequestURL().toString());
        request.setAttribute("clientInstanceId", clientInstanceId);
        request.setAttribute("anonymousPerson", yarrr.getAnonymousStringRef());
        String personId = ReferencableObjectRegistry.getReference(Person.class, session.getPerson().getReferenceId());
        request.setAttribute("person", personId);

        if (person == null)
            person = session.loadPerson();

        if (path.equals("/index.jsp")) {
            getIndex(request, response, clientInstanceId, person, "/topiclist.jsp");
            return;
        }
        
        if (path.equals("/wikiembed-topiclist")) {
            getIndex(request, response, clientInstanceId, person, "/wikiembed-topiclist.jsp");
            return;
        }        

        if (path.equals("/topic")) {
            requestedTopic = request.getPathInfo();
            if (requestedTopic != null && requestedTopic.startsWith("/")) {
                requestedTopic = requestedTopic.substring(1);
                getTopic(request, response, clientInstanceId, requestedTopic, false, "/topic.jsp", person);
                return;
            }
        }

        if (path.equals("/wikiembed")) {
            requestedTopic = request.getParameter("topic");
            String headerText = request.getParameter("headertext");
            if (headerText == null)
                headerText = "Discussion";
            request.setAttribute("headertext", new JSBuilder().append(URLDecoder.decode(headerText, "UTF-8")).toString());
            if (requestedTopic != null) {
                getTopic(request, response, clientInstanceId, requestedTopic, true, "/wikiembed.jsp", person);
            }
            return;
        }
        
        super.doGet(request, response);
        return;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        NDC.push("[GET/" + request.getRemoteAddr() + "]");
        HibernateUtil.beginTransaction();
        try {
            doGetInternal(request, response);
            HibernateUtil.commitTransaction();
        } finally {
            HibernateUtil.closeSession();
            NDC.pop();
        }
    }

    // FIXME: Make inline??
    class WhiteboardUploader extends DefaultFileItemFactory {
        public FileItem createItem(String fieldName, String contentType, boolean isFormField, String fileName) {
            if (fieldName.equals("image-content")) {
                return new ImageDecoderFileItem(fieldName, contentType, fileName);
            }
            return super.createItem(fieldName, contentType, isFormField, fileName);
        }
    }

    private void uploadWhiteboardImage(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        FileItemFactory fileItemFactory = new WhiteboardUploader();
        FileUpload fileUpload = new FileUpload(fileItemFactory);
        try {
            List fileItems = fileUpload.parseRequest(request);

            ImageDecoderFileItem imageFileItem = null;
            FileItem discussionIdItem = null;
            for (Iterator iter = fileItems.iterator(); iter.hasNext();) {
                FileItem fileItem = (FileItem) iter.next();
                if (fileItem.getFieldName().equals("image-content")) {
                    imageFileItem = (ImageDecoderFileItem) fileItem;
                } else if (fileItem.getFieldName().equals("discussionId")) {
                    discussionIdItem = fileItem;
                }
            }

            if (imageFileItem == null || imageFileItem.getImage() == null)
                throw new FileUploadException("No image in upload");
            if (discussionIdItem == null)
                throw new FileUploadException("No discussion id in upload");

            String discussionId = new String(discussionIdItem.get()); // FIXME:
            // encoding?
            Discussion discussion;
            discussion = (Discussion) ReferencableObjectRegistry.lookup(discussionId);

            if (discussion == null)
                throw new FileUploadException("No such discussion in upload");

            BufferedImage image = imageFileItem.getImage();

            try {
                discussion.uploadFile(null, image);
            } catch (ArchivedImage.ImageTooLargeException e) {
                // TODO: Handle this nicer
                throw new ServletException(e);
            }

        } catch (FileUploadException e) {
            throw new ServletException(e);
        } catch (LookupFailureException e) {
            throw new ServletException(e);
        }

        // TODO: better handling here
    }

    private void redirectBack(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String redirect = request.getParameter("redirect");

        if (redirect != null) {
            response.sendRedirect(redirect);
        } else {
            response.sendRedirect("/");
        }
    }

    public Person loginByTag(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Cookie cookies[] = request.getCookies();

        boolean validDB = false;
        Person person = null;
        String tag = null;
        for (int i = 0; cookies != null && i < cookies.length; i++) {
            String name = cookies[i].getName();
            String value = cookies[i].getValue();

            if (name.equals("yarrrDBValidity")) {
                long DBValidity = Long.parseLong(value);
                if (yarrr.getDBValidity() == DBValidity) {
                    validDB = true;
                }
            } else if (name.equals("yarrrPersonId")) {
                try {
                    person = (Person) ReferencableObjectRegistry.lookup(value);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else if (name.equals("yarrrPersonTag")) {
                tag = value;
            }
        }

        if (validDB && person != null && tag != null) {
            if (person.getTag().equals(tag)) {
                return person;
            }
        }
        
        return null;
    }
    
    private void login(YarrrSession session, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String nick = request.getParameter("nick");
        String password = request.getParameter("password");

        Person person = Person.lookup(nick);

        if (person == null) {
            try {
                person = session.loginAsGuest(nick);
            } catch (NickNameExistsException e) {
                RequestDispatcher dispatcher;
                dispatcher = getServletContext().getRequestDispatcher("/login-error.jsp");
                dispatcher.forward(request, response);                
            }
            redirectBack(request, response);
            return;
        }

        if (password == null) {
            if (person.isRegistered()) {
                RequestDispatcher dispatcher;
                request.setAttribute("nickName", nick);
                dispatcher = getServletContext().getRequestDispatcher("/login-form.jsp");
                dispatcher.forward(request, response);
            } else {
                /* FIXME already used nickname, show a better error */
                RequestDispatcher dispatcher;
                dispatcher = getServletContext().getRequestDispatcher("/login-error.jsp");
                dispatcher.forward(request, response);
            }
        } else {
            if (session.login(person, password)) {
                redirectBack(request, response);
            } else {
                RequestDispatcher dispatcher;
                dispatcher = getServletContext().getRequestDispatcher("/login-error.jsp");
                dispatcher.forward(request, response);
            }
        }
    }

    public void doPostInternal(YarrrSession session, HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String uri = request.getRequestURI();

        if (uri.equals("/yarrr/topic/upload-whiteboard-image")) {
            uploadWhiteboardImage(request, response);
            return;
        }

        if (request.getServletPath().equals("/register.jsp")) {
            Person person;
            String nick = request.getParameter("nick");
            if (nick != null) {
                try {
                    person = Person.create(nick);
                } catch (NickNameExistsException e) {
                    RequestDispatcher dispatcher;
                    dispatcher = getServletContext().getRequestDispatcher("/register-error.jsp");
                    dispatcher.forward(request, response);
                    return;
                }
            } else {
                person = session.getPerson().load();
            }
            session.registerPerson(person, request.getParameter("email"), request.getParameter("password"));
            redirectBack(request, response);
            return;
        }

        if (request.getServletPath().equals("/login.jsp")) {
            login(session, request, response);
            return;
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Person person = tryAutoLogin(request, response);
        HttpSession httpSession = request.getSession();
        YarrrSession session = setupHttpSession(request, response, httpSession, true);
        if (person != null) {
            session.setPerson(person.getRef());
        }

        NDC.push("[XML-RPC/" + request.getRemoteAddr() + "]");
        try {
            logger.debug("Request for path: " + request.getServletPath());
            if (request.getServletPath().equals("/execute")) {
                yarrrSession.set(session);

                byte[] result = this.xmlrpc.execute(request.getInputStream());
                response.setContentType("text/xml");
                response.setContentLength(result.length);
                OutputStream out = response.getOutputStream();
                out.write(result);
                out.flush();

                yarrrSession.set(null);
            } else {
                HibernateUtil.beginTransaction();
                try {
                    doPostInternal(session, request, response);
                    HibernateUtil.commitTransaction();
                } finally {
                    HibernateUtil.closeSession();
                }
            }
        } finally {
            NDC.pop();
        }
    }

}
