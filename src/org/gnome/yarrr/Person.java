/*
 * Created on Mar 30, 2005
 */
package org.gnome.yarrr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.gnome.yarrr.xmlrpc.XmlRpcSerializable;

/**
 * @author walters
 */
public class Person extends ReferencableObject implements Comparable, Serializable, XmlRpcSerializable {
	private static final long serialVersionUID = 1L;
    private String nickName;
    private Boolean registered;
    private String email;
    private String password;
    private String tag;
    private Integer version;
    
    private static Log logger = LogFactory.getLog(Person.class);
 
    public static class NickNameExistsException extends Exception {
        private static final long serialVersionUID = 1L;
        public NickNameExistsException() {
            super("The nick name already exists.");
        }
    }
    
    public Person() {}
    
	public Person(String tag) {
        this.registered = new Boolean(false);
        this.tag = tag;
        HibernateUtil.getSession().save(this);
	}
    
    public String getTag() {
        return tag;
    }

	public String getNickName() {
		return nickName;
	}
	
	public void setNickName(String nickName) throws NickNameExistsException {
        Person person = Person.lookup(nickName);
        
        /* FIXME this is racy. How can we ensure two equal (case insensitive)
         * nicks are never set in the db?
         */
        if (person != null) {
            throw new NickNameExistsException();
        }
        
		this.nickName = nickName;
	}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }    

    public boolean isRegistered() {
        return registered.booleanValue();
    }

    public void setRegistered(boolean registered) {
        this.registered = new Boolean(registered);
    }

	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String toString() {
		return this.getNickName();
	}

	public int compareTo(Object obj) {
		if (!(obj instanceof Person))
			return -1;
		Person p = (Person) obj;
		return this.getNickName().compareTo(p.getNickName());
	}
	
	public int hashCode() {
		return this.getRef().hashCode();
	}
	
	public boolean equals(Object obj) {
		if (!(obj instanceof Person))
			return false;
		Person p = (Person) obj;
		return this.getRef().equals(p.getRef());
	}
    
    public Ref getRef() {
        return new Ref(this);
    }
    
    public synchronized void xmlRpcSerialize(Map table) {
		super.xmlRpcSerialize(table);
		table.put("name", nickName);
        table.put("registered", registered);
	}
    
    /**
     * This is a reference to a person object. Its useful if you need to store a person
     * reference in a place where it lives longer than a hibernate session. 
     * 
     * @author alex
     */
    public static class Ref implements Serializable {
        private static final long serialVersionUID = 1L;
        private long id;

        public Ref(Person person) {
            id = person.getReferenceId();
        }
        
        public Long getReferenceId() {
            return new Long(id);   
        }
        
        public boolean isPerson(Person person) {
            return id == person.getReferenceId();
        }
        
        public Person load() {
            return Person.load(id);
        }
        
        public boolean equals(Object obj) {
            if (obj instanceof Ref) {
                Ref other = (Person.Ref) obj;
                
                return other.id == this.id;
            }
            return false;
        }
        public int hashCode() {
            return (int)id;
        }
        public String toString() {
            return "Person/" + id;
        }
        
        public static List /* Person */ load(Collection refs) {
            ArrayList l = new ArrayList();
            for (Iterator i = refs.iterator(); i.hasNext(); ) {
                Ref ref = (Ref) i.next();
                l.add(ref.load());
            }
            return l;
        }
    }
    
    public static Person lookup(String nickName) {
        String nick = new String(nickName).toLowerCase();
        Person person;
        
        try {
            person = (Person) HibernateUtil.getSession().
						      createQuery("from Person p where lower(p.nickName) = :nickName").
						      setString("nickName", nick).uniqueResult();
        } catch (Exception e) {
            person = null;
        }
        
        return person;
    }
    
    public static Person lookupOrCreate(String nickName) throws NickNameExistsException {
        Person p = lookup(nickName);
        if (p == null) {
            logger.info("Creating new person \"" + nickName + "\"");
            p = Person.create(nickName);
        }
        return p;
    }
    
    public static Person create(String nickName) throws NickNameExistsException {
        Person person = new Person(new IdentifierGenerator().nextStringIdentifier());
        person.setNickName(nickName);
        return person;
    }
    
    public static Person load(Long id) {
        return (Person)HibernateUtil.getSession().get(Person.class, id);
    }
    
    public static Person load(long id) {
        return load(new Long(id));
    }
}
