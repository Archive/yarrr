    /*
 * Created on May 17, 2005
 */
package org.gnome.yarrr;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * @author walters
 */
public class YarrrXmlRpcMethodsEmbedded extends YarrrXmlRpcMethods {
    private static Log logger = LogFactory.getLog(YarrrXmlRpcMethodsEmbedded.class);
    
    public YarrrXmlRpcMethodsEmbedded(Yarrr yarrr) {
        super(yarrr);
    }
    
    public boolean authenticateUser(String username, String password) {
        Person person = Person.lookup(username);
        if (person != null) {
            return person.getPassword().equals(password);
        } else {
            return false;
        }
    }
    
    public boolean login(String nick, String password) {
        return getSession().login(nick, password);
    }
    
    public boolean register(String nick, String email, String password) {
        return getSession().registerPerson(nick, email, password);
    }
    
    public boolean logout() {
        getSession().logout();
        return true;
    }

    public boolean userExists(String username) {
        Person person = Person.lookup(username);
        return person != null;
    }
    
    public void liveCommentSaving (LiveComment comment) {
        comment.setStateSaving(getCaller());
    }
    
    public void liveCommentSaveComplete (String topicName, String commentName) {
        LiveComment c = lookupLiveComment(topicName, commentName);
        c.setStateSaveComplete();
    }

	private LiveComment lookupLiveComment(String topicName, String commentName) {
		Topic t = this.yarrr.lookupTopic(topicName);
		logger.debug("Updating topic version for " + topicName);
		if (t == null)
            throw new RuntimeException("Invalid topic " + topicName);
		ActiveTopic at = this.yarrr.activateTopic(t);
		Discussion discussion = (Discussion) at.getDiscussions().get(0);
		LiveComment c = discussion.getComment(commentName);
		if (c == null)
            throw new RuntimeException("Invalid comment name " + commentName);
		return c;
	}
    
    public void topicUpdated(String topicName) {
        Topic t = this.yarrr.lookupTopic(topicName);
        logger.debug("Updating topic version for " + topicName);
        if (t == null)
            return;
        ActiveTopic at = this.yarrr.activateTopic(t);
        at.incrementUpdateVersion();
    }
    
    public boolean deleteLiveComment(Discussion discussion, LiveComment comment) {
        discussion.deleteComment(comment);
        return true;
    }
}
