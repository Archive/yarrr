package org.gnome.yarrr;

public interface DiffableBuilder {
    void append(Diffable diffable);
    Diffable toDiffable();
}
