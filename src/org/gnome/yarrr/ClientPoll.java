/*
 * Created on Apr 14, 2005
 */
package org.gnome.yarrr;

import java.net.InetAddress;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.gnome.yarrr.ReferencableObject.Reference;

/**
 * @author alex
 */
public class ClientPoll {
	static Log logger = LogFactory.getLog(ClientPoll.class);
	
	private String clientInstanceId;
	public InetAddress address;
	private String requestURL;
	private Date lastAccess;
	private ToplevelReferencableObject toplevel;
	private Person.Ref person;

	private Map /* ReferencableObject.Reference -> Integer(version) */ versions;
	private PollWebserver.PollObject pollObject;
	
	public ClientPoll(String clientInstanceId, InetAddress address, String requestURL, PollWebserver pollServer) {
		this.clientInstanceId = clientInstanceId;
		this.versions = new HashMap();
		this.address = address;
		this.requestURL = requestURL;
		pollObject = pollServer.newPollObjectFor(address);
		touch();
	}
    
    public String toString() {
        StringBuffer ret = new StringBuffer("[");
        ret.append(super.toString());
        ret.append("] (");
        Iterator it = this.versions.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry e = (Map.Entry) it.next();
            ret.append(e.getKey());
            ret.append(", ");
        }
        ret.append(") clientInstanceId: ");
        ret.append(clientInstanceId);
        if (this.person != null) {
            ret.append(" person: ");
            ret.append(person.getReferenceId());
        }
        return ret.toString();
    }
	
	public String getClientInstanceId() {
		return clientInstanceId;
	}
	
	public void setToplevel(ToplevelReferencableObject toplevel) {
		this.toplevel = toplevel;
	}
	
	public ToplevelReferencableObject getToplevel() {
		return toplevel;
	}

	public boolean updatePerson(Person newPerson) {
		touch();
		if (this.person != null && newPerson != null && 
				this.person.isPerson(newPerson))
			return false;
		this.person = newPerson.getRef();
		return true;
	}
	
	public Person getPerson() {
        if (this.person != null) {
            return this.person.load();
        } else {
            return null;      
        }
	}

	public void touch() {
		lastAccess = new Date();
	}
	
	public boolean isActiveSince(Date time) {
		if (lastAccess.after(time))
			return true;
		if (pollObject.isActive())
			return true;
		return false;
	}
	
	public void detectedInactive() {
		if (toplevel != null) {
			toplevel.clientInactive(this);
			toplevel = null;
		}
        if (pollObject != null) {
            pollObject.delete();
            pollObject = null;
        }
	}

	public String getPollUrl() {
		try {
			return pollObject.getURL(requestURL);
		} catch (URISyntaxException e) {
			logger.error(e);
			return "";
		}
	}
	
	public void markDone() {
	    // This makes the client request return, need to touch it so that we don't believe
		// the ClientPoll is inactive
		touch(); 
		pollObject.markDone();
	}

    private void setVersionFromMonitor(Reference ref) {
        Monitor monitor = ReferencableObjectRegistry.lookupMonitor(ref);
        if (monitor != null) 
            versions.put(ref, new Integer(monitor.getVersion()));
    }
    
	public void preloadVersion(ReferencableObject obj) {
        Reference ref = obj.getReference();
        if (!versions.containsKey(ref))
            setVersionFromMonitor(ref);
	}

    private boolean hasChanged(Reference ref, Monitor monitor) {
        int newVersion = monitor.getVersion();
        Integer lastVersion = (Integer)versions.get(ref);
        
        if (lastVersion == null) {
            // Only changed if not version zero.
            // Version zero change event is the change event of the object that owns this object
            if (newVersion != 0) {
                return true;
            }
        } else if (newVersion != lastVersion.intValue()) {
            return true;
        }
        return false;
    }
    
    /**
     * Returns true if getChanges() would return a non-empty list
     * 
     * @param monitors map from Reference to Monitor for all the current monitors for the toplevel object 
     * @return true if there are outstanding changes the client hasn't seen
     */
    public boolean hasChanges(Map monitors) {
        for (Iterator i = monitors.entrySet().iterator(); i.hasNext(); ) {
            Map.Entry e = (Map.Entry) i.next();
            Reference ref = (Reference)e.getKey();
            Monitor monitor = (Monitor)e.getValue();

            if (hasChanged(ref, monitor)) 
                return true;
        }
        return false;
    }
    
    /**
     * Get list of objects that have changed since the last time we checked.
     * Updates the internal list of tracked versions so that we'll know what changed
     * the next time.
     * 
     * Version zero of an object isn't considered a change. Instead the change that 
     * creates the object happens to the object that owns the new object. 
     * 
     * @param monitors map from Reference to Monitor for all the current monitors for the toplevel object
     * @return a list of ids for the objects that have changes the client hasn't seen 
     */
	public Vector /* String */ getChanges(Map monitors) {
		Vector res = new Vector();

        // Remove entries from versions for objects that no longer exists
        for (Iterator i = versions.keySet().iterator(); i.hasNext();) {
            Reference ref = (Reference) i.next();
            if (!monitors.containsKey(ref))
                i.remove();
        }
  
        for (Iterator i = monitors.entrySet().iterator(); i.hasNext(); ) {
            Map.Entry e = (Map.Entry) i.next();
            Reference ref = (Reference)e.getKey();
            Monitor monitor = (Monitor)e.getValue();
            
            if (hasChanged(ref, monitor)) {
                versions.put(ref, new Integer(monitor.getVersion()));
                res.add(monitor.getId());
            }
        }
        
		return res;
	}
}
