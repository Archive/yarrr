/*
 * Created on May 6, 2005
 *
 */
package org.gnome.yarrr;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

/**
 * @author alex
 */
public class SketchText extends SketchOp {
    private String text;
    private int x;
    private int y;
    private Color color;
    
    SketchText(String text, int x, int y, Color color) {
        this.text = text;
        this.x = x;
        this.y = y;
        this.color = color;
    }
    
    public void draw(Graphics2D g) {
        g.setColor(color);
        g.setFont(new Font("Serif", Font.PLAIN, 24));            
        g.drawString(text, x, y);
    }
}
