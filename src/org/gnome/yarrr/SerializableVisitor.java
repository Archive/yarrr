/*
 * Created on Apr 15, 2005
 */
package org.gnome.yarrr;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

/**
 * @author walters
 */
public abstract class SerializableVisitor {
	
	public class UnvisitableObjectException extends Exception {
		private static final long serialVersionUID = 1L;
		public UnvisitableObjectException(Object obj, String message) {
			super("Visiting object \"" + obj + "\" failed: " + message);
		}
		
		public UnvisitableObjectException(Object obj, boolean isMethod, String element) {
			super("Object \"" + obj + "\" has unvisitable element \"" + element + "\" of class \"" + obj.getClass().toString() + "\"");
		}
		
		public UnvisitableObjectException(Object obj, String element, Object member) {
			super("Object \"" + obj + "\" has unvisitable collection field \"" + element + "\" of class \"" + obj.getClass().toString() + "\" with unvisitable member \"" + member + "\"");
		}
	}
	private Set traversed;
	
	public SerializableVisitor() {
		this.traversed = new HashSet();
	}
	
	interface MapVisitor {
		void beginVisit();
		void visit(Serializable key, Serializable value, boolean last) throws UnvisitableObjectException;
		Object endVisit(boolean last);
	}
	
	interface CollectionVisitor {
		void beginVisit();
		void visit(Serializable item, boolean last) throws UnvisitableObjectException;
		Object endVisit(boolean last);
	}
    
    interface ObjectVisitor {
        void beginVisit();
        
        void marshalFieldName(String name);
        
        Object marshalInteger(Integer val, boolean last);
        
        Object marshalDouble(Double val, boolean last);
        
        Object marshalString(String val, boolean last);
        
        Object transformObject(Object val);
        
        Object endVisit(boolean last);

        void marshalFieldValue(Object val);
    }

	// Should be overridden by subclasses
	protected Set getIgnoredFieldsForClass(Class klass) {
		return null;
	}
	
	// Should be overridden by subclasses
	protected Set getIncludedMethodsForClass(Class klass) {
		return null;
	}
	
	private Set getIgnoredFields(Serializable obj) {
		Class objclass;
		Set ret = new HashSet();
		for (objclass = obj.getClass(); objclass != null && objclass.isAssignableFrom(Serializable.class); objclass = objclass.getSuperclass()) {
			Set ignored = getIgnoredFieldsForClass(objclass);
			if (ignored != null)
				ret.addAll(ignored);
			
		}
		return ret;
	}
    
    protected abstract ObjectVisitor getObjectVisitor();
    
	protected abstract MapVisitor getMapVisitor();
	
	protected abstract CollectionVisitor getListVisitor();
	
	protected abstract CollectionVisitor getSetVisitor();
    
    private boolean isPrimitive(Class type) {
        return (type == Integer.class || type == Double.class || type == String.class);
    }
    
    private Object marshalPrimitive(Object value) {
        Class type = value.getClass();
        if (type == Integer.class) {
            return this.marshalInteger((Integer) value);
        } else if (type == Double.class) {
            return this.marshalDouble((Double) value);
        } else if (type == String.class) {
            return this.marshalString((String) value);
        } else {
            assert(false);
            return null;
        }
    }
    
    private Object marshalPrimitive(ObjectVisitor visitor, Object value, boolean last) {
         Class type = value.getClass();
         if (type == Integer.class) {
             return visitor.marshalInteger((Integer) value, last);
         } else if (type == Double.class) {
             return visitor.marshalDouble((Double) value, last);
         } else if (type == String.class) {
             return visitor.marshalString((String) value, last);
         } else {
             assert(false);
             return null;
         }
    }
	
	private Object marshal(ObjectVisitor objVisitor, Object value, String element, boolean last) throws UnvisitableObjectException {
        Class type = value.getClass();
        
        if (objVisitor != null) {
            value = objVisitor.transformObject(value);
            objVisitor.marshalFieldName(element);
        }
        if (element == null)
            element = "@anonymous@";
       
        if (this.isPrimitive(type)) {
            if (objVisitor != null) {
                return this.marshalPrimitive(objVisitor, value, last);
            } else {
                return this.marshalPrimitive(value);
            }
        } else if (Map.class.isAssignableFrom(type)) {
        	Map map = (Map) value;
            MapVisitor visitor = getMapVisitor();
            Iterator it = map.entrySet().iterator();

            visitor.beginVisit();
            while (it.hasNext()) {
                Map.Entry entry = (Entry) it.next();
                Object key = entry.getKey();
                Object mapValue = entry.getValue();

                if (!(key instanceof Serializable)) {
                    throw new UnvisitableObjectException(value, element, key);
                }
                if (!(mapValue instanceof Serializable)) {
                    throw new UnvisitableObjectException(value, element, mapValue);
                }

                visitor.visit((Serializable) key, (Serializable) mapValue, !it.hasNext());
            }
            return visitor.endVisit(last);
        } else if (Collection.class.isAssignableFrom(type)) {
            CollectionVisitor visitor;
            Collection col = (Collection) value;

            if (List.class.isAssignableFrom(type)) {
                visitor = getListVisitor();
            } else if (Set.class.isAssignableFrom(type)) {
                visitor = getSetVisitor();
            } else {
                throw new UnvisitableObjectException(value, element);
            }
            
            Iterator it = col.iterator();

            visitor.beginVisit();
            while (it.hasNext()) {
                Object val = it.next();
                if (!(val instanceof Serializable)) {
                    throw new UnvisitableObjectException(value, element, val);
                }
                visitor.visit((Serializable) val, !it.hasNext());
            }
            return visitor.endVisit(last);
        } else if (Serializable.class.isAssignableFrom(type)) {
        	return this.marshalSerializable((Serializable) value, last);
        } else {
            throw new UnvisitableObjectException(value, element);
        }
    }
    
    protected abstract Object marshalInteger(Integer val);
    
    protected abstract Object marshalDouble(Double val);
    
    protected abstract Object marshalString(String val);
    
    protected boolean doSynchronized() {
        return true;
    }

    public Object visit(Object obj) throws UnvisitableObjectException {
        if (this.isPrimitive(obj.getClass()))
            return this.marshalPrimitive(obj);
        if (!(obj instanceof Serializable)) {
            throw new UnvisitableObjectException(obj, "not serializable");
        }
        return this.marshal(null, obj, null, true);
    }
    
	private Object marshalSerializable(Serializable obj, boolean last) throws UnvisitableObjectException {
		Class objclass;
        
        ObjectVisitor visitor = this.getObjectVisitor();
        
        visitor.beginVisit();
  
        objclass = obj.getClass();
        if (!isPrimitive(objclass)) {
            if (this.traversed.contains(obj))
                throw new UnvisitableObjectException(obj, "contains circular reference");
            this.traversed.add(obj);
        }
		
        List usedFields = new ArrayList();
        List usedMethods = new ArrayList();
        
		for (; objclass != null && Serializable.class.isAssignableFrom(objclass); objclass = objclass.getSuperclass()) {
			Field[] fields = objclass.getDeclaredFields();
			Set ignored = getIgnoredFields(obj);		
			
			for (int i = 0; i < fields.length; i++) {
				int modifiers = fields[i].getModifiers();
                String name = fields[i].getName();
				if (Modifier.isTransient(modifiers)) {
					continue;
				}
                if (Modifier.isStatic(modifiers)) {
                    continue;
                }
                if (name.startsWith("this$")) {
                    continue;
                }
				if (ignored.contains(name)) {
					continue;
				}
				usedFields.add(fields[i]);
			}
			
			Set includedMethods;
            includedMethods = getIncludedMethodsForClass(objclass);
	
            if (includedMethods != null) {
                Iterator methodsIterator = includedMethods.iterator();
                while (methodsIterator.hasNext()) {
                    String methodName = (String) methodsIterator.next();
        
                    try {
                        Method m = objclass.getMethod(methodName, new Class[0]);
                        usedMethods.add(m);
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
        
        if (this.doSynchronized()) {
            synchronized (obj) {
                marshalSerializableState(obj, visitor, usedMethods.iterator(), usedFields.iterator());
            }
        } else {
            marshalSerializableState(obj, visitor, usedMethods.iterator(), usedFields.iterator());   
        }
       
        return visitor.endVisit(last);
	}

	private void marshalSerializableState(Serializable obj, ObjectVisitor visitor, Iterator usedMethodsIterator, Iterator usedFieldsIterator) throws UnvisitableObjectException {
        boolean haveMethods = usedMethodsIterator.hasNext();
		while (usedFieldsIterator.hasNext()) {
            Field field = (Field) usedFieldsIterator.next();
            field.setAccessible(true);
            Class type = field.getType();
            boolean lastField = !usedFieldsIterator.hasNext() && !haveMethods;
            try {
                Object fieldValue;
                if (type == Integer.TYPE) {
                    fieldValue = new Integer(field.getInt(obj));
                } else if (type == Double.TYPE) {
                    fieldValue = new Double(field.getDouble(obj));
                } else {
                   fieldValue = field.get(obj);
                }
  
                Object val = marshal(visitor, fieldValue, field.getName(), lastField);
                visitor.marshalFieldValue(val);
            } catch (IllegalArgumentException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }

		while (usedMethodsIterator.hasNext()) {
            Method m = (Method) usedMethodsIterator.next();
            String methodName = m.getName();
            
            Object val = null;
            
            try {
                val = m.invoke(obj, new Object[0]);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            if (!(val instanceof Serializable)) {
                throw new UnvisitableObjectException(val, methodName);
            }
            
            StringBuffer subName = new StringBuffer(methodName);
            if (methodName.startsWith("get")) {
                subName.delete(0, 3);
                if (subName.length() > 0) {
                    subName.setCharAt(0, Character.toLowerCase(subName.charAt(0)));
                }
            }

            Object serializedVal = marshal(visitor, val, subName.toString(), !usedMethodsIterator.hasNext());
            visitor.marshalFieldValue(serializedVal);
        }
	}
}
