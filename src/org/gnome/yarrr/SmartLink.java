/*
 * Created on Apr 14, 2005
 */
package org.gnome.yarrr;

/**
 * @author marco
 */
public class SmartLink extends TextSubstitution {

	public SmartLink(String regex, String title, String address, int type) {
		super(regex, type);
		this.addProperty("title", title);
		this.addProperty("address", address);
	}

	static public void addSmartLink(SmartLink smartLink) {
		TextSubstitution.addTextSubstitution(smartLink);
	}
	
	static public void addSmartLinks() {
		SmartLink link;
			
		link = new SmartLink ("\\[GNOME ([A-Za-z0-9_/]+)\\]",
							  "GNOME \"$1\" wiki page",
							  "http://live.gnome.org/$1",
							  LINK_TYPE);
		SmartLink.addSmartLink(link);

		link = new SmartLink ("\\[([A-Za-z0-9_/ ]+)\\]",
							  "$1",
							  "$1",
							  TOPIC_TYPE); 
		SmartLink.addSmartLink(link);
			
		link = new SmartLink ("([A-Za-z0-9_/]+) wiki page",
				  			  "\"$1\" wiki page",
							  "http://live.gnome.org/$1",
							  LINK_TYPE);
		SmartLink.addSmartLink(link);
			
		link = new SmartLink ("http://live.gnome.org/([A-Za-z0-9_/]+)",
							  "\"$1\" wiki page",
		  					  "http://live.gnome.org/$1",
							  LINK_TYPE);
		SmartLink.addSmartLink(link);
			
		link = new SmartLink ("http://bugzilla.gnome.org/show_bug.cgi\\?id\\=([0-9]+)",
							  "GNOME bug $1",
							  "http://bugzilla.gnome.org/show_bug.cgi?id=$1",
							  LINK_TYPE);
		SmartLink.addSmartLink(link);
			
		link = new SmartLink ("https://bugzilla.redhat.com/bugzilla/show_bug.cgi\\?id\\=([0-9]+)",
				  			  "Red Hat bug $1",
							  "https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=$1",
							  LINK_TYPE);
		SmartLink.addSmartLink(link);
		
		link = new SmartLink ("GNOME bug ([0-9]+)",
	 			  			  "GNOME bug $1",
							  "http://bugzilla.gnome.org/show_bug.cgi?id=$1",
							  LINK_TYPE);
		SmartLink.addSmartLink(link);
			
		link = new SmartLink ("Red Hat bug ([0-9]+)",
				              "Red Hat bug $1",
	  			  	          "https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=$1",
				              LINK_TYPE);
		SmartLink.addSmartLink(link);

		link = new SmartLink ("bug ([0-9]+)",
	  			  			  "bug $1",
							  "http://bugzilla.gnome.org/show_bug.cgi?id=$1",
							  LINK_TYPE);
		SmartLink.addSmartLink(link);
			
		String userChars = "-A-Za-z0-9";
        String passChars = "-A-Za-z0-9,?;.:/!%$^*&~\"#'";
        String hostChars = "-A-Za-z0-9";
        String schemes = "(news|telnet|nttp|file|https?|ftps?|webcal)";
        String userAndPass = "[" + userChars +  "]+(:[" + passChars +  "]+)?";
        String email = "[a-z0-9][a-z0-9.-]*@[a-z0-9][a-z0-9-]*(\\.[a-z0-9][a-z0-9-]*)+";

        link = new SmartLink (
                "(" + schemes + "://(" + userAndPass +  "@)?)[" + hostChars +  ".]+(:[0-9]+)?" + 
                "(/[-A-Za-z0-9_$.+!*(),;:@&=?/~#%]*[^]'.}>) \t\r\n,\\\"])?",
                "$0",
                "$0",
                LINK_TYPE);
        SmartLink.addSmartLink(link);
        
        link = new SmartLink (
                "(www|ftp)[" + hostChars  + "]*\\.[" + hostChars + ".]+(:[0-9]+)?" + 
                "(/[-A-Za-z0-9_$.+!*(),;:@&=?/~#%]*[^]'.}>) \t\r\n,\\\"])?",
                "$0",
                "http://$0",
                LINK_TYPE);
        SmartLink.addSmartLink(link);
        
        link = new SmartLink ("(mailto:)"+email,
                "$0",
                "$0",
                LINK_TYPE);
        SmartLink.addSmartLink(link);
        link = new SmartLink (email,
                "$0",
                "mailto:$0",
                LINK_TYPE);
        SmartLink.addSmartLink(link);

        link = new SmartLink (
                "news:[-A-Z\\^_a-z{|}~!\"#$%&'()*+,./0-9;:=?`]+@[" + hostChars + ".]+(:[0-9]+)?",
                "$0",
                "$0",
                LINK_TYPE);
        SmartLink.addSmartLink(link);
	}
}