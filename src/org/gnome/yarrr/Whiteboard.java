/*
 * Created on 21-Mar-2005
 *
 */
package org.gnome.yarrr;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.ref.SoftReference;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.gnome.yarrr.ArchivedImage.ImageTooLargeException;
import org.gnome.yarrr.xmlrpc.XmlRpcHandler;
import org.gnome.yarrr.xmlrpc.XmlRpcMarshaller;
import org.gnome.yarrr.xmlrpc.XmlRpcSerializable;

import com.keypoint.PngEncoderB;


/**
 * @author dmalcolm
 * 
 */
public class Whiteboard extends ReferencableObject implements XmlRpcSerializable {
    static Log logger = LogFactory.getLog(Whiteboard.class);
    static byte[] emptyPng = makePng(makeEmptyImage(1, 1, Color.white), false);
    private final static int pngCompressionLevel = 7;
    private final boolean drawVersionDebugString = false;
    public static final int THUMBNAIL_SIZE = 128;

    private class ForegroundCache {
        private class CacheEntry {
            public int cachedVersion;
            public SoftReference softRef;            
            
            public CacheEntry(int version, Object data) {
                this.cachedVersion = version;
                this.softRef = new SoftReference(data);
            }
        }
        
        public class Cache {
            /* 
             * Simple 1-entry cache for each size: 
             * This should work OK for the case where most people are viewing the latest image.  
             * FIXME: Does this thrash when you start pasting lots of snapshots into a chat and have new clients connect? (we're saved a lot by clientside caching)  
             */
            CacheEntry entry;
            
            public synchronized byte [] get(int requestedVersion) {
                if (this.entry!=null) {
                    if (this.entry.cachedVersion==requestedVersion) {
                        return (byte [])this.entry.softRef.get();
                    }
                }
                
                return null;
            }
            
            public synchronized void put(int cacheVersion, Object data) {
                this.entry = new CacheEntry(cacheVersion, data);
            }
        }
        
        Cache fullsizePngCache = new Cache();
        Cache thumbnailPngCache = new Cache();
        
        private byte[] renderFullsize(int requestedVersion) {
            BufferedImage image = drawImage(requestedVersion, true);            
            byte[] fullBytes = makePng(image, true);
            fgCache.putFullsizePng(requestedVersion, fullBytes);
            
            // If you need the Full size image, your gonna need the thumb too
            // Lets create it so we don't have to render twice
            BufferedImage bufferedScaledImage = makeThumbnail(image);        
            byte[] thumbBytes = makePng(bufferedScaledImage, true);
            fgCache.putThumbnailPng(requestedVersion, thumbBytes);
            return fullBytes;
        }

        private byte[] renderThumbnail(int requestedVersion) {
            BufferedImage image = drawImage(requestedVersion, true);
            BufferedImage bufferedScaledImage = makeThumbnail(image);        
            byte[] thumbBytes = makePng(bufferedScaledImage, true);
            fgCache.putThumbnailPng(requestedVersion, thumbBytes);
            return thumbBytes;
        }

        public byte[] getFullsizePng(int requestedVersion) {
            return fullsizePngCache.get(requestedVersion);
        }
        public void putFullsizePng(int requestedVersion, byte[] data) {
            fullsizePngCache.put(requestedVersion, data);                
        }
        
        public byte[] getThumbnailPng(int requestedVersion) {
            return thumbnailPngCache.get(requestedVersion);
        }
        public void putThumbnailPng(int requestedVersion, byte[] data) {
            thumbnailPngCache.put(requestedVersion, data);                
        }
    }
    
    private ForegroundCache fgCache = new ForegroundCache();
    
    
    /**
     * @author dmalcolm
     * 
     * result of getOpIdsBetween
     */
    public class OpFetchResult implements XmlRpcMarshaller {
        private int latestAvailableVersion;
        private int latestFetchedVersion;
        private List opIds;
        
        /*
         * result contains:
         * the latest version available on the server
         * the latest fetched version by the client
         * a collection of stroke IDs  
         */
        public OpFetchResult(int latestAvailableVersion, int latestFetchedVersion, List opIds) {
            this.latestAvailableVersion = latestAvailableVersion;
            this.latestFetchedVersion = latestFetchedVersion;
            this.opIds = opIds;
        }
        
        public List getOpIds() {
            return opIds;
        }

        public Object xmlRpcMarshal(XmlRpcHandler handler) {
            Hashtable table = new Hashtable();
            table.put("latestAvailableVersion", new Integer(latestAvailableVersion));
            table.put("latestFetchedVersion", new Integer(latestFetchedVersion));
            table.put("opIds", this.opIds);
            return table;
        }

    }
   
    private int version;
    private Sketch sketch;
    /* ids[0] == operation that goes from version 0 to version 1 */
    private ArrayList /* String */ ids; 
    private Person.Ref creator;
    private ArchivedImage.Ref background;
    private ArchivedImage.Ref backgroundThumbnail;

    Whiteboard(Person creator, int width, int height) {
        if (creator != null)
            this.creator = creator.getRef();
        version = 0;
        sketch = new Sketch(width, height);
        
        background = null;
        backgroundThumbnail = null;
        
        this.ids = new ArrayList();
        
        ReferencableObjectRegistry.register(this);
    }
    
    public Whiteboard(Person creator, BufferedImage backgroundImage) throws ImageTooLargeException {
        if (creator != null)
            this.creator = creator.getRef();

        this.version = 0;
        sketch = new Sketch(backgroundImage.getWidth(), backgroundImage.getHeight());

        background = new ArchivedImage(backgroundImage, false).getRef();
        BufferedImage backgroundImageThumbnail = makeThumbnail(backgroundImage);
        backgroundThumbnail = new ArchivedImage(backgroundImageThumbnail, false).getRef();
        
        // Make sure the transaction succeeds before we register the Whiteboard
        HibernateUtil.commitTransaction(); 

        this.ids = new ArrayList();
        
        ReferencableObjectRegistry.register(this);        
    }

    synchronized public void addOp(SketchOp op, String id) {
        sketch.addOperation(op);
        ids.add(id);
        version++;
        // Preload the caches so there is no race to draw them.
        // A race for it could cause the thumb to be drawn first
        // thus requiring us to render the image twice
        fgCache.renderFullsize(version);
        this.signalChanged();
    }

    synchronized public void addStroke(SketchStroke stroke, String id) {
        addOp(stroke, id);
    }

    synchronized public void addText(String text, int x, int y, Color color) {
        SketchText op = new SketchText(text, x, y, color);
        addOp(op, null);
    }
    
    private static BufferedImage toBufferedImage(Image image, int type) {
        int w = image.getWidth(null);
        int h = image.getHeight(null);
        BufferedImage result = new BufferedImage(w, h, type);
        Graphics2D g = result.createGraphics();
        g.drawImage(image, 0, 0, null);
        g.dispose();
        return result;
    }
    
    public static BufferedImage makeThumbnail(BufferedImage image) {
        int size = image.getWidth();
        if (size < image.getHeight())
            size = image.getHeight();
        
        double factor = 1.0;
        if (size > THUMBNAIL_SIZE)
            factor = ((double)THUMBNAIL_SIZE)/size;
        
        Image scaledImage = image.getScaledInstance((int)(image.getWidth() * factor), (int)(image.getHeight() * factor), BufferedImage.SCALE_SMOOTH);
        BufferedImage bufferedScaledImage = toBufferedImage(scaledImage, BufferedImage.TYPE_INT_ARGB);        
        return bufferedScaledImage;
    }
    
    private static byte[] makePng(BufferedImage image, boolean encodeAlpha) {
        PngEncoderB encoder = new PngEncoderB(image, encodeAlpha, PngEncoderB.FILTER_NONE, pngCompressionLevel);        
        byte[] bytes = encoder.pngEncode();        
        return bytes;        
    }
    
    /* make an opaque image of the given directions: */
    private static BufferedImage makeEmptyImage(int width, int height, Color color) {
        BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = (Graphics2D) image.getGraphics();
        g.setBackground (color);
        g.clearRect(0, 0, width, height);
        return image;
    }

    public BufferedImage drawImage(int drawVersion, boolean transparent) {        
        // Log the cache misses:
        logger.debug("drawing version "+drawVersion);
        
        BufferedImage image = sketch.draw(drawVersion, transparent);
            
        if (drawVersionDebugString) {
            Graphics2D g = (Graphics2D) image.getGraphics();
            g.setColor(Color.lightGray);
            g.setFont(new Font("Serif", Font.ITALIC, 48));            
            g.drawString("version " + drawVersion, 10, 50);
        }
        
        return image;        
    }

    /*  
     * get strokes ids added between the versions startVersion 
     * and endVersion. e.g. getStrokeIdsBetween(3, 5)
     * returns the strokes added in versions 4 and 5
     * 
     * Also return the latest version available
     */
    public OpFetchResult getStrokeIdsBetween(int startVersion, int endVersion) {
        if (startVersion < 0)
            throw new IllegalArgumentException("Invalid version");
        if (startVersion > version) {
            startVersion = version;
        }
        if (endVersion < 0)
            throw new IllegalArgumentException("Invalid version");
        if (endVersion > version) {
            endVersion = version;
        }
        
        // Remember that ids[n] is the changes from version n-1 to n
        List idsBetween = ids.subList(startVersion, endVersion);
        List fetched = new ArrayList();
        for (Iterator i = idsBetween.iterator(); i.hasNext(); ) {
            String id = (String)i.next();
            if (id != null) 
                fetched.add (id);
        }
        
        logger.debug("Returning " + fetched.size() + " strokeIds for version delta from " + startVersion + " to " + endVersion);
        
        return new OpFetchResult(version, endVersion, fetched);
    }

    public Person getCreator() {
        if (creator != null)
            return creator.load();
        else
            return null;
    }
    public void setCreator(Person creator) {
        if (creator != null)
            this.creator = creator.getRef();
        else
            this.creator = null;
    }
    public int getHeight() {
        return sketch.getHeight();
    }
    public int getWidth() {
        return sketch.getWidth();
    }
    
    public int getVersion() {
        return version;   
    }

    public ArchivedWhiteboard archive(int archiveVersion) throws ImageTooLargeException {
        ArchivedImage bg, bgThumb;
        
        bg = bgThumb = null;
        if (background != null)
            bg = background.load();
        if (backgroundThumbnail != null)
            bgThumb = backgroundThumbnail.load();
        
        boolean transparent = (background != null);
        
        BufferedImage foreground = drawImage(archiveVersion, transparent);
        BufferedImage foregroundThumbnail = makeThumbnail(foreground); 
        
        ArchivedImage fg = new ArchivedImage(foreground, transparent);
        ArchivedImage fgThumb = new ArchivedImage(foregroundThumbnail, transparent);
        
        ArchivedWhiteboard archived = new ArchivedWhiteboard(fg, fgThumb, bg, bgThumb);
        
        return archived;
    }
    
    public synchronized void xmlRpcSerialize(Map table) {
    	super.xmlRpcSerialize(table);
        if (creator!=null) {
            table.put("creator", creator.load().toString());
        }
    	table.put("width", new Integer(getWidth()));
    	table.put("height", new Integer(getHeight()));
    }

    /**
     * @param data
     * @param contentType
     * @return
     * @throws IOException 
     */
    public static BufferedImage generateImageFromFile(byte[] data, String contentType) throws IOException {
        return ImageIO.read(new ByteArrayInputStream(data));
    }

    /**
     * Writes a thumnbnail of the background to the output stream an a PNG image
     * @param outputStream
     * @throws IOException 
     * @throws SQLException 
     */
    public void writeThumbnailBackground(OutputStream outputStream) throws IOException {
        if (backgroundThumbnail != null) 
            backgroundThumbnail.load().writeTo(outputStream);
        else
            outputStream.write(emptyPng);
    }

    /**
     * Writes the background to the output stream an a PNG image
     * @param outputStream
     * @throws IOException 
     * @throws SQLException 
     */
    public void writeFullsizeBackground(OutputStream outputStream) throws IOException {
        if (background != null)
            background.load().writeTo(outputStream);
        else
            outputStream.write(emptyPng);
    }

    private byte [] getFullsizeEditable(int requestedVersion) {
        byte[] png = fgCache.getFullsizePng(requestedVersion);
        
        if (png == null) { 
            logger.debug("fullsize png cache miss: v"+requestedVersion);
            png = fgCache.renderFullsize(requestedVersion);
        } else {
            logger.debug("fullsize png cache hit: v"+requestedVersion);
        }
        return png;
    }
    
    private byte [] getThumbnailEditable(int requestedVersion) {
        byte[] png = fgCache.getThumbnailPng(requestedVersion);
        
        if (png == null) { 
            logger.debug("thumbnail png cache miss: v"+requestedVersion);
            png = fgCache.renderThumbnail(requestedVersion);
        } else {
            logger.debug("thumbnail png cache hit: v"+requestedVersion);
        }
        return png;
    }
    
    /**
     * Writes a thumnbnail of the given version of the editable layer of the whiteboard to the output stream as a PNG image
     * @param requestedVersion
     * @param outputStream      markup.appendWhiteboard(board, version);

     * @throws IOException 
     */
    public synchronized void writeThumbnailEditableLayer(int requestedVersion, OutputStream outputStream) throws IOException {
        byte[] png = getThumbnailEditable(requestedVersion);
        outputStream.write(png);            
    }

    /**
     * Writes the given version of the editable layer of the whiteboard to the output stream as a PNG image
     * @param requestedVersion
     * @param outputStream
     * @throws IOException 
     */
    public synchronized void writeFullsizeEditableLayer(int requestedVersion, OutputStream outputStream) throws IOException {
        byte[] png = getFullsizeEditable(requestedVersion);        
        outputStream.write(png);            
    }
}
