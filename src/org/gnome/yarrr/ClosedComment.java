/*
 * Created on Mar 28, 2005
 */
package org.gnome.yarrr;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.gnome.yarrr.xmlrpc.XmlRpcSerializable;

/**
 * @author walters
 */
public class ClosedComment extends ReferencableObject implements XmlRpcSerializable {
    private Topic topic;
	private String content;
	private Person closer;
    private Date openDate;
	private Date closeDate;
	private Set /* Person */ proponents;
	private Set /* Person */ authors;
    private Integer version;
    private List /* ArchivedChatMessage */ chatLog;
            
    private static Log logger = LogFactory.getLog(ClosedComment.class);
    
    public static Set xmlRpcRemotableMethods = new HashSet();

    ClosedComment() {}
    
    public ClosedComment(Topic topic, Person closer, String content, Set /* Person */ authors, Date openDate, List /* ChatMessage */ messages) {
        this.openDate = openDate;
        this.topic = topic;
		this.content = new YarrrMarkup(content).toString();
		this.closer = closer;
		this.closeDate = new Date();
		this.proponents = new HashSet();
        this.authors = authors;
        this.chatLog = new ArrayList();
        for (Iterator i = messages.iterator(); i.hasNext(); ) {
            ChatMessage message = (ChatMessage) i.next();
            ArchivedChatMessage archived = message.archive();
            chatLog.add(archived);
        }
        HibernateUtil.getSession().save(this);
	}
    
	public List /* ArchivedChatMessage */ getMessages() {
		return chatLog;
	}
    
    public Vector /* Hashtable */ dumpMessages() {
        List msgs = getMessages();
        Vector ret = new Vector();
        Iterator it = msgs.iterator();
        while (it.hasNext()) {
            ArchivedChatMessage m = (ArchivedChatMessage) it.next();
            Map serialized = new HashMap();
            m.xmlRpcSerialize(serialized);
            ret.add(serialized);
        }
        return ret;
    }
    
    public synchronized String getContent() {
        return content;
    }
    
    static { xmlRpcRemotableMethods.add("addProponent"); }
	public synchronized void addProponent(Person proponent) {
		if (!this.authors.contains(proponent))
			throw new IllegalArgumentException("Proponent not an author: " + proponent);
		if (this.proponents.contains(proponent))
			throw new IllegalArgumentException("Proponent already added: " + proponent);
        logger.info("adding comment proponent: " + proponent);
		this.proponents.add(proponent);
		signalChanged();
	}
	
    static { xmlRpcRemotableMethods.add("getProponents"); }
	public synchronized Set getProponents() {
		return proponents;
	}

	public synchronized void xmlRpcSerialize(Map table) {
		super.xmlRpcSerialize(table);
		table.put("text", content);
		table.put("date", closeDate);
		table.put("closer", closer.toString());
		table.put("proponents", personCollectionToVector(proponents));
		table.put("authors", personCollectionToVector(authors));
	}

    static { xmlRpcRemotableMethods.add("getCloser"); }
    public Person getCloser() {
        return this.closer;
    }

    static { xmlRpcRemotableMethods.add("getAuthors"); }
    public Set getAuthors() {
        return this.authors;
    }
}

