/*
 * Created on Mar 16, 2005
 *
 */
package org.gnome.yarrr;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Describes the persistant state of a topic.
 */
public class Topic extends ReferencableObject {
	private String name;
	private Person creator;
	private Date creationDate;
    private List /* ClosedComment */closedComments;
    private List /* Statement */ statements;
    private Integer version;
    
    Topic(){   
    }
	
	public Topic(String name, Person creator) {
		this.name = name;
		this.creator = creator;
		this.creationDate = new Date();
        this.closedComments = new ArrayList();
        this.statements  = new ArrayList();
		ReferencableObjectRegistry.register(this);
        HibernateUtil.getSession().save(this);
	}
	
	public String getName() {
		return name;
	}
	public Person getCreator() {
		return creator;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	
	public boolean equals(Object obj) {
		return obj == this;
	}
	
	public synchronized void xmlRpcSerialize(Map table) {
		super.xmlRpcSerialize(table);
		table.put("name", this.name);
		table.put("creator", this.creator.toString());
		table.put("creationDate", this.creationDate);
        table.put("statementsCount", new Integer(this.getStatementCount()));
        table.put("closedCommentsCount", new Integer(this.getClosedCommentCount()));
    }
    
    public void addClosedComment(ClosedComment comment) {
        this.closedComments.add(comment);
    }

    public List getClosedComments() {
       return this.closedComments;
    }

    public void subscribeStatement(Statement statement, Person subscriber) {
        Iterator it = statements.iterator();
        while (it.hasNext()) {
            Statement s = (Statement)it.next();
            if (s.hasSubscriber(subscriber)) {
                unsubscribeStatement(s, subscriber);
            }
        }
        statement.addSubscriber(subscriber);
    }
    
    public void unsubscribeStatement(Statement statement, Person subscriber) {
        statement.removeSubscriber(subscriber);
        if (!statement.hasSubscribers()) {
        	this.statements.remove(statement);
        	statement.delete();
        	HibernateUtil.getSession().delete(statement);
        }
    }

    public void addStatement(Statement statement) {
        this.statements.add(statement);
    }

    public List getStatements() {
        return this.statements;
    }
    
    public int getStatementCount() {
        return this.statements.size();
    }
    
    public int getClosedCommentCount() {
        return this.closedComments.size();
    }
    
    public static Topic load(String name) {
        return (Topic) HibernateUtil.getSession().createQuery("from Topic t where t.name = :name").setString("name", name).uniqueResult();
    }
    
    public static List /* Topic */ allTopics() {
        return HibernateUtil.getSession().createCriteria(Topic.class).list();
    }
}
