/*
 * Created on Mar 16, 2005
 */
package org.gnome.yarrr;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.DriverManager;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.PropertyConfigurator;
import org.apache.xmlrpc.XmlRpcHandler;
import org.gnome.yarrr.Person.NickNameExistsException;
import org.gnome.yarrr.xmlrpc.StandaloneXmlRpcServer;
import org.gnome.yarrr.xmlrpc.XmlRpcSerializable;

/**
 * @author alex
 */
public class Yarrr extends ToplevelReferencableObject implements
		XmlRpcSerializable {
 
	/**
	 * @author walters
	 */
	public class TopicExistsException extends Exception {
		private static final long serialVersionUID = 1L;

		public TopicExistsException(String message) {
			super(message);
		}
	}

	private static Log logger = LogFactory.getLog(Yarrr.class);

	private StandaloneXmlRpcServer rpcServer;
    
    private boolean embedded;

	private Map /* String,ActiveTopic */activeTopics;
    
    private DBValidity dbValidity;

	private Person.Ref theCapn;
    private Person.Ref anonymous;

	private XmlRpcHandler handler;

	public PollWebserver pollWebserver;
	
    private Map clientPolls;
	
    private ClientReaper reaper;

    private String storePath;

    private static AtomicLong clientInstanceIdGenerator = new AtomicLong(0);
    
	/**
	 * This function is just used initially to test stuff a bit
	 */
	private void setupTestObjects() {
        HibernateUtil.beginTransaction();
        try {
            Topic welcome = Topic.load("Welcome to Yarrr");
            Topic rocks = Topic.load("Why Yarrr rocks!");
            try {
                if (welcome == null) {
                    createTopic("Welcome to Yarrr", this.getTheCapn());
                } 
                if (rocks == null) {
                    createTopic("Why Yarrr rocks!", this.getTheCapn());
                }
                HibernateUtil.commitTransaction();
            } catch (TopicExistsException e) {
                throw new Error("Topic " + e.getMessage() + " already exists!");
            }
        } finally {
            HibernateUtil.closeSession();
        }
	}

    private String getJDBCUrl(String opts) {
         StringBuffer jdbcURL;
         jdbcURL = new StringBuffer("jdbc:derby:");
         jdbcURL.append(this.storePath);
         jdbcURL.append(opts);
         return jdbcURL.toString();
    }
    
    public Yarrr() {
        this(System.getProperty("yarrr.storePath", null));
    }
    
	public Yarrr(String storePath) {
        super(null);
        URL url = Yarrr.class.getResource("yarrr.properties");
        if (url != null) {
            try {
                Properties props = new Properties();
                props.load(url.openStream());
                embedded = Boolean.parseBoolean(props.getProperty("embedded", "false"));
                if (embedded)
                    logger.warn("Using embedded mode");
                if (storePath == null) {
                    this.storePath = props.getProperty("storePath", "/tmp/yarrr-store");
                }
            } catch (IOException e) {
                logger.warn("Couldn't open yarrr.properties", e);
            }
        } else {
            logger.debug("No yarrr.properties found");
            this.storePath = storePath;            
        } 
        url = Yarrr.class.getResource("log4j.properties");
        if (url == null)
            throw new RuntimeException("no log4j.properties found!");
        PropertyConfigurator.configure(url);

	}
    
    private void init() {
        String opts;
        if (!new File(storePath).exists())
            opts = ";create=true";
        else
            opts = "";
        
        String jdbcURL = getJDBCUrl(opts);
        logger.info("using JDBC url: " + jdbcURL);
        HibernateUtil.init(HibernateUtil.buildConfiguration(jdbcURL));
        ReferencableObjectRegistry.registerPersistant(Topic.class);
        ReferencableObjectRegistry.registerPersistant(Person.class);
        ReferencableObjectRegistry.registerPersistant(Statement.class);
        ReferencableObjectRegistry.registerPersistant(ClosedComment.class);
        
        activeTopics = new HashMap();
        if (embedded) {
            handler = new YarrrXmlRpcMethodsEmbedded(this);
        } else {
            handler = new YarrrXmlRpcMethods(this);
        }
        clientPolls = Collections.synchronizedMap(new HashMap());
        
        HibernateUtil.beginTransaction();
        try {
            this.dbValidity = DBValidity.loadOrCreate();
            
            Person theCapnObj = Person.lookupOrCreate("\u2620");
            this.theCapn = theCapnObj.getRef();
            Person anonymousObj = Person.lookupOrCreate("anon");
            this.anonymous = anonymousObj.getRef();
            HibernateUtil.commitTransaction();
        } catch (NickNameExistsException e) {
            e.printStackTrace();
        } finally {
            HibernateUtil.closeSession();
        }
        
        ReferencableObjectRegistry.register(this);
        addMonitor(this);
        
        setupTestObjects();
    }
    
    public void startup(boolean startWebserver) {
        this.init();
        if (!startWebserver)
            return;
        
        reaper = new ClientReaper();
        reaper.start();
        
        try {
            pollWebserver = new PollWebserver(10000, 100);
            pollWebserver.start();
        } catch (IOException e) {
            logger.fatal(e);
            throw new RuntimeException(e);
        }
    }
    
    public void shutdownDB() {
        HibernateUtil.shutdown();
        String url = getJDBCUrl(";shutdown=true");
        try {
			DriverManager.getConnection(url);
		} catch (Exception e) {
			logger.info("database shutdown");
		}
    }
	
	public void shutdown() {
        this.shutdownDB();
		pollWebserver.shutdown();
		reaper.shutdown();
	}
    
    public YarrrSession openSession() {
        YarrrSession session = new YarrrSession(yarrr, getAnonymousRef());
        return session;
    }
    
    public String openClientInstance(YarrrSession session, String remoteIp, String requestURL) {
        String clientInstanceId = Long.toString(clientInstanceIdGenerator.incrementAndGet());
        initClientPollFor(clientInstanceId, remoteIp, requestURL);
        return clientInstanceId;
    }
    
	private void initClientPollFor(String instanceId, String remoteIp, String requestURL) {
		InetAddress address;
		try {
			address = InetAddress.getByName(remoteIp);
		} catch (UnknownHostException e) {
			logger.error("remote ip in wrong format: " + remoteIp, e);
            return;
		}
		
		ClientPoll poll = new ClientPoll(instanceId, address, requestURL, pollWebserver);
		clientPolls.put(instanceId, poll);
	}
 
	public ClientPoll getClientPollFor(String clientInstanceId) {
		return (ClientPoll) clientPolls.get(clientInstanceId);
	}
	
	public synchronized Topic lookupTopic(String name) {
		return Topic.load(name);
	}

	public synchronized Collection /* Topic */getInactiveTopics() {
		Set inactive = new HashSet(Topic.allTopics());
        Iterator it = inactive.iterator();
        while (it.hasNext()) {
            Topic t = (Topic) it.next();
            if (this.activeTopics.containsKey(t.getName())) {
                it.remove();
            }
        }
		logger.debug("" + inactive.size() + " inactive topics");
		return inactive;
	}

	public synchronized Set /* ActiveTopic */getActiveTopics() {
		return new HashSet(this.activeTopics.values());
	}

	public synchronized Topic createTopic(String name, Person creator)
			throws TopicExistsException {
		if (lookupTopic(name) != null) {
			throw new TopicExistsException(name);
		}
		logger.info("Creating topic: " + name);
		Topic topic = new Topic(name, creator);
		signalChanged();
		return topic;
	}

	/**
	 * Activate a topic, creating an in-memory representation of it. The active
	 * topic is only created if it doesn't exist already.
	 * 
	 * @param topic
	 *            a persistant topic object
	 */
	public synchronized ActiveTopic activateTopic(Topic topic) {
		ActiveTopic activeTopic;

		activeTopic = lookupActiveTopic(topic);
		if (activeTopic == null) {
			activeTopic = new ActiveTopic(this, topic.getReferenceId());
			activeTopics.put(topic.getName(), activeTopic);
            addMonitor(activeTopic);
            signalChanged();
		}
		return activeTopic;
	}

	public synchronized ActiveTopic lookupActiveTopic(Topic topic) {
		return (ActiveTopic) activeTopics.get(topic.getName());
	}

	public void stop() {
		rpcServer.stop();
	}

	public XmlRpcHandler getXmlRpcHandler() {
		return handler;
	}

	public static void main(String[] args) {
		Yarrr yarrr = new Yarrr();
        yarrr.startup(true);

		StandaloneXmlRpcServer rpcServer = StandaloneXmlRpcServer
				.getXmlRpcServer(19842);
		rpcServer.shareObject("$default", yarrr.getXmlRpcHandler());

		System.out.println("Press enter to terminate.");
		try {
			System.in.read();
		} catch (IOException e) {
			logger.error("Couldn't read from stdin", e);
		}

		logger.info("Yarrr shutting down.");
		yarrr.stop();
	}

	public Person getTheCapn() {
	    return theCapn.load();
	}
    
    public Person getAnonymous() {
        return anonymous.load();
    }
    
    public long getDBValidity() {
        return this.dbValidity.validity;
    }
    
    public Person.Ref getAnonymousRef() {
        return anonymous;
    }
    
    public String getAnonymousStringRef() {
        return ReferencableObjectRegistry.getReference(Person.class, anonymous.getReferenceId());
    }

	public synchronized void xmlRpcSerialize(Map table) {
		super.xmlRpcSerialize(table);
		table.put("activetopics", new Vector(this.activeTopics.values()));
		table.put("inactivetopics", new Vector(this.getInactiveTopics()));
	}
	
	private class ClientReaper extends Thread {
		public ClientReaper() {
			super("Yarrr client Reaper");
		}
		
		private void reap() {
			synchronized(clientPolls) {
				Date time = new Date(System.currentTimeMillis() - 30*1000);
				
				Collection values = clientPolls.values();
				for (Iterator i = values.iterator(); i.hasNext(); ) {
					ClientPoll clientPoll = (ClientPoll)i.next();
					
					if (!clientPoll.isActiveSince(time)) {
						logger.debug("Reaping client " + clientPoll);
						i.remove();
						clientPoll.detectedInactive();
					}
				}
			}
		}
		
		public void run() {
		    logger.debug("starting reaper thread");
			while (true) {
				try {
					Thread.sleep(1000*5);
					reap();
				} catch (InterruptedException e) {
					return;
				}
			}
		}
		public void shutdown() {
			this.interrupt();
		}
	}

    
    protected XmlRpcHandler getHandler() {
        return handler;
    }
}
