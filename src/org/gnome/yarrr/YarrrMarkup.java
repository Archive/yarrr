/*
 * Created on Apr 13, 2005
 */
package org.gnome.yarrr;

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;
import java.util.regex.Matcher;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.TextSerializer;
import org.apache.xml.serialize.XMLSerializer;
import org.gnome.yarrr.xmlrpc.XmlRpcHandler;
import org.gnome.yarrr.xmlrpc.XmlRpcMarshaller;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * @author walters
 */
public class YarrrMarkup implements XmlRpcMarshaller, Serializable {
	private static final long serialVersionUID = 1L;
    private Document content;
	private Node rootNode;
	private String stringContent;
    private DocumentBuilder builder;
	
	public YarrrMarkup() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            this.builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }
    
    private void initDocument() {
        if (this.content != null)
            return;
        if (this.stringContent == null) {
            this.content = builder.newDocument();
            this.rootNode = this.content.createElement("yarrr-markup");
            this.content.appendChild(this.rootNode);
        }
    }
	
	public YarrrMarkup(String userContent) {
		this();
		this.appendUserContent(userContent);
	}
	
	private void invalidateCache() {
	    this.stringContent = null;
	}
	
	public void appendText(String text) {
        initDocument();
		Node node = this.content.createElement("text");
		node.appendChild(this.content.createTextNode(text));
		this.rootNode.appendChild(node);
		invalidateCache();
	}
	
	public void appendTopicLink(String topicTitle) {
        initDocument();
		Element element = this.content.createElement("topic-link");
		element.appendChild(this.content.createTextNode(topicTitle));
		this.rootNode.appendChild(element);
		invalidateCache();
	}
	
	public void appendLink(String title, String address) {
        initDocument();
		Element element = this.content.createElement("link");
		element.setAttribute ("href", address);
		element.appendChild(this.content.createTextNode(title));
		this.rootNode.appendChild(element);
		invalidateCache();
	}
	
	public void appendBoldText(String text) {
        initDocument();
		Element element = this.content.createElement("bold");
		element.appendChild(this.content.createTextNode(text));
		this.rootNode.appendChild(element);
		invalidateCache();
	}
	
	public void appendUnderlinedText(String text) {
        initDocument();
		Element element = this.content.createElement("underlined");
		element.appendChild(this.content.createTextNode(text));
		this.rootNode.appendChild(element);
		invalidateCache();
	}
	
	private void appendSubstitution(TextSubstitution substitution, Matcher matcher) {
        initDocument();
		switch (substitution.getType()) {
			case TextSubstitution.TOPIC_TYPE:
				appendTopicLink(substitution.getProperty(matcher, "title"));
				break;
			case TextSubstitution.LINK_TYPE:
				appendLink(substitution.getProperty(matcher, "title"),
						   substitution.getProperty(matcher, "address"));
				break;
			case TextSubstitution.BOLD_TYPE:
				appendBoldText(substitution.getContent(matcher));
				break;
			case TextSubstitution.UNDERLINED_TYPE:
				appendUnderlinedText(substitution.getContent(matcher));
				break;
		}
	}
	
	private void appendWithSubstitutions(String newContent, List list, int index) {
		TextSubstitution substitution;
		Matcher matcher; 
		int lastPos = 0;
		
		if (list.size() > index) {
			substitution = (TextSubstitution)list.get(index);
			matcher = substitution.matcher(newContent);
		
			while (matcher.find()) {
				if (lastPos <= matcher.start()) {
					appendWithSubstitutions(newContent.substring(lastPos, matcher.start()),
									        list, index + 1);
				}
				appendSubstitution (substitution, matcher);
				lastPos = matcher.end();
			}
			if (lastPos < newContent.length()) {
				appendWithSubstitutions(newContent.substring(lastPos), list, index + 1);
			}
		} else {
			appendText(newContent.substring(lastPos));
		}
	}
	
	public void appendUserContent(String newContent) {
        initDocument();
		appendWithSubstitutions(newContent, TextSubstitution.getTextSubstitutions(), 0);
	}
	
	public String toString() {
		if (this.stringContent == null) {
            OutputFormat format = new OutputFormat();
            format.setIndenting(true);
            format.setLineWidth(0);             
            format.setPreserveSpace(true);
            Writer stringWriter = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(stringWriter, format);
            try {
                serializer.asDOMSerializer();
                serializer.serialize(this.content);
            } catch (IOException e) {
                // Should never happen
                throw new RuntimeException(e);
            }
            this.stringContent = stringWriter.toString();
            this.content = null;
            this.rootNode = null;
		}
		return this.stringContent;
	}
    
    public String toText() {
        Writer stringWriter = new StringWriter();
        TextSerializer serializer = new TextSerializer();
        serializer.setOutputCharStream(stringWriter);
        try {
            serializer.asDOMSerializer();
            serializer.serialize(this.content);
        } catch (IOException e) {
            // Should never happen
            throw new RuntimeException(e);
        }
        return stringWriter.toString();
    }
    
    public void parse(String newContent) throws SAXException, IOException {
        Reader stringReader = new StringReader(newContent);
        this.content = builder.parse(new InputSource(stringReader));
    }

	public Object xmlRpcMarshal(XmlRpcHandler handler) throws MarshallingException {
		return this.toString();
	}
}
