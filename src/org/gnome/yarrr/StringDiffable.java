package org.gnome.yarrr;

public class StringDiffable implements Diffable {
    String str;
    
    static private class StringDiffableBuilder implements DiffableBuilder {
        StringBuilder builder;
        public StringDiffableBuilder() {
            builder = new StringBuilder();
        }
        
        public void append(Diffable diffable) {
            StringDiffable diffableStr = (StringDiffable)diffable;
            builder.append(diffableStr.str);
        }

        public Diffable toDiffable() {
            return new StringDiffable(builder.toString());
        }
    }
    
    public StringDiffable(String str) {
        this.str = str;
    }
    
    public boolean equalAt(int offset, Diffable other, int otherOffset) {
        StringDiffable otherStr = (StringDiffable)other;
        return str.charAt(offset) == otherStr.str.charAt(otherOffset);
    }

    public Diffable getSubset(int offsetStart) {
        return new StringDiffable(str.substring(offsetStart));
    }
    /**
     * create a subset of the diffable, starting at index offsetStart
     * and ending at offsetEnd - 1. Thus, the length of the new diffable
     * is offsetEnd-offsetStart.
     */
    public Diffable getSubset(int offsetStart, int offsetEnd) {
        return new StringDiffable(str.substring(offsetStart, offsetEnd));
    }

    public boolean equals(Diffable other, int start, int length) {
        StringDiffable otherStr = (StringDiffable)other;
        return str.equals(otherStr.str.substring(start, start+length));
    }

    public int length() {
        return str.length();
    }

    public int findFirstMatch(Diffable match, int start) {
        StringDiffable matchStr = (StringDiffable)match;
        return str.indexOf(matchStr.str, start);
    }

    public int findLastMatch(Diffable match, int start) {
        StringDiffable matchStr = (StringDiffable)match;
        return str.lastIndexOf(matchStr.str, start);
    }

    public DiffableBuilder createBuilder() {
        return new StringDiffableBuilder();
    }

    public String toString() {
        return str;
    }
    
}
