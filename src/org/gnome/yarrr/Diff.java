/*
 * Created on Mar 22, 2005
 *
 * The EditScript parts are based on diff.c 
 * found at http://www.ioplex.com/~miallen/libmba/dl/src/diff.c
 * which has the following copyright notice:
 *
 * diff - compute a shortest edit script (SES) given two sequences
 * Copyright (c) 2004 Michael B. Allen <mba2000 ioplex.com>
 *
 * The MIT License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

/* This algorithm is basically Myers' solution to SES/LCS with
 * the Hirschberg linear space refinement as described in the
 * following publication:
 *
 *   E. Myers, ``An O(ND) Difference Algorithm and Its Variations,''
 *   Algorithmica 1, 2 (1986), 251-266.
 *   http://www.cs.arizona.edu/people/gene/PAPERS/diff.ps
 *
 * This is the same algorithm used by GNU diff(1).
 */


package org.gnome.yarrr;

import java.util.ArrayList;
import java.util.Vector;

/**
 * @author alex
 */
public class Diff {
	static public final int MATCH = 1;
	static public final int DELETE = 2;
	static public final int INSERT = 3;
	
	static public final int DEFAULT_CONTEXT_LENGTH = 5;
	
	private static final class EditScript {
		public static final class EditScriptLine {
			int op;
			int off;
			int len;
		}
		private static final class MiddleSnake {
			int x;
			int y;
			int u;
			int v;
		}
		
		private Diffable a;
		private Diffable b;
		private Vector ses;
		private Vector buf;
		public EditScriptLine lines[];
		
		private void addOp(int op, int off, int len) {
			EditScriptLine e;
			
			if (len == 0) {
				return;
			}
			
			/* Add an edit to the SES (or
			 * coalesce if the op is the same)
			 */
			e = null;
			if (ses.size() != 0)
				e = (EditScriptLine) ses.lastElement();
			if (e == null || e.op != op) {
				e = new EditScriptLine();
				ses.add(e);
				e.op = op;
				e.off = off;
				e.len = len;
			} else {
				e.len += len;
			}
		}
		
		void setv(int k, int r, int val) {
			int j;
		    
		    /* Pack -N to N into 0 to N * 2 */
		    j = (k <= 0) ? -k * 4 + r : k * 4 + (r - 2);
		    
		    if (buf.size() <= j)
		    	buf.setSize(j+1);
		    
		    buf.set(j, new Integer(val));
		}

		int v(int k, int r) {
		    int j;
		    
		    j = (k <= 0) ? -k * 4 + r : k * 4 + (r - 2);
		    
		    return ((Integer)buf.get(j)).intValue();
		}

		int FV(int k) {
		    return  v(k, 0);
		}

		int RV(int k) {
		    return v(k, 1);
		}

		int findMiddleSnake(int aoff, int n, int boff, int m, MiddleSnake ms) {
			int delta, mid, d;
			boolean odd;
			
			delta = n - m;
			odd = (delta & 1) == 1;
			mid = (n + m) / 2;
			if (odd)
				mid += 1;
			
			setv(1, 0, 0);
			setv(delta - 1, 1, n);
			
			for (d = 0; d <= mid; d++) {
				int k, x, y;
				
				for (k = d; k >= -d; k -= 2) {
					if (k == -d || (k != d && FV(k - 1) < FV(k + 1))) {
						x = FV(k + 1);
					} else {
						x = FV(k - 1) + 1;
					}
					y = x - k;
					
					ms.x = x;
					ms.y = y;
					
					while (x < n && y < m && a.equalAt(aoff+x, b, boff+y)) {
						x++; y++;
					}
					setv(k, 0, x);
					
					if (odd && k >= (delta - (d - 1)) && k <= (delta + (d - 1))) {
						if (x >= RV(k)) {
							ms.u = x;
							ms.v = y;
							return 2 * d - 1;
						}
					}
				}
				for (k = d; k >= -d; k -= 2) {
					int kr = (n - m) + k;
					
					if (k == d || (k != -d && RV(kr - 1) < RV(kr + 1))) {
						x = RV(kr - 1);
					} else {
						x = RV(kr + 1) - 1;
					}
					y = x - kr;
					
					ms.u = x;
					ms.v = y;
					while (x > 0 && y > 0 && a.equalAt(aoff + x - 1, b, boff + y - 1)) {
						x--; y--;
					}
					setv(kr, 1, x);
					
					if (!odd && kr >= -d && kr <= d) {
						if (x <= FV(kr)) {
							ms.x = x;
							ms.y = y;
							return 2 * d;
						}
					}
				}
			}
			
			return -1;
		}

		int findShortestEditScript(int aoff, int n, int boff, int m) {
			int d;
		    
			if (n == 0) {
				addOp(INSERT, boff, m);
				d = m;
			} else if (m == 0) {
				addOp(DELETE, aoff, n);
				d = n;
			} else {
				/* Find the middle "snake" around which we
				 * recursively solve the sub-problems.
				 */
				MiddleSnake ms = new MiddleSnake();
				d = findMiddleSnake(aoff, n, boff, m, ms);
				if (d == -1) {
					return -1;
				} else if (d > 1) {
					if (findShortestEditScript(aoff, ms.x, boff, ms.y) == -1) {
						return -1;
					}
					
					addOp(MATCH, aoff + ms.x, ms.u - ms.x);
					
					aoff += ms.u;
					boff += ms.v;
					n -= ms.u;
					m -= ms.v;
					if (findShortestEditScript(aoff, n, boff, m) == -1) {
						return -1;
					}
				} else {
					int x = ms.x;
					int u = ms.u;
					
					/* There are only 4 base cases when the
					 * edit distance is 1.
					 *
					 * n > m   m > n
					 *
					 *   -       |
					 *    \       \    x != u
					 *     \       \
					 *
					 *   \       \
					 *    \       \    x == u
					 *     -       |
					 */
					
					if (m > n) {
						if (x == u) {
							addOp(MATCH, aoff, n);
							addOp(INSERT, boff + (m - 1), 1);
						} else {
							addOp(INSERT, boff, 1);
							addOp(MATCH, aoff, n);
						}
					} else {
						if (x == u) {
							addOp(MATCH, aoff, m);
							addOp(DELETE, aoff + (n - 1), 1);
						} else {
							addOp(DELETE, aoff, 1);
							addOp(MATCH, aoff + 1, m);
						}
					}
				}
			}
			
			return d;
		}
		
		private EditScript(Diffable a, Diffable b) {
			int x, y;
			    
			this.a = a;
			this.b = b;
			buf = new Vector();
			ses = new Vector();
			
			int n = a.length();
			int m = b.length();
			
			/* The findShortestEditScript function assumes the SES will begin
			 * or end with a delete or insert. The following will insure this
			 * is true by eating any beginning matches. This is also a quick 
			 * way to process sequences that match entirely.
			 */
			x = y = 0;
			while (x < n && y < m && a.equalAt(x, b, x)) {
				x++; y++;
			}
			addOp(MATCH, 0, x);
			
			if (findShortestEditScript(x, n - x, y, m - y) == -1) {
				throw new RuntimeException("Unable to compute diff");
			}
			
			lines = (EditScriptLine[]) ses.toArray(new EditScriptLine[0]);
		}
		
		public int length() {
			return lines.length;
		}
	}
	
	public static final class HunkPart {
		int op;
		Diffable data;
	}
	public static final class Hunk {
		int offset;
		HunkPart [] parts;

		Hunk() {
		}
		Hunk(Diffable a, Diffable b, EditScript script, int start, int end, int context_len) {
		    ArrayList partslist = new ArrayList();

		    /* if needed and possible, extend range to get some MATCH context */
		    if (start > 0 && script.lines[start].op != MATCH)
		    	start--;
		    if (end < script.length() && script.lines[end-1].op != MATCH)
		    	end++;
		    
		    for (int i = start; i < end; i++) {
		    	int off, len;
		    	Diffable source;
		    	
		    	EditScript.EditScriptLine edit = script.lines[i];
		    	
		    	HunkPart part = new HunkPart();
		    	part.op = edit.op;
		    	
		    	off = edit.off;
		    	len = edit.len;
		    	
		    	if (edit.op == MATCH ||	edit.op == DELETE) {
		    		source = a;
		    	} else {
		    		source = b;
		    	}
		    	
		    	if (i == start) {
		    		if (edit.op == MATCH && edit.len > context_len) {
		    			off += (len - context_len);
		    			len = context_len;
		    		}
		    		offset = off;
		    	}
		    	if (i == end - 1 && edit.op == MATCH && edit.len > context_len) {
		    		len = context_len;
		    	}
		    	
		    	part.data = source.getSubset(off, off+len);
		    	
		    	partslist.add(part);
		    }
		    
		    this.parts = (HunkPart[]) partslist.toArray(new HunkPart[0]);
		}

		public boolean matches(Diffable str, int start, int pos) {
			// If first part wasn't a MATCH, then we need to match at the start
		    if (parts[0].op != MATCH && pos != start) {
		    	return false;
		    }
		    for (int i = 0; i < parts.length; i++) {
		    	HunkPart part = parts[i];
		    	if (part.op == MATCH || part.op == DELETE) {
		    		Diffable matchstr = part.data;
		    		if (!matchstr.equals(str, pos, matchstr.length()))
		    			return false;
		    		
		    		pos += matchstr.length();
		    	}
		    }
		    // If last part wasn't a MATCH, then we need to match at the end
		    if (parts[parts.length-1].op != MATCH && pos != str.length()) {
		    	return false;
		    }
		    return true;
		}
		
		public void toString(StringBuilder builder) {
			builder.append("[");
			builder.append(offset);
			builder.append(":");
		    for (int i = 0; i < parts.length; i++) {
		    	HunkPart part = parts[i];
		    	if (part.op == DELETE)
		    		builder.append("-");
		    	else if (part.op == INSERT) 
		    		builder.append("+");
		    	
	    		builder.append(part.data);
	    		
		    	if (part.op == DELETE)
		    		builder.append("-");
		    	else if (part.op == INSERT) 
		    		builder.append("+");
		    }
			
			builder.append("]");
		}
	}
	
	protected Hunk [] hunks;
	
	/**
	 * Create an empty diff;
	 *
	 */
	public Diff() {
		hunks = new Hunk[0];
	}

	public Diff(Diffable a, Diffable b) {
		EditScript script = new EditScript(a, b);
		
		int context_len = DEFAULT_CONTEXT_LENGTH;

		ArrayList hunksList = new ArrayList();
		
		int start, end;
		if (script.length() == 0)
			return;
		    
		start = 0;

		while (start < script.length()) {
			// Find first non-match or short match 
			while (script.lines[start].op == MATCH &&
					script.lines[start].len > context_len) {
				start++;
				if (start >= script.length())
					break;
			}
			
			end = start + 1;
			
			while (end < script.length() &&
					(script.lines[end].op != MATCH ||
							script.lines[end].len <= context_len)) {
				end++;
			}
			
			
			if (start < script.length() &&
					!(end == start + 1 && script.lines[start].op == MATCH))
				hunksList.add(new Hunk(a, b, script, start, end, context_len));
			
			start = end + 1;
		}
		
		this.hunks = (Hunk[]) hunksList.toArray(new Hunk[0]);
	}

	
	private static final class Applier {
		Diffable input;
		DiffableBuilder output;
		int start;
		int apply_offset;
		
		int findMatchingPosition(Hunk hunk) {
			// Check if hunk offset is right
			int guess = hunk.offset + apply_offset;
			
			// Never look before start
			if (guess < start)
				guess = start;
			
			if (hunk.matches(input, start, guess))
				return guess;
			
			Diffable match_string;
			
			if (hunk.parts[0].op != MATCH) {
				/* no context before, must be at start of string */
				if (hunk.matches(input, start, start))
					return 0;
				return -1;
			}
			match_string = hunk.parts[0].data;
			
			int before, after, middle;
			boolean update_before, update_after;
			
			middle = guess;
			before = after = middle;
			update_before = update_after = true;
			do {
				if (update_after) {
					/* Search next forward */
					if (after == -1 || after >= input.length() - 1)
						after = -1;
					else
						after = input.findFirstMatch(match_string, after + 1);
				}
				
				if (update_before) {
					/* Search next backward */
					if (before == -1 || before <= start)
						before = -1;
					else {
						before = input.findLastMatch(match_string, before - 1);
						if (before < start)
							before = -1;
					}
				}
				
				if (after != -1 && before != -1) {
					if (middle - before < after - middle) {
						/* Try before */
						if (hunk.matches(input, start, before))
							return before;
						update_before = true;
						update_after = false;
					} else {
						if (hunk.matches(input, start, after))
							return after;
						update_before = false;
						update_after = true;
					}
				} else if (after != -1) {
					if (hunk.matches(input, start, after))
						return after;
					update_before = false;
					update_after = true;
				} else if (before != -1) {
					if (hunk.matches(input, start, before))
						return before;
					update_before = true;
					update_after = false;
				}
			} while (before != -1 ||  after != -1);
			
			return -1;
		}

		public boolean applyHunk(Hunk hunk) {
		    for (int i = 0; i < hunk.parts.length; i++) {
		    	HunkPart part = hunk.parts[i];
		    	Diffable matchstr = part.data;
		    	if (part.op == INSERT) {
                    output.append(matchstr);
		    	} else if (part.op == MATCH) {
                    // Copy from input, not patch, since we know it will match
                    // but the patch may not contain all info (wildcard 
                    output.append(input.getSubset(start, start + matchstr.length()));
		    		start += matchstr.length();
		    	} else {
		    		/* DELETE */
		    		start += matchstr.length();
		    	}
		    }
		    return true;
		}
	}
	
	public Diffable applyTo(Diffable diffable) {
		Applier applier = new Applier();
		
	    applier.input = diffable;
	    applier.apply_offset = 0;
	    applier.start = 0;
	    applier.output = diffable.createBuilder();

	    for (int i = 0; i < hunks.length; i++) {
	    	Hunk hunk = hunks[i];
	    	
	    	int pos = applier.findMatchingPosition(hunk);
	    	if (pos == -1)
	    		return null;
	    	applier.apply_offset = pos - hunk.offset;
	    	
	    	applier.output.append(diffable.getSubset(applier.start, pos));
	    	applier.start = pos;
	    	applier.applyHunk(hunk);
	    }
	    
	    applier.output.append(diffable.getSubset(applier.start));
	    
	    return applier.output.toDiffable();
	}

	public int length() {
		return hunks.length;
	}

}
