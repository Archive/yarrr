/*
 * Created on Mar 16, 2005
 */
package org.gnome.yarrr;

import java.awt.Color;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.gnome.yarrr.LiveComment.VersionAndDiff;
import org.gnome.yarrr.Person.NickNameExistsException;
import org.gnome.yarrr.ReferencableObjectRegistry.UnknownReferenceException;
import org.gnome.yarrr.Yarrr.TopicExistsException;
import org.gnome.yarrr.xmlrpc.XmlRpcHandler;
import org.gnome.yarrr.xmlrpc.XmlRpcSerializable;

/**
 * @author alex
 */
public class YarrrXmlRpcMethods extends XmlRpcHandler {
	private static Log logger = LogFactory.getLog(YarrrXmlRpcMethods.class);
	Yarrr yarrr;
    
    protected YarrrSession getSession() {
        return YarrrServlet.getCurrentSession();
    }
	
    protected Person getCaller() {
		return getSession().loadPerson();
	}
    
    public boolean setPersonName(String name) {
            try {
                getCaller().setNickName(name);
            } catch (NickNameExistsException e) {  
                return false;
            }
            return true;
    }
    
	public int createTopic(String name, Person creator) throws TopicExistsException {
		yarrr.createTopic(name, creator);
		return 0;
	}
	
	public Vector dumpObjects(Vector objrefs) {
		Iterator it = objrefs.iterator();
		Vector ret = new Vector();
		
		while (it.hasNext()) {
			String objref = (String) it.next();
			Object obj;
            try {
				obj = ReferencableObjectRegistry.lookup(objref);
                if (!(obj instanceof XmlRpcSerializable)) {
                    throw new RuntimeException("Object " + obj + " for reference \"" + objref + "\" does not implement XmlRpcSerializable");
                }
                    
                Map table = new Hashtable();
                ((XmlRpcSerializable) obj).xmlRpcSerialize(table);
                ret.add((Object) table);
			} catch (UnknownReferenceException e) {
			    ret.add("UnknownReferenceException");
			} catch (ReferencableObjectRegistry.MalformedReferenceException e) {
			    throw new RuntimeException(e);
            }
		}
		return ret;
	}
	
	public boolean openDiscussion(ActiveTopic topic) {
		topic.openDiscussion(getCaller());
		return true;
	}
	
	public boolean setDiscussionTitle(Discussion d, String newtitle) {
		d.setTitle(newtitle);
		return true;
	}
	
	public boolean subscribeStatement (ActiveTopic topic, Statement statement) {
		topic.subscribeStatement(statement, getCaller());
		return true;
	}
	
	public boolean unsubscribeStatement (ActiveTopic topic, Statement statement) {
		topic.unsubscribeStatement(statement, getCaller());
		return true;
	}
	
	public Statement addStatement(ActiveTopic topic, String content) {
		return topic.addStatement(getCaller(), content);
	}
	
	public LiveComment openLiveComment(Discussion discussion, String name, String contents) {
		return discussion.openComment(getCaller(), name, contents);
	}

	public boolean deAuthorLiveComment(Discussion discussion, LiveComment comment) {
		return discussion.deAuthorLiveComment(comment, getCaller());
	}
	
	public Vector getClosedCommentMessages(ClosedComment comment) {
		return comment.dumpMessages();
	}

	public Vector getLiveCommentDiff(LiveComment comment, int oldVersion) throws Exception {
		TaggedTextDiff diff;
		int version;
		
		synchronized (comment) {
			version = comment.getVersion();
			diff = comment.getDiffFrom(oldVersion);
		}
		
		Vector res = new Vector();
		res.add(new Integer(version));
		res.add(diff);
		return res;
	}
	
	public VersionAndDiff updateLiveComment(LiveComment comment, StringDiff diff, int version) throws Exception {
		return comment.updateContents(getCaller(), diff, version);
	}

	public ClosedComment closeLiveComment(ActiveTopic topic, Discussion discussion, LiveComment comment) throws Exception {
        return topic.closeComment(discussion, comment, getCaller());
	}

	public int addChatMessage(ActiveTopic topic, Chat chat, String contents) {
		return topic.addChatMessage(chat, getCaller(), contents, false);
	}
	
	public int addChatAction(ActiveTopic topic, Chat chat, String contents) {
		return topic.addChatMessage(chat, getCaller(), contents, true);
	}
	
	public int addChatWhiteboard(ActiveTopic topic, Chat chat, Whiteboard whiteboard, int version) {
		return topic.addChatWhiteboard(chat, getCaller(), whiteboard, version);
	}
	
	public boolean subscribeChatMessage (Chat chat, int index) {
		chat.addMessageSubscriber(index, getCaller());
		return true;
	}
	
	public boolean unsubscribeChatMessage (Chat chat, int index) {
		chat.removeMessageSubscriber(index, getCaller());
		return true;
	}

    public Vector getChatMessageSubscribers (Chat chat, int index) {
        return chat.getChatMessageSubscribers (index);
    }

	public String getPollUrl(ToplevelReferencableObject obj, String clientInstanceId) {
		return obj.getPollUrl(getCaller(), clientInstanceId);
	}

	public ToplevelReferencableObject.PollChangesResult getPollChanges(ToplevelReferencableObject obj, String clientInstanceId) {
		return obj.getPollChanges(getCaller(), clientInstanceId);
	}
	
    public Whiteboard createWhiteboard(Discussion discussion) {
        return discussion.createWhiteboard(getCaller());
    }
    
    public int addWhiteboardStroke(Whiteboard whiteboard, SketchStroke stroke, String id) throws Exception {
        whiteboard.addStroke(stroke, id);
        return 0;
    }

    public int addWhiteboardText(Whiteboard whiteboard, String text, int x, int y, String color) throws Exception {
        whiteboard.addText(text, x, y, Color.decode(color));
        return 0;
    }
    
    public Whiteboard.OpFetchResult getOpIdsBetween(Whiteboard whiteboard, int startVersion, int endVersion) {
        return whiteboard.getStrokeIdsBetween(startVersion, endVersion);
    }

    public int getWhiteboardVersion(Whiteboard whiteboard) {
        return whiteboard.getVersion();        
    }
    
	public YarrrXmlRpcMethods(Yarrr yarrr) {
		this.yarrr = yarrr;
        this.addDispatcher(new YarrrXmlRpcDispatcher(this));
	}

	public Yarrr getYarrr() {
		return yarrr;
	}
}
