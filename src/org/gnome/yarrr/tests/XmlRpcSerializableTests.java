/*
 * Created on Apr 18, 2005
 */
package org.gnome.yarrr.tests;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

import org.gnome.yarrr.XmlRpcSerializableVisitor;

/**
 * @author walters
 */
public class XmlRpcSerializableTests extends SerializableUsingTestCase {
    
    public static class Frob implements Serializable {
        private static final long serialVersionUID = 1L;
        public Set getMoo() {
            Set ret = new HashSet();
            ret.add("foo");
            ret.add("bar");
            return ret;
        }
        
        public static final String[] xmlRpcSerializableMethods = new String[] {"getMoo"};
    }
   

    public void testIntegerSerialization() throws Exception {
        Integer x = new Integer(42);
        XmlRpcSerializableVisitor visitor = new XmlRpcSerializableVisitor();
        Integer ret = (Integer) visitor.visit(x);
        assertEquals(x.intValue(), ret.intValue());
    }
    
    public void testFooSerialization() throws Exception {
        Foo f = new Foo();
        f.moo = "whee";
        XmlRpcSerializableVisitor visitor = new XmlRpcSerializableVisitor();
        Hashtable ret = (Hashtable) visitor.visit(f);
        String moo = (String) ret.get("moo");
        assertNotNull(moo);
        assertEquals("whee", moo);     
    }
    
    public void testBarSerialization() throws Exception {
        Bar b = new Bar();
        b.moo = "MOO";
        b.bar = 42;
        XmlRpcSerializableVisitor visitor = new XmlRpcSerializableVisitor();
        Hashtable ret = (Hashtable) visitor.visit(b);
        assertEquals("MOO", ret.get("moo"));
        assertEquals(42, ((Integer) ret.get("bar")).intValue()); 
    }
    
    public void testMooSerialization() throws Exception {
        Moo m = new Moo();
        m.set = new HashSet();
        m.set.add("foo");
        m.set.add("bar");
        m.list = new ArrayList();
        m.list.add("baz");
        m.list.add(new Integer(42));
        XmlRpcSerializableVisitor visitor = new XmlRpcSerializableVisitor();
        Hashtable ret = (Hashtable) visitor.visit(m);
        
        ArrayList list = (ArrayList) ret.get("list");
        assertNotNull(list);
        assertEquals("baz", (String) list.get(0));
        assertEquals(42, ((Integer) list.get(1)).intValue());
        
        ArrayList set = (ArrayList) ret.get("set");
        assertNotNull(set);
        assertEquals("foo", (String) set.get(0));
        assertEquals("bar", (String) set.get(1));
    }
    
    public void testBazSerialization() throws Exception {
        Baz b = new Baz();
        b.moo = "yay";
        b.bar = 38;
        b.map = new HashMap();
        b.map.put("foo", new Integer(20));
        b.map.put("baz", "moo");
        XmlRpcSerializableVisitor visitor = new XmlRpcSerializableVisitor();
        Hashtable ret = (Hashtable) visitor.visit(b);
        
        String moo = (String) ret.get("moo");
        assertNotNull(moo);
        assertEquals("yay", moo);
        
        Integer bar = (Integer) ret.get("bar");
        assertNotNull(bar);
        assertEquals(38, bar.intValue());
        
        Hashtable map = (Hashtable) ret.get("map");
        assertNotNull(map);
        Integer foo = (Integer) map.get("foo");
        assertNotNull(foo);
        assertEquals(20, foo.intValue());
        String baz = (String) map.get("baz");
        assertNotNull(baz);
        assertEquals("moo", baz);        
    }
    
    public void testFrobSerialization() throws Exception {
        Frob f = new Frob();
        XmlRpcSerializableVisitor visitor = new XmlRpcSerializableVisitor();
        Hashtable ret = (Hashtable) visitor.visit(f);
        ArrayList s = (ArrayList) ret.get("moo");
        assertNotNull(s);
        assertEquals("foo", (String) s.get(0));
        assertEquals("bar", (String) s.get(1));
    }
}
