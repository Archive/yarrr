/*
 * Created on Apr 21, 2005
 */
package org.gnome.yarrr.tests;

import java.io.File;
import java.io.IOException;

/**
 * @author walters
 */
public class TestUtils {
    
    /**
     * Recursively deletes a directory (or file)
     * @param directory
     */
    static void delete(File file) {
        if (file.isDirectory()) {
            String[] list = file.list();
            for (int i=0; i < list.length; i++) {
                delete(new File(file, list[i]));
            }
        }
        file.delete();
    }
    

    public static File getTemporaryDir() throws IOException {
        File nonExistantDir = File.createTempFile("testMessageStore-", "-dir");
        nonExistantDir.delete();
        return nonExistantDir;
    }
  
}
