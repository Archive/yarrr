/*
 * Created on Mar 22, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.gnome.yarrr.tests;

import org.gnome.yarrr.StringDiff;

import junit.framework.TestCase;

/**
 * @author alex
 */
public class StringDiffTest extends TestCase {
	
	private StringDiff diffTest(String a, String b, int diffSize) {
		StringDiff diff = new StringDiff(a, b);

		assertEquals(diff.length(), diffSize);
		String c = diff.applyTo(a);
		assertEquals(b, c);
		
		StringDiff reverse = new StringDiff(b, a);
		assertEquals(reverse.length(), diffSize);
		c = reverse.applyTo(b);
		assertEquals(a, c);
		
		return diff;
	}
	
	
	public void testIdentical() {
		diffTest("dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
				"dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
				0);
	}
	
	public void testStart() {
		diffTest("dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
				"aaadude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
				1);
	}

	public void testNearStart() {
		diffTest("dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
				"duaaade, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
				1);
	}

	
	public void testEnd() {
		diffTest("dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
		     	"dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.bbb",
		     	1);
	}

	public void testNearEnd() {
		diffTest("dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
				"dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeeaaand.",
				1);
	}

	public void testMiddle() {
		diffTest("dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
				"dude, what the fuckbbbb is going on here. This string is pretty long. And it never seems to eeeeeend.",
				1);
	}
	public void testMultipleMiddle() {
		diffTest("dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
				"dude, what the fuckbbbb is going on here. This string is pretty lobbbng. And it never seems to eeeeeend.",
				2);
	}

	public void testMultipleNear() {
		diffTest("dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
				"dude, what the fuck is going on haaaeraaae. This string is pretty long. And it never seems to eeeeeend.",
				1);
	}
	
	public void testRandom() {
		diffTest("dude, what the fuck is going on here. This string is pretty long. And it never seems to eeeeeend.",
		     	"dude, what the *beep* is going on here. This string is pretty long. And it never seems to end.",
		     	2);
	}
}
