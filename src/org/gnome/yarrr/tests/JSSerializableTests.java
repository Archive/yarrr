/*
 * Created on Apr 17, 2005
 */
package org.gnome.yarrr.tests;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.gnome.yarrr.JSSerializableVisitor;

/**
 * @author walters
 */
public class JSSerializableTests extends SerializableUsingTestCase {

    public void testStringSerialization() throws Exception {
        JSSerializableVisitor visitor = new JSSerializableVisitor();
        String foo = "foo";
        
        visitor.visit(foo);
        
        String serialized = visitor.toString();
        assertEquals("'foo'", serialized);
    }
    
    public void testFooSerialization() throws Exception {
        Foo f = new Foo();
        f.moo = "whee";
        JSSerializableVisitor visitor = new JSSerializableVisitor();
        visitor.visit(f);
        String serialized = visitor.toString();
        assertEquals("{'moo': 'whee'}", serialized);        
    }
    
    public void testBarSerialization() throws Exception {
        Bar b = new Bar();
        b.moo = "MOO";
        b.bar = 42;
        JSSerializableVisitor visitor = new JSSerializableVisitor();
        visitor.visit(b);
        String serialized = visitor.toString();
        assertEquals("{'bar': 42, 'moo': 'MOO'}", serialized);        
    }
    
    public void testMooSerialization() throws Exception {
        Moo m = new Moo();
        m.set = new HashSet();
        m.set.add("foo");
        m.set.add("bar");
        m.list = new ArrayList();
        m.list.add("baz");
        m.list.add(new Integer(42));
        JSSerializableVisitor visitor = new JSSerializableVisitor();
        visitor.visit(m);
        String serialized = visitor.toString();
        assertEquals("{'set': ['foo', 'bar'], 'list': ['baz', 42]}", serialized); 
    }
    
    public void testBazSerialization() throws Exception {
        Baz b = new Baz();
        b.moo = "yay";
        b.bar = 38;
        b.map = new HashMap();
        b.map.put("foo", new Integer(20));
        b.map.put("baz", "moo");
        JSSerializableVisitor visitor = new JSSerializableVisitor();
        visitor.visit(b);
        String serialized = visitor.toString();
        assertEquals("{'map': {'foo': 20, 'baz': 'moo'}, 'bar': 38, 'moo': 'yay'}", serialized); 
    }
    
    public void testBazRecSerialization() throws Exception {
        Baz b = new Baz();
        b.moo = "yay";
        b.bar = 38;
        b.map = new HashMap();
        Map recmap = new HashMap();
        recmap.put("moo", "cow");
        recmap.put("crack", "fun");
        b.map.put("recmap", recmap);
        b.map.put("baz", "mooo");
        JSSerializableVisitor visitor = new JSSerializableVisitor();
        visitor.visit(b);
        String serialized = visitor.toString();
        assertEquals("{'map': {'baz': 'mooo', 'recmap': {'moo': 'cow', 'crack': 'fun'}}, 'bar': 38, 'moo': 'yay'}", serialized); 
    }
}
