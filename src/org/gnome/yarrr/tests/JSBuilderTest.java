/*
 * Created on Apr 1, 2005
 */
package org.gnome.yarrr.tests;

import org.gnome.yarrr.JSBuilder;

import junit.framework.TestCase;
;

/**
 * @author alex
 */
public class JSBuilderTest extends TestCase {
	public void testBasics() {
		JSBuilder builder = new JSBuilder();
		builder.appendRaw("a").appendRaw("b");
		assertEquals("ab", builder.toString());
	}
	public void testEscaping() {
		JSBuilder builder = new JSBuilder();
		builder.append("\" ' & < >");
		assertEquals("'\\\" \\' \\& \\< \\>'", builder.toString());
	}
	public void testNewline() {
		JSBuilder builder = new JSBuilder();
		builder.append("a\nb");
		assertEquals("'a\\nb'", builder.toString());
	}	
}
