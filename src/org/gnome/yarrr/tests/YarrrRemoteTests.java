/*
 * Created on Apr 26, 2005
 */
package org.gnome.yarrr.tests;

import java.util.Hashtable;
import java.util.Vector;

import org.gnome.yarrr.ActiveTopic;
import org.gnome.yarrr.Chat;
import org.gnome.yarrr.ClosedComment;
import org.gnome.yarrr.Discussion;
import org.gnome.yarrr.HibernateUtil;
import org.gnome.yarrr.LiveComment;
import org.gnome.yarrr.Person;
import org.gnome.yarrr.ReferencableObjectRegistry;
import org.gnome.yarrr.Statement;
import org.gnome.yarrr.Whiteboard;
import org.gnome.yarrr.YarrrXmlRpcDispatcher;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * @author walters
 */
public class YarrrRemoteTests extends YarrrUsingTest {
    
    private void setupTransaction() {
        HibernateUtil.beginTransaction();
    }
    
    private void commitTransaction() {
        HibernateUtil.commitTransaction();
        HibernateUtil.closeSession();
    }
    
    public void setUp() throws Exception {
        super.setUp();
        YarrrXmlRpcDispatcher.wrapInvocationsInHibernate = true;
    }
    
    public void disableAutomaticRpcTransactions() {
        YarrrXmlRpcDispatcher.wrapInvocationsInHibernate = false;
    }
       
    public void testRemoteGetPerson() throws Exception {
        setupTransaction();
        
        Person p1 = getPerson1();
        String p1Ref = ReferencableObjectRegistry.getReference(p1);
        
        commitTransaction();
        
        Vector args = new Vector();
        args.add("person1");
        Object ret = getYarrr().getXmlRpcHandler().execute("getPerson", args);
        assertEquals(p1Ref, ret);
    }
    
    public void testRemoteAddStatement() throws Exception {
        setupTransaction();
        
        ActiveTopic t = activateTopic();
        String tRef = ReferencableObjectRegistry.getReference(t);
        Person p1 = getPerson1();
        String p1Ref = ReferencableObjectRegistry.getReference(p1);
        
        commitTransaction();

        Vector args = new Vector();
        args.add(tRef);
        args.add(p1Ref);
        args.add("foo bar baz");
        String sRef = (String) getYarrr().getXmlRpcHandler().execute("addStatement", args);
        
        setupTransaction();
        
        p1 = (Person) ReferencableObjectRegistry.lookup(p1Ref);
        Statement s = (Statement) ReferencableObjectRegistry.lookup(sRef);
        assertEquals(p1, s.getAuthor());
        assertEquals("foo bar baz", s.getContent());
        
        commitTransaction();
    }
    
    public void testRemoteCloseComment() throws Exception {
        setupTransaction();
        
        ActiveTopic t = activateTopic();
        String tRef = ReferencableObjectRegistry.getReference(t);
        Discussion d = getDefaultDiscussion();
        String dRef = ReferencableObjectRegistry.getReference(d);
        LiveComment c = getLiveComment();
        String cRef = ReferencableObjectRegistry.getReference(c);
        Person p2 = getPerson2();
        String p2Ref = ReferencableObjectRegistry.getReference(p2);
        
        commitTransaction();
        
        Vector args = new Vector();
        args.add(tRef);
        args.add(dRef);
        args.add(cRef);
        args.add(p2Ref);
        String closedRef = (String) getYarrr().getXmlRpcHandler().execute("closeLiveComment", args);
    }
    
    public void testRemoteAddChatMessage() throws Exception {
        setupTransaction();
        
        ActiveTopic t = activateTopic();
        String tRef = ReferencableObjectRegistry.getReference(t);
        Discussion d = getDefaultDiscussion();
        Chat c = d.getChat();
        String cRef = ReferencableObjectRegistry.getReference(c);
        String p1Ref = ReferencableObjectRegistry.getReference(getPerson1());
        
        commitTransaction();
        
        Vector args = new Vector();
        args.add(tRef);
        args.add(cRef);
        args.add(p1Ref);
        args.add("foo bar baz whee");
        getYarrr().getXmlRpcHandler().execute("addChatMessage", args);
    }
    
    public void testRemoteGetChatMessages() throws Exception {
        setupTransaction();
        
        ActiveTopic t = activateTopic();
        Discussion d = getDefaultDiscussion();
        Chat c = d.getChat();
        Integer messageIndex = new Integer(c.getLastMessageIndex());
        Integer changeIndex = new Integer(c.getLastChangeIndex());
        t.addChatMessage(c, getPerson2(), "hi there", false);
        String cRef = ReferencableObjectRegistry.getReference(c);
        String p1Ref = ReferencableObjectRegistry.getReference(getPerson1());
        
        commitTransaction();
        
        Vector args = new Vector();
        args.add(cRef);
        args.add(messageIndex);
        args.add(changeIndex);
        Hashtable result = (Hashtable) getYarrr().getXmlRpcHandler().execute("getChatUpdates", args);
        
        Vector msgs = (Vector) result.get("messages"); 
        assertEquals(1, msgs.size());
        String msgRef = (String) msgs.get(0);
        
        args.clear();
        Vector objlist = new Vector();
        objlist.add(msgRef);
        args.add(objlist);
        Hashtable msg = (Hashtable) ((Vector) getYarrr().getXmlRpcHandler().execute("dumpObjects", args)).get(0);
        
        assertEquals("person2", msg.get("author"));
        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<yarrr-markup><text>hi there</text></yarrr-markup>\n", msg.get("contents"));
    }
    
    public void testRemoteClosedCommentProponent() throws Exception {
        setupTransaction();
        ClosedComment c = getClosedComment();
        String cRef = ReferencableObjectRegistry.getReference(c);
        
        commitTransaction();
        
        Person capn = getYarrr().getTheCapn();
        String capnRef = ReferencableObjectRegistry.getReference(capn);
        c = (ClosedComment) ReferencableObjectRegistry.lookup(cRef);
        assertEquals(0, c.getProponents().size());
        Vector args = new Vector();
        args.add(cRef);
        args.add(capnRef);
        getYarrr().getXmlRpcHandler().execute("addProponent", args);
        
        setupTransaction();
        
        c = (ClosedComment) ReferencableObjectRegistry.lookup(cRef);
        assertEquals(1, c.getProponents().size());
        capn = getYarrr().getTheCapn();
        assertTrue(c.getProponents().contains(capn));
        
        commitTransaction();
    }
    
    public void testRemoteClosedCommentSimultaneousProponent() throws Exception {
        disableAutomaticRpcTransactions();
        
        setupTransaction();
        
        ActiveTopic t = activateTopic();
        Discussion d = getDefaultDiscussion();
        LiveComment lc = getLiveComment();
        lc.setContents("foo bar baz");
        lc.addAuthor(getYarrr().getTheCapn());
        lc.addAuthor(getPerson2());
        ClosedComment c = t.closeComment(d, lc, getYarrr().getTheCapn());
        String cRef = ReferencableObjectRegistry.getReference(c);
        
        commitTransaction();
        
        HibernateUtil.beginTransaction();
        
        Person capn = getYarrr().getTheCapn();
        String capnRef = ReferencableObjectRegistry.getReference(capn);
        c = (ClosedComment) ReferencableObjectRegistry.lookup(cRef);
        assertEquals(0, c.getProponents().size());
        Vector args = new Vector();
        args.add(cRef);
        args.add(capnRef);
        getYarrr().getXmlRpcHandler().execute("addProponent", args);
        // Don't commit yet, simulate simultaneous execution
        
        Session savedSession = HibernateUtil.getSession();
        Transaction savedTransaction = HibernateUtil.getTransaction();
        Session session2 = HibernateUtil.openNewSession();
        HibernateUtil.beginTransaction();
        Transaction transaction2 = HibernateUtil.getTransaction();
        
        Person p2 = getPerson2();
        String p2Ref = ReferencableObjectRegistry.getReference(p2);
        c = (ClosedComment) ReferencableObjectRegistry.lookup(cRef);
        // At this point we should not see the other proponent
        assertEquals(1, c.getProponents().size());
        args = new Vector();
        args.add(cRef);
        args.add(p2Ref);
        getYarrr().getXmlRpcHandler().execute("addProponent", args);
        
        // Switch back to first session, commit it
        HibernateUtil.setTransaction(savedSession, savedTransaction);
        HibernateUtil.commitTransaction();
        HibernateUtil.closeSession();
        
        // Now commit second session
        HibernateUtil.setTransaction(session2, transaction2);
        HibernateUtil.commitTransaction();
        HibernateUtil.closeSession();
        
        // Now investigate object, we should have both proponents
        setupTransaction();
        c = (ClosedComment) ReferencableObjectRegistry.lookup(cRef);
        assertEquals(2, c.getProponents().size());
        assertTrue(c.getProponents().contains(getYarrr().getTheCapn()));
        assertTrue(c.getProponents().contains(getPerson2()));
        commitTransaction();
    }
    
    public void testRemoteCreateWhiteboard() throws Exception {
        setupTransaction();
        
        Discussion d = getDefaultDiscussion();
        String dRef = ReferencableObjectRegistry.getReference(d);
        Person p1 = getPerson1();
        String p1Ref = ReferencableObjectRegistry.getReference(p1);
        
        commitTransaction();

        Vector args = new Vector();
        args.add(dRef);
        args.add(p1Ref);
        String wRef = (String) getYarrr().getXmlRpcHandler().execute("createWhiteboard", args);
        
        setupTransaction();
        
        Object w = ReferencableObjectRegistry.lookup(wRef);
        assertTrue(w instanceof Whiteboard);
        
        commitTransaction();
    }
}
