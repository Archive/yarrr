/*
 * Created on Mar 18, 2005
 */
package org.gnome.yarrr.tests;

import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import junit.framework.TestCase;

import org.gnome.yarrr.ActiveTopic;
import org.gnome.yarrr.Chat;
import org.gnome.yarrr.ClosedComment;
import org.gnome.yarrr.Discussion;
import org.gnome.yarrr.LiveComment;
import org.gnome.yarrr.Person;
import org.gnome.yarrr.ReferencableObject;
import org.gnome.yarrr.ReferencableObjectRegistry;
import org.gnome.yarrr.Topic;
import org.gnome.yarrr.ToplevelReferencableObject;
import org.gnome.yarrr.Yarrr;
import org.gnome.yarrr.Person.NickNameExistsException;

/**
 * @author walters
 */
public class YarrrUsingTest extends TestCase implements ToplevelReferencableObject.ChangeListener {

	private Yarrr theYarrr;
    private File tmpPath;
    private Set changedObjs;
    private Set expectedChangedObjs;
    protected static final String COMMENT_TEXT = "foo bar baz whee fun foo bar baz whee foo bar foo bar baz whee fun foo bar baz whee foo bar foo bar baz whee fun foo bar baz whee foo bar";

	protected Yarrr getYarrr() {
		assert theYarrr != null;
		return theYarrr;
	}

	public void setUp() throws Exception {
		super.setUp();
        tmpPath = TestUtils.getTemporaryDir();
		theYarrr = new Yarrr(tmpPath.toString());
        theYarrr.startup(false);
        ToplevelReferencableObject.setChangeListener(this);
        this.changedObjs = new HashSet();
        this.expectedChangedObjs = new HashSet();
	}
    
    protected void clearChanged() {
        this.changedObjs.clear();
    }
    
    protected void expectChanged(ReferencableObject obj) {
        this.expectedChangedObjs.add(ReferencableObjectRegistry.getReference(obj));
    }
    
    protected Person getPerson1() throws NickNameExistsException {
       return Person.lookupOrCreate("person1");
    }
    
    protected Person getPerson2() throws NickNameExistsException {
       return Person.lookupOrCreate("person2");
    }
    
    private void printIterator(String header, Iterator it) {
        System.out.print(header);
        while (it.hasNext()) {
            System.out.print(it.next());
            System.out.print(", ");
        }
        System.out.println();
    }
    
    protected void verifyChanged() throws Exception {
        Iterator it = expectedChangedObjs.iterator();
        while (it.hasNext()) {
            String expected = (String) it.next();
            assertTrue("Object \"" + expected + "\" changed", changedObjs.contains(expected));
        }
        expectedChangedObjs.removeAll(changedObjs);
        assertEquals(0, expectedChangedObjs.size());
        this.changedObjs = new HashSet();
    }
 
	public void tearDown() throws Exception {
        verifyChanged();
        theYarrr.shutdownDB();
        TestUtils.delete(tmpPath);
		theYarrr = null;
        ReferencableObjectRegistry.clear();
        super.tearDown();
	}

    public void objectChanged(String reference) {
         
    }
    
    protected String yarrrMarkupify(String str) {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<yarrr-markup><text>" + str + "</text></yarrr-markup>\n";
    }

    protected Topic loadTopic() {
        return this.getYarrr().lookupTopic("Welcome to Yarrr");
    }

    protected ActiveTopic activateTopic() {
         Topic topic = this.loadTopic();
         return this.getYarrr().activateTopic(topic);
    }

    protected Discussion getDefaultDiscussion() {
        Iterator it = this.activateTopic().getDiscussions().iterator();
        while (it.hasNext()) {
            Discussion d = (Discussion) it.next();
            if (d.isDefault()) 
                return d;
        }
        throw new Error("should not be reached");
    }
    
    protected Chat getChat() {
        return getDefaultDiscussion().getChat();
    }

    protected LiveComment getLiveComment() {
        Discussion d = getDefaultDiscussion();
        return d.openComment(this.getYarrr().getTheCapn(), "", "");
    }

    protected ClosedComment getClosedComment() throws Exception {
        ActiveTopic t = activateTopic();
        Discussion d = getDefaultDiscussion();
        LiveComment c = getLiveComment();
        c.setContents(COMMENT_TEXT);
        return t.closeComment(d, c, getYarrr().getTheCapn());
    }

    public void changed(ToplevelReferencableObject obj, String ref) {
        this.changedObjs.add(ref); 
    }
}
