/*
 * Created on Apr 18, 2005
 */
package org.gnome.yarrr.tests;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;

/**
 * @author walters
 */
public class SerializableUsingTestCase extends TestCase {
    public class Foo implements Serializable {
        private static final long serialVersionUID = 1L;
        public String moo;
    }
    
    public class Bar extends Foo {
        private static final long serialVersionUID = 1L;
        int bar;
    }
    
    public class Moo implements Serializable {
        private static final long serialVersionUID = 1L;
        Set set;
        List list;
    }
    
    public class Baz extends Bar {
        private static final long serialVersionUID = 1L;
        Map map;
    }
}
