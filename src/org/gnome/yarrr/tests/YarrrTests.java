/*
 * Created on Mar 23, 2005
 */
package org.gnome.yarrr.tests;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.gnome.yarrr.ActiveTopic;
import org.gnome.yarrr.ArchivedChatMessage;
import org.gnome.yarrr.Chat;
import org.gnome.yarrr.ChatMessage;
import org.gnome.yarrr.ClosedComment;
import org.gnome.yarrr.Discussion;
import org.gnome.yarrr.HibernateUtil;
import org.gnome.yarrr.LiveComment;
import org.gnome.yarrr.Person;
import org.gnome.yarrr.ReferencableObjectRegistry;
import org.gnome.yarrr.Statement;
import org.gnome.yarrr.Topic;

/**
 * @author walters
 */
public class YarrrTests extends YarrrUsingTest {
    
    public void setUp() throws Exception {
        super.setUp();
        HibernateUtil.beginTransaction();
    }

    public void tearDown() throws Exception {
        HibernateUtil.commitTransaction();
        HibernateUtil.closeSession();
        super.tearDown();
    }
    
    
    public void newTestTransaction() throws Exception {
        HibernateUtil.commitTransaction();
        HibernateUtil.closeSession();
        verifyChanged();
        HibernateUtil.beginTransaction();
    }
    
    public void testLoadTopic() throws Exception {
		assertNotNull(this.loadTopic());
	}
    
    public void testCreatePersons() throws Exception {
        Person p = getPerson1();
        String pRef = ReferencableObjectRegistry.getReference(p);
        
        newTestTransaction();
        
        p = (Person) ReferencableObjectRegistry.lookup(pRef);
        assertEquals("person1", p.getNickName());
        
        newTestTransaction();
       
        Person p2 = getPerson2();
        String p2Ref = ReferencableObjectRegistry.getReference(p2);
        
        newTestTransaction();
        
        p = (Person) ReferencableObjectRegistry.lookup(pRef);
        assertEquals("person1", p.getNickName());
        p2 = (Person) ReferencableObjectRegistry.lookup(p2Ref);
        assertEquals("person2", p2.getNickName());
    }
    
    public void testTopicActivation() throws Exception {
       ActiveTopic t = this.activateTopic();
       assertEquals(0, t.getClosedComments().size());
       assertEquals(1, t.getDiscussions().size());
    }
    
    public void testGetDefaultDiscussion() {
        Discussion d = getDefaultDiscussion();
        assertEquals(0, d.getComments().size());
    }
    
    public void testOpenLiveComment() throws Exception {
        Discussion d = getDefaultDiscussion();
        LiveComment c = getLiveComment();
        assertEquals(1, d.getComments().size());
        assertEquals(c, d.getComments().iterator().next());
        expectChanged(d);
    }
    
    public void testSendChatMessage() throws Exception {
        ActiveTopic t = activateTopic();
        Person p1 = getPerson1();
        Chat c = getChat();
        int messageIndex = c.getLastMessageIndex();
        t.addChatMessage(c, p1, "hi there", false);
        Chat.Update update = c.getChatUpdates(messageIndex, c.getLastChangeIndex());
        assertEquals(1, update.getMessages().size());
        Map mesgMap = (Map) update.getMessages().get(0);
        String expected = yarrrMarkupify("hi there");
        String actual = (String)mesgMap.get("contents");
        assertEquals(expected, actual);
    }

    private void postCloseAsserts(ActiveTopic t, ClosedComment closed) {
        assertEquals(1, t.getClosedComments().size());
        assertEquals(closed, t.getClosedComments().iterator().next());
        assertEquals(getYarrr().getTheCapn(), closed.getCloser());
        assertEquals(0, closed.getProponents().size());
    }
    
    public void testCloseCommentWithContent() throws Exception {
        ActiveTopic t = activateTopic();
        Discussion d = getDefaultDiscussion();
        assertEquals(0, t.getClosedComments().size());
        ClosedComment closed = getClosedComment();
        expectChanged(t);
        expectChanged(d);

        postCloseAsserts(t, closed);
    }
    
    public void testCloseCommentWithChat() throws Exception {
        ActiveTopic t = activateTopic();
        LiveComment live = getLiveComment();
        Chat c = getChat();
        t.addChatMessage(c, getYarrr().getTheCapn(), "test1", false);
        t.addChatMessage(c, getPerson1(), "test2", false);

        Discussion d = getDefaultDiscussion();
        
        ClosedComment closed = t.closeComment(d, live, getYarrr().getTheCapn());
        String cRef = ReferencableObjectRegistry.getReference(closed);
        
        // Should not be retrieved
        t.addChatMessage(c, getPerson2(), "test3", false);
        
        newTestTransaction();
        
        closed = (ClosedComment) ReferencableObjectRegistry.lookup(cRef);
        List messages = closed.getMessages();
        assertEquals(2, messages.size());
        ArchivedChatMessage m0 = (ArchivedChatMessage) messages.get(0);
        ArchivedChatMessage m1 = (ArchivedChatMessage) messages.get(1);
        assertEquals(yarrrMarkupify("test1"), m0.getContents());
        assertEquals(getYarrr().getTheCapn().getRef(), m0.getAuthor().getRef());
        assertEquals(yarrrMarkupify("test2"), m1.getContents());
        assertEquals(getPerson1().getRef(), m1.getAuthor().getRef());       
    }

    public void testLoadClosedComment() throws Exception {
        ActiveTopic t = activateTopic();
        ClosedComment closed = getClosedComment();
        String ref = ReferencableObjectRegistry.getReference(closed);
        closed = null;
        
        newTestTransaction();
        
        closed = (ClosedComment) ReferencableObjectRegistry.lookup(ref);
        assertNotNull(closed);
        assertEquals(yarrrMarkupify(COMMENT_TEXT), closed.getContent());   
        postCloseAsserts(t, closed);
    }
    
    ClosedComment getMultiAuthorClosedComment() throws Exception {
        ActiveTopic t = activateTopic();
        Discussion d = getDefaultDiscussion();
        LiveComment lc = getLiveComment();
        lc.setContents("moo");
        lc.addAuthor(getPerson1());
        lc.addAuthor(getPerson2());
        return t.closeComment(d, lc, getYarrr().getAnonymous());
    }
    
    public void testCloseCommentMultiAuthor() throws Exception {
        ClosedComment closed = getMultiAuthorClosedComment();
        String closedRef = ReferencableObjectRegistry.getReference(closed);
        
        newTestTransaction();
        closed = (ClosedComment) ReferencableObjectRegistry.lookup(closedRef);
        Set authors = closed.getAuthors();
        assertEquals(3, authors.size());
        assertTrue(authors.contains(getYarrr().getTheCapn()));
        assertTrue(authors.contains(getPerson1()));
        assertTrue(authors.contains(getPerson2()));
    }
    
    public void testProponentClosedComment() throws Exception {
        ClosedComment closed = getClosedComment();
        String ref = ReferencableObjectRegistry.getReference(closed);

        newTestTransaction();
        
        closed = (ClosedComment) ReferencableObjectRegistry.lookup(ref);
        closed.addProponent(getYarrr().getTheCapn());
        expectChanged(closed);
        
        newTestTransaction();
        
        closed = (ClosedComment) ReferencableObjectRegistry.lookup(ref);
        Iterator it = closed.getProponents().iterator();
        assertTrue(it.hasNext());
        assertEquals(getYarrr().getTheCapn(), it.next());
        assertFalse(it.hasNext());
    }
    
    public void testProponentMultipleClosedComments() throws Exception {
        ClosedComment closed = getMultiAuthorClosedComment();
        Set proponents = closed.getProponents();
        assertEquals(0, proponents.size());
        closed.addProponent(getPerson2());
        proponents = closed.getProponents();
        assertEquals(1, proponents.size());
        
        String closedRef = ReferencableObjectRegistry.getReference(closed);
        
        ActiveTopic t = activateTopic();
        Discussion d = getDefaultDiscussion();
        LiveComment c = getLiveComment();
        c.setContents("yay whee fun");
        c.addAuthor(getYarrr().getTheCapn());
        c.addAuthor(getPerson1());
        ClosedComment closed2 = t.closeComment(d, c, getYarrr().getAnonymous());
        proponents = closed2.getProponents();
        assertEquals(0, proponents.size());
        closed2.addProponent(getYarrr().getTheCapn());
        closed2.addProponent(getPerson1());
        proponents = closed2.getProponents();
        assertEquals(2, proponents.size());
        String closed2ref = ReferencableObjectRegistry.getReference(closed2);
        
        newTestTransaction();
        
        closed = (ClosedComment) ReferencableObjectRegistry.lookup(closedRef);
        proponents = closed.getProponents();
        assertEquals(1, proponents.size());
        assertEquals(getPerson2(), proponents.iterator().next());
        
        closed2 = (ClosedComment) ReferencableObjectRegistry.lookup(closed2ref);
        proponents = closed2.getProponents();
        assertEquals(2, proponents.size());
        assertTrue(proponents.contains(getYarrr().getTheCapn()));
        assertTrue(proponents.contains(getPerson1()));
    }
    
    Statement getStatement() throws Exception {
        ActiveTopic t = activateTopic();
        return t.addStatement(getYarrr().getAnonymous(), "babies are tasty");
    }
    
    public void testCreateStatement() throws Exception {
        ActiveTopic t = activateTopic();
        clearChanged();
        Statement s = getStatement();
        expectChanged(t);
        assertEquals(1, s.getSubscribers().size());
        assertEquals(getYarrr().getAnonymous(), s.getAuthor());
    }

    public void testSubscribeStatement() throws Exception {
        ActiveTopic t = activateTopic();
        Statement s = getStatement();
        String ref = ReferencableObjectRegistry.getReference(s);
        
        newTestTransaction();
        
        s = (Statement) ReferencableObjectRegistry.lookup(ref);
        assertFalse(s.hasSubscriber(getYarrr().getTheCapn()));
        s.addSubscriber(getYarrr().getTheCapn());
        assertTrue(s.hasSubscriber(getYarrr().getTheCapn()));
        expectChanged(s);
              
        newTestTransaction();
        
        s = (Statement) ReferencableObjectRegistry.lookup(ref);
        assertTrue(s.hasSubscriber(getYarrr().getTheCapn()));
        
        Person p = Person.lookupOrCreate("moo");
        String pRef = ReferencableObjectRegistry.getReference(p);
        s.addSubscriber(p);
        
        newTestTransaction();
        
        s = (Statement) ReferencableObjectRegistry.lookup(ref);
        p = (Person) ReferencableObjectRegistry.lookup(pRef);
        assertTrue(s.hasSubscriber(p));
    }
    
    private void verifyStatement(Statement s) {
        assertTrue(s.hasSubscriber(getYarrr().getAnonymous()));
        assertFalse(s.hasSubscriber(getYarrr().getTheCapn()));
    }
    
    public void testSubscribeStatement2() throws Exception {
        testSubscribeStatement();
        ActiveTopic t = activateTopic();
        Statement s1 = getStatement();
        String s1ref = ReferencableObjectRegistry.getReference(s1);
        verifyStatement(s1);
        
        newTestTransaction();
        
        s1 = (Statement) ReferencableObjectRegistry.lookup(s1ref);
        verifyStatement(s1);
        
        Statement s2 = t.addStatement(getPerson1(), "foo bar baz");
        String s2ref = ReferencableObjectRegistry.getReference(s2);
        assertEquals(getPerson1(), s2.getAuthor());
        assertTrue(s2.hasSubscriber(getPerson1()));
        
        newTestTransaction();
        
        s1 = (Statement) ReferencableObjectRegistry.lookup(s1ref);
        verifyStatement(s1);
        s2 = (Statement) ReferencableObjectRegistry.lookup(s2ref);
        assertEquals(getPerson1(), s2.getAuthor());
        assertTrue(s2.hasSubscriber(getPerson1()));
        s2.addSubscriber(getYarrr().getTheCapn());
        assertTrue(s2.hasSubscriber(getYarrr().getTheCapn()));
        
        newTestTransaction();
        
        s1 = (Statement) ReferencableObjectRegistry.lookup(s1ref);
        verifyStatement(s1);
        s2 = (Statement) ReferencableObjectRegistry.lookup(s2ref);
        assertTrue(s2.hasSubscriber(getPerson1()));
        assertTrue(s2.hasSubscriber(getYarrr().getTheCapn()));
    }
    
    public void testunSubscribeStatement() throws Exception {
        ActiveTopic t = activateTopic();
        Statement s1 = getStatement();
        String s1ref = ReferencableObjectRegistry.getReference(s1);
        verifyStatement(s1);
        t.subscribeStatement(s1, getPerson2());
        assertEquals(2, s1.getSubscribers().size());
        
        newTestTransaction();
        
        s1 = (Statement) ReferencableObjectRegistry.lookup(s1ref);
        assertEquals(2, s1.getSubscribers().size());
        t.unsubscribeStatement(s1, getPerson2());
        
        expectChanged(s1);
        
        newTestTransaction();
        
        s1 = (Statement) ReferencableObjectRegistry.lookup(s1ref);
        assertEquals(1, s1.getSubscribers().size());
        assertTrue(s1.hasSubscriber(getYarrr().getAnonymous()));
        assertFalse(s1.hasSubscriber(getPerson2()));
    }
    
    public void testunSubscribeDeleteStatement() throws Exception {
        ActiveTopic t = activateTopic();
        String tRef = ReferencableObjectRegistry.getReference(t);
        Statement s1 = getStatement();
        String s1ref = ReferencableObjectRegistry.getReference(s1);
        
        newTestTransaction();
        
        t = (ActiveTopic) ReferencableObjectRegistry.lookup(tRef);
        s1 = (Statement) ReferencableObjectRegistry.lookup(s1ref);
        assertEquals(1, s1.getSubscribers().size());
        assertTrue(s1.hasSubscriber(getYarrr().getAnonymous()));
        t.unsubscribeStatement(s1, getYarrr().getAnonymous());
        
        expectChanged(t);
    }
    
    public void testCreateTopic() throws Exception {
        Topic t = getYarrr().createTopic("Foo", getPerson1());
        assertEquals(0, t.getStatementCount());
        assertEquals(0, t.getClosedCommentCount());
        String tRef = ReferencableObjectRegistry.getReference(t);
        
        newTestTransaction();
        
        t = (Topic) ReferencableObjectRegistry.lookup(tRef);
        assertEquals("Foo", t.getName());
        assertEquals(0, t.getStatementCount());
        assertEquals(0, t.getClosedCommentCount());
    }
    
    
    public void testSubscribeChatMessage() throws Exception {
        ActiveTopic t = activateTopic();
        Discussion d = getDefaultDiscussion();
        Person p1 = getPerson1();
        String p1Ref = ReferencableObjectRegistry.getReference(p1);
        Chat c = getChat();
        int messageIndex = c.getLastMessageIndex();
        int changeIndex = c.getLastChangeIndex();
        t.addChatMessage(c, p1, "hello world", false);

        newTestTransaction();

        Chat.Update update = c.getChatUpdates(messageIndex, changeIndex);
        assertEquals(1, update.getMessages().size());
        Map mesgMap = (Map) update.getMessages().get(0);
        int mesgIndex = ((Integer)mesgMap.get("index")).intValue();
        ChatMessage mesg = c.getMessage(mesgIndex);

        Person p2 = getPerson2();
        String p2Ref = ReferencableObjectRegistry.getReference(p2);

        clearChanged();
        assertEquals(0, mesg.getSubscriberSize());
        c.addMessageSubscriber(mesgIndex, p2);
        assertEquals(1, mesg.getSubscriberSize());
        assertTrue(mesg.hasSubscriber(p2));
        expectChanged(c);

        newTestTransaction();

        p2 = (Person) ReferencableObjectRegistry.lookup(p2Ref);

        assertEquals(1, mesg.getSubscriberSize());
        assertTrue(mesg.hasSubscriber(p2));
    }
    
    public void testunSubscribeChatMessage() throws Exception {
        ActiveTopic t = activateTopic();
        Discussion d = getDefaultDiscussion();
        Person p1 = getPerson1();
        String p1Ref = ReferencableObjectRegistry.getReference(p1);
        Chat c = getChat();
        int messageIndex = c.getLastMessageIndex();
        int changeIndex = c.getLastChangeIndex();
        t.addChatMessage(c, p1, "hello world", false);
        Chat.Update update = c.getChatUpdates(messageIndex, changeIndex);
        Map mesgMap = (Map) update.getMessages().get(0);
        int mesgIndex = ((Integer)mesgMap.get("index")).intValue();
        ChatMessage mesg = c.getMessage(mesgIndex);
        mesg.addSubscriber(getPerson2());

        newTestTransaction();

        Person p2 = getPerson2();

        clearChanged();
        c.removeMessageSubscriber(mesgIndex, p2);
        expectChanged(c);

        newTestTransaction();

        assertEquals(0, mesg.getSubscriberSize());
    }

}
