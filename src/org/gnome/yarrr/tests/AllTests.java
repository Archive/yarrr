/*
 * Created on May 6, 2005
 */
package org.gnome.yarrr.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @author walters
 */
public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite("All tests");
        suite.addTest(new TestSuite(YarrrTests.class));
        suite.addTest(new TestSuite(YarrrRemoteTests.class));
        return suite;
    }

}
