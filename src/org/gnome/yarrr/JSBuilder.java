/*
 * Created on Apr 1, 2005
 */
package org.gnome.yarrr;

import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.gnome.yarrr.xmlrpc.XmlRpcSerializable;

/**
 * @author alex
 */
public class JSBuilder {
	StringBuffer builder;
	Pattern stringPattern;
	
	public JSBuilder() {
		builder = new StringBuffer();
		
		stringPattern = Pattern.compile("[\\\\\"'<>&\\n]");
	}
	
	public String toString() {
		return builder.toString();
	}
	
	public JSBuilder appendRaw(String s) {
		builder.append(s);
		return this; 
	}
	
	public JSBuilder append(String s) {
		builder.append("'");
		
		Matcher m = stringPattern.matcher(s);
		while (m.find()) {
		    m.appendReplacement(builder, "\\\\");
		    char c = s.charAt(m.start());
		    if (c == '\n')
		    	builder.append("n");
		    else
		    	builder.append(c);
		    	
		}
		m.appendTail(builder);
		
		builder.append("'");
		
		return this;
	}

	public JSBuilder append(int i) {
		builder.append(i);
		return this;
	}
	
    /* objects which implement both XmlRpcSerializable and
     * ReferencableObject are serialized as ids. 
     */
	private JSBuilder appendObject(Object obj) {
		if (obj instanceof String) {
			return this.append((String) obj);
		} else if (obj instanceof Date) {
			return this.append((Date) obj);
		} else if (obj instanceof Integer) {
			return this.append(((Integer) obj).intValue());
		} else if (obj instanceof Boolean) {
			return this.append(((Boolean) obj).booleanValue());
		} else if (obj instanceof Map) {
			return this.appendStringMap((Map) obj);
		} else if (obj instanceof Collection) {
			return this.appendCollection((Collection) obj);
		} else if (obj instanceof ReferencableObject) {
			return this.appendRef((ReferencableObject) obj);
        } else if (obj instanceof XmlRpcSerializable) {
            Map table = new Hashtable();
            ((XmlRpcSerializable) obj).xmlRpcSerialize(table);
            return this.appendStringMap(table);
		} else if (obj instanceof YarrrMarkup) {
			return this.append((String) obj.toString());
		} else {
			throw new IllegalArgumentException("Cannot append object of class \"" + obj.getClass() + "\"");
		}
	}


	public JSBuilder append(boolean b) {
		if (b) {
			this.appendRaw("true");
		} else {
			this.appendRaw("false");
		}
		return this;
	}

	public JSBuilder append(Date closeDate) {
		//FIXME: This seems to get the timezone wrong
		builder.append("new Date(").append(closeDate.getTime()).append(")");
		return this;
	}

	public JSBuilder appendRef(ReferencableObject obj) {
		String id = ReferencableObjectRegistry.getReference(obj);
		append(id);
		return this;
	}
	
	/**
	 * Converts a collection into JavaScript.  Note that this in general
	 * is not a "deep copy" of the collection.  In particular, if the
	 * collection contains objects which implement both XmlRpcSerializable and
	 * ReferencableObject (as is typical), we choose to turn them into reference strings rather
	 * than recursively dumping them.
	 * 
	 * @param obj an object to convert to JavaScript
	 * @return
	 */
	public JSBuilder appendCollection(Collection collection) {
		builder.append("[");
		for (Iterator i = collection.iterator(); i.hasNext(); ) {
			Object obj = i.next();
			this.appendObject(obj);
			if (i.hasNext())
				builder.append(",");
		}
		builder.append("]");
		return this;
	}

	public JSBuilder appendStringMap(Map map) {
		this.appendRaw("{");
		
		Iterator it = map.entrySet().iterator();
		
		while (it.hasNext()) {
			Map.Entry entry = (Entry) it.next();
			
			this.append((String) entry.getKey());
			this.appendRaw(": ");
			this.appendObject(entry.getValue());
			if (it.hasNext()) {
				this.appendRaw(", ");
			}
		}
		
		this.appendRaw("}");
		
		return this;
	}
}

