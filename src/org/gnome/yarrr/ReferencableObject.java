/*
 * Created on Mar 24, 2005
 */
package org.gnome.yarrr;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.gnome.yarrr.xmlrpc.XmlRpcSerializable;

/**
 * @author walters
 * 
 * Every ReferencableObject has an ID: java class name/an integer
 * e.g. "org.gnome.yarrr.Chat/5" (the referenceId).
 * It corresponds to a clientside JavaScript ReferencableObject
 * This name is used as a key to the object in the ReferencableObjectRegistry
 * 
 * Has an integer version; you may want to look at #ActiveTopicObject which overrides incVersion
 */
public abstract class ReferencableObject implements XmlRpcSerializable {
    static Log logger = LogFactory.getLog(ReferencableObject.class);
    
    public static final class Reference {
        Class klass;
        long id;
        
        public Reference() {
        }
        
        public Reference(long id, Class klass) {
            this.id = id;
            this.klass = klass;
        }
        public boolean equals(Object obj) {
            if (obj instanceof Reference) {
                Reference other = (Reference) obj;
                return (this.klass == other.klass) &&
                       (this.id == other.id);
            }
            return false;
        }
        public int hashCode() {
            return klass.hashCode() ^ (int)id;
        }
    }
    
	protected long referenceId;
    
	protected ReferencableObject() {
        this.referenceId = -1;
	}

    public Reference getReference() {
        return new Reference(this.referenceId, this.getClass());
    }
    
	public long getReferenceId() {
		return this.referenceId;
	}

	public void setReferenceId(long id) {
		this.referenceId = id;
	}
	
    public Monitor getMonitor() {
        return ReferencableObjectRegistry.lookupMonitor(this);
    }
    
	public void delete() {
		ReferencableObjectRegistry.deregister(this);
	}

	public synchronized void xmlRpcSerialize(Map table) {
		table.put("id", ReferencableObjectRegistry.getReference(this));
	}
	
	// FIXME this function should go away
	public Vector personCollectionToVector(Collection persons) {
		Iterator it = persons.iterator();
		Vector ret = new Vector();
		while (it.hasNext()) {
		    Person person = (Person) it.next(); 
		    ret.add(person.toString());
		}
		return ret;
	}
    
//   FIXME this function should go away
    public static Vector personRefSetToVector(Set personSet) {
        Iterator it = personSet.iterator();
        Vector ret = new Vector();
        while (it.hasNext()) {
            Person.Ref person = (Person.Ref) it.next(); 
            ret.add(person.load().getNickName());
        }
        return ret;
    }
    
    public void signalChanged() {
        Monitor m = getMonitor();
        if (logger.isDebugEnabled()) {
            logger.debug("signalChanged("+ ReferencableObjectRegistry.getReference(this) + ") - monitor: " + m);
        }
        m.queueSignalChanged();
    }
    
}
