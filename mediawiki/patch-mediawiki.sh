#!/bin/bash

dirname=$(dirname $0)
cd $dirname

# Usage: patch-mediawiki.sh MEDIAWIKI_INSTALLATION_DIRECTORY YARRR_HOST

if [ ! $# -eq 2 ] ; then
        echo "usage: $0 MEDIAWIKI_INSTALLATION_DIRECTORY YARRR_HOST"
        exit 1;
fi

TARGET=$1
YARRR_HOST=$2
EXTENSIONS="yarrr.php yarrr-xmlrpc.php yarrr-auth.php"

echo Installing extensions...
cp $EXTENSIONS $TARGET/extensions/

echo Patching LocalSettings.php...
if [ -s $TARGET/LocalSettings.php.original ]
then
  cp $TARGET/LocalSettings.php.original $TARGET/LocalSettings.php
else
  cp $TARGET/LocalSettings.php $TARGET/LocalSettings.php.original
fi

cat LocalSettings.php.append | sed s/\\[YARRR_HOST\\]/$YARRR_HOST/>> $TARGET/LocalSettings.php


echo Install the yarrr skin...
if [ ! -d $TARGET/skins/yarrr/ ]
then
  mkdir -p $TARGET/skins/yarrr/
fi

cp Yarrr.php $TARGET/skins/
cp skin/*.css $TARGET/skins/yarrr/
cp skin/*.{gif,png} $TARGET/skins/yarrr/

echo Replace Userlogin.php...
if [ ! -s $TARGET/includes/templates/Userlogin.php.original ]
then
  cp  $TARGET/includes/templates/Userlogin.php $TARGET/includes/templates/Userlogin.php.original
fi
cp Userlogin.php $TARGET/includes/templates

echo Patch EditPage.php...

function patchfile () {
  basedir=$2
  if [ -z "$basedir" ]; then
    basedir=includes
  fi
  echo "Patching $1..."
  echo "$TARGET/$basedir/$1.original"
  if [ -s $TARGET/$basedir/$1.original ]; then
    cp  $TARGET/$basedir/$1.original $TARGET/includes/$1
  else
    cp  $TARGET/$basedir/$1 $TARGET/includes/$1.original
  fi
  # We set TMPDIR to avoid a bug in patch where it creates temporary
  # files in /tmp and then renames/moves; it causes us to lose
  # whatever SELinux security context was set
  TMPDIR="$TARGET/$basedir" patch -d $TARGET/$basedir < $1.patch
  echo "Patching $1...done"
}

patchfile EditPage.php
patchfile SkinTemplate.php
patchfile Article.php
patchfile OutputPage.php
patchfile SpecialRecentchanges.php
patchfile Language.php "languages"

echo Done.
