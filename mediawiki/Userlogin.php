<?php

if( !defined( 'MEDIAWIKI' ) ) die();

require_once( 'LocalSettings.php' );
require_once( 'includes/SkinTemplate.php' );

/**
 * HTML template for Special:Userlogin form
 * @package MediaWiki
 * @subpackage Templates
 */
class UserloginTemplate extends QuickTemplate {

	function xmlRpcURL() {
		global $yarrrWikiXmlRpcURL;
		echo $yarrrWikiXmlRpcURL;
	}
	
	function yarrrURL( $path ) {
		global $yarrrServerRoot;
		echo $yarrrServerRoot;
		echo $path;
	}

	function execute() {
		if( $this->data['error'] ) {
?>
	<h2><?php $this->msg('loginerror') ?>:</h2>
	<p class='error'><?php $this->html('error') ?></p>
<?php } else { ?>
	<h2><?php $this->msg('login'      ) ?>:</h2>
	<?php  $this->msgWiki('loginprompt') ?>
<?php } ?>

<script type="text/javascript" src="<?php $this->yarrrURL("/client.js"); ?>"></script>

<script type="text/javascript">

function yarrrLogin () {
  if (!yarrr)
    yarrrInitXmlRpc("<?php $this->xmlRpcURL(); ?>");
	
  var name = document.getElementById("wpName");
  var password = document.getElementById("wpPassword");
  if (!yarrr.login(name.value, password.value)) {
    alert ("Login failed.");
    return false;
  }
  
  return true;
}

function yarrrRegister () {
  if (!yarrr)
    yarrrInitXmlRpc("<?php $this->xmlRpcURL(); ?>");
	
  var name = document.getElementById("registerName");
  var email = document.getElementById("registerEmail");
  var password = document.getElementById("registerPassword");

  if (!yarrr.register(name.value, email.value, password.value)) {
  	alert ("Registration failed.");
  	return false;
  } else {
    if (!yarrr.login(name.value, password.value)) {
      alert ("Login failed.");
      return false;
    }
    return true;
  }
}

</script>

<form name="userlogin" onSubmit="yarrrLogin()" id="userlogin" method="post" action="<?php $this->text('action') ?>">
	<table border='0'>
		<tr>
			<td align='right'><label for='wpName'><?php $this->msg('yourname') ?>:</label></td>
			<td align='left'>
				<input tabindex='1' type='text' name="wpName" id="wpName"
					value="<?php $this->text('name') ?>" size='20' />
			</td>
			<td align='left'>
				<input tabindex='3' type='checkbox' name="wpRemember"
					value="1" id="wpRemember"
					<?php if( $this->data['remember'] ) { ?>checked="checked"<?php } ?>
					/><label for="wpRemember"><?php $this->msg('remembermypassword') ?></label>
			</td>
		</tr>
		<tr>
			<td align='right'><label for='wpPassword'><?php $this->msg('yourpassword') ?>:</label></td>
			<td align='left'>
				<input tabindex='2' type='password' name="wpPassword" id="wpPassword"
					value="<?php $this->text('password') ?>" size='20' />
			</td>
			<td align='left'>
				<input tabindex='4' type='submit' name="wpLoginattempt"
					value="<?php $this->msg('login') ?>" />
			</td>
		</tr>
	</table>
</form>	

<?php
$currentURL = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://'
. $_SERVER['HTTP_HOST']
. $_SERVER['REQUEST_URI']; 
?>

<?php if( $this->data['create'] ) { ?>
<h2>Create an account</h2>
<form name="usercreate" onSubmit="yarrrRegister()" id="usercreate" method="post" action="<?php $this->text('action') ?>">
	<input type="hidden" name="redirect" value="<?php echo $currentURL ?>"/>
	<table>
		<tr>
			<td align='right'><label for='registerName'><?php $this->msg('yourname') ?>:</label></td>
			<td align='left'>
				<input tabindex='10' type='text' name="wpName" id="registerName" size='20' />
			</td>
		</tr>
		<tr>
			<td align='right'><label for='registerEmail'><?php $this->msg('youremail') ?>:</label></td>
			<td align='left'>
				<input tabindex='10' type='text' name="wpEmail" id="registerEmail" size='20' />
			</td>
		</tr>
		<tr>
			<td align='right'><label for='registerPassword'><?php $this->msg('yourpassword') ?>:</label></td>
			<td align='left'>
				<input tabindex='11' type='password' name="wpPassword" id="registerPassword" size='20' />
			</td>
			<td align='left'>
				<input tabindex='12' type='submit' name="wpLoginattempt"
					value="<?php $this->msg('createaccount') ?>" />
			</td>
		</tr>
	</table>
</form>
<?php } ?>

<?php
		$this->msgWiki( 'loginend' );
	}
}

?>
