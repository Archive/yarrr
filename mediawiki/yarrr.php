<?php
$wgExtensionFunctions[] = "wfYarrrExtension";

require_once ("LocalSettings.php");
require_once("yarrr-xmlrpc.php");

function wfYarrrExtension () {
	global $wgParser;
	global $wgHooks;
}

/* called from patch to EditPage.php */
function yarrr_section_updated(&$article, &$user, $section) {
  global $yarrrXmlRpcURL;
  $client = new xmlrpc_client($yarrrXmlRpcURL);
  $title = $article->getTitle();
  $client->liveCommentSaveComplete($title->getPrefixedText(), $section);
  return true;
}

?>
