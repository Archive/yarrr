#!/bin/bash

dirname=$(dirname $0)
cd $dirname

# Usage: diff-mediawiki.sh MEDIAWIKI_INSTALLATION_DIRECTORY

if [ ! $# -eq 1 ] ; then
        echo "usage: $0 MEDIAWIKI_INSTALLATION_DIRECTORY"
        exit 1;
fi

SOURCE=$1

echo Diff index.php...
diff -u $SOURCE/index.php.original $SOURCE/index.php > index.php.patch

echo Diff EditPage.php...
diff -u $SOURCE/includes/EditPage.php.original $SOURCE/includes/EditPage.php > EditPage.php.patch

echo Diff Article.php...
diff -u $SOURCE/includes/Article.php.original $SOURCE/includes/Article.php > Article.php.patch

echo Diff OutputPage.php...
diff -u $SOURCE/includes/OutputPage.php.original $SOURCE/includes/OutputPage.php > OutputPage.php.patch

echo Diff SkinTemplate.php...
diff -u $SOURCE/includes/SkinTemplate.php.original $SOURCE/includes/SkinTemplate.php > SkinTemplate.php.patch

echo Diff Monobook.php...
diff -u $SOURCE/skins/MonoBook.php.original $SOURCE/skins/MonoBook.php > MonoBook.php.patch

echo Done.
