<?php

class xmlrpc_client
{
	var $url;
	var $host;
	var $port;
	var $path;

	function xmlrpc_client($url)
	{
		$this->url = $url;
		$urlparts = parse_url($this->url);
		$this->host = $urlparts['host'];
		$this->port = $urlparts['port'];
		$this->path = $urlparts['path'];
	}

	function call($function, $params)
	{
		$fp = fsockopen($this->host, $this->port, $errno, $errstr);

		$request = xmlrpc_encode_request($function, $params);
		$content_len = strlen($request);
		$query =
		"POST $this->path HTTP/1.0\r\n" .
		"User-Agent: yarrr (PHP)\r\n" .
		"Host: $this->host:$this->port\r\n" .
		"Content-Type: text/xml\r\n" .
		"Content-Length: $content_len\r\n" .
		"\r\n" .
		$request;

		fputs($fp, $query, strlen($query));

		$response_buf = "";
		$header_parsed = false;

		$line = fgets($fp, 4096);
		while ($line) {
			if (!$header_parsed) {
				if ($line === "\r\n" || $line === "\n") {
					$header_parsed = 1;
     			}
            } else {
               $response_buf .= $line;
            }
            $line = fgets($fp, 4096);
   		}
   		
		fclose($fp);

		echo $response_buf;

		return xmlrpc_decode($response_buf);
	}
	
	function userExists($username)
	{
		return $this->call('userExists', array($username));
	}
	
	function authenticateUser($username, $password)
	{
		return $this->call('authenticateUser', array($username, $password));
	}
	
	function topicUpdated($name)
	{
		return $this->call('topicUpdated', array($name));
	}
	
	function liveCommentSaveComplete($name, $section)
	{
		return $this->call('liveCommentSaveComplete', array($name, $section));
	}	
}

?>