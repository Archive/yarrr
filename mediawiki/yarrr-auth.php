<?php

require_once($IP."/includes/AuthPlugin.php");
require_once("LocalSettings.php");
require_once("yarrr-xmlrpc.php");

class YarrrAuth extends AuthPlugin {

        function userExists( $username ) {
        	global $yarrrXmlRpcURL;
        
        	$client = new xmlrpc_client($yarrrXmlRpcURL);
        	return $client->userExists($username);
        }

        function authenticate( $username, $password ) {
        	global $yarrrXmlRpcURL;
        
        	$client = new xmlrpc_client($yarrrXmlRpcURL);
        	return $client->authenticateUser($username, $password);
        }

        function autoCreate() {
                return true;
        }

        function strict() {
                return true;
        }

        function initUser( &$user ) {
        }
}

?>